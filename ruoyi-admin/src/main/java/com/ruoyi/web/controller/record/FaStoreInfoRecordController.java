package com.ruoyi.web.controller.record;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStoreInfoRecord;
import com.ruoyi.system.service.IFaStoreInfoRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺信息记录Controller
 *
 * @author ruoyi
 * @date 2021-10-30
 */
@RestController
@RequestMapping("/storeInfoRecord/storeInfoRecord")
public class FaStoreInfoRecordController extends BaseController
{
    @Autowired
    private IFaStoreInfoRecordService faStoreInfoRecordService;


    /**
     * 查询店铺信息记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FaStoreInfoRecord faStoreInfoRecord)
    {
        startPage();
        List<FaStoreInfoRecord> list = faStoreInfoRecordService.selectFaStoreInfoRecordList(faStoreInfoRecord);
        return getDataTable(list);
    }
    /**
     * 查询店铺信息记录列表
     */
    @GetMapping("/myRecordList")
    public TableDataInfo myRecordList(FaStoreInfoRecord faStoreInfoRecord)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreInfoRecord.setStoreInfoId(storeInfoId);
        startPage();
        List<FaStoreInfoRecord> list = faStoreInfoRecordService.selectFaStoreInfoRecordList(faStoreInfoRecord);
        return getDataTable(list);
    }
    /**
     * 导出店铺信息记录列表
     */
    @PreAuthorize("@ss.hasPermi('storeInfoRecord:storeInfoRecord:export')")
    @Log(title = "店铺信息记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreInfoRecord faStoreInfoRecord)
    {
        List<FaStoreInfoRecord> list = faStoreInfoRecordService.selectFaStoreInfoRecordList(faStoreInfoRecord);
        ExcelUtil<FaStoreInfoRecord> util = new ExcelUtil<FaStoreInfoRecord>(FaStoreInfoRecord.class);
        return util.exportExcel(list, "店铺信息记录数据");
    }

    /**
     * 获取店铺信息记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeInfoRecord:storeInfoRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreInfoRecordService.selectFaStoreInfoRecordById(id));
    }

    /**
     * 新增店铺信息记录
     */
    @Log(title = "店铺信息记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreInfoRecord faStoreInfoRecord)
    {

        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreInfoRecord.setStoreInfoId(storeInfoId);
        faStoreInfoRecord.setStatus(0L);
        return faStoreInfoRecordService.insertFaStoreInfoRecord(faStoreInfoRecord);
    }
    @Log(title = "系统手动创建店铺", businessType = BusinessType.INSERT)
    @PostMapping("/sysAdd")
    public AjaxResult sysAdd(@RequestBody FaStoreInfoRecord faStoreInfoRecord)
    {
        return faStoreInfoRecordService.sysInsertFaStoreInfoRecord(faStoreInfoRecord);
    }
    /**
     * 修改店铺信息记录
     */
    @PreAuthorize("@ss.hasPermi('storeInfoRecord:storeInfoRecord:edit')")
    @Log(title = "店铺信息记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreInfoRecord faStoreInfoRecord)
    {
        return faStoreInfoRecordService.updateFaStoreInfoRecord(faStoreInfoRecord);
    }


    /**
     * 删除店铺信息记录
     */
    @PreAuthorize("@ss.hasPermi('storeInfoRecord:storeInfoRecord:remove')")
    @Log(title = "店铺信息记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreInfoRecordService.deleteFaStoreInfoRecordByIds(ids));
    }
}
