package com.ruoyi.web.controller.game;

import java.util.List;

import com.ruoyi.system.domain.vo.GameGoodsVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameGoods;
import com.ruoyi.system.service.IFaGameGoodsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/mallgoods")
public class FaGameGoodsController extends BaseController
{
    @Autowired
    private IFaGameGoodsService faGameGoodsService;

    /**
     * 查询商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:mallgoods:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameGoods faGameGoods)
    {
        startPage();
        List<FaGameGoods> list = faGameGoodsService.selectFaGameGoodsList(faGameGoods);
        return getDataTable(list);
    }

    /**
     * 导出商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:mallgoods:export')")
    @Log(title = "商品管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameGoods faGameGoods)
    {
        List<FaGameGoods> list = faGameGoodsService.selectFaGameGoodsList(faGameGoods);
        ExcelUtil<FaGameGoods> util = new ExcelUtil<FaGameGoods>(FaGameGoods.class);
        return util.exportExcel(list, "商品管理数据");
    }

    /**
     * 获取商品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:mallgoods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGameGoodsService.selectFaGameGoodsById(id));
    }

    /**
     * 新增商品管理
     */
    @PreAuthorize("@ss.hasPermi('game:mallgoods:add')")
    @Log(title = "商品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GameGoodsVo faGameGoods)
    {
        return toAjax(faGameGoodsService.insertFaGameGoods(faGameGoods));
    }

    /**
     * 修改商品管理
     */
    @PreAuthorize("@ss.hasPermi('game:mallgoods:edit')")
    @Log(title = "商品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GameGoodsVo faGameGoods)
    {
        return toAjax(faGameGoodsService.updateFaGameGoods(faGameGoods));
    }

    /**
     * 删除商品管理
     */
    @PreAuthorize("@ss.hasPermi('game:mallgoods:remove')")
    @Log(title = "商品管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGameGoodsService.deleteFaGameGoodsByIds(ids));
    }

    /**
     * 查询商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:mallgoods:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(GameGoodsVo faGameGoods)
    {
        startPage();
        List<GameGoodsVo> list = faGameGoodsService.listGameGoodsVo(faGameGoods);
        return getDataTable(list);
    }
}
