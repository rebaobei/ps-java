package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.OrderInvoiceVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaOrderInvoice;
import com.ruoyi.system.service.IFaOrderInvoiceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单发票Controller
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/invoice")
public class FaOrderInvoiceController extends BaseController
{
    @Autowired
    private IFaOrderInvoiceService faOrderInvoiceService;

    /**
     * 查询订单发票列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:invoice:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaOrderInvoice faOrderInvoice)
    {
        startPage();
        List<FaOrderInvoice> list = faOrderInvoiceService.selectFaOrderInvoiceList(faOrderInvoice);
        return getDataTable(list);
    }

    /**
     * 导出订单发票列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:invoice:export')")
    @Log(title = "订单发票", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaOrderInvoice faOrderInvoice)
    {
        List<FaOrderInvoice> list = faOrderInvoiceService.selectFaOrderInvoiceList(faOrderInvoice);
        ExcelUtil<FaOrderInvoice> util = new ExcelUtil<FaOrderInvoice>(FaOrderInvoice.class);
        return util.exportExcel(list, "订单发票数据");
    }

    /**
     * 获取订单发票详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:invoice:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faOrderInvoiceService.selectFaOrderInvoiceById(id));
    }

    /**
     * 新增订单发票
     */
    @PreAuthorize("@ss.hasPermi('orderlist:invoice:add')")
    @Log(title = "订单发票", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaOrderInvoice faOrderInvoice)
    {
        return toAjax(faOrderInvoiceService.insertFaOrderInvoice(faOrderInvoice));
    }

    /**
     * 修改订单发票
     */
    @PreAuthorize("@ss.hasPermi('orderlist:invoice:edit')")
    @Log(title = "订单发票", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaOrderInvoice faOrderInvoice)
    {
        return toAjax(faOrderInvoiceService.updateFaOrderInvoice(faOrderInvoice));
    }

    /**
     * 删除订单发票
     */
    @PreAuthorize("@ss.hasPermi('orderlist:invoice:remove')")
    @Log(title = "订单发票", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faOrderInvoiceService.deleteFaOrderInvoiceByIds(ids));
    }


    /**
     * 查询订单发票列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:invoice:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(OrderInvoiceVo faOrderInvoice)
    {
        startPage();
        List<OrderInvoiceVo> list = faOrderInvoiceService.listOrderInvoiceVo(faOrderInvoice);
        return getDataTable(list);
    }
}
