package com.ruoyi.web.controller.mobile;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaPaymentlist;
import com.ruoyi.system.service.IFaPaymentlistService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 支付类型Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/mobile/paymentlist")
public class FaPaymentlistController extends BaseController
{
    @Autowired
    private IFaPaymentlistService faPaymentlistService;

    /**
     * 查询支付类型列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:paymentlist:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaPaymentlist faPaymentlist)
    {
        startPage();
        List<FaPaymentlist> list = faPaymentlistService.selectFaPaymentlistList(faPaymentlist);
        return getDataTable(list);
    }

    /**
     * 导出支付类型列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:paymentlist:export')")
    @Log(title = "支付类型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaPaymentlist faPaymentlist)
    {
        List<FaPaymentlist> list = faPaymentlistService.selectFaPaymentlistList(faPaymentlist);
        ExcelUtil<FaPaymentlist> util = new ExcelUtil<FaPaymentlist>(FaPaymentlist.class);
        return util.exportExcel(list, "支付类型数据");
    }

    /**
     * 获取支付类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('mobile:paymentlist:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faPaymentlistService.selectFaPaymentlistById(id));
    }

    /**
     * 新增支付类型
     */
    @PreAuthorize("@ss.hasPermi('mobile:paymentlist:add')")
    @Log(title = "支付类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaPaymentlist faPaymentlist)
    {
        return toAjax(faPaymentlistService.insertFaPaymentlist(faPaymentlist));
    }

    /**
     * 修改支付类型
     */
    @PreAuthorize("@ss.hasPermi('mobile:paymentlist:edit')")
    @Log(title = "支付类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaPaymentlist faPaymentlist)
    {
        return toAjax(faPaymentlistService.updateFaPaymentlist(faPaymentlist));
    }

    /**
     * 删除支付类型
     */
    @PreAuthorize("@ss.hasPermi('mobile:paymentlist:remove')")
    @Log(title = "支付类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faPaymentlistService.deleteFaPaymentlistByIds(ids));
    }
}
