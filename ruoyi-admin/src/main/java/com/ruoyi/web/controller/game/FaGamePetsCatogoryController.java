package com.ruoyi.web.controller.game;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGamePetsCatogory;
import com.ruoyi.system.service.IFaGamePetsCatogoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物分类Controller
 *
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/petcatogory")
public class FaGamePetsCatogoryController extends BaseController {
    @Autowired
    private IFaGamePetsCatogoryService faGamePetsCatogoryService;

    /**
     * 查询宠物分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:petcatogory:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGamePetsCatogory faGamePetsCatogory) {
        startPage();
        List<FaGamePetsCatogory> list = faGamePetsCatogoryService.selectFaGamePetsCatogoryList(faGamePetsCatogory);
        return getDataTable(list);
    }

    /**
     * 导出宠物分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:petcatogory:export')")
    @Log(title = "宠物分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGamePetsCatogory faGamePetsCatogory) {
        List<FaGamePetsCatogory> list = faGamePetsCatogoryService.selectFaGamePetsCatogoryList(faGamePetsCatogory);
        ExcelUtil<FaGamePetsCatogory> util = new ExcelUtil<FaGamePetsCatogory>(FaGamePetsCatogory.class);
        return util.exportExcel(list, "宠物分类数据");
    }

    /**
     * 获取宠物分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:petcatogory:query')")
    @GetMapping(value = "/{mainCategoryId}")
    public AjaxResult getInfo(@PathVariable("mainCategoryId") Long mainCategoryId) {
        return AjaxResult.success(faGamePetsCatogoryService.selectFaGamePetsCatogoryById(mainCategoryId));
    }

    /**
     * 新增宠物分类
     */
    @PreAuthorize("@ss.hasPermi('game:petcatogory:add')")
    @Log(title = "宠物分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGamePetsCatogory faGamePetsCatogory) {
        return toAjax(faGamePetsCatogoryService.insertFaGamePetsCatogory(faGamePetsCatogory));
    }

    /**
     * 修改宠物分类
     */
    @PreAuthorize("@ss.hasPermi('game:petcatogory:edit')")
    @Log(title = "宠物分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGamePetsCatogory faGamePetsCatogory) {
        return toAjax(faGamePetsCatogoryService.updateFaGamePetsCatogory(faGamePetsCatogory));
    }

    /**
     * 删除宠物分类
     */
    @PreAuthorize("@ss.hasPermi('game:petcatogory:remove')")
    @Log(title = "宠物分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{mainCategoryIds}")
    public AjaxResult remove(@PathVariable Long[] mainCategoryIds) {
        return toAjax(faGamePetsCatogoryService.deleteFaGamePetsCatogoryByIds(mainCategoryIds));
    }

    /**
     * 查询宠物分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:petcatogory:list')")
    @GetMapping("/all_list")
    public List<FaGamePetsCatogory> allList(FaGamePetsCatogory faGamePetsCatogory) {
        return faGamePetsCatogoryService.selectFaGamePetsCatogoryList(faGamePetsCatogory);
    }
}
