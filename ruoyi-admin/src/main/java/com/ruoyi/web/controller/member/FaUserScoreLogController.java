package com.ruoyi.web.controller.member;

import java.util.List;

import com.ruoyi.system.domain.vo.ScoreLogWithUserInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserScoreLog;
import com.ruoyi.system.service.IFaUserScoreLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 积分变动Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/score")
public class FaUserScoreLogController extends BaseController
{
    @Autowired
    private IFaUserScoreLogService faUserScoreLogService;

    /**
     * 查询积分变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:score:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserScoreLog faUserScoreLog)
    {
        startPage();
        List<FaUserScoreLog> list = faUserScoreLogService.selectFaUserScoreLogList(faUserScoreLog);
        return getDataTable(list);
    }

    /**
     * 导出积分变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:score:export')")
    @Log(title = "积分变动", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserScoreLog faUserScoreLog)
    {
        List<FaUserScoreLog> list = faUserScoreLogService.selectFaUserScoreLogList(faUserScoreLog);
        ExcelUtil<FaUserScoreLog> util = new ExcelUtil<FaUserScoreLog>(FaUserScoreLog.class);
        return util.exportExcel(list, "积分变动数据");
    }

    /**
     * 获取积分变动详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:score:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faUserScoreLogService.selectFaUserScoreLogById(id));
    }

    /**
     * 新增积分变动
     */
    @PreAuthorize("@ss.hasPermi('member:score:add')")
    @Log(title = "积分变动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserScoreLog faUserScoreLog)
    {
        return toAjax(faUserScoreLogService.insertFaUserScoreLog(faUserScoreLog));
    }

    /**
     * 修改积分变动
     */
    @PreAuthorize("@ss.hasPermi('member:score:edit')")
    @Log(title = "积分变动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserScoreLog faUserScoreLog)
    {
        return toAjax(faUserScoreLogService.updateFaUserScoreLog(faUserScoreLog));
    }

    /**
     * 删除积分变动
     */
    @PreAuthorize("@ss.hasPermi('member:score:remove')")
    @Log(title = "积分变动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faUserScoreLogService.deleteFaUserScoreLogByIds(ids));
    }

    /**
     * my查询积分变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:score:list')")
    @GetMapping("/get_list")
    public TableDataInfo MyList(FaUserScoreLog faUserScoreLog)
    {
        startPage();
        List<ScoreLogWithUserInfo> list = faUserScoreLogService.listScoreLogWithUserInfo(faUserScoreLog);
        return getDataTable(list);
    }

}
