package com.ruoyi.web.controller.dynamic;

import java.util.List;

import com.ruoyi.system.domain.vo.TopicVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBallTopic;
import com.ruoyi.system.service.IFaBallTopicService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 话题管理Controller
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@RestController
@RequestMapping("/dynamic/topic")
public class FaBallTopicController extends BaseController
{
    @Autowired
    private IFaBallTopicService faBallTopicService;

    /**
     * 查询话题管理列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:topic:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBallTopic faBallTopic)
    {
        startPage();
        List<FaBallTopic> list = faBallTopicService.selectFaBallTopicList(faBallTopic);
        return getDataTable(list);
    }

    /**
     * 导出话题管理列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:topic:export')")
    @Log(title = "话题管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBallTopic faBallTopic)
    {
        List<FaBallTopic> list = faBallTopicService.selectFaBallTopicList(faBallTopic);
        ExcelUtil<FaBallTopic> util = new ExcelUtil<FaBallTopic>(FaBallTopic.class);
        return util.exportExcel(list, "话题管理数据");
    }

    /**
     * 获取话题管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('dynamic:topic:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faBallTopicService.selectFaBallTopicById(id));
    }

    /**
     * 新增话题管理
     */
    @PreAuthorize("@ss.hasPermi('dynamic:topic:add')")
    @Log(title = "话题管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBallTopic faBallTopic)
    {
        return toAjax(faBallTopicService.insertFaBallTopic(faBallTopic));
    }

    /**
     * 修改话题管理
     */
    @PreAuthorize("@ss.hasPermi('dynamic:topic:edit')")
    @Log(title = "话题管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBallTopic faBallTopic)
    {
        return toAjax(faBallTopicService.updateFaBallTopic(faBallTopic));
    }

    /**
     * 删除话题管理
     */
    @PreAuthorize("@ss.hasPermi('dynamic:topic:remove')")
    @Log(title = "话题管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faBallTopicService.deleteFaBallTopicByIds(ids));
    }

    /**
     * 查询话题管理列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:topic:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaBallTopic faBallTopic)
    {
        startPage();
        List<TopicVo> list = faBallTopicService.listTopicVo(faBallTopic);
        return getDataTable(list);
    }

}
