package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.CarOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaCarOrder;
import com.ruoyi.system.service.IFaCarOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 同城接送订单Controller
 *
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/carorder")
public class FaCarOrderController extends BaseController {
    @Autowired
    private IFaCarOrderService faCarOrderService;

    /**
     * 查询同城接送订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:carorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaCarOrder faCarOrder) {
        startPage();
        List<FaCarOrder> list = faCarOrderService.selectFaCarOrderList(faCarOrder);
        return getDataTable(list);
    }

    /**
     * 导出同城接送订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:carorder:export')")
    @Log(title = "同城接送订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaCarOrder faCarOrder) {
        List<FaCarOrder> list = faCarOrderService.selectFaCarOrderList(faCarOrder);
        ExcelUtil<FaCarOrder> util = new ExcelUtil<FaCarOrder>(FaCarOrder.class);
        return util.exportExcel(list, "同城接送订单数据");
    }

    /**
     * 获取同城接送订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:carorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(faCarOrderService.selectFaCarOrderById(id));
    }

    /**
     * 新增同城接送订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:carorder:add')")
    @Log(title = "同城接送订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaCarOrder faCarOrder) {
        return toAjax(faCarOrderService.insertFaCarOrder(faCarOrder));
    }

    /**
     * 修改同城接送订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:carorder:edit')")
    @Log(title = "同城接送订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaCarOrder faCarOrder) {
        return toAjax(faCarOrderService.updateFaCarOrder(faCarOrder));
    }

    /**
     * 删除同城接送订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:carorder:remove')")
    @Log(title = "同城接送订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(faCarOrderService.deleteFaCarOrderByIds(ids));
    }

    /**
     * 查询同城接送订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:carorder:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(CarOrderVo faCarOrder) {
        startPage();
        List<CarOrderVo> list = faCarOrderService.listCarOrderVo(faCarOrder);
        return getDataTable(list);
    }
}
