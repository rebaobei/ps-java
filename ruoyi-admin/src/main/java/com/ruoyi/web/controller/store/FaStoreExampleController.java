package com.ruoyi.web.controller.store;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStoreExample;
import com.ruoyi.system.service.IFaStoreExampleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺服务案Controller
 *
 * @author ruoyi
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/storeinfo/example")
public class FaStoreExampleController extends BaseController
{
    @Autowired
    private IFaStoreExampleService faStoreExampleService;

    /**
     * 查询店铺服务案列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStoreExample faStoreExample)
    {
        startPage();
        List<FaStoreExample> list = faStoreExampleService.selectFaStoreExampleList(faStoreExample);
        return getDataTable(list);
    }
    /**
     * 查询店铺服务案列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:list')")
    @GetMapping("/myList")
    public TableDataInfo myList(FaStoreExample faStoreExample)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreExample.setStoreId(storeInfoId);
        startPage();
        List<FaStoreExample> list = faStoreExampleService.selectFaStoreExampleList(faStoreExample);
        return getDataTable(list);
    }

    /**
     * 导出店铺服务案列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:export')")
    @Log(title = "店铺服务案", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreExample faStoreExample)
    {
        List<FaStoreExample> list = faStoreExampleService.selectFaStoreExampleList(faStoreExample);
        ExcelUtil<FaStoreExample> util = new ExcelUtil<FaStoreExample>(FaStoreExample.class);
        return util.exportExcel(list, "店铺服务案数据");
    }

    /**
     * 获取店铺服务案详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreExampleService.selectFaStoreExampleById(id));
    }

    /**
     * 新增店铺服务案
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:add')")
    @Log(title = "店铺服务案", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreExample faStoreExample)
    {
        String[] personIds = faStoreExample.getPersonIds();
        String join = StringUtils.join(personIds,",");
        faStoreExample.setPersonId(join);
        return toAjax(faStoreExampleService.insertFaStoreExample(faStoreExample));
    }

    /**
     * 修改店铺服务案
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:edit')")
    @Log(title = "店铺服务案", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreExample faStoreExample)
    {
        String[] personIds = faStoreExample.getPersonIds();
        String join = StringUtils.join(personIds,",");
        faStoreExample.setPersonId(join);
        faStoreExample.setStatus(0);
        return toAjax(faStoreExampleService.updateFaStoreExample(faStoreExample));
    }

    /**
     * 修改店铺服务案
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:edit')")
    @Log(title = "店铺服务案", businessType = BusinessType.UPDATE)
    @PutMapping("/check")
    public AjaxResult check(@RequestBody FaStoreExample faStoreExample)
    {
        String[] personIds = faStoreExample.getPersonIds();
        String join = StringUtils.join(personIds,",");
        faStoreExample.setPersonId(join);
        return toAjax(faStoreExampleService.updateFaStoreExample(faStoreExample));
    }

    /**
     * 删除店铺服务案
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:example:remove')")
    @Log(title = "店铺服务案", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreExampleService.deleteFaStoreExampleByIds(ids));
    }
}
