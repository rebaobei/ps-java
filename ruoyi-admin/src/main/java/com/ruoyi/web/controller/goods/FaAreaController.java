package com.ruoyi.web.controller.goods;

import java.util.List;

import com.ruoyi.system.domain.vo.CascadeProvinceVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaArea;
import com.ruoyi.system.service.IFaAreaService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 地区Controller
 * 
 * @author ruoyi
 * @date 2021-07-20
 */
@RestController
@RequestMapping("/goods/area")
public class FaAreaController extends BaseController
{
    @Autowired
    private IFaAreaService faAreaService;

    /**
     * 查询地区列表
     */
    @PreAuthorize("@ss.hasPermi('goods:area:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaArea faArea)
    {
        startPage();
        List<FaArea> list = faAreaService.selectFaAreaList(faArea);
        return getDataTable(list);
    }

    /**
     * 导出地区列表
     */
    @PreAuthorize("@ss.hasPermi('goods:area:export')")
    @Log(title = "地区", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaArea faArea)
    {
        List<FaArea> list = faAreaService.selectFaAreaList(faArea);
        ExcelUtil<FaArea> util = new ExcelUtil<FaArea>(FaArea.class);
        return util.exportExcel(list, "地区数据");
    }

    /**
     * 获取地区详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:area:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faAreaService.selectFaAreaById(id));
    }

    /**
     * 新增地区
     */
    @PreAuthorize("@ss.hasPermi('goods:area:add')")
    @Log(title = "地区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaArea faArea)
    {
        return toAjax(faAreaService.insertFaArea(faArea));
    }

    /**
     * 修改地区
     */
    @PreAuthorize("@ss.hasPermi('goods:area:edit')")
    @Log(title = "地区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaArea faArea)
    {
        return toAjax(faAreaService.updateFaArea(faArea));
    }

    /**
     * 删除地区
     */
    @PreAuthorize("@ss.hasPermi('goods:area:remove')")
    @Log(title = "地区", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faAreaService.deleteFaAreaByIds(ids));
    }



    /**
     * 查询具有 层级关系的地区列表
     */
    @PreAuthorize("@ss.hasPermi('goods:area:list')")
    @GetMapping("/cascade")
    public List<CascadeProvinceVo> getCascadeAreaList(){
        List<CascadeProvinceVo> cascadeProvinceVos = faAreaService.listCascade();
        return cascadeProvinceVos;
    }
}
