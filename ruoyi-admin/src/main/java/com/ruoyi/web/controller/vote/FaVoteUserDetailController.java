package com.ruoyi.web.controller.vote;

import java.util.List;

import com.ruoyi.system.domain.vo.UserVoteDetailVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaVoteUserDetail;
import com.ruoyi.system.service.IFaVoteUserDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 投票明细Controller
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@RestController
@RequestMapping("/vote/detail")
public class FaVoteUserDetailController extends BaseController
{
    @Autowired
    private IFaVoteUserDetailService faVoteUserDetailService;

    /**
     * 查询投票明细列表
     */
    @PreAuthorize("@ss.hasPermi('vote:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaVoteUserDetail faVoteUserDetail)
    {
        startPage();
        List<FaVoteUserDetail> list = faVoteUserDetailService.selectFaVoteUserDetailList(faVoteUserDetail);
        return getDataTable(list);
    }

    /**
     * 导出投票明细列表
     */
    @PreAuthorize("@ss.hasPermi('vote:detail:export')")
    @Log(title = "投票明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaVoteUserDetail faVoteUserDetail)
    {
        List<FaVoteUserDetail> list = faVoteUserDetailService.selectFaVoteUserDetailList(faVoteUserDetail);
        ExcelUtil<FaVoteUserDetail> util = new ExcelUtil<FaVoteUserDetail>(FaVoteUserDetail.class);
        return util.exportExcel(list, "投票明细数据");
    }

    /**
     * 获取投票明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('vote:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faVoteUserDetailService.selectFaVoteUserDetailById(id));
    }

    /**
     * 新增投票明细
     */
    @PreAuthorize("@ss.hasPermi('vote:detail:add')")
    @Log(title = "投票明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaVoteUserDetail faVoteUserDetail)
    {
        return toAjax(faVoteUserDetailService.insertFaVoteUserDetail(faVoteUserDetail));
    }

    /**
     * 修改投票明细
     */
    @PreAuthorize("@ss.hasPermi('vote:detail:edit')")
    @Log(title = "投票明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaVoteUserDetail faVoteUserDetail)
    {
        return toAjax(faVoteUserDetailService.updateFaVoteUserDetail(faVoteUserDetail));
    }

    /**
     * 删除投票明细
     */
    @PreAuthorize("@ss.hasPermi('vote:detail:remove')")
    @Log(title = "投票明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faVoteUserDetailService.deleteFaVoteUserDetailByIds(ids));
    }

    /**
     * 查询投票明细列表
     */
    @PreAuthorize("@ss.hasPermi('vote:detail:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(UserVoteDetailVo faVoteUserDetail)
    {
        startPage();
        List<UserVoteDetailVo> list = faVoteUserDetailService.listUserVoteDetailVo(faVoteUserDetail);
        return getDataTable(list);
    }
}
