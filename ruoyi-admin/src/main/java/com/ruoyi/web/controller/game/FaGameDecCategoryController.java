package com.ruoyi.web.controller.game;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameDecCategory;
import com.ruoyi.system.service.IFaGameDecCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 装扮分类Controller
 *
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/deccategory")
public class FaGameDecCategoryController extends BaseController {
    @Autowired
    private IFaGameDecCategoryService faGameDecCategoryService;

    /**
     * 查询装扮分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:deccategory:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameDecCategory faGameDecCategory) {
        startPage();
        List<FaGameDecCategory> list = faGameDecCategoryService.selectFaGameDecCategoryList(faGameDecCategory);
        return getDataTable(list);
    }

    /**
     * 导出装扮分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:deccategory:export')")
    @Log(title = "装扮分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameDecCategory faGameDecCategory) {
        List<FaGameDecCategory> list = faGameDecCategoryService.selectFaGameDecCategoryList(faGameDecCategory);
        ExcelUtil<FaGameDecCategory> util = new ExcelUtil<FaGameDecCategory>(FaGameDecCategory.class);
        return util.exportExcel(list, "装扮分类数据");
    }

    /**
     * 获取装扮分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:deccategory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(faGameDecCategoryService.selectFaGameDecCategoryById(id));
    }

    /**
     * 新增装扮分类
     */
    @PreAuthorize("@ss.hasPermi('game:deccategory:add')")
    @Log(title = "装扮分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameDecCategory faGameDecCategory) {
        return toAjax(faGameDecCategoryService.insertFaGameDecCategory(faGameDecCategory));
    }

    /**
     * 修改装扮分类
     */
    @PreAuthorize("@ss.hasPermi('game:deccategory:edit')")
    @Log(title = "装扮分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameDecCategory faGameDecCategory) {
        return toAjax(faGameDecCategoryService.updateFaGameDecCategory(faGameDecCategory));
    }

    /**
     * 删除装扮分类
     */
    @PreAuthorize("@ss.hasPermi('game:deccategory:remove')")
    @Log(title = "装扮分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(faGameDecCategoryService.deleteFaGameDecCategoryByIds(ids));
    }

    /**
     * 查询装扮分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:deccategory:list')")
    @GetMapping("/all_list")
    public List<FaGameDecCategory> allList(FaGameDecCategory faGameDecCategory) {
        return faGameDecCategoryService.selectFaGameDecCategoryList(faGameDecCategory);
    }
}
