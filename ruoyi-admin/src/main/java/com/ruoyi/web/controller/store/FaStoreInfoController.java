package com.ruoyi.web.controller.store;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.FaStoreInfo;
import com.ruoyi.system.service.IFaStoreInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 店铺信息Controller
 *
 * @author ruoyi
 * @date 2021-10-30
 */
@RestController
@RequestMapping("/storeinfo/storeinfo")
public class FaStoreInfoController extends BaseController {
    @Autowired
    private IFaStoreInfoService faStoreInfoService;

    /**
     * 查询店铺信息列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:storeinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStoreInfo faStoreInfo) {
        startPage();
        List<FaStoreInfo> list = faStoreInfoService.selectFaStoreInfoList(faStoreInfo);
        return getDataTable(list);
    }

    /**
     * 获取店铺信息详细信息
     */
    @GetMapping(value = "/getMyStoreInfo")
    public AjaxResult getMyStoreInfo()
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        return AjaxResult.success(faStoreInfoService.selectFaStoreInfoById(storeInfoId));
    }

    /**
     * 导出店铺信息列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:storeinfo:export')")
    @Log(title = "店铺信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreInfo faStoreInfo) {
        List<FaStoreInfo> list = faStoreInfoService.selectFaStoreInfoList(faStoreInfo);
        ExcelUtil<FaStoreInfo> util = new ExcelUtil<FaStoreInfo>(FaStoreInfo.class);
        return util.exportExcel(list, "店铺信息数据");
    }

    /**
     * 获取店铺信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(faStoreInfoService.selectFaStoreInfoById(id));
    }

    /**
     * 新增店铺信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:storeinfo:add')")
    @Log(title = "店铺信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreInfo faStoreInfo) {
        return toAjax(faStoreInfoService.insertFaStoreInfo(faStoreInfo));
    }

    /**
     * 修改店铺信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:storeinfo:edit')")
    @Log(title = "店铺信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreInfo faStoreInfo) {
        return faStoreInfoService.updateFaStoreInfo(faStoreInfo);
    }

    /**
     * 删除店铺信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:storeinfo:remove')")
    @Log(title = "店铺信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(faStoreInfoService.deleteFaStoreInfoByIds(ids));
    }
}
