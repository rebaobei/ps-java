package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.PayListStatisticVo;
import com.ruoyi.system.domain.vo.PayListVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaOrderPaylist;
import com.ruoyi.system.service.IFaOrderPaylistService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 支付记录Controller
 *
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/paylist")
public class FaOrderPaylistController extends BaseController
{
    @Autowired
    private IFaOrderPaylistService faOrderPaylistService;

    /**
     * 查询支付记录列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:paylist:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaOrderPaylist faOrderPaylist)
    {
        startPage();
        List<FaOrderPaylist> list = faOrderPaylistService.selectFaOrderPaylistList(faOrderPaylist);
        return getDataTable(list);
    }

    /**
     * 导出支付记录列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:paylist:export')")
    @Log(title = "支付记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaOrderPaylist faOrderPaylist)
    {
        List<FaOrderPaylist> list = faOrderPaylistService.selectFaOrderPaylistList(faOrderPaylist);
        ExcelUtil<FaOrderPaylist> util = new ExcelUtil<FaOrderPaylist>(FaOrderPaylist.class);
        return util.exportExcel(list, "支付记录数据");
    }

    /**
     * 获取支付记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:paylist:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faOrderPaylistService.selectFaOrderPaylistById(id));
    }

    /**
     * 新增支付记录
     */
    @PreAuthorize("@ss.hasPermi('orderlist:paylist:add')")
    @Log(title = "支付记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaOrderPaylist faOrderPaylist)
    {
        return toAjax(faOrderPaylistService.insertFaOrderPaylist(faOrderPaylist));
    }

    /**
     * 修改支付记录
     */
    @PreAuthorize("@ss.hasPermi('orderlist:paylist:edit')")
    @Log(title = "支付记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaOrderPaylist faOrderPaylist)
    {
        return toAjax(faOrderPaylistService.updateFaOrderPaylist(faOrderPaylist));
    }

    /**
     * 删除支付记录
     */
    @PreAuthorize("@ss.hasPermi('orderlist:paylist:remove')")
    @Log(title = "支付记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faOrderPaylistService.deleteFaOrderPaylistByIds(ids));
    }


    /**
     * 查询支付记录列表
     */
    @GetMapping("/get_list")
    public TableDataInfo myList(PayListVo faOrderPaylist)
    {
        startPage();
        List<PayListVo> list = faOrderPaylistService.listPaylistVo(faOrderPaylist);
        return getDataTable(list);
    }

    /**
     * 查询支付记录列表
     */
    @GetMapping("/statistic")
    public List<PayListStatisticVo> myList(PayListStatisticVo vo)
    {
        List<PayListStatisticVo> list = faOrderPaylistService.lsitPayStatisticVo(vo);
        return list;
    }
}
