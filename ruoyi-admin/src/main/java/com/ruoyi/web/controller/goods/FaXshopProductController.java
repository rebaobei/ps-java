package com.ruoyi.web.controller.goods;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.ProductVo;
import com.ruoyi.system.domain.vo.XshopOrderProductVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopProduct;
import com.ruoyi.system.service.IFaXshopProductService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品Controller
 *
 * @author ruoyi
 * @date 2021-07-16
 */
@RestController
@RequestMapping("/goods/product")
public class FaXshopProductController extends BaseController {
    @Autowired
    private IFaXshopProductService faXshopProductService;

    /**
     * 查询商品列表
     */
    @PreAuthorize("@ss.hasPermi('goods:product:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopProduct faXshopProduct) {
        startPage();
        List<FaXshopProduct> list = faXshopProductService.selectFaXshopProductList(faXshopProduct);
        return getDataTable(list);
    }

    /**
     * 导出商品列表
     */
    @PreAuthorize("@ss.hasPermi('goods:product:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopProduct faXshopProduct) {
        List<FaXshopProduct> list = faXshopProductService.selectFaXshopProductList(faXshopProduct);
        ExcelUtil<FaXshopProduct> util = new ExcelUtil<FaXshopProduct>(FaXshopProduct.class);
        return util.exportExcel(list, "商品数据");
    }

    /**
     * 获取商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:product:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(faXshopProductService.selectFaXshopProductById(id));
    }

    /**
     * 新增商品
     */
    @PreAuthorize("@ss.hasPermi('goods:product:add')")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopProduct faXshopProduct) {
        return toAjax(faXshopProductService.insertFaXshopProduct(faXshopProduct));
    }

    /**
     * 修改商品
     */
    @PreAuthorize("@ss.hasPermi('goods:product:edit')")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopProduct faXshopProduct) {
        return toAjax(faXshopProductService.updateFaXshopProduct(faXshopProduct));
    }

    /**
     * 删除商品
     */
    @PreAuthorize("@ss.hasPermi('goods:product:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(faXshopProductService.deleteFaXshopProductByIds(ids));
    }


    /**
     * 查询商品列表
     */
    @PreAuthorize("@ss.hasPermi('goods:product:list')")
    @GetMapping("/get_list")
    public TableDataInfo getList(FaXshopProduct faXshopProduct) {
        startPage();
        List<ProductVo> list = faXshopProductService.listProductVo(faXshopProduct);
        return getDataTable(list);
    }
    /**
     * 查询商品列表
     */
    @PreAuthorize("@ss.hasPermi('goods:product:list')")
    @GetMapping("/myList")
    public TableDataInfo myList(FaXshopProduct faXshopProduct) {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faXshopProduct.setStoreInfoId(storeInfoId);
        startPage();
        List<ProductVo> list = faXshopProductService.listProductVo(faXshopProduct);
        return getDataTable(list);
    }

    /**
     * 查询所有商品列表
     */
    @PreAuthorize("@ss.hasPermi('goods:product:list')")
    @GetMapping("/all_list")
    public List<Map<String,Object>> allList() {
        return faXshopProductService.listProductVoForSelect();
    }


}
