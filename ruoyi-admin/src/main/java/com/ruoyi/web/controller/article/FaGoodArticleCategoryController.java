package com.ruoyi.web.controller.article;

import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGoodArticleCategory;
import com.ruoyi.system.service.IFaGoodArticleCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 动保公益分类Controller
 *
 * @author ruoyi
 * @date 2021-12-04
 */
@RestController
@RequestMapping("/article/goodcategory")
public class FaGoodArticleCategoryController extends BaseController
{
    @Autowired
    private IFaGoodArticleCategoryService faGoodArticleCategoryService;

    /**
     * 查询动保公益分类列表
     */
    @PreAuthorize("@ss.hasPermi('article:goodcategory:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGoodArticleCategory faGoodArticleCategory)
    {
        startPage();
        List<FaGoodArticleCategory> list = faGoodArticleCategoryService.selectFaGoodArticleCategoryList(faGoodArticleCategory);
        return getDataTable(list);
    }

    /**
     * 导出动保公益分类列表
     */
    @PreAuthorize("@ss.hasPermi('article:goodcategory:export')")
    @Log(title = "动保公益分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGoodArticleCategory faGoodArticleCategory)
    {
        List<FaGoodArticleCategory> list = faGoodArticleCategoryService.selectFaGoodArticleCategoryList(faGoodArticleCategory);
        ExcelUtil<FaGoodArticleCategory> util = new ExcelUtil<FaGoodArticleCategory>(FaGoodArticleCategory.class);
        return util.exportExcel(list, "动保公益分类数据");
    }

    /**
     * 获取动保公益分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:goodcategory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGoodArticleCategoryService.selectFaGoodArticleCategoryById(id));
    }

    /**
     * 新增动保公益分类
     */
    @PreAuthorize("@ss.hasPermi('article:goodcategory:add')")
    @Log(title = "动保公益分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGoodArticleCategory faGoodArticleCategory)
    {
        return toAjax(faGoodArticleCategoryService.insertFaGoodArticleCategory(faGoodArticleCategory));
    }

    /**
     * 修改动保公益分类
     */
    @PreAuthorize("@ss.hasPermi('article:goodcategory:edit')")
    @Log(title = "动保公益分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGoodArticleCategory faGoodArticleCategory)
    {
        return toAjax(faGoodArticleCategoryService.updateFaGoodArticleCategory(faGoodArticleCategory));
    }

    /**
     * 删除动保公益分类
     */
    @PreAuthorize("@ss.hasPermi('article:goodcategory:remove')")
    @Log(title = "动保公益分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGoodArticleCategoryService.deleteFaGoodArticleCategoryByIds(ids));
    }

    /**
     * 查询动保公益分类列表
     */
    @PreAuthorize("@ss.hasPermi('article:goodcategory:list')")
    @GetMapping("/selectList")
    public List<FaGoodArticleCategory> selectList(FaGoodArticleCategory faGoodArticleCategory)
    {
        List<FaGoodArticleCategory> list = faGoodArticleCategoryService.selectFaGoodArticleCategoryList(faGoodArticleCategory);
        return list;
    }
}
