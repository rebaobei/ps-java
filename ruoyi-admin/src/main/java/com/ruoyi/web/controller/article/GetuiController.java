package com.ruoyi.web.controller.article;

import com.getui.push.v2.sdk.api.PushApi;
import com.getui.push.v2.sdk.common.ApiResult;
import com.getui.push.v2.sdk.dto.req.Audience;
import com.getui.push.v2.sdk.dto.req.AudienceDTO;
import com.getui.push.v2.sdk.dto.req.message.PushChannel;
import com.getui.push.v2.sdk.dto.req.message.PushDTO;
import com.getui.push.v2.sdk.dto.req.message.PushMessage;
import com.getui.push.v2.sdk.dto.req.message.android.AndroidDTO;
import com.getui.push.v2.sdk.dto.req.message.android.GTNotification;
import com.getui.push.v2.sdk.dto.res.TaskIdDTO;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.FaCmsArchives;
import com.ruoyi.system.domain.FaUser;
import com.ruoyi.system.service.IFaCmsArchivesService;
import com.ruoyi.system.service.IFaUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description 个推
 * @Author fan
 * @Date 2021/8/12
 **/
@RestController
@RequestMapping("/push")
public class GetuiController {

    @Autowired
    private PushApi pushApi;

    @Autowired
    private IFaCmsArchivesService cmsArchivesService;

    @Autowired
    private IFaUserService userService;

    @Value("{getui.msgUrl}")
    private String msgUrl;

    /**
     * 批量推送：先创建消息再推送
     *
     * @param articleId
     * @return
     */
    @RequestMapping("/send")
    public AjaxResult sendMsg(@RequestParam("id") Integer articleId) {
        String taskId = getTaskId(articleId);
        List<String> pushCid = userService.getPushCid();
        AudienceDTO audienceDTO = new AudienceDTO();
        Audience audience = new Audience();
        audience.setCid(pushCid);
        audienceDTO.setAudience(audience);
        audienceDTO.setTaskid(taskId);
        ApiResult<Map<String, Map<String, String>>> mapApiResult = pushApi.pushListByCid(audienceDTO);
        if (mapApiResult.isSuccess()) {
            return AjaxResult.success("发送成功");
        } else {
            return AjaxResult.error("发送失败");
        }
    }


    /**
     * 创建消息
     *
     * @param articleId 文章ID
     * @return taskId
     */
    private String getTaskId(Integer articleId) {
        FaCmsArchives archives = cmsArchivesService.selectFaCmsArchivesById(articleId);
        PushDTO<Audience> pushDTO = new PushDTO<>();
        // 设置推送参数
        pushDTO.setRequestId(System.currentTimeMillis() + "");
        PushMessage pushMessage = new PushMessage();
        pushDTO.setPushMessage(pushMessage);
        GTNotification notification = new GTNotification();
        pushMessage.setNotification(notification);
        // todo 消息内容设置
        notification.setTitle(archives.getTitle());
        notification.setBody(archives.getDescription());
        notification.setClickType("url");
        notification.setUrl(msgUrl);
        // 设置接收人信息
        Audience audience = new Audience();
        pushDTO.setAudience(audience);
        ApiResult<TaskIdDTO> msg = pushApi.createMsg(pushDTO);
        return msg.getData().getTaskId();
    }


}
