package com.ruoyi.web.controller.commonsetting;

import java.util.List;

import com.ruoyi.system.domain.vo.CommonSettingConfigVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaConfig;
import com.ruoyi.system.service.IFaConfigService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * App配置Controller
 *
 * @author ruoyi
 * @date 2021-08-14
 */
@RestController
@RequestMapping("/common/appconfig")
public class FaConfigController extends BaseController {
    @Autowired
    private IFaConfigService faConfigService;

    /**
     * 查询App配置列表
     */
    @PreAuthorize("@ss.hasPermi('common:appconfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaConfig faConfig) {
        startPage();
        List<FaConfig> list = faConfigService.selectFaConfigList(faConfig);
        return getDataTable(list);
    }

    /**
     * 导出App配置列表
     */
    @PreAuthorize("@ss.hasPermi('common:appconfig:export')")
    @Log(title = "App配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaConfig faConfig) {
        List<FaConfig> list = faConfigService.selectFaConfigList(faConfig);
        ExcelUtil<FaConfig> util = new ExcelUtil<FaConfig>(FaConfig.class);
        return util.exportExcel(list, "App配置数据");
    }

    /**
     * 获取App配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:appconfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(faConfigService.selectFaConfigById(id));
    }

    /**
     * 新增App配置
     */
    @PreAuthorize("@ss.hasPermi('common:appconfig:add')")
    @Log(title = "App配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaConfig faConfig) {
        return toAjax(faConfigService.insertFaConfig(faConfig));
    }

    /**
     * 修改App配置
     */
    @PreAuthorize("@ss.hasPermi('common:appconfig:edit')")
    @Log(title = "App配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaConfig faConfig) {
        return toAjax(faConfigService.updateFaConfig(faConfig));
    }

    /**
     * 删除App配置
     */
    @PreAuthorize("@ss.hasPermi('common:appconfig:remove')")
    @Log(title = "App配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(faConfigService.deleteFaConfigByIds(ids));
    }

    /**
     * 查询App配置列表
     */
    @PreAuthorize("@ss.hasPermi('common:appconfig:list')")
    @GetMapping("/get_list")
    public List<CommonSettingConfigVo> myList(String type) {
        return faConfigService.listCommonSettingConfigVo(type);
    }


}
