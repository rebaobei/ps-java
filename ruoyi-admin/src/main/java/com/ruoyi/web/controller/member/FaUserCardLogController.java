package com.ruoyi.web.controller.member;

import java.util.List;

import com.ruoyi.system.domain.vo.CardLogWithUserInfoVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserCardLog;
import com.ruoyi.system.service.IFaUserCardLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 储值卡变动Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/card")
public class FaUserCardLogController extends BaseController
{
    @Autowired
    private IFaUserCardLogService faUserCardLogService;

    /**
     * 查询储值卡变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:card:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserCardLog faUserCardLog)
    {
        startPage();
        List<FaUserCardLog> list = faUserCardLogService.selectFaUserCardLogList(faUserCardLog);
        return getDataTable(list);
    }

    /**
     * 导出储值卡变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:card:export')")
    @Log(title = "储值卡变动", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserCardLog faUserCardLog)
    {
        List<FaUserCardLog> list = faUserCardLogService.selectFaUserCardLogList(faUserCardLog);
        ExcelUtil<FaUserCardLog> util = new ExcelUtil<FaUserCardLog>(FaUserCardLog.class);
        return util.exportExcel(list, "储值卡变动数据");
    }

    /**
     * 获取储值卡变动详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:card:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faUserCardLogService.selectFaUserCardLogById(id));
    }

    /**
     * 新增储值卡变动
     */
    @PreAuthorize("@ss.hasPermi('member:card:add')")
    @Log(title = "储值卡变动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserCardLog faUserCardLog)
    {
        return toAjax(faUserCardLogService.insertFaUserCardLog(faUserCardLog));
    }

    /**
     * 修改储值卡变动
     */
    @PreAuthorize("@ss.hasPermi('member:card:edit')")
    @Log(title = "储值卡变动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserCardLog faUserCardLog)
    {
        return toAjax(faUserCardLogService.updateFaUserCardLog(faUserCardLog));
    }

    /**
     * 删除储值卡变动
     */
    @PreAuthorize("@ss.hasPermi('member:card:remove')")
    @Log(title = "储值卡变动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faUserCardLogService.deleteFaUserCardLogByIds(ids));
    }

    /**
     * 查询储值卡变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:card:list')")
    @GetMapping("/get_list")
    public TableDataInfo MyList(FaUserCardLog faUserCardLog)
    {
        startPage();
        List<CardLogWithUserInfoVo> list = faUserCardLogService.listCardLogWithUserInfo(faUserCardLog);
        return getDataTable(list);
    }
}
