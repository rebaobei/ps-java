package com.ruoyi.web.controller.label;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBallLabelfenlei;
import com.ruoyi.system.service.IFaBallLabelfenleiService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 标签分类Controller
 *
 * @author ruoyi
 * @date 2021-07-31
 */
@RestController
@RequestMapping("/label/category")
public class FaBallLabelfenleiController extends BaseController {
    @Autowired
    private IFaBallLabelfenleiService faBallLabelfenleiService;

    /**
     * 查询标签分类列表
     */
    @PreAuthorize("@ss.hasPermi('label:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBallLabelfenlei faBallLabelfenlei) {
        startPage();
        List<FaBallLabelfenlei> list = faBallLabelfenleiService.selectFaBallLabelfenleiList(faBallLabelfenlei);
        return getDataTable(list);
    }

    /**
     * 导出标签分类列表
     */
    @PreAuthorize("@ss.hasPermi('label:category:export')")
    @Log(title = "标签分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBallLabelfenlei faBallLabelfenlei) {
        List<FaBallLabelfenlei> list = faBallLabelfenleiService.selectFaBallLabelfenleiList(faBallLabelfenlei);
        ExcelUtil<FaBallLabelfenlei> util = new ExcelUtil<FaBallLabelfenlei>(FaBallLabelfenlei.class);
        return util.exportExcel(list, "标签分类数据");
    }

    /**
     * 获取标签分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('label:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(faBallLabelfenleiService.selectFaBallLabelfenleiById(id));
    }

    /**
     * 新增标签分类
     */
    @PreAuthorize("@ss.hasPermi('label:category:add')")
    @Log(title = "标签分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBallLabelfenlei faBallLabelfenlei) {
        return toAjax(faBallLabelfenleiService.insertFaBallLabelfenlei(faBallLabelfenlei));
    }

    /**
     * 修改标签分类
     */
    @PreAuthorize("@ss.hasPermi('label:category:edit')")
    @Log(title = "标签分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBallLabelfenlei faBallLabelfenlei) {
        return toAjax(faBallLabelfenleiService.updateFaBallLabelfenlei(faBallLabelfenlei));
    }

    /**
     * 删除标签分类
     */
    @PreAuthorize("@ss.hasPermi('label:category:remove')")
    @Log(title = "标签分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(faBallLabelfenleiService.deleteFaBallLabelfenleiByIds(ids));
    }

    /**
     * 查询所有标签分类列表
     */
    @PreAuthorize("@ss.hasPermi('label:category:list')")
    @GetMapping("/all_list")
    public List<FaBallLabelfenlei> allList(FaBallLabelfenlei faBallLabelfenlei) {
        List<FaBallLabelfenlei> list = faBallLabelfenleiService.selectFaBallLabelfenleiList(faBallLabelfenlei);
        return list;
    }
}
