package com.ruoyi.web.controller.vote;

import java.util.List;

import com.ruoyi.system.domain.vo.VoteUserVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaVoteUser;
import com.ruoyi.system.service.IFaVoteUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 参赛人员Controller
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@RestController
@RequestMapping("/vote/user")
public class FaVoteUserController extends BaseController
{
    @Autowired
    private IFaVoteUserService faVoteUserService;

    /**
     * 查询参赛人员列表
     */
    @PreAuthorize("@ss.hasPermi('vote:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaVoteUser faVoteUser)
    {
        startPage();
        List<FaVoteUser> list = faVoteUserService.selectFaVoteUserList(faVoteUser);
        return getDataTable(list);
    }

    /**
     * 导出参赛人员列表
     */
    @PreAuthorize("@ss.hasPermi('vote:user:export')")
    @Log(title = "参赛人员", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaVoteUser faVoteUser)
    {
        List<FaVoteUser> list = faVoteUserService.selectFaVoteUserList(faVoteUser);
        ExcelUtil<FaVoteUser> util = new ExcelUtil<FaVoteUser>(FaVoteUser.class);
        return util.exportExcel(list, "参赛人员数据");
    }

    /**
     * 获取参赛人员详细信息
     */
    @PreAuthorize("@ss.hasPermi('vote:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faVoteUserService.selectFaVoteUserById(id));
    }

    /**
     * 新增参赛人员
     */
    @PreAuthorize("@ss.hasPermi('vote:user:add')")
    @Log(title = "参赛人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaVoteUser faVoteUser)
    {
        return toAjax(faVoteUserService.insertFaVoteUser(faVoteUser));
    }

    /**
     * 修改参赛人员
     */
    @PreAuthorize("@ss.hasPermi('vote:user:edit')")
    @Log(title = "参赛人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaVoteUser faVoteUser)
    {
        return toAjax(faVoteUserService.updateFaVoteUser(faVoteUser));
    }

    /**
     * 删除参赛人员
     */
    @PreAuthorize("@ss.hasPermi('vote:user:remove')")
    @Log(title = "参赛人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faVoteUserService.deleteFaVoteUserByIds(ids));
    }


    /**
     * 查询参赛人员列表
     */
    @PreAuthorize("@ss.hasPermi('vote:user:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(VoteUserVo faVoteUser)
    {
        startPage();
        List<VoteUserVo> list = faVoteUserService.listVoteUserVo(faVoteUser);
        return getDataTable(list);
    }
}
