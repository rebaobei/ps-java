package com.ruoyi.web.controller.task;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserTask;
import com.ruoyi.system.service.IFaUserTaskService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 任务系统设置Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/task/tasksetting")
public class FaUserTaskController extends BaseController
{
    @Autowired
    private IFaUserTaskService faUserTaskService;

    /**
     * 查询任务系统设置列表
     */
    @PreAuthorize("@ss.hasPermi('task:tasksetting:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserTask faUserTask)
    {
        startPage();
        List<FaUserTask> list = faUserTaskService.selectFaUserTaskList(faUserTask);
        return getDataTable(list);
    }

    /**
     * 导出任务系统设置列表
     */
    @PreAuthorize("@ss.hasPermi('task:tasksetting:export')")
    @Log(title = "任务系统设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserTask faUserTask)
    {
        List<FaUserTask> list = faUserTaskService.selectFaUserTaskList(faUserTask);
        ExcelUtil<FaUserTask> util = new ExcelUtil<FaUserTask>(FaUserTask.class);
        return util.exportExcel(list, "任务系统设置数据");
    }

    /**
     * 获取任务系统设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('task:tasksetting:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faUserTaskService.selectFaUserTaskById(id));
    }

    /**
     * 新增任务系统设置
     */
    @PreAuthorize("@ss.hasPermi('task:tasksetting:add')")
    @Log(title = "任务系统设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserTask faUserTask)
    {
        return toAjax(faUserTaskService.insertFaUserTask(faUserTask));
    }

    /**
     * 修改任务系统设置
     */
    @PreAuthorize("@ss.hasPermi('task:tasksetting:edit')")
    @Log(title = "任务系统设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserTask faUserTask)
    {
        return toAjax(faUserTaskService.updateFaUserTask(faUserTask));
    }

    /**
     * 删除任务系统设置
     */
    @PreAuthorize("@ss.hasPermi('task:tasksetting:remove')")
    @Log(title = "任务系统设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faUserTaskService.deleteFaUserTaskByIds(ids));
    }
}
