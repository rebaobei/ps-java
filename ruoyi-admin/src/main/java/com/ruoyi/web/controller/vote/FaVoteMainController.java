package com.ruoyi.web.controller.vote;

import java.util.List;

import com.ruoyi.system.domain.vo.VoteMainVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaVoteMain;
import com.ruoyi.system.service.IFaVoteMainService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 投票管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@RestController
@RequestMapping("/vote/votemain")
public class FaVoteMainController extends BaseController
{
    @Autowired
    private IFaVoteMainService faVoteMainService;

    /**
     * 查询投票管理列表
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaVoteMain faVoteMain)
    {
        startPage();
        List<FaVoteMain> list = faVoteMainService.selectFaVoteMainList(faVoteMain);
        return getDataTable(list);
    }

    /**
     * 导出投票管理列表
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:export')")
    @Log(title = "投票管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaVoteMain faVoteMain)
    {
        List<FaVoteMain> list = faVoteMainService.selectFaVoteMainList(faVoteMain);
        ExcelUtil<FaVoteMain> util = new ExcelUtil<FaVoteMain>(FaVoteMain.class);
        return util.exportExcel(list, "投票管理数据");
    }

    /**
     * 获取投票管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faVoteMainService.selectFaVoteMainById(id));
    }

    /**
     * 新增投票管理
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:add')")
    @Log(title = "投票管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaVoteMain faVoteMain)
    {
        return toAjax(faVoteMainService.insertFaVoteMain(faVoteMain));
    }

    /**
     * 修改投票管理
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:edit')")
    @Log(title = "投票管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaVoteMain faVoteMain)
    {
        return toAjax(faVoteMainService.updateFaVoteMain(faVoteMain));
    }

    /**
     * 删除投票管理
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:remove')")
    @Log(title = "投票管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faVoteMainService.deleteFaVoteMainByIds(ids));
    }

    /**
     * 查询投票管理列表
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaVoteMain faVoteMain)
    {
        startPage();
        List<VoteMainVo> list = faVoteMainService.listVoteMainVo(faVoteMain);
        return getDataTable(list);
    }

    /**
     * 获取投票管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('vote:votemain:query')")
    @GetMapping(value = "/get/{id}")
    public AjaxResult getVoteMainInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faVoteMainService.getVoteMainVoById(id));
    }
}
