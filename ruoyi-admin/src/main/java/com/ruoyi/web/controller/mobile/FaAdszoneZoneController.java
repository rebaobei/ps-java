package com.ruoyi.web.controller.mobile;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaAdszoneZone;
import com.ruoyi.system.service.IFaAdszoneZoneService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 海报&广告位Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/mobile/adzone")
public class FaAdszoneZoneController extends BaseController
{
    @Autowired
    private IFaAdszoneZoneService faAdszoneZoneService;

    /**
     * 查询海报&广告位列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:adzone:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaAdszoneZone faAdszoneZone)
    {
        startPage();
        List<FaAdszoneZone> list = faAdszoneZoneService.selectFaAdszoneZoneList(faAdszoneZone);
        return getDataTable(list);
    }

    /**
     * 导出海报&广告位列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:adzone:export')")
    @Log(title = "海报&广告位", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaAdszoneZone faAdszoneZone)
    {
        List<FaAdszoneZone> list = faAdszoneZoneService.selectFaAdszoneZoneList(faAdszoneZone);
        ExcelUtil<FaAdszoneZone> util = new ExcelUtil<FaAdszoneZone>(FaAdszoneZone.class);
        return util.exportExcel(list, "海报&广告位数据");
    }

    /**
     * 获取海报&广告位详细信息
     */
    @PreAuthorize("@ss.hasPermi('mobile:adzone:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faAdszoneZoneService.selectFaAdszoneZoneById(id));
    }

    /**
     * 新增海报&广告位
     */
    @PreAuthorize("@ss.hasPermi('mobile:adzone:add')")
    @Log(title = "海报&广告位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaAdszoneZone faAdszoneZone)
    {
        return toAjax(faAdszoneZoneService.insertFaAdszoneZone(faAdszoneZone));
    }

    /**
     * 修改海报&广告位
     */
    @PreAuthorize("@ss.hasPermi('mobile:adzone:edit')")
    @Log(title = "海报&广告位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaAdszoneZone faAdszoneZone)
    {
        return toAjax(faAdszoneZoneService.updateFaAdszoneZone(faAdszoneZone));
    }

    /**
     * 删除海报&广告位
     */
    @PreAuthorize("@ss.hasPermi('mobile:adzone:remove')")
    @Log(title = "海报&广告位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faAdszoneZoneService.deleteFaAdszoneZoneByIds(ids));
    }
}
