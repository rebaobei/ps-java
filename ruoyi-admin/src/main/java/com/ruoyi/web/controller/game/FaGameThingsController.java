package com.ruoyi.web.controller.game;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameThings;
import com.ruoyi.system.service.IFaGameThingsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物品管理Controller
 *
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/things")
public class FaGameThingsController extends BaseController {
    @Autowired
    private IFaGameThingsService faGameThingsService;

    /**
     * 查询物品管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:things:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameThings faGameThings) {
        startPage();
        List<FaGameThings> list = faGameThingsService.selectFaGameThingsList(faGameThings);
        return getDataTable(list);
    }

    /**
     * 导出物品管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:things:export')")
    @Log(title = "物品管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameThings faGameThings) {
        List<FaGameThings> list = faGameThingsService.selectFaGameThingsList(faGameThings);
        ExcelUtil<FaGameThings> util = new ExcelUtil<FaGameThings>(FaGameThings.class);
        return util.exportExcel(list, "物品管理数据");
    }

    /**
     * 获取物品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:things:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(faGameThingsService.selectFaGameThingsById(id));
    }

    /**
     * 新增物品管理
     */
    @PreAuthorize("@ss.hasPermi('game:things:add')")
    @Log(title = "物品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameThings faGameThings) {
        return toAjax(faGameThingsService.insertFaGameThings(faGameThings));
    }

    /**
     * 修改物品管理
     */
    @PreAuthorize("@ss.hasPermi('game:things:edit')")
    @Log(title = "物品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameThings faGameThings) {
        return toAjax(faGameThingsService.updateFaGameThings(faGameThings));
    }

    /**
     * 删除物品管理
     */
    @PreAuthorize("@ss.hasPermi('game:things:remove')")
    @Log(title = "物品管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(faGameThingsService.deleteFaGameThingsByIds(ids));
    }

    /**
     * 所有装扮类型物品
     */
    @PreAuthorize("@ss.hasPermi('game:things:list')")
    @GetMapping("/dec_list")
    public List<FaGameThings> list() {
        FaGameThings things = new FaGameThings();
        things.setCategoryId(2L);
        return faGameThingsService.selectFaGameThingsList(things);
    }
}
