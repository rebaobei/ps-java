package com.ruoyi.web.controller.article;

import java.util.List;

import com.ruoyi.system.domain.vo.CmsArchivesVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaCmsArchives;
import com.ruoyi.system.service.IFaCmsArchivesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文章管理Controller
 * 
 * @author zzp
 * @date 2021-08-11
 */
@RestController
@RequestMapping("/article/article")
public class FaCmsArchivesController extends BaseController
{
    @Autowired
    private IFaCmsArchivesService faCmsArchivesService;

    /**
     * 查询文章管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:article:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaCmsArchives faCmsArchives)
    {
        startPage();
        List<FaCmsArchives> list = faCmsArchivesService.selectFaCmsArchivesList(faCmsArchives);
        return getDataTable(list);
    }

    /**
     * 导出文章管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:article:export')")
    @Log(title = "文章管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaCmsArchives faCmsArchives)
    {
        List<FaCmsArchives> list = faCmsArchivesService.selectFaCmsArchivesList(faCmsArchives);
        ExcelUtil<FaCmsArchives> util = new ExcelUtil<FaCmsArchives>(FaCmsArchives.class);
        return util.exportExcel(list, "文章管理数据");
    }

    /**
     * 获取文章管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:article:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faCmsArchivesService.selectFaCmsArchivesById(id));
    }

    /**
     * 新增文章管理
     */
    @PreAuthorize("@ss.hasPermi('article:article:add')")
    @Log(title = "文章管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsArchivesVo faCmsArchives)
    {
        return toAjax(faCmsArchivesService.insertFaCmsArchives(faCmsArchives));
    }

    /**
     * 修改文章管理
     */
    @PreAuthorize("@ss.hasPermi('article:article:edit')")
    @Log(title = "文章管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsArchivesVo faCmsArchives)
    {
        return toAjax(faCmsArchivesService.updateFaCmsArchives(faCmsArchives));
    }

    /**
     * 删除文章管理
     */
    @PreAuthorize("@ss.hasPermi('article:article:remove')")
    @Log(title = "文章管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faCmsArchivesService.deleteFaCmsArchivesByIds(ids));
    }


    /**
     * 获取文章管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:article:query')")
    @GetMapping(value = "/vo/{id}")
    public AjaxResult getInfoVo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faCmsArchivesService.selectFaCmsArchivesVoById(id));
    }

}
