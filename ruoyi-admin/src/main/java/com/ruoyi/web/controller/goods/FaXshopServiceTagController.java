package com.ruoyi.web.controller.goods;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopServiceTag;
import com.ruoyi.system.service.IFaXshopServiceTagService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 服务标签Controller
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
@RestController
@RequestMapping("/goods/servicetag")
public class FaXshopServiceTagController extends BaseController
{
    @Autowired
    private IFaXshopServiceTagService faXshopServiceTagService;

    /**
     * 查询服务标签列表
     */
    @PreAuthorize("@ss.hasPermi('goods:servicetag:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopServiceTag faXshopServiceTag)
    {
        startPage();
        List<FaXshopServiceTag> list = faXshopServiceTagService.selectFaXshopServiceTagList(faXshopServiceTag);
        return getDataTable(list);
    }

    /**
     * 导出服务标签列表
     */
    @PreAuthorize("@ss.hasPermi('goods:servicetag:export')")
    @Log(title = "服务标签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopServiceTag faXshopServiceTag)
    {
        List<FaXshopServiceTag> list = faXshopServiceTagService.selectFaXshopServiceTagList(faXshopServiceTag);
        ExcelUtil<FaXshopServiceTag> util = new ExcelUtil<FaXshopServiceTag>(FaXshopServiceTag.class);
        return util.exportExcel(list, "服务标签数据");
    }

    /**
     * 获取服务标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:servicetag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopServiceTagService.selectFaXshopServiceTagById(id));
    }

    /**
     * 新增服务标签
     */
    @PreAuthorize("@ss.hasPermi('goods:servicetag:add')")
    @Log(title = "服务标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopServiceTag faXshopServiceTag)
    {
        return toAjax(faXshopServiceTagService.insertFaXshopServiceTag(faXshopServiceTag));
    }

    /**
     * 修改服务标签
     */
    @PreAuthorize("@ss.hasPermi('goods:servicetag:edit')")
    @Log(title = "服务标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopServiceTag faXshopServiceTag)
    {
        return toAjax(faXshopServiceTagService.updateFaXshopServiceTag(faXshopServiceTag));
    }

    /**
     * 删除服务标签
     */
    @PreAuthorize("@ss.hasPermi('goods:servicetag:remove')")
    @Log(title = "服务标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopServiceTagService.deleteFaXshopServiceTagByIds(ids));
    }

    /**
     * 查询所有服务标签列表
     */
    @PreAuthorize("@ss.hasPermi('goods:servicetag:list')")
    @GetMapping("/all_list")
    public List<FaXshopServiceTag> allList(FaXshopServiceTag faXshopServiceTag)
    {
        return faXshopServiceTagService.selectFaXshopServiceTagList(faXshopServiceTag);
    }
}
