package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.RechargeOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaRechargeOrder;
import com.ruoyi.system.service.IFaRechargeOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 充值订单Controller
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/rechargeorder")
public class FaRechargeOrderController extends BaseController
{
    @Autowired
    private IFaRechargeOrderService faRechargeOrderService;

    /**
     * 查询充值订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:rechargeorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaRechargeOrder faRechargeOrder)
    {
        startPage();
        List<FaRechargeOrder> list = faRechargeOrderService.selectFaRechargeOrderList(faRechargeOrder);
        return getDataTable(list);
    }

    /**
     * 导出充值订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:rechargeorder:export')")
    @Log(title = "充值订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaRechargeOrder faRechargeOrder)
    {
        List<FaRechargeOrder> list = faRechargeOrderService.selectFaRechargeOrderList(faRechargeOrder);
        ExcelUtil<FaRechargeOrder> util = new ExcelUtil<FaRechargeOrder>(FaRechargeOrder.class);
        return util.exportExcel(list, "充值订单数据");
    }

    /**
     * 获取充值订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:rechargeorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faRechargeOrderService.selectFaRechargeOrderById(id));
    }

    /**
     * 新增充值订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:rechargeorder:add')")
    @Log(title = "充值订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaRechargeOrder faRechargeOrder)
    {
        return toAjax(faRechargeOrderService.insertFaRechargeOrder(faRechargeOrder));
    }

    /**
     * 修改充值订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:rechargeorder:edit')")
    @Log(title = "充值订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaRechargeOrder faRechargeOrder)
    {
        return toAjax(faRechargeOrderService.updateFaRechargeOrder(faRechargeOrder));
    }

    /**
     * 删除充值订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:rechargeorder:remove')")
    @Log(title = "充值订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faRechargeOrderService.deleteFaRechargeOrderByIds(ids));
    }


    /**
     * 查询充值订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:rechargeorder:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(RechargeOrderVo faRechargeOrder)
    {
        startPage();
        List<RechargeOrderVo> list = faRechargeOrderService.listRechargeOrderVo(faRechargeOrder);
        return getDataTable(list);
    }
}
