package com.ruoyi.web.controller.member;

import java.util.List;

import com.ruoyi.system.domain.vo.UserQiandaoLogWithUserInfoVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBallUserqiandaodetail;
import com.ruoyi.system.service.IFaBallUserqiandaodetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 签到明细Controller
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@RestController
@RequestMapping("/member/qiandao")
public class FaBallUserqiandaodetailController extends BaseController
{
    @Autowired
    private IFaBallUserqiandaodetailService faBallUserqiandaodetailService;

    /**
     * 查询签到明细列表
     */
    @PreAuthorize("@ss.hasPermi('member:qiandao:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        startPage();
        List<FaBallUserqiandaodetail> list = faBallUserqiandaodetailService.selectFaBallUserqiandaodetailList(faBallUserqiandaodetail);
        return getDataTable(list);
    }

    /**
     * 导出签到明细列表
     */
    @PreAuthorize("@ss.hasPermi('member:qiandao:export')")
    @Log(title = "签到明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        List<FaBallUserqiandaodetail> list = faBallUserqiandaodetailService.selectFaBallUserqiandaodetailList(faBallUserqiandaodetail);
        ExcelUtil<FaBallUserqiandaodetail> util = new ExcelUtil<FaBallUserqiandaodetail>(FaBallUserqiandaodetail.class);
        return util.exportExcel(list, "签到明细数据");
    }

    /**
     * 获取签到明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:qiandao:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faBallUserqiandaodetailService.selectFaBallUserqiandaodetailById(id));
    }

    /**
     * 新增签到明细
     */
    @PreAuthorize("@ss.hasPermi('member:qiandao:add')")
    @Log(title = "签到明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        return toAjax(faBallUserqiandaodetailService.insertFaBallUserqiandaodetail(faBallUserqiandaodetail));
    }

    /**
     * 修改签到明细
     */
    @PreAuthorize("@ss.hasPermi('member:qiandao:edit')")
    @Log(title = "签到明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        return toAjax(faBallUserqiandaodetailService.updateFaBallUserqiandaodetail(faBallUserqiandaodetail));
    }

    /**
     * 删除签到明细
     */
    @PreAuthorize("@ss.hasPermi('member:qiandao:remove')")
    @Log(title = "签到明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faBallUserqiandaodetailService.deleteFaBallUserqiandaodetailByIds(ids));
    }

    /**
     * 查询签到明细列表
     */
    @PreAuthorize("@ss.hasPermi('member:qiandao:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        startPage();
        List<UserQiandaoLogWithUserInfoVo> list = faBallUserqiandaodetailService.listUserQianDaoLog(faBallUserqiandaodetail);
        return getDataTable(list);
    }

}
