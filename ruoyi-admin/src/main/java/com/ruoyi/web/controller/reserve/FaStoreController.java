package com.ruoyi.web.controller.reserve;

import java.util.List;

import com.ruoyi.system.domain.vo.StoreVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStore;
import com.ruoyi.system.service.IFaStoreService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 门店管理Controller
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
@RestController
@RequestMapping("/reserve/store")
public class FaStoreController extends BaseController
{
    @Autowired
    private IFaStoreService faStoreService;

    /**
     * 查询门店管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:store:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStore faStore)
    {
        startPage();
        List<FaStore> list = faStoreService.selectFaStoreList(faStore);
        return getDataTable(list);
    }

    /**
     * 导出门店管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:store:export')")
    @Log(title = "门店管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStore faStore)
    {
        List<FaStore> list = faStoreService.selectFaStoreList(faStore);
        ExcelUtil<FaStore> util = new ExcelUtil<FaStore>(FaStore.class);
        return util.exportExcel(list, "门店管理数据");
    }

    /**
     * 获取门店管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:store:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreService.selectFaStoreById(id));
    }

    /**
     * 新增门店管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:store:add')")
    @Log(title = "门店管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStore faStore)
    {
        return toAjax(faStoreService.insertFaStore(faStore));
    }

    /**
     * 修改门店管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:store:edit')")
    @Log(title = "门店管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStore faStore)
    {
        return toAjax(faStoreService.updateFaStore(faStore));
    }

    /**
     * 删除门店管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:store:remove')")
    @Log(title = "门店管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreService.deleteFaStoreByIds(ids));
    }

    /**
     * 查询门店管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:store:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaStore faStore)
    {
        startPage();
        List<StoreVo> list = faStoreService.listStoreVo(faStore);
        return getDataTable(list);
    }
}
