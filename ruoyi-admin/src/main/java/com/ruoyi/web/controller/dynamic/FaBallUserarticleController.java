package com.ruoyi.web.controller.dynamic;

import java.util.List;

import com.ruoyi.system.domain.vo.UserArticleVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBallUserarticle;
import com.ruoyi.system.service.IFaBallUserarticleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 朋友圈明细Controller
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@RestController
@RequestMapping("/dynamic/userarticle")
public class FaBallUserarticleController extends BaseController
{
    @Autowired
    private IFaBallUserarticleService faBallUserarticleService;

    /**
     * 查询朋友圈明细列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userarticle:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBallUserarticle faBallUserarticle)
    {
        startPage();
        List<FaBallUserarticle> list = faBallUserarticleService.selectFaBallUserarticleList(faBallUserarticle);
        return getDataTable(list);
    }

    /**
     * 导出朋友圈明细列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userarticle:export')")
    @Log(title = "朋友圈明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBallUserarticle faBallUserarticle)
    {
        List<FaBallUserarticle> list = faBallUserarticleService.selectFaBallUserarticleList(faBallUserarticle);
        ExcelUtil<FaBallUserarticle> util = new ExcelUtil<FaBallUserarticle>(FaBallUserarticle.class);
        return util.exportExcel(list, "朋友圈明细数据");
    }

    /**
     * 获取朋友圈明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userarticle:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faBallUserarticleService.selectFaBallUserarticleById(id));
    }

    /**
     * 新增朋友圈明细
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userarticle:add')")
    @Log(title = "朋友圈明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBallUserarticle faBallUserarticle)
    {
        return toAjax(faBallUserarticleService.insertFaBallUserarticle(faBallUserarticle));
    }

    /**
     * 修改朋友圈明细
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userarticle:edit')")
    @Log(title = "朋友圈明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBallUserarticle faBallUserarticle)
    {
        return toAjax(faBallUserarticleService.updateFaBallUserarticle(faBallUserarticle));
    }

    /**
     * 删除朋友圈明细
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userarticle:remove')")
    @Log(title = "朋友圈明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faBallUserarticleService.deleteFaBallUserarticleByIds(ids));
    }

    /**
     * 查询朋友圈明细列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userarticle:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaBallUserarticle faBallUserarticle)
    {
        startPage();
        List<UserArticleVo> list = faBallUserarticleService.listUserArticleVo(faBallUserarticle);
        return getDataTable(list);
    }
}
