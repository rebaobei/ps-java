package com.ruoyi.web.controller.mobile;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaVideoList;
import com.ruoyi.system.service.IFaVideoListService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 视频管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/mobile/videolist")
public class FaVideoListController extends BaseController
{
    @Autowired
    private IFaVideoListService faVideoListService;

    /**
     * 查询视频管理列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:videolist:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaVideoList faVideoList)
    {
        startPage();
        List<FaVideoList> list = faVideoListService.selectFaVideoListList(faVideoList);
        return getDataTable(list);
    }

    /**
     * 导出视频管理列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:videolist:export')")
    @Log(title = "视频管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaVideoList faVideoList)
    {
        List<FaVideoList> list = faVideoListService.selectFaVideoListList(faVideoList);
        ExcelUtil<FaVideoList> util = new ExcelUtil<FaVideoList>(FaVideoList.class);
        return util.exportExcel(list, "视频管理数据");
    }

    /**
     * 获取视频管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mobile:videolist:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faVideoListService.selectFaVideoListById(id));
    }

    /**
     * 新增视频管理
     */
    @PreAuthorize("@ss.hasPermi('mobile:videolist:add')")
    @Log(title = "视频管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaVideoList faVideoList)
    {
        return toAjax(faVideoListService.insertFaVideoList(faVideoList));
    }

    /**
     * 修改视频管理
     */
    @PreAuthorize("@ss.hasPermi('mobile:videolist:edit')")
    @Log(title = "视频管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaVideoList faVideoList)
    {
        return toAjax(faVideoListService.updateFaVideoList(faVideoList));
    }

    /**
     * 删除视频管理
     */
    @PreAuthorize("@ss.hasPermi('mobile:videolist:remove')")
    @Log(title = "视频管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faVideoListService.deleteFaVideoListByIds(ids));
    }

    /**
     * 查询视频管理列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:videolist:list')")
    @GetMapping("/all_list")
    public List<FaVideoList> allList(FaVideoList faVideoList)
    {
        List<FaVideoList> list = faVideoListService.selectFaVideoListList(faVideoList);
        return list;
    }
}
