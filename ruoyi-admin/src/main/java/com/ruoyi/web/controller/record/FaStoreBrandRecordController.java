package com.ruoyi.web.controller.record;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.FaStoreBrandRecord;
import com.ruoyi.system.service.IFaStoreBrandRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 品牌审核记录Controller
 *
 * @author ruoyi
 * @date 2021-11-02
 */
@RestController
@RequestMapping("/storeBrandRecord/storeBrandRecord")
public class FaStoreBrandRecordController extends BaseController {
    @Autowired
    private IFaStoreBrandRecordService faStoreBrandRecordService;

    /**
     * 查询品牌审核记录列表
     */
    @PreAuthorize("@ss.hasPermi('storeBrandRecord:storeBrandRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStoreBrandRecord faStoreBrandRecord) {
        startPage();
        List<FaStoreBrandRecord> list = faStoreBrandRecordService.selectFaStoreBrandRecordList(faStoreBrandRecord);
        return getDataTable(list);
    }

    /**
     * 查询品牌审核记录列表
     */
    @GetMapping("/getMyList")
    public TableDataInfo getMyList(FaStoreBrandRecord faStoreBrandRecord) {
        Long userId = SecurityUtils.getLoginUser().getUser().getUserId();
        faStoreBrandRecord.setUserId(userId);
        startPage();
        List<FaStoreBrandRecord> list = faStoreBrandRecordService.selectFaStoreBrandRecordList(faStoreBrandRecord);
        return getDataTable(list);
    }

    /**
     * 导出品牌审核记录列表
     */
    @PreAuthorize("@ss.hasPermi('storeBrandRecord:storeBrandRecord:export')")
    @Log(title = "品牌审核记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreBrandRecord faStoreBrandRecord) {
        List<FaStoreBrandRecord> list = faStoreBrandRecordService.selectFaStoreBrandRecordList(faStoreBrandRecord);
        ExcelUtil<FaStoreBrandRecord> util = new ExcelUtil<FaStoreBrandRecord>(FaStoreBrandRecord.class);
        return util.exportExcel(list, "品牌审核记录数据");
    }

    /**
     * 获取品牌审核记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeBrandRecord:storeBrandRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(faStoreBrandRecordService.selectFaStoreBrandRecordById(id));
    }

    /**
     * 新增品牌审核记录
     */
    @Log(title = "品牌审核记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreBrandRecord faStoreBrandRecord) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        faStoreBrandRecord.setUserId(user.getUserId());
        faStoreBrandRecord.setStoreInfoId(user.getStoreInfoId());
        return toAjax(faStoreBrandRecordService.insertFaStoreBrandRecord(faStoreBrandRecord));
    }

    /**
     * 入驻品牌
     */
    @Log(title = "品牌审核记录", businessType = BusinessType.INSERT)
    @PostMapping("/join")
    public AjaxResult join(@RequestBody Long id) {
        return faStoreBrandRecordService.joinFaStoreBrandRecord(id);
    }

    /**
     * 修改品牌审核记录
     */
    @PreAuthorize("@ss.hasPermi('storeBrandRecord:storeBrandRecord:edit')")
    @Log(title = "品牌审核记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreBrandRecord faStoreBrandRecord) {
        return toAjax(faStoreBrandRecordService.updateFaStoreBrandRecord(faStoreBrandRecord));
    }
    /**
     * 审核品牌
     */
    @PreAuthorize("@ss.hasPermi('storeBrandRecord:storeBrandRecord:edit')")
    @Log(title = "品牌审核记录", businessType = BusinessType.UPDATE)
    @PutMapping("/check")
    public AjaxResult check(@RequestBody FaStoreBrandRecord faStoreBrandRecord) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        Long id = faStoreBrandRecord.getId();
        Integer status = faStoreBrandRecord.getStatus();
        return faStoreBrandRecordService.checkFaStoreBrandRecord(id,status);
    }


    /**
     * 删除品牌审核记录
     */
    @PreAuthorize("@ss.hasPermi('storeBrandRecord:storeBrandRecord:remove')")
    @Log(title = "品牌审核记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(faStoreBrandRecordService.deleteFaStoreBrandRecordByIds(ids));
    }
}
