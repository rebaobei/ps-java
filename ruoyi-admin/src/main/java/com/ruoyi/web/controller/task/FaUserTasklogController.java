package com.ruoyi.web.controller.task;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserTasklog;
import com.ruoyi.system.service.IFaUserTasklogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 完成任务记录Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/task/tasklog")
public class FaUserTasklogController extends BaseController
{
    @Autowired
    private IFaUserTasklogService faUserTasklogService;

    /**
     * 查询完成任务记录列表
     */
    @PreAuthorize("@ss.hasPermi('task:tasklog:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserTasklog faUserTasklog)
    {
        startPage();
        List<FaUserTasklog> list = faUserTasklogService.selectFaUserTasklogList(faUserTasklog);
        return getDataTable(list);
    }

    /**
     * 导出完成任务记录列表
     */
    @PreAuthorize("@ss.hasPermi('task:tasklog:export')")
    @Log(title = "完成任务记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserTasklog faUserTasklog)
    {
        List<FaUserTasklog> list = faUserTasklogService.selectFaUserTasklogList(faUserTasklog);
        ExcelUtil<FaUserTasklog> util = new ExcelUtil<FaUserTasklog>(FaUserTasklog.class);
        return util.exportExcel(list, "完成任务记录数据");
    }

    /**
     * 获取完成任务记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('task:tasklog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faUserTasklogService.selectFaUserTasklogById(id));
    }

    /**
     * 新增完成任务记录
     */
    @PreAuthorize("@ss.hasPermi('task:tasklog:add')")
    @Log(title = "完成任务记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserTasklog faUserTasklog)
    {
        return toAjax(faUserTasklogService.insertFaUserTasklog(faUserTasklog));
    }

    /**
     * 修改完成任务记录
     */
    @PreAuthorize("@ss.hasPermi('task:tasklog:edit')")
    @Log(title = "完成任务记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserTasklog faUserTasklog)
    {
        return toAjax(faUserTasklogService.updateFaUserTasklog(faUserTasklog));
    }

    /**
     * 删除完成任务记录
     */
    @PreAuthorize("@ss.hasPermi('task:tasklog:remove')")
    @Log(title = "完成任务记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faUserTasklogService.deleteFaUserTasklogByIds(ids));
    }
}
