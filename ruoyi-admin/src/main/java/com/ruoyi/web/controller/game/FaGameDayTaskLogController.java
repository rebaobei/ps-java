package com.ruoyi.web.controller.game;

import java.util.List;

import com.ruoyi.system.domain.vo.GameDayTaskLogVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameDayTaskLog;
import com.ruoyi.system.service.IFaGameDayTaskLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 每日任务完成记录Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/daytasklog")
public class FaGameDayTaskLogController extends BaseController
{
    @Autowired
    private IFaGameDayTaskLogService faGameDayTaskLogService;

    /**
     * 查询每日任务完成记录列表
     */
    @PreAuthorize("@ss.hasPermi('game:daytasklog:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameDayTaskLog faGameDayTaskLog)
    {
        startPage();
        List<FaGameDayTaskLog> list = faGameDayTaskLogService.selectFaGameDayTaskLogList(faGameDayTaskLog);
        return getDataTable(list);
    }

    /**
     * 导出每日任务完成记录列表
     */
    @PreAuthorize("@ss.hasPermi('game:daytasklog:export')")
    @Log(title = "每日任务完成记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameDayTaskLog faGameDayTaskLog)
    {
        List<FaGameDayTaskLog> list = faGameDayTaskLogService.selectFaGameDayTaskLogList(faGameDayTaskLog);
        ExcelUtil<FaGameDayTaskLog> util = new ExcelUtil<FaGameDayTaskLog>(FaGameDayTaskLog.class);
        return util.exportExcel(list, "每日任务完成记录数据");
    }

    /**
     * 获取每日任务完成记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:daytasklog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGameDayTaskLogService.selectFaGameDayTaskLogById(id));
    }

    /**
     * 新增每日任务完成记录
     */
    @PreAuthorize("@ss.hasPermi('game:daytasklog:add')")
    @Log(title = "每日任务完成记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameDayTaskLog faGameDayTaskLog)
    {
        return toAjax(faGameDayTaskLogService.insertFaGameDayTaskLog(faGameDayTaskLog));
    }

    /**
     * 修改每日任务完成记录
     */
    @PreAuthorize("@ss.hasPermi('game:daytasklog:edit')")
    @Log(title = "每日任务完成记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameDayTaskLog faGameDayTaskLog)
    {
        return toAjax(faGameDayTaskLogService.updateFaGameDayTaskLog(faGameDayTaskLog));
    }

    /**
     * 删除每日任务完成记录
     */
    @PreAuthorize("@ss.hasPermi('game:daytasklog:remove')")
    @Log(title = "每日任务完成记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGameDayTaskLogService.deleteFaGameDayTaskLogByIds(ids));
    }


    /**
     * 查询每日任务完成记录列表
     */
    @PreAuthorize("@ss.hasPermi('game:daytasklog:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(GameDayTaskLogVo faGameDayTaskLog)
    {
        startPage();
        List<GameDayTaskLogVo> list = faGameDayTaskLogService.listGameDayTaskLogVo(faGameDayTaskLog);
        return getDataTable(list);
    }
}
