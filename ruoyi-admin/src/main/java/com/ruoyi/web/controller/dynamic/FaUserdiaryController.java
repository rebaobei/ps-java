package com.ruoyi.web.controller.dynamic;

import java.util.List;

import com.ruoyi.system.domain.vo.UserDiaryVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserdiary;
import com.ruoyi.system.service.IFaUserdiaryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物日记Controller
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@RestController
@RequestMapping("/dynamic/userdiary")
public class FaUserdiaryController extends BaseController
{
    @Autowired
    private IFaUserdiaryService faUserdiaryService;

    /**
     * 查询宠物日记列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userdiary:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserdiary faUserdiary)
    {
        startPage();
        List<FaUserdiary> list = faUserdiaryService.selectFaUserdiaryList(faUserdiary);
        return getDataTable(list);
    }

    /**
     * 导出宠物日记列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userdiary:export')")
    @Log(title = "宠物日记", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserdiary faUserdiary)
    {
        List<FaUserdiary> list = faUserdiaryService.selectFaUserdiaryList(faUserdiary);
        ExcelUtil<FaUserdiary> util = new ExcelUtil<FaUserdiary>(FaUserdiary.class);
        return util.exportExcel(list, "宠物日记数据");
    }

    /**
     * 获取宠物日记详细信息
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userdiary:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faUserdiaryService.selectFaUserdiaryById(id));
    }

    /**
     * 新增宠物日记
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userdiary:add')")
    @Log(title = "宠物日记", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserdiary faUserdiary)
    {
        return toAjax(faUserdiaryService.insertFaUserdiary(faUserdiary));
    }

    /**
     * 修改宠物日记
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userdiary:edit')")
    @Log(title = "宠物日记", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserdiary faUserdiary)
    {
        return toAjax(faUserdiaryService.updateFaUserdiary(faUserdiary));
    }

    /**
     * 删除宠物日记
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userdiary:remove')")
    @Log(title = "宠物日记", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faUserdiaryService.deleteFaUserdiaryByIds(ids));
    }


    /**
     * 查询宠物日记列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:userdiary:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(UserDiaryVo userDiaryVo)
    {
        startPage();
        List<UserDiaryVo> list = faUserdiaryService.listUserDiaryVo(userDiaryVo);
        return getDataTable(list);
    }

}
