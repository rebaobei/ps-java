package com.ruoyi.web.controller.article;

import java.util.List;

import com.ruoyi.system.domain.vo.CmsChannelTreeVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaCmsChannel;
import com.ruoyi.system.service.IFaCmsChannelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 分类管理Controller
 *
 * @author ruoyi
 * @date 2021-08-11
 */
@RestController
@RequestMapping("/article/channel")
public class FaCmsChannelController extends BaseController {
    @Autowired
    private IFaCmsChannelService faCmsChannelService;

    /**
     * 查询分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:channel:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaCmsChannel faCmsChannel) {
        startPage();
        List<FaCmsChannel> list = faCmsChannelService.selectFaCmsChannelList(faCmsChannel);
        return getDataTable(list);
    }

    /**
     * 导出分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:channel:export')")
    @Log(title = "分类管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaCmsChannel faCmsChannel) {
        List<FaCmsChannel> list = faCmsChannelService.selectFaCmsChannelList(faCmsChannel);
        ExcelUtil<FaCmsChannel> util = new ExcelUtil<FaCmsChannel>(FaCmsChannel.class);
        return util.exportExcel(list, "分类管理数据");
    }

    /**
     * 获取分类管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:channel:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(faCmsChannelService.selectFaCmsChannelById(id));
    }

    /**
     * 新增分类管理
     */
    @PreAuthorize("@ss.hasPermi('article:channel:add')")
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaCmsChannel faCmsChannel) {
        return toAjax(faCmsChannelService.insertFaCmsChannel(faCmsChannel));
    }

    /**
     * 修改分类管理
     */
    @PreAuthorize("@ss.hasPermi('article:channel:edit')")
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaCmsChannel faCmsChannel) {
        return toAjax(faCmsChannelService.updateFaCmsChannel(faCmsChannel));
    }

    /**
     * 删除分类管理
     */
    @PreAuthorize("@ss.hasPermi('article:channel:remove')")
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(faCmsChannelService.deleteFaCmsChannelByIds(ids));
    }

    /**
     * 查询分类管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:channel:list')")
    @GetMapping("/all_list")
    public List<FaCmsChannel> allList(FaCmsChannel faCmsChannel) {
        return faCmsChannelService.selectFaCmsChannelList(faCmsChannel);
    }

    /**
     * 分类树形数据
     */
    @PreAuthorize("@ss.hasPermi('article:channel:list')")
    @GetMapping("/tree")
    public List<CmsChannelTreeVo> treeList() {
        return faCmsChannelService.getChannelTreeData();
    }
}
