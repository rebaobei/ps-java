package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.JisuYuyueOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaJIsuYuyueOrder;
import com.ruoyi.system.service.IFaJIsuYuyueOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 寄宿预约订单Controller
 * 
 * @author ruoyi
 * @date 2021-08-06
 */
@RestController
@RequestMapping("/orderlist/roomyuyueorder")
public class FaJIsuYuyueOrderController extends BaseController
{
    @Autowired
    private IFaJIsuYuyueOrderService faJIsuYuyueOrderService;

    /**
     * 查询寄宿预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:roomyuyueorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(JisuYuyueOrderVo faJIsuYuyueOrder)
    {
        startPage();
        List<FaJIsuYuyueOrder> list = faJIsuYuyueOrderService.selectFaJIsuYuyueOrderList(faJIsuYuyueOrder);
        return getDataTable(list);
    }

    /**
     * 导出寄宿预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:roomyuyueorder:export')")
    @Log(title = "寄宿预约订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(JisuYuyueOrderVo faJIsuYuyueOrder)
    {
        List<FaJIsuYuyueOrder> list = faJIsuYuyueOrderService.selectFaJIsuYuyueOrderList(faJIsuYuyueOrder);
        ExcelUtil<FaJIsuYuyueOrder> util = new ExcelUtil<FaJIsuYuyueOrder>(FaJIsuYuyueOrder.class);
        return util.exportExcel(list, "寄宿预约订单数据");
    }

    /**
     * 获取寄宿预约订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:roomyuyueorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faJIsuYuyueOrderService.selectFaJIsuYuyueOrderById(id));
    }

    /**
     * 新增寄宿预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:roomyuyueorder:add')")
    @Log(title = "寄宿预约订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaJIsuYuyueOrder faJIsuYuyueOrder)
    {
        return toAjax(faJIsuYuyueOrderService.insertFaJIsuYuyueOrder(faJIsuYuyueOrder));
    }

    /**
     * 修改寄宿预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:roomyuyueorder:edit')")
    @Log(title = "寄宿预约订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaJIsuYuyueOrder faJIsuYuyueOrder)
    {
        return toAjax(faJIsuYuyueOrderService.updateFaJIsuYuyueOrder(faJIsuYuyueOrder));
    }

    /**
     * 删除寄宿预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:roomyuyueorder:remove')")
    @Log(title = "寄宿预约订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faJIsuYuyueOrderService.deleteFaJIsuYuyueOrderByIds(ids));
    }
}
