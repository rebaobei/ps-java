package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.ServiceOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaServiceYuyueOrder;
import com.ruoyi.system.service.IFaServiceYuyueOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 服务预约订单Controller
 *
 * @author ruoyi
 * @date 2021-08-06
 */
@RestController
@RequestMapping("/orderlist/serviceyuyueorder")
public class FaServiceYuyueOrderController extends BaseController
{
    @Autowired
    private IFaServiceYuyueOrderService faServiceYuyueOrderService;

    /**
     * 查询服务预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaServiceYuyueOrder faServiceYuyueOrder)
    {
        startPage();
        List<FaServiceYuyueOrder> list = faServiceYuyueOrderService.selectFaServiceYuyueOrderList(faServiceYuyueOrder);
        return getDataTable(list);
    }
    /**
     * 查询服务预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:list')")
    @GetMapping("/myList")
    public TableDataInfo myList(FaServiceYuyueOrder faServiceYuyueOrder)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faServiceYuyueOrder.setStoreId(storeInfoId);
        startPage();
        List<FaServiceYuyueOrder> list = faServiceYuyueOrderService.selectFaServiceYuyueOrderList(faServiceYuyueOrder);
        return getDataTable(list);
    }

    /**
     * 导出服务预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:export')")
    @Log(title = "服务预约订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaServiceYuyueOrder faServiceYuyueOrder)
    {
        List<FaServiceYuyueOrder> list = faServiceYuyueOrderService.selectFaServiceYuyueOrderList(faServiceYuyueOrder);
        ExcelUtil<FaServiceYuyueOrder> util = new ExcelUtil<FaServiceYuyueOrder>(FaServiceYuyueOrder.class);
        return util.exportExcel(list, "服务预约订单数据");
    }

    /**
     * 获取服务预约订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faServiceYuyueOrderService.selectFaServiceYuyueOrderById(id));
    }

    /**
     * 新增服务预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:add')")
    @Log(title = "服务预约订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaServiceYuyueOrder faServiceYuyueOrder)
    {
        return toAjax(faServiceYuyueOrderService.insertFaServiceYuyueOrder(faServiceYuyueOrder));
    }

    /**
     * 修改服务预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:edit')")
    @Log(title = "服务预约订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaServiceYuyueOrder faServiceYuyueOrder)
    {
        return toAjax(faServiceYuyueOrderService.updateFaServiceYuyueOrder(faServiceYuyueOrder));
    }

    /**
     * 删除服务预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:remove')")
    @Log(title = "服务预约订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faServiceYuyueOrderService.deleteFaServiceYuyueOrderByIds(ids));
    }

    /**
     * 查询服务预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(ServiceOrderVo faServiceYuyueOrder)
    {
        startPage();
        List<ServiceOrderVo> list = faServiceYuyueOrderService.listServiceOrderVo(faServiceYuyueOrder);
        return getDataTable(list);
    }

    /**
     * 查询服务预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:serviceyuyueorder:list')")
    @GetMapping("/getMyList")
    public TableDataInfo getMyList(ServiceOrderVo faServiceYuyueOrder)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faServiceYuyueOrder.setStoreId(storeInfoId);
        startPage();
        List<ServiceOrderVo> list = faServiceYuyueOrderService.listServiceOrderVo(faServiceYuyueOrder);
        return getDataTable(list);
    }
}
