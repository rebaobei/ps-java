package com.ruoyi.web.controller.game;

import java.util.List;

import com.ruoyi.system.domain.vo.GameCommonTaskLogVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameCommonTaskLog;
import com.ruoyi.system.service.IFaGameCommonTaskLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 常规任务记录Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/commontasklog")
public class FaGameCommonTaskLogController extends BaseController
{
    @Autowired
    private IFaGameCommonTaskLogService faGameCommonTaskLogService;

    /**
     * 查询常规任务记录列表
     */
    @PreAuthorize("@ss.hasPermi('game:commontasklog:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameCommonTaskLog faGameCommonTaskLog)
    {
        startPage();
        List<FaGameCommonTaskLog> list = faGameCommonTaskLogService.selectFaGameCommonTaskLogList(faGameCommonTaskLog);
        return getDataTable(list);
    }

    /**
     * 导出常规任务记录列表
     */
    @PreAuthorize("@ss.hasPermi('game:commontasklog:export')")
    @Log(title = "常规任务记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameCommonTaskLog faGameCommonTaskLog)
    {
        List<FaGameCommonTaskLog> list = faGameCommonTaskLogService.selectFaGameCommonTaskLogList(faGameCommonTaskLog);
        ExcelUtil<FaGameCommonTaskLog> util = new ExcelUtil<FaGameCommonTaskLog>(FaGameCommonTaskLog.class);
        return util.exportExcel(list, "常规任务记录数据");
    }

    /**
     * 获取常规任务记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:commontasklog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGameCommonTaskLogService.selectFaGameCommonTaskLogById(id));
    }

    /**
     * 新增常规任务记录
     */
    @PreAuthorize("@ss.hasPermi('game:commontasklog:add')")
    @Log(title = "常规任务记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameCommonTaskLog faGameCommonTaskLog)
    {
        return toAjax(faGameCommonTaskLogService.insertFaGameCommonTaskLog(faGameCommonTaskLog));
    }

    /**
     * 修改常规任务记录
     */
    @PreAuthorize("@ss.hasPermi('game:commontasklog:edit')")
    @Log(title = "常规任务记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameCommonTaskLog faGameCommonTaskLog)
    {
        return toAjax(faGameCommonTaskLogService.updateFaGameCommonTaskLog(faGameCommonTaskLog));
    }

    /**
     * 删除常规任务记录
     */
    @PreAuthorize("@ss.hasPermi('game:commontasklog:remove')")
    @Log(title = "常规任务记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGameCommonTaskLogService.deleteFaGameCommonTaskLogByIds(ids));
    }



    /**
     * 查询常规任务记录列表
     */
    @PreAuthorize("@ss.hasPermi('game:commontasklog:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(GameCommonTaskLogVo faGameCommonTaskLog)
    {
        startPage();
        List<GameCommonTaskLogVo> list = faGameCommonTaskLogService.listCommonTaskLogVo(faGameCommonTaskLog);
        return getDataTable(list);
    }
}
