package com.ruoyi.web.controller.game;

import java.util.List;

import com.ruoyi.system.domain.vo.PetHasOwnerVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGamePetsHasOwner;
import com.ruoyi.system.service.IFaGamePetsHasOwnerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 已领养宠物Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/pethasowner")
public class FaGamePetsHasOwnerController extends BaseController
{
    @Autowired
    private IFaGamePetsHasOwnerService faGamePetsHasOwnerService;

    /**
     * 查询已领养宠物列表
     */
    @PreAuthorize("@ss.hasPermi('game:pethasowner:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGamePetsHasOwner faGamePetsHasOwner)
    {
        startPage();
        List<FaGamePetsHasOwner> list = faGamePetsHasOwnerService.selectFaGamePetsHasOwnerList(faGamePetsHasOwner);
        return getDataTable(list);
    }

    /**
     * 导出已领养宠物列表
     */
    @PreAuthorize("@ss.hasPermi('game:pethasowner:export')")
    @Log(title = "已领养宠物", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGamePetsHasOwner faGamePetsHasOwner)
    {
        List<FaGamePetsHasOwner> list = faGamePetsHasOwnerService.selectFaGamePetsHasOwnerList(faGamePetsHasOwner);
        ExcelUtil<FaGamePetsHasOwner> util = new ExcelUtil<FaGamePetsHasOwner>(FaGamePetsHasOwner.class);
        return util.exportExcel(list, "已领养宠物数据");
    }

    /**
     * 获取已领养宠物详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:pethasowner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGamePetsHasOwnerService.selectFaGamePetsHasOwnerById(id));
    }

    /**
     * 新增已领养宠物
     */
    @PreAuthorize("@ss.hasPermi('game:pethasowner:add')")
    @Log(title = "已领养宠物", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGamePetsHasOwner faGamePetsHasOwner)
    {
        return toAjax(faGamePetsHasOwnerService.insertFaGamePetsHasOwner(faGamePetsHasOwner));
    }

    /**
     * 修改已领养宠物
     */
    @PreAuthorize("@ss.hasPermi('game:pethasowner:edit')")
    @Log(title = "已领养宠物", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGamePetsHasOwner faGamePetsHasOwner)
    {
        return toAjax(faGamePetsHasOwnerService.updateFaGamePetsHasOwner(faGamePetsHasOwner));
    }

    /**
     * 删除已领养宠物
     */
    @PreAuthorize("@ss.hasPermi('game:pethasowner:remove')")
    @Log(title = "已领养宠物", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGamePetsHasOwnerService.deleteFaGamePetsHasOwnerByIds(ids));
    }

    /**
     * 查询已领养宠物列表
     */
    @PreAuthorize("@ss.hasPermi('game:pethasowner:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(PetHasOwnerVo faGamePetsHasOwner)
    {
        startPage();
        List<PetHasOwnerVo> list = faGamePetsHasOwnerService.listPetHasOwnerVo(faGamePetsHasOwner);
        return getDataTable(list);
    }


}
