package com.ruoyi.web.controller.goods;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopCategory;
import com.ruoyi.system.service.IFaXshopCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品分类Controller
 * 
 * @author ruoyi
 * @date 2021-07-16
 */
@RestController
@RequestMapping("/goods/category")
public class FaXshopCategoryController extends BaseController
{
    @Autowired
    private IFaXshopCategoryService faXshopCategoryService;

    /**
     * 查询商品分类列表
     */
    @PreAuthorize("@ss.hasPermi('goods:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopCategory faXshopCategory)
    {
        startPage();
        List<FaXshopCategory> list = faXshopCategoryService.selectFaXshopCategoryList(faXshopCategory);
        return getDataTable(list);
    }

    /**
     * 导出商品分类列表
     */
    @PreAuthorize("@ss.hasPermi('goods:category:export')")
    @Log(title = "商品分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopCategory faXshopCategory)
    {
        List<FaXshopCategory> list = faXshopCategoryService.selectFaXshopCategoryList(faXshopCategory);
        ExcelUtil<FaXshopCategory> util = new ExcelUtil<FaXshopCategory>(FaXshopCategory.class);
        return util.exportExcel(list, "商品分类数据");
    }

    /**
     * 获取商品分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopCategoryService.selectFaXshopCategoryById(id));
    }

    /**
     * 新增商品分类
     */
    @PreAuthorize("@ss.hasPermi('goods:category:add')")
    @Log(title = "商品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopCategory faXshopCategory)
    {
        faXshopCategory.setCreatetime(System.currentTimeMillis()/1000);
        faXshopCategory.setUpdatetime(System.currentTimeMillis()/1000);
        return toAjax(faXshopCategoryService.insertFaXshopCategory(faXshopCategory));
    }

    /**
     * 修改商品分类
     */
    @PreAuthorize("@ss.hasPermi('goods:category:edit')")
    @Log(title = "商品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopCategory faXshopCategory)
    {
        faXshopCategory.setUpdatetime(System.currentTimeMillis()/1000);
        return toAjax(faXshopCategoryService.updateFaXshopCategory(faXshopCategory));
    }

    /**
     * 删除商品分类
     */
    @PreAuthorize("@ss.hasPermi('goods:category:remove')")
    @Log(title = "商品分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopCategoryService.deleteFaXshopCategoryByIds(ids));
    }


    /**
     * 查询商品分类列表
     */
    @PreAuthorize("@ss.hasPermi('goods:category:list')")
    @GetMapping("/all")
    public List<FaXshopCategory> allList()
    {
        List<FaXshopCategory> list = faXshopCategoryService.selectFaXshopCategoryList(new FaXshopCategory());
        FaXshopCategory category = new FaXshopCategory();
        category.setId(0);
        category.setName("无");
        list.add(category);
        return list;
    }
}
