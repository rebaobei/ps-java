package com.ruoyi.web.controller.upload;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.IOssuploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传
 */
@RestController
@RequestMapping("/Ossupload")
public class OssuploadController extends BaseController {

    @Autowired
    private IOssuploadService ossuploadService;

    @PostMapping("/upload")
    public AjaxResult upload(@RequestParam("file") MultipartFile file) {
        return ossuploadService.upload(file);
    }
    @PostMapping("/uploadvideo")
    public AjaxResult uploadVideo(@RequestParam("file") MultipartFile file) {
        return ossuploadService.uploadVideo(file);
    }
}
