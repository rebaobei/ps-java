package com.ruoyi.web.controller.member;

import java.util.List;

import com.ruoyi.system.domain.vo.MoneyLogWithUserInfoVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserMoneyLog;
import com.ruoyi.system.service.IFaUserMoneyLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 余额变动Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/money")
public class FaUserMoneyLogController extends BaseController
{
    @Autowired
    private IFaUserMoneyLogService faUserMoneyLogService;

    /**
     * 查询余额变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:money:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserMoneyLog faUserMoneyLog)
    {
        startPage();
        List<FaUserMoneyLog> list = faUserMoneyLogService.selectFaUserMoneyLogList(faUserMoneyLog);
        return getDataTable(list);
    }

    /**
     * 导出余额变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:money:export')")
    @Log(title = "余额变动", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserMoneyLog faUserMoneyLog)
    {
        List<FaUserMoneyLog> list = faUserMoneyLogService.selectFaUserMoneyLogList(faUserMoneyLog);
        ExcelUtil<FaUserMoneyLog> util = new ExcelUtil<FaUserMoneyLog>(FaUserMoneyLog.class);
        return util.exportExcel(list, "余额变动数据");
    }

    /**
     * 获取余额变动详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:money:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faUserMoneyLogService.selectFaUserMoneyLogById(id));
    }

    /**
     * 新增余额变动
     */
    @PreAuthorize("@ss.hasPermi('member:money:add')")
    @Log(title = "余额变动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserMoneyLog faUserMoneyLog)
    {
        return toAjax(faUserMoneyLogService.insertFaUserMoneyLog(faUserMoneyLog));
    }

    /**
     * 修改余额变动
     */
    @PreAuthorize("@ss.hasPermi('member:money:edit')")
    @Log(title = "余额变动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserMoneyLog faUserMoneyLog)
    {
        return toAjax(faUserMoneyLogService.updateFaUserMoneyLog(faUserMoneyLog));
    }

    /**
     * 删除余额变动
     */
    @PreAuthorize("@ss.hasPermi('member:money:remove')")
    @Log(title = "余额变动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faUserMoneyLogService.deleteFaUserMoneyLogByIds(ids));
    }

    /**
     * 查询余额变动列表
     */
    @PreAuthorize("@ss.hasPermi('member:money:list')")
    @GetMapping("/get_list")
    public TableDataInfo MyList(FaUserMoneyLog faUserMoneyLog)
    {
        startPage();
        List<MoneyLogWithUserInfoVo> list = faUserMoneyLogService.listMoneyLogWithUserInfo(faUserMoneyLog);
        return getDataTable(list);
    }

}
