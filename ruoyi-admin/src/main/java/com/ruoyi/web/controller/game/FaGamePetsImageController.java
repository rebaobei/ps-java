package com.ruoyi.web.controller.game;

import java.util.List;

import com.ruoyi.system.domain.vo.GamePetsImageVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGamePetsImage;
import com.ruoyi.system.service.IFaGamePetsImageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 装扮管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/clothimage")
public class FaGamePetsImageController extends BaseController
{
    @Autowired
    private IFaGamePetsImageService faGamePetsImageService;

    /**
     * 查询装扮管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:clothimage:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGamePetsImage faGamePetsImage)
    {
        startPage();
        List<FaGamePetsImage> list = faGamePetsImageService.selectFaGamePetsImageList(faGamePetsImage);
        return getDataTable(list);
    }

    /**
     * 导出装扮管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:clothimage:export')")
    @Log(title = "装扮管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGamePetsImage faGamePetsImage)
    {
        List<FaGamePetsImage> list = faGamePetsImageService.selectFaGamePetsImageList(faGamePetsImage);
        ExcelUtil<FaGamePetsImage> util = new ExcelUtil<FaGamePetsImage>(FaGamePetsImage.class);
        return util.exportExcel(list, "装扮管理数据");
    }

    /**
     * 获取装扮管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:clothimage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGamePetsImageService.selectFaGamePetsImageById(id));
    }

    /**
     * 新增装扮管理
     */
    @PreAuthorize("@ss.hasPermi('game:clothimage:add')")
    @Log(title = "装扮管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGamePetsImage faGamePetsImage)
    {
        return toAjax(faGamePetsImageService.insertFaGamePetsImage(faGamePetsImage));
    }

    /**
     * 修改装扮管理
     */
    @PreAuthorize("@ss.hasPermi('game:clothimage:edit')")
    @Log(title = "装扮管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGamePetsImage faGamePetsImage)
    {
        return toAjax(faGamePetsImageService.updateFaGamePetsImage(faGamePetsImage));
    }

    /**
     * 删除装扮管理
     */
    @PreAuthorize("@ss.hasPermi('game:clothimage:remove')")
    @Log(title = "装扮管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGamePetsImageService.deleteFaGamePetsImageByIds(ids));
    }

    /**
     * 查询装扮管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:clothimage:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaGamePetsImage faGamePetsImage)
    {
        startPage();
        List<GamePetsImageVo> list = faGamePetsImageService.listPetsImageVo(faGamePetsImage);
        return getDataTable(list);
    }

}
