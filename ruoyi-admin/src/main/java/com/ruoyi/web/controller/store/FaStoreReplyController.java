package com.ruoyi.web.controller.store;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStoreReply;
import com.ruoyi.system.service.IFaStoreReplyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺问题回复Controller
 *
 * @author ruoyi
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/storeinfo/reply")
public class FaStoreReplyController extends BaseController
{
    @Autowired
    private IFaStoreReplyService faStoreReplyService;

    /**
     * 查询店铺问题回复列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:reply:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStoreReply faStoreReply)
    {
        startPage();
        List<FaStoreReply> list = faStoreReplyService.selectFaStoreReplyList(faStoreReply);
        return getDataTable(list);
    }

    /**
     * 导出店铺问题回复列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:reply:export')")
    @Log(title = "店铺问题回复", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreReply faStoreReply)
    {
        List<FaStoreReply> list = faStoreReplyService.selectFaStoreReplyList(faStoreReply);
        ExcelUtil<FaStoreReply> util = new ExcelUtil<FaStoreReply>(FaStoreReply.class);
        return util.exportExcel(list, "店铺问题回复数据");
    }

    /**
     * 获取店铺问题回复详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:reply:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreReplyService.selectFaStoreReplyById(id));
    }

    /**
     * 新增店铺问题回复
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:reply:add')")
    @Log(title = "店铺问题回复", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreReply faStoreReply)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreReply.setStatus(0L);
        faStoreReply.setLikes(0L);
        faStoreReply.setReturnType("1");
        faStoreReply.setStoreId(storeInfoId);
        return toAjax(faStoreReplyService.insertFaStoreReply(faStoreReply));
    }

    /**
     * 修改店铺问题回复
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:reply:edit')")
    @Log(title = "店铺问题回复", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreReply faStoreReply)
    {
        return toAjax(faStoreReplyService.updateFaStoreReply(faStoreReply));
    }

    /**
     * 删除店铺问题回复
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:reply:remove')")
    @Log(title = "店铺问题回复", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreReplyService.deleteFaStoreReplyByIds(ids));
    }
}
