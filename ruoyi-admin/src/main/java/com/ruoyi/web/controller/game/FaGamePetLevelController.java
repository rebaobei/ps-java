package com.ruoyi.web.controller.game;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGamePetLevel;
import com.ruoyi.system.service.IFaGamePetLevelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物等级Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/level")
public class FaGamePetLevelController extends BaseController
{
    @Autowired
    private IFaGamePetLevelService faGamePetLevelService;

    /**
     * 查询宠物等级列表
     */
    @PreAuthorize("@ss.hasPermi('game:level:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGamePetLevel faGamePetLevel)
    {
        startPage();
        List<FaGamePetLevel> list = faGamePetLevelService.selectFaGamePetLevelList(faGamePetLevel);
        return getDataTable(list);
    }

    /**
     * 导出宠物等级列表
     */
    @PreAuthorize("@ss.hasPermi('game:level:export')")
    @Log(title = "宠物等级", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGamePetLevel faGamePetLevel)
    {
        List<FaGamePetLevel> list = faGamePetLevelService.selectFaGamePetLevelList(faGamePetLevel);
        ExcelUtil<FaGamePetLevel> util = new ExcelUtil<FaGamePetLevel>(FaGamePetLevel.class);
        return util.exportExcel(list, "宠物等级数据");
    }

    /**
     * 获取宠物等级详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:level:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGamePetLevelService.selectFaGamePetLevelById(id));
    }

    /**
     * 新增宠物等级
     */
    @PreAuthorize("@ss.hasPermi('game:level:add')")
    @Log(title = "宠物等级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGamePetLevel faGamePetLevel)
    {
        return toAjax(faGamePetLevelService.insertFaGamePetLevel(faGamePetLevel));
    }

    /**
     * 修改宠物等级
     */
    @PreAuthorize("@ss.hasPermi('game:level:edit')")
    @Log(title = "宠物等级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGamePetLevel faGamePetLevel)
    {
        return toAjax(faGamePetLevelService.updateFaGamePetLevel(faGamePetLevel));
    }

    /**
     * 删除宠物等级
     */
    @PreAuthorize("@ss.hasPermi('game:level:remove')")
    @Log(title = "宠物等级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGamePetLevelService.deleteFaGamePetLevelByIds(ids));
    }

    /**
     * 查询宠物等级列表
     */
    @PreAuthorize("@ss.hasPermi('game:level:list')")
    @GetMapping("/all_list")
    public List<FaGamePetLevel> allList(FaGamePetLevel faGamePetLevel)
    {
        return faGamePetLevelService.selectFaGamePetLevelList(faGamePetLevel);
    }
}
