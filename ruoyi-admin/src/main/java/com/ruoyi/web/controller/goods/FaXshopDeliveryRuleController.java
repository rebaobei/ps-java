package com.ruoyi.web.controller.goods;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopDeliveryRule;
import com.ruoyi.system.service.IFaXshopDeliveryRuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运费规则Controller
 * 
 * @author ruoyi
 * @date 2021-07-20
 */
@RestController
@RequestMapping("/goods/rule")
public class FaXshopDeliveryRuleController extends BaseController
{
    @Autowired
    private IFaXshopDeliveryRuleService faXshopDeliveryRuleService;

    /**
     * 查询运费规则列表
     */
    @PreAuthorize("@ss.hasPermi('goods:rule:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopDeliveryRule faXshopDeliveryRule)
    {
        startPage();
        List<FaXshopDeliveryRule> list = faXshopDeliveryRuleService.selectFaXshopDeliveryRuleList(faXshopDeliveryRule);
        return getDataTable(list);
    }

    /**
     * 导出运费规则列表
     */
    @PreAuthorize("@ss.hasPermi('goods:rule:export')")
    @Log(title = "运费规则", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopDeliveryRule faXshopDeliveryRule)
    {
        List<FaXshopDeliveryRule> list = faXshopDeliveryRuleService.selectFaXshopDeliveryRuleList(faXshopDeliveryRule);
        ExcelUtil<FaXshopDeliveryRule> util = new ExcelUtil<FaXshopDeliveryRule>(FaXshopDeliveryRule.class);
        return util.exportExcel(list, "运费规则数据");
    }

    /**
     * 获取运费规则详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:rule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopDeliveryRuleService.selectFaXshopDeliveryRuleById(id));
    }

    /**
     * 新增运费规则
     */
    @PreAuthorize("@ss.hasPermi('goods:rule:add')")
    @Log(title = "运费规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopDeliveryRule faXshopDeliveryRule)
    {
        return toAjax(faXshopDeliveryRuleService.insertFaXshopDeliveryRule(faXshopDeliveryRule));
    }

    /**
     * 修改运费规则
     */
    @PreAuthorize("@ss.hasPermi('goods:rule:edit')")
    @Log(title = "运费规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopDeliveryRule faXshopDeliveryRule)
    {
        return toAjax(faXshopDeliveryRuleService.updateFaXshopDeliveryRule(faXshopDeliveryRule));
    }

    /**
     * 删除运费规则
     */
    @PreAuthorize("@ss.hasPermi('goods:rule:remove')")
    @Log(title = "运费规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopDeliveryRuleService.deleteFaXshopDeliveryRuleByIds(ids));
    }


    /**
     * 查询运费规则列表
     */
    @PreAuthorize("@ss.hasPermi('goods:rule:list')")
    @GetMapping("/get_list")
    public List<FaXshopDeliveryRule> list(@RequestParam("id") Integer id)
    {
        List<FaXshopDeliveryRule> faXshopDeliveryRules = faXshopDeliveryRuleService.listRuleByTemplateId(id);
        return faXshopDeliveryRules;
    }
}
