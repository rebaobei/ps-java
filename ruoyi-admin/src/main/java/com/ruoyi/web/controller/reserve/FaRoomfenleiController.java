package com.ruoyi.web.controller.reserve;

import java.util.List;

import com.ruoyi.system.domain.vo.RoomCategoryVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaRoomfenlei;
import com.ruoyi.system.service.IFaRoomfenleiService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 房间分类Controller
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@RestController
@RequestMapping("/reserve/roomfenlei")
public class FaRoomfenleiController extends BaseController
{
    @Autowired
    private IFaRoomfenleiService faRoomfenleiService;

    /**
     * 查询房间分类列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaRoomfenlei faRoomfenlei)
    {
        startPage();
        List<FaRoomfenlei> list = faRoomfenleiService.selectFaRoomfenleiList(faRoomfenlei);
        return getDataTable(list);
    }

    /**
     * 导出房间分类列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:export')")
    @Log(title = "房间分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaRoomfenlei faRoomfenlei)
    {
        List<FaRoomfenlei> list = faRoomfenleiService.selectFaRoomfenleiList(faRoomfenlei);
        ExcelUtil<FaRoomfenlei> util = new ExcelUtil<FaRoomfenlei>(FaRoomfenlei.class);
        return util.exportExcel(list, "房间分类数据");
    }

    /**
     * 获取房间分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faRoomfenleiService.selectFaRoomfenleiById(id));
    }

    /**
     * 新增房间分类
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:add')")
    @Log(title = "房间分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaRoomfenlei faRoomfenlei)
    {
        return toAjax(faRoomfenleiService.insertFaRoomfenlei(faRoomfenlei));
    }

    /**
     * 修改房间分类
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:edit')")
    @Log(title = "房间分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaRoomfenlei faRoomfenlei)
    {
        return toAjax(faRoomfenleiService.updateFaRoomfenlei(faRoomfenlei));
    }

    /**
     * 删除房间分类
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:remove')")
    @Log(title = "房间分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faRoomfenleiService.deleteFaRoomfenleiByIds(ids));
    }

    /**
     * 查询房间分类列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:list')")
    @GetMapping("/get_list")
    public TableDataInfo MyList(FaRoomfenlei faRoomfenlei)
    {
        startPage();
        List<RoomCategoryVo> list = faRoomfenleiService.listRoomFenlei(faRoomfenlei);
        return getDataTable(list);
    }


    /**
     * 查询所有房间分类列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:roomfenlei:list')")
    @GetMapping("/all_list")
    public List<FaRoomfenlei> ALlList(FaRoomfenlei faRoomfenlei)
    {
        List<FaRoomfenlei> list = faRoomfenleiService.selectFaRoomfenleiList(faRoomfenlei);
        return list;
    }

}
