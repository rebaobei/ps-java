package com.ruoyi.web.controller.goods;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopDeliveryTpl;
import com.ruoyi.system.service.IFaXshopDeliveryTplService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运费模板Controller
 *
 * @author ruoyi
 * @date 2021-07-19
 */
@RestController
@RequestMapping("/goods/delivery")
public class FaXshopDeliveryTplController extends BaseController {
    @Autowired
    private IFaXshopDeliveryTplService faXshopDeliveryTplService;

    /**
     * 查询运费模板列表
     */
    @PreAuthorize("@ss.hasPermi('goods:delivery:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopDeliveryTpl faXshopDeliveryTpl) {
        startPage();
        List<FaXshopDeliveryTpl> list = faXshopDeliveryTplService.selectFaXshopDeliveryTplList(faXshopDeliveryTpl);
        return getDataTable(list);
    }

    /**
     * 导出运费模板列表
     */
    @PreAuthorize("@ss.hasPermi('goods:delivery:export')")
    @Log(title = "运费模板", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopDeliveryTpl faXshopDeliveryTpl) {
        List<FaXshopDeliveryTpl> list = faXshopDeliveryTplService.selectFaXshopDeliveryTplList(faXshopDeliveryTpl);
        ExcelUtil<FaXshopDeliveryTpl> util = new ExcelUtil<FaXshopDeliveryTpl>(FaXshopDeliveryTpl.class);
        return util.exportExcel(list, "运费模板数据");
    }

    /**
     * 获取运费模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:delivery:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(faXshopDeliveryTplService.selectFaXshopDeliveryTplById(id));
    }

    /**
     * 新增运费模板
     */
    @PreAuthorize("@ss.hasPermi('goods:delivery:add')")
    @Log(title = "运费模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopDeliveryTpl faXshopDeliveryTpl) {
        return toAjax(faXshopDeliveryTplService.insertFaXshopDeliveryTpl(faXshopDeliveryTpl));
    }

    /**
     * 修改运费模板
     */
    @PreAuthorize("@ss.hasPermi('goods:delivery:edit')")
    @Log(title = "运费模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopDeliveryTpl faXshopDeliveryTpl) {
        return toAjax(faXshopDeliveryTplService.updateFaXshopDeliveryTpl(faXshopDeliveryTpl));
    }

    /**
     * 删除运费模板
     */
    @PreAuthorize("@ss.hasPermi('goods:delivery:remove')")
    @Log(title = "运费模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(faXshopDeliveryTplService.deleteFaXshopDeliveryTplByIds(ids));
    }

    /**
     * 查询所有运费模板列表
     */
    @PreAuthorize("@ss.hasPermi('goods:delivery:list')")
    @GetMapping("/all_list")
    public List<FaXshopDeliveryTpl> allList(FaXshopDeliveryTpl faXshopDeliveryTpl) {
        return faXshopDeliveryTplService.selectFaXshopDeliveryTplList(faXshopDeliveryTpl);
    }
}
