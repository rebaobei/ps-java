package com.ruoyi.web.controller.store;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.FaStoreInfo;
import com.ruoyi.system.service.IFaStoreInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStoreBrand;
import com.ruoyi.system.service.IFaStoreBrandService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 品牌信息Controller
 *
 * @author ruoyi
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/storeinfo/brand")
public class FaStoreBrandController extends BaseController
{
    @Autowired
    private IFaStoreBrandService faStoreBrandService;
    @Autowired
    private IFaStoreInfoService faStoreInfoService;
    /**
     * 查询品牌信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FaStoreBrand faStoreBrand)
    {
        startPage();
        List<FaStoreBrand> list = faStoreBrandService.selectFaStoreBrandList(faStoreBrand);
        return getDataTable(list);
    }
    @GetMapping("/myList")
    public TableDataInfo myList(FaStoreBrand faStoreBrand)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        FaStoreInfo faStoreInfo = faStoreInfoService.selectFaStoreInfoById(storeInfoId);
        Long brandId = faStoreInfo.getBrandId();
        faStoreBrand.setId(brandId);
        List<FaStoreBrand> list = new ArrayList<>();
        FaStoreBrand faStoreBrand1 = faStoreBrandService.selectFaStoreBrandById(brandId);
        if(faStoreBrand1!=null){
            list.add(faStoreBrand1);
        }
        return getDataTable(list);
    }

    /**
     * 导出品牌信息列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:brand:export')")
    @Log(title = "品牌信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreBrand faStoreBrand)
    {
        List<FaStoreBrand> list = faStoreBrandService.selectFaStoreBrandList(faStoreBrand);
        ExcelUtil<FaStoreBrand> util = new ExcelUtil<FaStoreBrand>(FaStoreBrand.class);
        return util.exportExcel(list, "品牌信息数据");
    }

    /**
     * 获取品牌信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreBrandService.selectFaStoreBrandById(id));
    }

    /**
     * 新增品牌信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:brand:add')")
    @Log(title = "品牌信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreBrand faStoreBrand)
    {
        return toAjax(faStoreBrandService.insertFaStoreBrand(faStoreBrand));
    }

    /**
     * 修改品牌信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:brand:edit')")
    @Log(title = "品牌信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreBrand faStoreBrand)
    {
        return toAjax(faStoreBrandService.updateFaStoreBrand(faStoreBrand));
    }

    /**
     * 删除品牌信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:brand:remove')")
    @Log(title = "品牌信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreBrandService.deleteFaStoreBrandByIds(ids));
    }
}
