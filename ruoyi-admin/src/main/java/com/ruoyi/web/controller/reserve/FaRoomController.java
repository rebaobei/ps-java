package com.ruoyi.web.controller.reserve;

import java.util.List;

import com.ruoyi.system.domain.vo.RoomVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaRoom;
import com.ruoyi.system.service.IFaRoomService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 房间管理Controller
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@RestController
@RequestMapping("/reserve/room")
public class FaRoomController extends BaseController
{
    @Autowired
    private IFaRoomService faRoomService;

    /**
     * 查询房间管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:room:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaRoom faRoom)
    {
        startPage();
        List<FaRoom> list = faRoomService.selectFaRoomList(faRoom);
        return getDataTable(list);
    }

    /**
     * 导出房间管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:room:export')")
    @Log(title = "房间管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaRoom faRoom)
    {
        List<FaRoom> list = faRoomService.selectFaRoomList(faRoom);
        ExcelUtil<FaRoom> util = new ExcelUtil<FaRoom>(FaRoom.class);
        return util.exportExcel(list, "房间管理数据");
    }

    /**
     * 获取房间管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:room:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faRoomService.selectFaRoomById(id));
    }

    /**
     * 新增房间管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:room:add')")
    @Log(title = "房间管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaRoom faRoom)
    {
        return toAjax(faRoomService.insertFaRoom(faRoom));
    }

    /**
     * 修改房间管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:room:edit')")
    @Log(title = "房间管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaRoom faRoom)
    {
        return toAjax(faRoomService.updateFaRoom(faRoom));
    }

    /**
     * 删除房间管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:room:remove')")
    @Log(title = "房间管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faRoomService.deleteFaRoomByIds(ids));
    }


    /**
     * 查询房间管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:room:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaRoom faRoom)
    {
        startPage();
        List<RoomVo> list = faRoomService.listRoomVo(faRoom);
        return getDataTable(list);
    }

}
