package com.ruoyi.web.controller.article;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaCmsFlag;
import com.ruoyi.system.service.IFaCmsFlagService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文章标志Controller
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
@RestController
@RequestMapping("/article/articleflag")
public class FaCmsFlagController extends BaseController
{
    @Autowired
    private IFaCmsFlagService faCmsFlagService;

    /**
     * 查询文章标志列表
     */
    @PreAuthorize("@ss.hasPermi('article:articleflag:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaCmsFlag faCmsFlag)
    {
        startPage();
        List<FaCmsFlag> list = faCmsFlagService.selectFaCmsFlagList(faCmsFlag);
        return getDataTable(list);
    }

    /**
     * 导出文章标志列表
     */
    @PreAuthorize("@ss.hasPermi('article:articleflag:export')")
    @Log(title = "文章标志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaCmsFlag faCmsFlag)
    {
        List<FaCmsFlag> list = faCmsFlagService.selectFaCmsFlagList(faCmsFlag);
        ExcelUtil<FaCmsFlag> util = new ExcelUtil<FaCmsFlag>(FaCmsFlag.class);
        return util.exportExcel(list, "文章标志数据");
    }

    /**
     * 获取文章标志详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:articleflag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faCmsFlagService.selectFaCmsFlagById(id));
    }

    /**
     * 新增文章标志
     */
    @PreAuthorize("@ss.hasPermi('article:articleflag:add')")
    @Log(title = "文章标志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaCmsFlag faCmsFlag)
    {
        return toAjax(faCmsFlagService.insertFaCmsFlag(faCmsFlag));
    }

    /**
     * 修改文章标志
     */
    @PreAuthorize("@ss.hasPermi('article:articleflag:edit')")
    @Log(title = "文章标志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaCmsFlag faCmsFlag)
    {
        return toAjax(faCmsFlagService.updateFaCmsFlag(faCmsFlag));
    }

    /**
     * 删除文章标志
     */
    @PreAuthorize("@ss.hasPermi('article:articleflag:remove')")
    @Log(title = "文章标志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faCmsFlagService.deleteFaCmsFlagByIds(ids));
    }


    /**
     * 查询所有文章标志列表
     */
    @PreAuthorize("@ss.hasPermi('article:articleflag:list')")
    @GetMapping("/all_list")
    public List<FaCmsFlag> allList(FaCmsFlag faCmsFlag)
    {
        return faCmsFlagService.selectFaCmsFlagList(faCmsFlag);
    }
}
