package com.ruoyi.web.controller.store;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStoreIssue;
import com.ruoyi.system.service.IFaStoreIssueService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺问题Controller
 *
 * @author ruoyi
 * @date 2021-11-21
 */
@RestController
@RequestMapping("/storeinfo/issue")
public class FaStoreIssueController extends BaseController
{
    @Autowired
    private IFaStoreIssueService faStoreIssueService;

    /**
     * 查询店铺问题列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:issue:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStoreIssue faStoreIssue)
    {
        startPage();
        List<FaStoreIssue> list = faStoreIssueService.selectFaStoreIssueList(faStoreIssue);
        return getDataTable(list);
    }
    /**
     * 查询店铺问题列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:issue:list')")
    @GetMapping("/myList")
    public TableDataInfo myList(FaStoreIssue faStoreIssue)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreIssue.setStoreId(storeInfoId);
        startPage();
        List<FaStoreIssue> list = faStoreIssueService.selectFaStoreIssueList(faStoreIssue);
        return getDataTable(list);
    }


    /**
     * 导出店铺问题列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:issue:export')")
    @Log(title = "店铺问题", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreIssue faStoreIssue)
    {
        List<FaStoreIssue> list = faStoreIssueService.selectFaStoreIssueList(faStoreIssue);
        ExcelUtil<FaStoreIssue> util = new ExcelUtil<FaStoreIssue>(FaStoreIssue.class);
        return util.exportExcel(list, "店铺问题数据");
    }

    /**
     * 获取店铺问题详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:issue:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreIssueService.selectFaStoreIssueById(id));
    }

    /**
     * 新增店铺问题
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:issue:add')")
    @Log(title = "店铺问题", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreIssue faStoreIssue)
    {
        faStoreIssue.setStatus(0L);
        faStoreIssue.setLikes(0L);
        return toAjax(faStoreIssueService.insertFaStoreIssue(faStoreIssue));
    }

    /**
     * 修改店铺问题
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:issue:edit')")
    @Log(title = "店铺问题", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreIssue faStoreIssue)
    {
        return toAjax(faStoreIssueService.updateFaStoreIssue(faStoreIssue));
    }

    /**
     * 删除店铺问题
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:issue:remove')")
    @Log(title = "店铺问题", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreIssueService.deleteFaStoreIssueByIds(ids));
    }
}
