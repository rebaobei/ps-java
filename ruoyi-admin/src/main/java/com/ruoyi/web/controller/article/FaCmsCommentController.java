package com.ruoyi.web.controller.article;

import java.util.List;

import com.ruoyi.system.domain.vo.CmsCommentVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaCmsComment;
import com.ruoyi.system.service.IFaCmsCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 评论管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
@RestController
@RequestMapping("/article/articlecomment")
public class FaCmsCommentController extends BaseController
{
    @Autowired
    private IFaCmsCommentService faCmsCommentService;

    /**
     * 查询评论管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:articlecomment:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaCmsComment faCmsComment)
    {
        startPage();
        List<FaCmsComment> list = faCmsCommentService.selectFaCmsCommentList(faCmsComment);
        return getDataTable(list);
    }

    /**
     * 导出评论管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:articlecomment:export')")
    @Log(title = "评论管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaCmsComment faCmsComment)
    {
        List<FaCmsComment> list = faCmsCommentService.selectFaCmsCommentList(faCmsComment);
        ExcelUtil<FaCmsComment> util = new ExcelUtil<FaCmsComment>(FaCmsComment.class);
        return util.exportExcel(list, "评论管理数据");
    }

    /**
     * 获取评论管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:articlecomment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faCmsCommentService.selectFaCmsCommentById(id));
    }

    /**
     * 新增评论管理
     */
    @PreAuthorize("@ss.hasPermi('article:articlecomment:add')")
    @Log(title = "评论管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaCmsComment faCmsComment)
    {
        return toAjax(faCmsCommentService.insertFaCmsComment(faCmsComment));
    }

    /**
     * 修改评论管理
     */
    @PreAuthorize("@ss.hasPermi('article:articlecomment:edit')")
    @Log(title = "评论管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaCmsComment faCmsComment)
    {
        return toAjax(faCmsCommentService.updateFaCmsComment(faCmsComment));
    }

    /**
     * 删除评论管理
     */
    @PreAuthorize("@ss.hasPermi('article:articlecomment:remove')")
    @Log(title = "评论管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faCmsCommentService.deleteFaCmsCommentByIds(ids));
    }

    /**
     * 查询评论管理列表
     */
    @PreAuthorize("@ss.hasPermi('article:articlecomment:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(CmsCommentVo faCmsComment)
    {
        startPage();
        List<CmsCommentVo> list = faCmsCommentService.listCmsCommentVo(faCmsComment);
        return getDataTable(list);
    }
}
