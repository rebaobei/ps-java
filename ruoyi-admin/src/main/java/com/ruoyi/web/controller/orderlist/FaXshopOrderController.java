package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.XshopOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopOrder;
import com.ruoyi.system.service.IFaXshopOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商城订单Controller
 *
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/mallorder")
public class FaXshopOrderController extends BaseController
{
    @Autowired
    private IFaXshopOrderService faXshopOrderService;

    /**
     * 查询商城订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopOrder faXshopOrder)
    {
        startPage();
        List<FaXshopOrder> list = faXshopOrderService.selectFaXshopOrderList(faXshopOrder);
        return getDataTable(list);
    }

    /**
     * 导出商城订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:export')")
    @Log(title = "商城订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopOrder faXshopOrder)
    {
        List<FaXshopOrder> list = faXshopOrderService.selectFaXshopOrderList(faXshopOrder);
        ExcelUtil<FaXshopOrder> util = new ExcelUtil<FaXshopOrder>(FaXshopOrder.class);
        return util.exportExcel(list, "商城订单数据");
    }

    /**
     * 获取商城订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopOrderService.selectFaXshopOrderById(id));
    }

    /**
     * 新增商城订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:add')")
    @Log(title = "商城订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopOrder faXshopOrder)
    {
        return toAjax(faXshopOrderService.insertFaXshopOrder(faXshopOrder));
    }

    /**
     * 修改商城订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:edit')")
    @Log(title = "商城订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopOrder faXshopOrder)
    {
        return toAjax(faXshopOrderService.updateFaXshopOrder(faXshopOrder));
    }

    /**
     * 删除商城订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:remove')")
    @Log(title = "商城订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopOrderService.deleteFaXshopOrderByIds(ids));
    }

    /**
     * 查询商城订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(XshopOrderVo faXshopOrder)
    {
        startPage();
        List<XshopOrderVo> list = faXshopOrderService.listXshopOrderVo(faXshopOrder);
        return getDataTable(list);
    }

    /**
     * 发货
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:edit')")
    @Log(title = "发货", businessType = BusinessType.UPDATE)
    @PutMapping("/send")
    public AjaxResult sendProduct(@RequestBody FaXshopOrder faXshopOrder)
    {
        faXshopOrder.setIsDelivery(1);
        faXshopOrder.setDelivery(Integer.valueOf(String.valueOf(System.currentTimeMillis()/1000)));
        return toAjax(faXshopOrderService.updateFaXshopOrder(faXshopOrder));
    }

    /**
     * 退款
     */
    @PreAuthorize("@ss.hasPermi('orderlist:mallorder:edit')")
    @Log(title = "退款", businessType = BusinessType.UPDATE)
    @GetMapping("/refund")
    public AjaxResult refundMoney(@RequestParam("id") String orderId){
        return AjaxResult.success(faXshopOrderService.refundMoney(orderId));
    }

}
