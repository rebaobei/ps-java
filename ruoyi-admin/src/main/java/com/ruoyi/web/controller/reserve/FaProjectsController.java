package com.ruoyi.web.controller.reserve;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.ProjectVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaProjects;
import com.ruoyi.system.service.IFaProjectsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 预约项目Controller
 *
 * @author ruoyi
 * @date 2021-07-26
 */
@RestController
@RequestMapping("/reserve/project")
public class FaProjectsController extends BaseController
{
    @Autowired
    private IFaProjectsService faProjectsService;

    /**
     * 查询预约项目列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaProjects faProjects)
    {
        startPage();
        List<FaProjects> list = faProjectsService.selectFaProjectsList(faProjects);
        return getDataTable(list);
    }

    /**
     * 导出预约项目列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:export')")
    @Log(title = "预约项目", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaProjects faProjects)
    {
        List<FaProjects> list = faProjectsService.selectFaProjectsList(faProjects);
        ExcelUtil<FaProjects> util = new ExcelUtil<FaProjects>(FaProjects.class);
        return util.exportExcel(list, "预约项目数据");
    }

    /**
     * 获取预约项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faProjectsService.selectFaProjectsById(id));
    }

    /**
     * 新增预约项目
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:add')")
    @Log(title = "预约项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaProjects faProjects)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faProjects.setStoreInfoId(storeInfoId);
        return toAjax(faProjectsService.insertFaProjects(faProjects));
    }

    /**
     * 修改预约项目
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:edit')")
    @Log(title = "预约项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaProjects faProjects)
    {
        return faProjectsService.updateFaProjects(faProjects);
    }

    /**
     * 删除预约项目
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:remove')")
    @Log(title = "预约项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faProjectsService.deleteFaProjectsByIds(ids));
    }

    /**
     * 所有 预约项目列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:list')")
    @GetMapping("/all_list")
    public List<FaProjects> allList()
    {
        return faProjectsService.list();
    }

    /**
     * 查询预约项目列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaProjects faProjects)
    {
        startPage();
        List<ProjectVo> resultList = faProjectsService.listProjectVo(faProjects);
        return getDataTable(resultList);
    }

    /**
     * 查询预约项目列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:list')")
    @GetMapping("/getMyListProject")
    public TableDataInfo getMyListProject(FaProjects faProjects)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faProjects.setStoreInfoId(storeInfoId);
        startPage();
        List<ProjectVo> resultList = faProjectsService.listProjectVo(faProjects);
        return getDataTable(resultList);
    }

    /**
     * 查询所有项目列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:list')")
    @GetMapping("/all_select_list")
    public List<Map<String,Object>> all_select_list() {
        return faProjectsService.listProjectVoForSelect();
    }

}
