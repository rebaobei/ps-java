package com.ruoyi.web.controller.mobile;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopNav;
import com.ruoyi.system.service.IFaXshopNavService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 首页设置Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/mobile/nav")
public class FaXshopNavController extends BaseController
{
    @Autowired
    private IFaXshopNavService faXshopNavService;

    /**
     * 查询首页设置列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:nav:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopNav faXshopNav)
    {
        startPage();
        List<FaXshopNav> list = faXshopNavService.selectFaXshopNavList(faXshopNav);
        return getDataTable(list);
    }

    /**
     * 导出首页设置列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:nav:export')")
    @Log(title = "首页设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopNav faXshopNav)
    {
        List<FaXshopNav> list = faXshopNavService.selectFaXshopNavList(faXshopNav);
        ExcelUtil<FaXshopNav> util = new ExcelUtil<FaXshopNav>(FaXshopNav.class);
        return util.exportExcel(list, "首页设置数据");
    }

    /**
     * 获取首页设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('mobile:nav:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopNavService.selectFaXshopNavById(id));
    }

    /**
     * 新增首页设置
     */
    @PreAuthorize("@ss.hasPermi('mobile:nav:add')")
    @Log(title = "首页设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopNav faXshopNav)
    {
        return toAjax(faXshopNavService.insertFaXshopNav(faXshopNav));
    }

    /**
     * 修改首页设置
     */
    @PreAuthorize("@ss.hasPermi('mobile:nav:edit')")
    @Log(title = "首页设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopNav faXshopNav)
    {
        return toAjax(faXshopNavService.updateFaXshopNav(faXshopNav));
    }

    /**
     * 删除首页设置
     */
    @PreAuthorize("@ss.hasPermi('mobile:nav:remove')")
    @Log(title = "首页设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopNavService.deleteFaXshopNavByIds(ids));
    }
}
