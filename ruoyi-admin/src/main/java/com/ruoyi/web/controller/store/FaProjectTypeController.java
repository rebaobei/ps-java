package com.ruoyi.web.controller.store;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaProjectType;
import com.ruoyi.system.service.IFaProjectTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺服务分类Controller
 *
 * @author ruoyi
 * @date 2021-11-13
 */
@RestController
@RequestMapping("/storeinfo/project/type")
public class FaProjectTypeController extends BaseController
{
    @Autowired
    private IFaProjectTypeService faProjectTypeService;

    /**
     * 查询店铺服务分类列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:projecttype:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaProjectType faProjectType)
    {
        startPage();
        List<FaProjectType> list = faProjectTypeService.selectFaProjectTypeList(faProjectType);
        return getDataTable(list);
    }
    /**
     * 查询店铺服务分类列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:projecttype:list')")
    @GetMapping("/myList")
    public TableDataInfo myList(FaProjectType faProjectType)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faProjectType.setStoreId(storeInfoId);
        startPage();
        List<FaProjectType> list = faProjectTypeService.selectFaProjectTypeList(faProjectType);
        return getDataTable(list);
    }
    /**
     * 导出店铺服务分类列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:projecttype:export')")
    @Log(title = "店铺服务分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaProjectType faProjectType)
    {
        List<FaProjectType> list = faProjectTypeService.selectFaProjectTypeList(faProjectType);
        ExcelUtil<FaProjectType> util = new ExcelUtil<FaProjectType>(FaProjectType.class);
        return util.exportExcel(list, "店铺服务分类数据");
    }

    /**
     * 获取店铺服务分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:projecttype:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id,String storeId)
    {
        return AjaxResult.success(faProjectTypeService.selectFaProjectTypeById(id,storeId));
    }

    /**
     * 新增店铺服务分类
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:projecttype:add')")
    @Log(title = "店铺服务分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaProjectType faProjectType)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faProjectType.setStoreId(storeInfoId);
        return toAjax(faProjectTypeService.insertFaProjectType(faProjectType));
    }

    /**
     * 修改店铺服务分类
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:projecttype:edit')")
    @Log(title = "店铺服务分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaProjectType faProjectType)
    {
        return toAjax(faProjectTypeService.updateFaProjectType(faProjectType));
    }

    /**
     * 删除店铺服务分类
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:projecttype:remove')")
    @Log(title = "店铺服务分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faProjectTypeService.deleteFaProjectTypeByIds(ids));
    }
    /**
     * 查询此店铺所有项目分类
     */
    @PreAuthorize("@ss.hasPermi('reserve:project:list')")
    @GetMapping("/listForSelect")
    public List<SysDictData> listForSelect() {
        return faProjectTypeService.listForSelect();
    }
}
