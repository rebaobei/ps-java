package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.BuyLevelOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBuylevelorder;
import com.ruoyi.system.service.IFaBuylevelorderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 购买等级Controller
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/buylevelorder")
public class FaBuylevelorderController extends BaseController
{
    @Autowired
    private IFaBuylevelorderService faBuylevelorderService;

    /**
     * 查询购买等级列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:buylevelorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBuylevelorder faBuylevelorder)
    {
        startPage();
        List<FaBuylevelorder> list = faBuylevelorderService.selectFaBuylevelorderList(faBuylevelorder);
        return getDataTable(list);
    }

    /**
     * 导出购买等级列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:buylevelorder:export')")
    @Log(title = "购买等级", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBuylevelorder faBuylevelorder)
    {
        List<FaBuylevelorder> list = faBuylevelorderService.selectFaBuylevelorderList(faBuylevelorder);
        ExcelUtil<FaBuylevelorder> util = new ExcelUtil<FaBuylevelorder>(FaBuylevelorder.class);
        return util.exportExcel(list, "购买等级数据");
    }

    /**
     * 获取购买等级详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:buylevelorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faBuylevelorderService.selectFaBuylevelorderById(id));
    }

    /**
     * 新增购买等级
     */
    @PreAuthorize("@ss.hasPermi('orderlist:buylevelorder:add')")
    @Log(title = "购买等级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBuylevelorder faBuylevelorder)
    {
        return toAjax(faBuylevelorderService.insertFaBuylevelorder(faBuylevelorder));
    }

    /**
     * 修改购买等级
     */
    @PreAuthorize("@ss.hasPermi('orderlist:buylevelorder:edit')")
    @Log(title = "购买等级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBuylevelorder faBuylevelorder)
    {
        return toAjax(faBuylevelorderService.updateFaBuylevelorder(faBuylevelorder));
    }

    /**
     * 删除购买等级
     */
    @PreAuthorize("@ss.hasPermi('orderlist:buylevelorder:remove')")
    @Log(title = "购买等级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faBuylevelorderService.deleteFaBuylevelorderByIds(ids));
    }

    /**
     * 查询购买等级列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:buylevelorder:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(BuyLevelOrderVo faBuylevelorder)
    {
        startPage();
        List<BuyLevelOrderVo> list = faBuylevelorderService.listBuyLevelOrderVo(faBuylevelorder);
        return getDataTable(list);
    }
}
