package com.ruoyi.web.controller.rules;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopArticle;
import com.ruoyi.system.service.IFaXshopArticleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 规则列表Controller
 * 
 * @author zzp
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/rules/rules")
public class FaXshopArticleController extends BaseController
{
    @Autowired
    private IFaXshopArticleService faXshopArticleService;

    /**
     * 查询规则列表列表
     */
    @PreAuthorize("@ss.hasPermi('rules:rules:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopArticle faXshopArticle)
    {
        startPage();
        List<FaXshopArticle> list = faXshopArticleService.selectFaXshopArticleList(faXshopArticle);
        return getDataTable(list);
    }

    /**
     * 导出规则列表列表
     */
    @PreAuthorize("@ss.hasPermi('rules:rules:export')")
    @Log(title = "规则列表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopArticle faXshopArticle)
    {
        List<FaXshopArticle> list = faXshopArticleService.selectFaXshopArticleList(faXshopArticle);
        ExcelUtil<FaXshopArticle> util = new ExcelUtil<FaXshopArticle>(FaXshopArticle.class);
        return util.exportExcel(list, "规则列表数据");
    }

    /**
     * 获取规则列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('rules:rules:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faXshopArticleService.selectFaXshopArticleById(id));
    }

    /**
     * 新增规则列表
     */
    @PreAuthorize("@ss.hasPermi('rules:rules:add')")
    @Log(title = "规则列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopArticle faXshopArticle)
    {
        return toAjax(faXshopArticleService.insertFaXshopArticle(faXshopArticle));
    }

    /**
     * 修改规则列表
     */
    @PreAuthorize("@ss.hasPermi('rules:rules:edit')")
    @Log(title = "规则列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopArticle faXshopArticle)
    {
        return toAjax(faXshopArticleService.updateFaXshopArticle(faXshopArticle));
    }

    /**
     * 删除规则列表
     */
    @PreAuthorize("@ss.hasPermi('rules:rules:remove')")
    @Log(title = "规则列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faXshopArticleService.deleteFaXshopArticleByIds(ids));
    }
}
