package com.ruoyi.web.controller.orderlist;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaYuyueOrder;
import com.ruoyi.system.service.IFaYuyueOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 预约订单Controller
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/yuyueorder")
public class FaYuyueOrderController extends BaseController
{
    @Autowired
    private IFaYuyueOrderService faYuyueOrderService;

    /**
     * 查询预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:yuyueorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaYuyueOrder faYuyueOrder)
    {
        startPage();
        List<FaYuyueOrder> list = faYuyueOrderService.selectFaYuyueOrderList(faYuyueOrder);
        return getDataTable(list);
    }

    /**
     * 导出预约订单列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:yuyueorder:export')")
    @Log(title = "预约订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaYuyueOrder faYuyueOrder)
    {
        List<FaYuyueOrder> list = faYuyueOrderService.selectFaYuyueOrderList(faYuyueOrder);
        ExcelUtil<FaYuyueOrder> util = new ExcelUtil<FaYuyueOrder>(FaYuyueOrder.class);
        return util.exportExcel(list, "预约订单数据");
    }

    /**
     * 获取预约订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:yuyueorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faYuyueOrderService.selectFaYuyueOrderById(id));
    }

    /**
     * 新增预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:yuyueorder:add')")
    @Log(title = "预约订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaYuyueOrder faYuyueOrder)
    {
        return toAjax(faYuyueOrderService.insertFaYuyueOrder(faYuyueOrder));
    }

    /**
     * 修改预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:yuyueorder:edit')")
    @Log(title = "预约订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaYuyueOrder faYuyueOrder)
    {
        return toAjax(faYuyueOrderService.updateFaYuyueOrder(faYuyueOrder));
    }

    /**
     * 删除预约订单
     */
    @PreAuthorize("@ss.hasPermi('orderlist:yuyueorder:remove')")
    @Log(title = "预约订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faYuyueOrderService.deleteFaYuyueOrderByIds(ids));
    }
}
