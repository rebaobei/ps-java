package com.ruoyi.web.controller.member;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserBaoming;
import com.ruoyi.system.service.IFaUserBaomingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动报名Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/baoming")
public class FaUserBaomingController extends BaseController
{
    @Autowired
    private IFaUserBaomingService faUserBaomingService;

    /**
     * 查询活动报名列表
     */
    @PreAuthorize("@ss.hasPermi('member:baoming:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserBaoming faUserBaoming)
    {
        startPage();
        List<FaUserBaoming> list = faUserBaomingService.selectFaUserBaomingList(faUserBaoming);
        return getDataTable(list);
    }

    /**
     * 导出活动报名列表
     */
    @PreAuthorize("@ss.hasPermi('member:baoming:export')")
    @Log(title = "活动报名", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserBaoming faUserBaoming)
    {
        List<FaUserBaoming> list = faUserBaomingService.selectFaUserBaomingList(faUserBaoming);
        ExcelUtil<FaUserBaoming> util = new ExcelUtil<FaUserBaoming>(FaUserBaoming.class);
        return util.exportExcel(list, "活动报名数据");
    }

    /**
     * 获取活动报名详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:baoming:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faUserBaomingService.selectFaUserBaomingById(id));
    }

    /**
     * 新增活动报名
     */
    @PreAuthorize("@ss.hasPermi('member:baoming:add')")
    @Log(title = "活动报名", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserBaoming faUserBaoming)
    {
        return toAjax(faUserBaomingService.insertFaUserBaoming(faUserBaoming));
    }

    /**
     * 修改活动报名
     */
    @PreAuthorize("@ss.hasPermi('member:baoming:edit')")
    @Log(title = "活动报名", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserBaoming faUserBaoming)
    {
        return toAjax(faUserBaomingService.updateFaUserBaoming(faUserBaoming));
    }

    /**
     * 删除活动报名
     */
    @PreAuthorize("@ss.hasPermi('member:baoming:remove')")
    @Log(title = "活动报名", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faUserBaomingService.deleteFaUserBaomingByIds(ids));
    }
}
