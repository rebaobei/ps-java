package com.ruoyi.web.controller.game;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameThingsCategory;
import com.ruoyi.system.service.IFaGameThingsCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物品分类Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/thingscategory")
public class FaGameThingsCategoryController extends BaseController
{
    @Autowired
    private IFaGameThingsCategoryService faGameThingsCategoryService;

    /**
     * 查询物品分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:thingscategory:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameThingsCategory faGameThingsCategory)
    {
        startPage();
        List<FaGameThingsCategory> list = faGameThingsCategoryService.selectFaGameThingsCategoryList(faGameThingsCategory);
        return getDataTable(list);
    }

    /**
     * 导出物品分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:thingscategory:export')")
    @Log(title = "物品分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameThingsCategory faGameThingsCategory)
    {
        List<FaGameThingsCategory> list = faGameThingsCategoryService.selectFaGameThingsCategoryList(faGameThingsCategory);
        ExcelUtil<FaGameThingsCategory> util = new ExcelUtil<FaGameThingsCategory>(FaGameThingsCategory.class);
        return util.exportExcel(list, "物品分类数据");
    }

    /**
     * 获取物品分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:thingscategory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGameThingsCategoryService.selectFaGameThingsCategoryById(id));
    }

    /**
     * 新增物品分类
     */
    @PreAuthorize("@ss.hasPermi('game:thingscategory:add')")
    @Log(title = "物品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameThingsCategory faGameThingsCategory)
    {
        return toAjax(faGameThingsCategoryService.insertFaGameThingsCategory(faGameThingsCategory));
    }

    /**
     * 修改物品分类
     */
    @PreAuthorize("@ss.hasPermi('game:thingscategory:edit')")
    @Log(title = "物品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameThingsCategory faGameThingsCategory)
    {
        return toAjax(faGameThingsCategoryService.updateFaGameThingsCategory(faGameThingsCategory));
    }

    /**
     * 删除物品分类
     */
    @PreAuthorize("@ss.hasPermi('game:thingscategory:remove')")
    @Log(title = "物品分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGameThingsCategoryService.deleteFaGameThingsCategoryByIds(ids));
    }

    /**
     * 查询物品分类列表
     */
    @PreAuthorize("@ss.hasPermi('game:thingscategory:list')")
    @GetMapping("/all_list")
    public List<FaGameThingsCategory> allList(FaGameThingsCategory faGameThingsCategory)
    {
        return faGameThingsCategoryService.selectFaGameThingsCategoryList(faGameThingsCategory);
    }

}
