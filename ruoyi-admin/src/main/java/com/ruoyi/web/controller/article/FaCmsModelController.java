package com.ruoyi.web.controller.article;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaCmsModel;
import com.ruoyi.system.service.IFaCmsModelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 内容模型Controller
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
@RestController
@RequestMapping("/article/model")
public class FaCmsModelController extends BaseController
{
    @Autowired
    private IFaCmsModelService faCmsModelService;

    /**
     * 查询内容模型列表
     */
    @PreAuthorize("@ss.hasPermi('article:model:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaCmsModel faCmsModel)
    {
        startPage();
        List<FaCmsModel> list = faCmsModelService.selectFaCmsModelList(faCmsModel);
        return getDataTable(list);
    }

    /**
     * 导出内容模型列表
     */
    @PreAuthorize("@ss.hasPermi('article:model:export')")
    @Log(title = "内容模型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaCmsModel faCmsModel)
    {
        List<FaCmsModel> list = faCmsModelService.selectFaCmsModelList(faCmsModel);
        ExcelUtil<FaCmsModel> util = new ExcelUtil<FaCmsModel>(FaCmsModel.class);
        return util.exportExcel(list, "内容模型数据");
    }

    /**
     * 获取内容模型详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:model:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faCmsModelService.selectFaCmsModelById(id));
    }

    /**
     * 新增内容模型
     */
    @PreAuthorize("@ss.hasPermi('article:model:add')")
    @Log(title = "内容模型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaCmsModel faCmsModel)
    {
        return toAjax(faCmsModelService.insertFaCmsModel(faCmsModel));
    }

    /**
     * 修改内容模型
     */
    @PreAuthorize("@ss.hasPermi('article:model:edit')")
    @Log(title = "内容模型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaCmsModel faCmsModel)
    {
        return toAjax(faCmsModelService.updateFaCmsModel(faCmsModel));
    }

    /**
     * 删除内容模型
     */
    @PreAuthorize("@ss.hasPermi('article:model:remove')")
    @Log(title = "内容模型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faCmsModelService.deleteFaCmsModelByIds(ids));
    }

    /**
     * 查询内容模型列表
     */
    @PreAuthorize("@ss.hasPermi('article:model:list')")
    @GetMapping("/all_list")
    public List<FaCmsModel> allList(FaCmsModel faCmsModel)
    {
        return faCmsModelService.selectFaCmsModelList(faCmsModel);
    }
}
