package com.ruoyi.web.controller.mobile;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaVersion;
import com.ruoyi.system.service.IFaVersionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * APP版本管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/mobile/version")
public class FaVersionController extends BaseController
{
    @Autowired
    private IFaVersionService faVersionService;

    /**
     * 查询APP版本管理列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:version:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaVersion faVersion)
    {
        startPage();
        List<FaVersion> list = faVersionService.selectFaVersionList(faVersion);
        return getDataTable(list);
    }

    /**
     * 导出APP版本管理列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:version:export')")
    @Log(title = "APP版本管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaVersion faVersion)
    {
        List<FaVersion> list = faVersionService.selectFaVersionList(faVersion);
        ExcelUtil<FaVersion> util = new ExcelUtil<FaVersion>(FaVersion.class);
        return util.exportExcel(list, "APP版本管理数据");
    }

    /**
     * 获取APP版本管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mobile:version:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faVersionService.selectFaVersionById(id));
    }

    /**
     * 新增APP版本管理
     */
    @PreAuthorize("@ss.hasPermi('mobile:version:add')")
    @Log(title = "APP版本管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaVersion faVersion)
    {
        return toAjax(faVersionService.insertFaVersion(faVersion));
    }

    /**
     * 修改APP版本管理
     */
    @PreAuthorize("@ss.hasPermi('mobile:version:edit')")
    @Log(title = "APP版本管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaVersion faVersion)
    {
        return toAjax(faVersionService.updateFaVersion(faVersion));
    }

    /**
     * 删除APP版本管理
     */
    @PreAuthorize("@ss.hasPermi('mobile:version:remove')")
    @Log(title = "APP版本管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faVersionService.deleteFaVersionByIds(ids));
    }
}
