package com.ruoyi.web.controller.reserve;

import java.util.List;

import com.ruoyi.system.domain.vo.PetCategoryVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaTechnicians;
import com.ruoyi.system.service.IFaTechniciansService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物分类Controller
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@RestController
@RequestMapping("/reserve/pet_catogory")
public class FaTechniciansController extends BaseController
{
    @Autowired
    private IFaTechniciansService faTechniciansService;

    /**
     * 查询宠物分类列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaTechnicians faTechnicians)
    {
        startPage();
        List<FaTechnicians> list = faTechniciansService.selectFaTechniciansList(faTechnicians);
        return getDataTable(list);
    }

    /**
     * 导出宠物分类列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:export')")
    @Log(title = "宠物分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaTechnicians faTechnicians)
    {
        List<FaTechnicians> list = faTechniciansService.selectFaTechniciansList(faTechnicians);
        ExcelUtil<FaTechnicians> util = new ExcelUtil<FaTechnicians>(FaTechnicians.class);
        return util.exportExcel(list, "宠物分类数据");
    }

    /**
     * 获取宠物分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faTechniciansService.selectFaTechniciansById(id));
    }

    /**
     * 新增宠物分类
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:add')")
    @Log(title = "宠物分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaTechnicians faTechnicians)
    {
        return toAjax(faTechniciansService.insertFaTechnicians(faTechnicians));
    }

    /**
     * 修改宠物分类
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:edit')")
    @Log(title = "宠物分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaTechnicians faTechnicians)
    {
        return toAjax(faTechniciansService.updateFaTechnicians(faTechnicians));
    }

    /**
     * 删除宠物分类
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:remove')")
    @Log(title = "宠物分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faTechniciansService.deleteFaTechniciansByIds(ids));
    }


    /**
     * 查询宠物分类列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaTechnicians faTechnicians)
    {
        startPage();
        List<PetCategoryVo> list = faTechniciansService.listPetCategoty(faTechnicians);
        return getDataTable(list);
    }

    /**
     * 宠物分类列表所有
     */
    @PreAuthorize("@ss.hasPermi('reserve:pet_catogory:list')")
    @GetMapping("/all_list")
    public List<PetCategoryVo> allList(FaTechnicians faTechnicians)
    {
        startPage();
        List<PetCategoryVo> list = faTechniciansService.listPetCategoty(faTechnicians);
        return list;
    }

}
