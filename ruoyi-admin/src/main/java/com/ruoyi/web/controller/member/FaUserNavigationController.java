package com.ruoyi.web.controller.member;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserNavigation;
import com.ruoyi.system.service.IFaUserNavigationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户中心导航Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/navigation")
public class FaUserNavigationController extends BaseController
{
    @Autowired
    private IFaUserNavigationService faUserNavigationService;

    /**
     * 查询用户中心导航列表
     */
    @PreAuthorize("@ss.hasPermi('member:navigation:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserNavigation faUserNavigation)
    {
        startPage();
        List<FaUserNavigation> list = faUserNavigationService.selectFaUserNavigationList(faUserNavigation);
        return getDataTable(list);
    }

    /**
     * 导出用户中心导航列表
     */
    @PreAuthorize("@ss.hasPermi('member:navigation:export')")
    @Log(title = "用户中心导航", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserNavigation faUserNavigation)
    {
        List<FaUserNavigation> list = faUserNavigationService.selectFaUserNavigationList(faUserNavigation);
        ExcelUtil<FaUserNavigation> util = new ExcelUtil<FaUserNavigation>(FaUserNavigation.class);
        return util.exportExcel(list, "用户中心导航数据");
    }

    /**
     * 获取用户中心导航详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:navigation:query')")
    @GetMapping(value = "/{nId}")
    public AjaxResult getInfo(@PathVariable("nId") Integer nId)
    {
        return AjaxResult.success(faUserNavigationService.selectFaUserNavigationById(nId));
    }

    /**
     * 新增用户中心导航
     */
    @PreAuthorize("@ss.hasPermi('member:navigation:add')")
    @Log(title = "用户中心导航", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserNavigation faUserNavigation)
    {
        return toAjax(faUserNavigationService.insertFaUserNavigation(faUserNavigation));
    }

    /**
     * 修改用户中心导航
     */
    @PreAuthorize("@ss.hasPermi('member:navigation:edit')")
    @Log(title = "用户中心导航", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserNavigation faUserNavigation)
    {
        return toAjax(faUserNavigationService.updateFaUserNavigation(faUserNavigation));
    }

    /**
     * 删除用户中心导航
     */
    @PreAuthorize("@ss.hasPermi('member:navigation:remove')")
    @Log(title = "用户中心导航", businessType = BusinessType.DELETE)
	@DeleteMapping("/{nIds}")
    public AjaxResult remove(@PathVariable Integer[] nIds)
    {
        return toAjax(faUserNavigationService.deleteFaUserNavigationByIds(nIds));
    }
}
