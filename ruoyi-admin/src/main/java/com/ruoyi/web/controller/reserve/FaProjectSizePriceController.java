package com.ruoyi.web.controller.reserve;

import java.util.List;

import com.ruoyi.system.domain.vo.ProjectPriceSizeVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaProjectSizePrice;
import com.ruoyi.system.service.IFaProjectSizePriceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 预约项目价格Controller
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
@RestController
@RequestMapping("/reserve/price")
public class FaProjectSizePriceController extends BaseController
{
    @Autowired
    private IFaProjectSizePriceService faProjectSizePriceService;

    /**
     * 查询预约项目价格列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:price:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaProjectSizePrice faProjectSizePrice)
    {
        startPage();
        List<FaProjectSizePrice> list = faProjectSizePriceService.selectFaProjectSizePriceList(faProjectSizePrice);
        return getDataTable(list);
    }

    /**
     * 导出预约项目价格列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:price:export')")
    @Log(title = "预约项目价格", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaProjectSizePrice faProjectSizePrice)
    {
        List<FaProjectSizePrice> list = faProjectSizePriceService.selectFaProjectSizePriceList(faProjectSizePrice);
        ExcelUtil<FaProjectSizePrice> util = new ExcelUtil<FaProjectSizePrice>(FaProjectSizePrice.class);
        return util.exportExcel(list, "预约项目价格数据");
    }

    /**
     * 获取预约项目价格详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:price:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faProjectSizePriceService.selectFaProjectSizePriceById(id));
    }

    /**
     * 新增预约项目价格
     */
    @PreAuthorize("@ss.hasPermi('reserve:price:add')")
    @Log(title = "预约项目价格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaProjectSizePrice faProjectSizePrice)
    {
        return toAjax(faProjectSizePriceService.insertFaProjectSizePrice(faProjectSizePrice));
    }

    /**
     * 修改预约项目价格
     */
    @PreAuthorize("@ss.hasPermi('reserve:price:edit')")
    @Log(title = "预约项目价格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaProjectSizePrice faProjectSizePrice)
    {
        return toAjax(faProjectSizePriceService.updateFaProjectSizePrice(faProjectSizePrice));
    }

    /**
     * 删除预约项目价格
     */
    @PreAuthorize("@ss.hasPermi('reserve:price:remove')")
    @Log(title = "预约项目价格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faProjectSizePriceService.deleteFaProjectSizePriceByIds(ids));
    }

    /**
     * 查询预约项目价格列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:price:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaProjectSizePrice faProjectSizePrice)
    {
        startPage();
        List<ProjectPriceSizeVo> list = faProjectSizePriceService.listProjectSizeVo(faProjectSizePrice);
        return getDataTable(list);
    }


}
