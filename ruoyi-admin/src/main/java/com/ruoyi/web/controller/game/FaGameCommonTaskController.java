package com.ruoyi.web.controller.game;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameCommonTask;
import com.ruoyi.system.service.IFaGameCommonTaskService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 常规任务Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/commontask")
public class FaGameCommonTaskController extends BaseController
{
    @Autowired
    private IFaGameCommonTaskService faGameCommonTaskService;

    /**
     * 查询常规任务列表
     */
    @PreAuthorize("@ss.hasPermi('game:commontask:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameCommonTask faGameCommonTask)
    {
        startPage();
        List<FaGameCommonTask> list = faGameCommonTaskService.selectFaGameCommonTaskList(faGameCommonTask);
        return getDataTable(list);
    }

    /**
     * 导出常规任务列表
     */
    @PreAuthorize("@ss.hasPermi('game:commontask:export')")
    @Log(title = "常规任务", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameCommonTask faGameCommonTask)
    {
        List<FaGameCommonTask> list = faGameCommonTaskService.selectFaGameCommonTaskList(faGameCommonTask);
        ExcelUtil<FaGameCommonTask> util = new ExcelUtil<FaGameCommonTask>(FaGameCommonTask.class);
        return util.exportExcel(list, "常规任务数据");
    }

    /**
     * 获取常规任务详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:commontask:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGameCommonTaskService.selectFaGameCommonTaskById(id));
    }

    /**
     * 新增常规任务
     */
    @PreAuthorize("@ss.hasPermi('game:commontask:add')")
    @Log(title = "常规任务", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameCommonTask faGameCommonTask)
    {
        return toAjax(faGameCommonTaskService.insertFaGameCommonTask(faGameCommonTask));
    }

    /**
     * 修改常规任务
     */
    @PreAuthorize("@ss.hasPermi('game:commontask:edit')")
    @Log(title = "常规任务", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameCommonTask faGameCommonTask)
    {
        return toAjax(faGameCommonTaskService.updateFaGameCommonTask(faGameCommonTask));
    }

    /**
     * 删除常规任务
     */
    @PreAuthorize("@ss.hasPermi('game:commontask:remove')")
    @Log(title = "常规任务", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGameCommonTaskService.deleteFaGameCommonTaskByIds(ids));
    }
}
