package com.ruoyi.web.controller.vote;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaVoteSite;
import com.ruoyi.system.service.IFaVoteSiteService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打赏设置Controller
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@RestController
@RequestMapping("/vote/site")
public class FaVoteSiteController extends BaseController
{
    @Autowired
    private IFaVoteSiteService faVoteSiteService;

    /**
     * 查询打赏设置列表
     */
    @PreAuthorize("@ss.hasPermi('vote:site:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaVoteSite faVoteSite)
    {
        startPage();
        List<FaVoteSite> list = faVoteSiteService.selectFaVoteSiteList(faVoteSite);
        return getDataTable(list);
    }

    /**
     * 导出打赏设置列表
     */
    @PreAuthorize("@ss.hasPermi('vote:site:export')")
    @Log(title = "打赏设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaVoteSite faVoteSite)
    {
        List<FaVoteSite> list = faVoteSiteService.selectFaVoteSiteList(faVoteSite);
        ExcelUtil<FaVoteSite> util = new ExcelUtil<FaVoteSite>(FaVoteSite.class);
        return util.exportExcel(list, "打赏设置数据");
    }

    /**
     * 获取打赏设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('vote:site:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faVoteSiteService.selectFaVoteSiteById(id));
    }

    /**
     * 新增打赏设置
     */
    @PreAuthorize("@ss.hasPermi('vote:site:add')")
    @Log(title = "打赏设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaVoteSite faVoteSite)
    {
        return toAjax(faVoteSiteService.insertFaVoteSite(faVoteSite));
    }

    /**
     * 修改打赏设置
     */
    @PreAuthorize("@ss.hasPermi('vote:site:edit')")
    @Log(title = "打赏设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaVoteSite faVoteSite)
    {
        return toAjax(faVoteSiteService.updateFaVoteSite(faVoteSite));
    }

    /**
     * 删除打赏设置
     */
    @PreAuthorize("@ss.hasPermi('vote:site:remove')")
    @Log(title = "打赏设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faVoteSiteService.deleteFaVoteSiteByIds(ids));
    }
}
