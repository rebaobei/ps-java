package com.ruoyi.web.controller.mobile;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaAdszoneAds;
import com.ruoyi.system.service.IFaAdszoneAdsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 广告项目Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/mobile/ads")
public class FaAdszoneAdsController extends BaseController
{
    @Autowired
    private IFaAdszoneAdsService faAdszoneAdsService;

    /**
     * 查询广告项目列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:ads:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaAdszoneAds faAdszoneAds)
    {
        startPage();
        List<FaAdszoneAds> list = faAdszoneAdsService.selectFaAdszoneAdsList(faAdszoneAds);
        return getDataTable(list);
    }

    /**
     * 导出广告项目列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:ads:export')")
    @Log(title = "广告项目", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaAdszoneAds faAdszoneAds)
    {
        List<FaAdszoneAds> list = faAdszoneAdsService.selectFaAdszoneAdsList(faAdszoneAds);
        ExcelUtil<FaAdszoneAds> util = new ExcelUtil<FaAdszoneAds>(FaAdszoneAds.class);
        return util.exportExcel(list, "广告项目数据");
    }

    /**
     * 获取广告项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('mobile:ads:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faAdszoneAdsService.selectFaAdszoneAdsById(id));
    }

    /**
     * 新增广告项目
     */
    @PreAuthorize("@ss.hasPermi('mobile:ads:add')")
    @Log(title = "广告项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaAdszoneAds faAdszoneAds)
    {
        return toAjax(faAdszoneAdsService.insertFaAdszoneAds(faAdszoneAds));
    }

    /**
     * 修改广告项目
     */
    @PreAuthorize("@ss.hasPermi('mobile:ads:edit')")
    @Log(title = "广告项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaAdszoneAds faAdszoneAds)
    {
        return toAjax(faAdszoneAdsService.updateFaAdszoneAds(faAdszoneAds));
    }

    /**
     * 删除广告项目
     */
    @PreAuthorize("@ss.hasPermi('mobile:ads:remove')")
    @Log(title = "广告项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faAdszoneAdsService.deleteFaAdszoneAdsByIds(ids));
    }
}
