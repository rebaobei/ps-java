package com.ruoyi.web.controller.label;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBallLabel;
import com.ruoyi.system.service.IFaBallLabelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 标签详情Controller
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@RestController
@RequestMapping("/label/detail")
public class FaBallLabelController extends BaseController
{
    @Autowired
    private IFaBallLabelService faBallLabelService;

    /**
     * 查询标签详情列表
     */
    @PreAuthorize("@ss.hasPermi('label:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBallLabel faBallLabel)
    {
        startPage();
        List<FaBallLabel> list = faBallLabelService.selectFaBallLabelList(faBallLabel);
        return getDataTable(list);
    }

    /**
     * 导出标签详情列表
     */
    @PreAuthorize("@ss.hasPermi('label:detail:export')")
    @Log(title = "标签详情", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBallLabel faBallLabel)
    {
        List<FaBallLabel> list = faBallLabelService.selectFaBallLabelList(faBallLabel);
        ExcelUtil<FaBallLabel> util = new ExcelUtil<FaBallLabel>(FaBallLabel.class);
        return util.exportExcel(list, "标签详情数据");
    }

    /**
     * 获取标签详情详细信息
     */
    @PreAuthorize("@ss.hasPermi('label:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faBallLabelService.selectFaBallLabelById(id));
    }

    /**
     * 新增标签详情
     */
    @PreAuthorize("@ss.hasPermi('label:detail:add')")
    @Log(title = "标签详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBallLabel faBallLabel)
    {
        return toAjax(faBallLabelService.insertFaBallLabel(faBallLabel));
    }

    /**
     * 修改标签详情
     */
    @PreAuthorize("@ss.hasPermi('label:detail:edit')")
    @Log(title = "标签详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBallLabel faBallLabel)
    {
        return toAjax(faBallLabelService.updateFaBallLabel(faBallLabel));
    }

    /**
     * 删除标签详情
     */
    @PreAuthorize("@ss.hasPermi('label:detail:remove')")
    @Log(title = "标签详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faBallLabelService.deleteFaBallLabelByIds(ids));
    }
}
