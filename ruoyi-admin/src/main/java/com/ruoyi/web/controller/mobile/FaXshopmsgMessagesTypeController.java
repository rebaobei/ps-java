package com.ruoyi.web.controller.mobile;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopmsgMessagesType;
import com.ruoyi.system.service.IFaXshopmsgMessagesTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员消息类型Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/mobile/msgtype")
public class FaXshopmsgMessagesTypeController extends BaseController
{
    @Autowired
    private IFaXshopmsgMessagesTypeService faXshopmsgMessagesTypeService;

    /**
     * 查询会员消息类型列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:msgtype:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopmsgMessagesType faXshopmsgMessagesType)
    {
        startPage();
        List<FaXshopmsgMessagesType> list = faXshopmsgMessagesTypeService.selectFaXshopmsgMessagesTypeList(faXshopmsgMessagesType);
        return getDataTable(list);
    }

    /**
     * 导出会员消息类型列表
     */
    @PreAuthorize("@ss.hasPermi('mobile:msgtype:export')")
    @Log(title = "会员消息类型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopmsgMessagesType faXshopmsgMessagesType)
    {
        List<FaXshopmsgMessagesType> list = faXshopmsgMessagesTypeService.selectFaXshopmsgMessagesTypeList(faXshopmsgMessagesType);
        ExcelUtil<FaXshopmsgMessagesType> util = new ExcelUtil<FaXshopmsgMessagesType>(FaXshopmsgMessagesType.class);
        return util.exportExcel(list, "会员消息类型数据");
    }

    /**
     * 获取会员消息类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('mobile:msgtype:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faXshopmsgMessagesTypeService.selectFaXshopmsgMessagesTypeById(id));
    }

    /**
     * 新增会员消息类型
     */
    @PreAuthorize("@ss.hasPermi('mobile:msgtype:add')")
    @Log(title = "会员消息类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopmsgMessagesType faXshopmsgMessagesType)
    {
        return toAjax(faXshopmsgMessagesTypeService.insertFaXshopmsgMessagesType(faXshopmsgMessagesType));
    }

    /**
     * 修改会员消息类型
     */
    @PreAuthorize("@ss.hasPermi('mobile:msgtype:edit')")
    @Log(title = "会员消息类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopmsgMessagesType faXshopmsgMessagesType)
    {
        return toAjax(faXshopmsgMessagesTypeService.updateFaXshopmsgMessagesType(faXshopmsgMessagesType));
    }

    /**
     * 删除会员消息类型
     */
    @PreAuthorize("@ss.hasPermi('mobile:msgtype:remove')")
    @Log(title = "会员消息类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faXshopmsgMessagesTypeService.deleteFaXshopmsgMessagesTypeByIds(ids));
    }
}
