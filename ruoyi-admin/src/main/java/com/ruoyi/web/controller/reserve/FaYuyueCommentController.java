package com.ruoyi.web.controller.reserve;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.YuYueCommentVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaYuyueComment;
import com.ruoyi.system.service.IFaYuyueCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 预约订单评论Controller
 *
 * @author ruoyi
 * @date 2021-07-28
 */
@RestController
@RequestMapping("/reserve/comment")
public class FaYuyueCommentController extends BaseController
{
    @Autowired
    private IFaYuyueCommentService faYuyueCommentService;

    /**
     * 查询预约订单评论列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaYuyueComment faYuyueComment)
    {
        startPage();
        List<FaYuyueComment> list = faYuyueCommentService.selectFaYuyueCommentList(faYuyueComment);
        return getDataTable(list);
    }

    /**
     * 导出预约订单评论列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:export')")
    @Log(title = "预约订单评论", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaYuyueComment faYuyueComment)
    {
        List<FaYuyueComment> list = faYuyueCommentService.selectFaYuyueCommentList(faYuyueComment);
        ExcelUtil<FaYuyueComment> util = new ExcelUtil<FaYuyueComment>(FaYuyueComment.class);
        return util.exportExcel(list, "预约订单评论数据");
    }

    /**
     * 获取预约订单评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faYuyueCommentService.selectFaYuyueCommentById(id));
    }

    /**
     * 新增预约订单评论
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:add')")
    @Log(title = "预约订单评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaYuyueComment faYuyueComment)
    {
        return toAjax(faYuyueCommentService.insertFaYuyueComment(faYuyueComment));
    }

    /**
     * 修改预约订单评论
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:edit')")
    @Log(title = "预约订单评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaYuyueComment faYuyueComment)
    {
        return toAjax(faYuyueCommentService.updateFaYuyueComment(faYuyueComment));
    }

    /**
     * 删除预约订单评论
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:remove')")
    @Log(title = "预约订单评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faYuyueCommentService.deleteFaYuyueCommentByIds(ids));
    }

    /**
     * 查询预约订单评论列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:list')")
    @GetMapping("/get_list")
    public TableDataInfo MyList(FaYuyueComment faYuyueComment)
    {
        startPage();
        List<YuYueCommentVo> list = faYuyueCommentService.listCommnentVo(faYuyueComment);
        return getDataTable(list);
    }
    /**
     * 查询预约订单评论列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:comment:list')")
    @GetMapping("/getMyList")
    public TableDataInfo getMyList(FaYuyueComment faYuyueComment)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faYuyueComment.setStoreId(storeInfoId);
        startPage();
        List<YuYueCommentVo> list = faYuyueCommentService.listCommnentVo(faYuyueComment);
        return getDataTable(list);
    }

}
