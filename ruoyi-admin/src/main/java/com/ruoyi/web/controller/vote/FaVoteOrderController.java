package com.ruoyi.web.controller.vote;

import java.util.List;

import com.ruoyi.system.domain.vo.VoteOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaVoteOrder;
import com.ruoyi.system.service.IFaVoteOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 打赏订单Controller
 *
 * @author ruoyi
 * @date 2021-08-02
 */
@RestController
@RequestMapping("/vote/voteorder")
public class FaVoteOrderController extends BaseController {
    @Autowired
    private IFaVoteOrderService faVoteOrderService;

    /**
     * 查询打赏订单列表
     */
    @PreAuthorize("@ss.hasPermi('vote:voteorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaVoteOrder faVoteOrder) {
        startPage();
        List<FaVoteOrder> list = faVoteOrderService.selectFaVoteOrderList(faVoteOrder);
        return getDataTable(list);
    }

    /**
     * 导出打赏订单列表
     */
    @PreAuthorize("@ss.hasPermi('vote:voteorder:export')")
    @Log(title = "打赏订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaVoteOrder faVoteOrder) {
        List<FaVoteOrder> list = faVoteOrderService.selectFaVoteOrderList(faVoteOrder);
        ExcelUtil<FaVoteOrder> util = new ExcelUtil<FaVoteOrder>(FaVoteOrder.class);
        return util.exportExcel(list, "打赏订单数据");
    }

    /**
     * 获取打赏订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('vote:voteorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(faVoteOrderService.selectFaVoteOrderById(id));
    }

    /**
     * 新增打赏订单
     */
    @PreAuthorize("@ss.hasPermi('vote:voteorder:add')")
    @Log(title = "打赏订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaVoteOrder faVoteOrder) {
        return toAjax(faVoteOrderService.insertFaVoteOrder(faVoteOrder));
    }

    /**
     * 修改打赏订单
     */
    @PreAuthorize("@ss.hasPermi('vote:voteorder:edit')")
    @Log(title = "打赏订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaVoteOrder faVoteOrder) {
        return toAjax(faVoteOrderService.updateFaVoteOrder(faVoteOrder));
    }

    /**
     * 删除打赏订单
     */
    @PreAuthorize("@ss.hasPermi('vote:voteorder:remove')")
    @Log(title = "打赏订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(faVoteOrderService.deleteFaVoteOrderByIds(ids));
    }


    /**
     * 查询打赏订单列表
     */
    @PreAuthorize("@ss.hasPermi('vote:voteorder:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(VoteOrderVo faVoteOrder) {
        startPage();
        List<VoteOrderVo> list = faVoteOrderService.listVoteOrderVo(faVoteOrder);
        return getDataTable(list);
    }
}
