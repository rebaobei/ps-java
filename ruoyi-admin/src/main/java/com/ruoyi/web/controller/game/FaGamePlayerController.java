package com.ruoyi.web.controller.game;

import java.util.List;

import com.ruoyi.system.domain.vo.PlayerInfoVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGamePlayer;
import com.ruoyi.system.service.IFaGamePlayerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游戏玩家Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/player")
public class FaGamePlayerController extends BaseController
{
    @Autowired
    private IFaGamePlayerService faGamePlayerService;

    /**
     * 查询游戏玩家列表
     */
    @PreAuthorize("@ss.hasPermi('game:player:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGamePlayer faGamePlayer)
    {
        startPage();
        List<FaGamePlayer> list = faGamePlayerService.selectFaGamePlayerList(faGamePlayer);
        return getDataTable(list);
    }

    /**
     * 导出游戏玩家列表
     */
    @PreAuthorize("@ss.hasPermi('game:player:export')")
    @Log(title = "游戏玩家", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGamePlayer faGamePlayer)
    {
        List<FaGamePlayer> list = faGamePlayerService.selectFaGamePlayerList(faGamePlayer);
        ExcelUtil<FaGamePlayer> util = new ExcelUtil<FaGamePlayer>(FaGamePlayer.class);
        return util.exportExcel(list, "游戏玩家数据");
    }

    /**
     * 获取游戏玩家详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:player:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGamePlayerService.selectFaGamePlayerById(id));
    }

    /**
     * 新增游戏玩家
     */
    @PreAuthorize("@ss.hasPermi('game:player:add')")
    @Log(title = "游戏玩家", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGamePlayer faGamePlayer)
    {
        return toAjax(faGamePlayerService.insertFaGamePlayer(faGamePlayer));
    }

    /**
     * 修改游戏玩家
     */
    @PreAuthorize("@ss.hasPermi('game:player:edit')")
    @Log(title = "游戏玩家", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGamePlayer faGamePlayer)
    {
        return toAjax(faGamePlayerService.updateFaGamePlayer(faGamePlayer));
    }

    /**
     * 删除游戏玩家
     */
    @PreAuthorize("@ss.hasPermi('game:player:remove')")
    @Log(title = "游戏玩家", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGamePlayerService.deleteFaGamePlayerByIds(ids));
    }


    /**
     * 查询游戏玩家列表
     */
    @PreAuthorize("@ss.hasPermi('game:player:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(PlayerInfoVo faGamePlayer)
    {
        startPage();
        List<PlayerInfoVo> list = faGamePlayerService.lsitPlayerInfoVo(faGamePlayer);
        return getDataTable(list);
    }
}
