package com.ruoyi.web.controller.game;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGamePetsPosture;
import com.ruoyi.system.service.IFaGamePetsPostureService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物姿势Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/posture")
public class FaGamePetsPostureController extends BaseController
{
    @Autowired
    private IFaGamePetsPostureService faGamePetsPostureService;

    /**
     * 查询宠物姿势列表
     */
    @PreAuthorize("@ss.hasPermi('game:posture:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGamePetsPosture faGamePetsPosture)
    {
        startPage();
        List<FaGamePetsPosture> list = faGamePetsPostureService.selectFaGamePetsPostureList(faGamePetsPosture);
        return getDataTable(list);
    }

    /**
     * 导出宠物姿势列表
     */
    @PreAuthorize("@ss.hasPermi('game:posture:export')")
    @Log(title = "宠物姿势", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGamePetsPosture faGamePetsPosture)
    {
        List<FaGamePetsPosture> list = faGamePetsPostureService.selectFaGamePetsPostureList(faGamePetsPosture);
        ExcelUtil<FaGamePetsPosture> util = new ExcelUtil<FaGamePetsPosture>(FaGamePetsPosture.class);
        return util.exportExcel(list, "宠物姿势数据");
    }

    /**
     * 获取宠物姿势详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:posture:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGamePetsPostureService.selectFaGamePetsPostureById(id));
    }

    /**
     * 新增宠物姿势
     */
    @PreAuthorize("@ss.hasPermi('game:posture:add')")
    @Log(title = "宠物姿势", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGamePetsPosture faGamePetsPosture)
    {
        return toAjax(faGamePetsPostureService.insertFaGamePetsPosture(faGamePetsPosture));
    }

    /**
     * 修改宠物姿势
     */
    @PreAuthorize("@ss.hasPermi('game:posture:edit')")
    @Log(title = "宠物姿势", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGamePetsPosture faGamePetsPosture)
    {
        return toAjax(faGamePetsPostureService.updateFaGamePetsPosture(faGamePetsPosture));
    }

    /**
     * 删除宠物姿势
     */
    @PreAuthorize("@ss.hasPermi('game:posture:remove')")
    @Log(title = "宠物姿势", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGamePetsPostureService.deleteFaGamePetsPostureByIds(ids));
    }

    /**
     * 查询宠物姿势列表
     */
    @PreAuthorize("@ss.hasPermi('game:posture:list')")
    @GetMapping("/all_list")
    public List<FaGamePetsPosture> allList(FaGamePetsPosture faGamePetsPosture)
    {
        return faGamePetsPostureService.selectFaGamePetsPostureList(faGamePetsPosture);

    }

}
