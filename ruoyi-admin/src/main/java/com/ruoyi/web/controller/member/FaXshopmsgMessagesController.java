package com.ruoyi.web.controller.member;

import java.util.List;

import com.ruoyi.system.domain.vo.MsgLogWithUserInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopmsgMessages;
import com.ruoyi.system.service.IFaXshopmsgMessagesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 消息管理Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/messages")
public class FaXshopmsgMessagesController extends BaseController
{
    @Autowired
    private IFaXshopmsgMessagesService faXshopmsgMessagesService;

    /**
     * 查询消息管理列表
     */
    @PreAuthorize("@ss.hasPermi('member:messages:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopmsgMessages faXshopmsgMessages)
    {
        startPage();
        List<FaXshopmsgMessages> list = faXshopmsgMessagesService.selectFaXshopmsgMessagesList(faXshopmsgMessages);
        return getDataTable(list);
    }

    /**
     * 导出消息管理列表
     */
    @PreAuthorize("@ss.hasPermi('member:messages:export')")
    @Log(title = "消息管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopmsgMessages faXshopmsgMessages)
    {
        List<FaXshopmsgMessages> list = faXshopmsgMessagesService.selectFaXshopmsgMessagesList(faXshopmsgMessages);
        ExcelUtil<FaXshopmsgMessages> util = new ExcelUtil<FaXshopmsgMessages>(FaXshopmsgMessages.class);
        return util.exportExcel(list, "消息管理数据");
    }

    /**
     * 获取消息管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:messages:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faXshopmsgMessagesService.selectFaXshopmsgMessagesById(id));
    }

    /**
     * 新增消息管理
     */
    @PreAuthorize("@ss.hasPermi('member:messages:add')")
    @Log(title = "消息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopmsgMessages faXshopmsgMessages)
    {
        return toAjax(faXshopmsgMessagesService.insertFaXshopmsgMessages(faXshopmsgMessages));
    }

    /**
     * 修改消息管理
     */
    @PreAuthorize("@ss.hasPermi('member:messages:edit')")
    @Log(title = "消息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopmsgMessages faXshopmsgMessages)
    {
        return toAjax(faXshopmsgMessagesService.updateFaXshopmsgMessages(faXshopmsgMessages));
    }

    /**
     * 删除消息管理
     */
    @PreAuthorize("@ss.hasPermi('member:messages:remove')")
    @Log(title = "消息管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faXshopmsgMessagesService.deleteFaXshopmsgMessagesByIds(ids));
    }

    /**
     * 查询消息管理列表
     */
    @PreAuthorize("@ss.hasPermi('member:messages:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaXshopmsgMessages faXshopmsgMessages)
    {
        startPage();
        List<MsgLogWithUserInfo> list = faXshopmsgMessagesService.listMsgLog(faXshopmsgMessages);
        return getDataTable(list);
    }
}
