package com.ruoyi.web.controller.member;

import java.util.List;

import com.ruoyi.system.domain.vo.ThirdLogWithUserInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaThird;
import com.ruoyi.system.service.IFaThirdService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 第三方登录Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/third_login")
public class FaThirdController extends BaseController
{
    @Autowired
    private IFaThirdService faThirdService;

    /**
     * 查询第三方登录列表
     */
    @PreAuthorize("@ss.hasPermi('member:third_login:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaThird faThird)
    {
        startPage();
        List<FaThird> list = faThirdService.selectFaThirdList(faThird);
        return getDataTable(list);
    }

    /**
     * 导出第三方登录列表
     */
    @PreAuthorize("@ss.hasPermi('member:third_login:export')")
    @Log(title = "第三方登录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaThird faThird)
    {
        List<FaThird> list = faThirdService.selectFaThirdList(faThird);
        ExcelUtil<FaThird> util = new ExcelUtil<FaThird>(FaThird.class);
        return util.exportExcel(list, "第三方登录数据");
    }

    /**
     * 获取第三方登录详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:third_login:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faThirdService.selectFaThirdById(id));
    }

    /**
     * 新增第三方登录
     */
    @PreAuthorize("@ss.hasPermi('member:third_login:add')")
    @Log(title = "第三方登录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaThird faThird)
    {
        return toAjax(faThirdService.insertFaThird(faThird));
    }

    /**
     * 修改第三方登录
     */
    @PreAuthorize("@ss.hasPermi('member:third_login:edit')")
    @Log(title = "第三方登录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaThird faThird)
    {
        return toAjax(faThirdService.updateFaThird(faThird));
    }

    /**
     * 删除第三方登录
     */
    @PreAuthorize("@ss.hasPermi('member:third_login:remove')")
    @Log(title = "第三方登录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faThirdService.deleteFaThirdByIds(ids));
    }


    /**
     * 查询第三方登录列表
     */
    @PreAuthorize("@ss.hasPermi('member:third_login:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaThird faThird)
    {
        startPage();
        List<ThirdLogWithUserInfo> list = faThirdService.listThirdLog(faThird);
        return getDataTable(list);
    }
}
