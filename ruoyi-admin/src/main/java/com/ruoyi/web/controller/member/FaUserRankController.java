package com.ruoyi.web.controller.member;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserRank;
import com.ruoyi.system.service.IFaUserRankService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 等级管理Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/rank")
public class FaUserRankController extends BaseController
{
    @Autowired
    private IFaUserRankService faUserRankService;

    /**
     * 查询等级管理列表
     */
    @PreAuthorize("@ss.hasPermi('member:rank:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserRank faUserRank)
    {
        startPage();
        List<FaUserRank> list = faUserRankService.selectFaUserRankList(faUserRank);
        return getDataTable(list);
    }

    /**
     * 导出等级管理列表
     */
    @PreAuthorize("@ss.hasPermi('member:rank:export')")
    @Log(title = "等级管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserRank faUserRank)
    {
        List<FaUserRank> list = faUserRankService.selectFaUserRankList(faUserRank);
        ExcelUtil<FaUserRank> util = new ExcelUtil<FaUserRank>(FaUserRank.class);
        return util.exportExcel(list, "等级管理数据");
    }

    /**
     * 获取等级管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:rank:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faUserRankService.selectFaUserRankById(id));
    }

    /**
     * 新增等级管理
     */
    @PreAuthorize("@ss.hasPermi('member:rank:add')")
    @Log(title = "等级管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserRank faUserRank)
    {
        faUserRank.setCreatetime(System.currentTimeMillis()/1000);
        return toAjax(faUserRankService.insertFaUserRank(faUserRank));
    }

    /**
     * 修改等级管理
     */
    @PreAuthorize("@ss.hasPermi('member:rank:edit')")
    @Log(title = "等级管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserRank faUserRank)
    {
        return toAjax(faUserRankService.updateFaUserRank(faUserRank));
    }

    /**
     * 删除等级管理
     */
    @PreAuthorize("@ss.hasPermi('member:rank:remove')")
    @Log(title = "等级管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faUserRankService.deleteFaUserRankByIds(ids));
    }
}
