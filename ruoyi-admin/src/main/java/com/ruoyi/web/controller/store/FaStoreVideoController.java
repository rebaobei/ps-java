package com.ruoyi.web.controller.store;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStoreVideo;
import com.ruoyi.system.service.IFaStoreVideoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺视频Controller
 *
 * @author ruoyi
 * @date 2021-12-01
 */
@RestController
@RequestMapping("/storeinfo/video")
public class FaStoreVideoController extends BaseController
{
    @Autowired
    private IFaStoreVideoService faStoreVideoService;

    /**
     * 查询店铺视频列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:video:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStoreVideo faStoreVideo)
    {
        startPage();
        List<FaStoreVideo> list = faStoreVideoService.selectFaStoreVideoList(faStoreVideo);
        return getDataTable(list);
    }
    /**
     * 查询店铺视频列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:video:list')")
    @GetMapping("/getMyList")
    public TableDataInfo getMyListVideo(FaStoreVideo faStoreVideo)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreVideo.setStoreId(storeInfoId);
        startPage();
        List<FaStoreVideo> list = faStoreVideoService.selectFaStoreVideoList(faStoreVideo);
        return getDataTable(list);
    }
    /**
     * 导出店铺视频列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:video:export')")
    @Log(title = "店铺视频", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreVideo faStoreVideo)
    {
        List<FaStoreVideo> list = faStoreVideoService.selectFaStoreVideoList(faStoreVideo);
        ExcelUtil<FaStoreVideo> util = new ExcelUtil<FaStoreVideo>(FaStoreVideo.class);
        return util.exportExcel(list, "店铺视频数据");
    }

    /**
     * 获取店铺视频详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:video:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreVideoService.selectFaStoreVideoById(id));
    }

    /**
     * 新增店铺视频
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:video:add')")
    @Log(title = "店铺视频", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStoreVideo faStoreVideo)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreVideo.setStoreId(storeInfoId);
        faStoreVideo.setHomeRecommend("0");
        faStoreVideo.setUpvote(0L);
        return toAjax(faStoreVideoService.insertFaStoreVideo(faStoreVideo));
    }

    /**
     * 修改店铺视频
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:video:edit')")
    @Log(title = "店铺视频", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreVideo faStoreVideo)
    {
        return toAjax(faStoreVideoService.updateFaStoreVideo(faStoreVideo));
    }

    /**
     * 删除店铺视频
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:video:remove')")
    @Log(title = "店铺视频", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreVideoService.deleteFaStoreVideoByIds(ids));
    }
}
