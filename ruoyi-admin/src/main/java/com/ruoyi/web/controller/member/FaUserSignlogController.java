package com.ruoyi.web.controller.member;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserSignlog;
import com.ruoyi.system.service.IFaUserSignlogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 签到明细Controller
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@RestController
@RequestMapping("/member/signlog")
public class FaUserSignlogController extends BaseController
{
    @Autowired
    private IFaUserSignlogService faUserSignlogService;

    /**
     * 查询签到明细列表
     */
    @PreAuthorize("@ss.hasPermi('member:signlog:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserSignlog faUserSignlog)
    {
        startPage();
        List<FaUserSignlog> list = faUserSignlogService.selectFaUserSignlogList(faUserSignlog);
        return getDataTable(list);
    }

    /**
     * 导出签到明细列表
     */
    @PreAuthorize("@ss.hasPermi('member:signlog:export')")
    @Log(title = "签到明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserSignlog faUserSignlog)
    {
        List<FaUserSignlog> list = faUserSignlogService.selectFaUserSignlogList(faUserSignlog);
        ExcelUtil<FaUserSignlog> util = new ExcelUtil<FaUserSignlog>(FaUserSignlog.class);
        return util.exportExcel(list, "签到明细数据");
    }

    /**
     * 获取签到明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:signlog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faUserSignlogService.selectFaUserSignlogById(id));
    }

    /**
     * 新增签到明细
     */
    @PreAuthorize("@ss.hasPermi('member:signlog:add')")
    @Log(title = "签到明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserSignlog faUserSignlog)
    {
        return toAjax(faUserSignlogService.insertFaUserSignlog(faUserSignlog));
    }

    /**
     * 修改签到明细
     */
    @PreAuthorize("@ss.hasPermi('member:signlog:edit')")
    @Log(title = "签到明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserSignlog faUserSignlog)
    {
        return toAjax(faUserSignlogService.updateFaUserSignlog(faUserSignlog));
    }

    /**
     * 删除签到明细
     */
    @PreAuthorize("@ss.hasPermi('member:signlog:remove')")
    @Log(title = "签到明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faUserSignlogService.deleteFaUserSignlogByIds(ids));
    }
}
