package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.XshopOrderProductVo;
import com.ruoyi.system.domain.vo.XshopOrderVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopOrderProduct;
import com.ruoyi.system.service.IFaXshopOrderProductService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单商品Controller
 *
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/orderproduct")
public class FaXshopOrderProductController extends BaseController
{
    @Autowired
    private IFaXshopOrderProductService faXshopOrderProductService;

    /**
     * 查询订单商品列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopOrderProduct faXshopOrderProduct)
    {
        startPage();
        List<FaXshopOrderProduct> list = faXshopOrderProductService.selectFaXshopOrderProductList(faXshopOrderProduct);
        return getDataTable(list);
    }

    /**
     * 导出订单商品列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:export')")
    @Log(title = "订单商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopOrderProduct faXshopOrderProduct)
    {
        List<FaXshopOrderProduct> list = faXshopOrderProductService.selectFaXshopOrderProductList(faXshopOrderProduct);
        ExcelUtil<FaXshopOrderProduct> util = new ExcelUtil<FaXshopOrderProduct>(FaXshopOrderProduct.class);
        return util.exportExcel(list, "订单商品数据");
    }

    /**
     * 获取订单商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopOrderProductService.selectFaXshopOrderProductById(id));
    }

    /**
     * 新增订单商品
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:add')")
    @Log(title = "订单商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopOrderProduct faXshopOrderProduct)
    {
        return toAjax(faXshopOrderProductService.insertFaXshopOrderProduct(faXshopOrderProduct));
    }

    /**
     * 修改订单商品
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:edit')")
    @Log(title = "订单商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopOrderProduct faXshopOrderProduct)
    {
        return toAjax(faXshopOrderProductService.updateFaXshopOrderProduct(faXshopOrderProduct));
    }

    /**
     * 删除订单商品
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:remove')")
    @Log(title = "订单商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopOrderProductService.deleteFaXshopOrderProductByIds(ids));
    }

    /**
     * 查询订单商品列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(XshopOrderProductVo faXshopOrderProduct)
    {
        startPage();
        List<XshopOrderProductVo> list = faXshopOrderProductService.listXshopOrderProductVo(faXshopOrderProduct);
        return getDataTable(list);
    }
    /**
     * 查询订单商品列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderproduct:list')")
    @GetMapping("/getMyList")
    public TableDataInfo getMyList(XshopOrderProductVo faXshopOrderProduct)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faXshopOrderProduct.setStoreId(storeInfoId);
        startPage();
        List<XshopOrderProductVo> list = faXshopOrderProductService.listXshopOrderProductVo(faXshopOrderProduct);
        return getDataTable(list);
    }
}
