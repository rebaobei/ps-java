package com.ruoyi.web.controller.member;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.vo.LevelChangeVo;
import com.ruoyi.system.domain.vo.MemberInfoChangeVo;
import com.ruoyi.system.domain.vo.MsgVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUser;
import com.ruoyi.system.service.IFaUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员管理111Controller
 *
 * @author ruoyi
 * @date 2021-07-05
 */
@RestController
@RequestMapping("/member/manage")
public class FaUserController extends BaseController {
    @Autowired
    private IFaUserService faUserService;

    /**
     * 查询会员管理111列表
     */
    @PreAuthorize("@ss.hasPermi('member:manage:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUser faUser) {
        startPage();
        List<FaUser> list = faUserService.selectFaUserList(faUser);
        return getDataTable(list);
    }
    /**
     * 查询会员管理111列表
     */
    @GetMapping("/getMyList")
    public TableDataInfo getMyList(FaUser faUser) {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faUser.setStoreId(storeInfoId);
        faUser.setUserType(1);
        startPage();
        List<FaUser> list = faUserService.selectFaUserList(faUser);
        return getDataTable(list);
    }
    /**
     * 导出会员管理111列表
     */
    @PreAuthorize("@ss.hasPermi('member:manage:export')")
    @Log(title = "会员管理111", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUser faUser) {
        List<FaUser> list = faUserService.selectFaUserList(faUser);
        ExcelUtil<FaUser> util = new ExcelUtil<FaUser>(FaUser.class);
        return util.exportExcel(list, "会员管理111数据");
    }

    /**
     * 获取会员管理111详细信息
     */
    @PreAuthorize("@ss.hasPermi('member:manage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(faUserService.selectFaUserById(id));
    }
    /**
     * 根据手机号获取会员详情
     */
    @GetMapping(value = "/getManageByMobile/{mobile}")
    public AjaxResult getManageByMobile(@PathVariable("mobile") String mobile) {
        if(StringUtils.isEmpty(mobile)){
            return AjaxResult.error("请输入手机号");
        }
        FaUser user = faUserService.selectFaUserByMobile(mobile);
        if(StringUtils.isNull(user)){
            return AjaxResult.error("手机号不存在【账号不存在】,请在app端绑定手机号");
        }
        return AjaxResult.success(user);
    }

    /**
     * 发送手机验证码
     */
    @GetMapping(value = "/getCaptchaByMobile/{mobile}")
    public AjaxResult getCaptchaByMobile(@PathVariable("mobile") String mobile) {
        if(StringUtils.isEmpty(mobile)){
            return AjaxResult.error("请输入手机号");
        }
        AjaxResult  ajaxResult = faUserService.getCaptchaByMobile(mobile);

        return ajaxResult;
    }

    /**
     * 新增会员管理111
     */
    @PreAuthorize("@ss.hasPermi('member:manage:add')")
    @Log(title = "会员管理111", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUser faUser) {
        return toAjax(faUserService.insertFaUser(faUser));
    }

    /**
     * 修改会员管理111
     */
    @PreAuthorize("@ss.hasPermi('member:manage:edit')")
    @Log(title = "会员管理111", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUser faUser) {
        return toAjax(faUserService.updateFaUser(faUser));
    }
    /**
     * 添加店员绑定
     */
    @Log(title = "添加店员绑定", businessType = BusinessType.UPDATE)
    @PutMapping("/addStore")
    public AjaxResult addStore(@RequestBody FaUser faUser) {
        return faUserService.addStore(faUser);
    }
    /**
     * 解除店员绑定
     */
    @Log(title = "解除店员绑定", businessType = BusinessType.UPDATE)
    @PutMapping("/removeStore")
    public AjaxResult removeStore(@RequestBody FaUser faUser) {
        FaUser faUser1 = new FaUser();
        faUser1.setStoreId(faUser.getStoreId());
        faUser1.setId(faUser.getId());
        faUser1.setUserType(0);
        faUser1.setStoreId(null);
        return toAjax(faUserService.updateFaUser(faUser1));
    }

    /**
     * 删除会员管理111
     */
    @PreAuthorize("@ss.hasPermi('member:manage:remove')")
    @Log(title = "会员管理111", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(faUserService.deleteFaUserByIds(ids));
    }

    /**
     * 调整积分
     */
    @PreAuthorize("@ss.hasPermi('member:manage:edit')")
    @Log(title = "调整积分", businessType = BusinessType.UPDATE)
    @PostMapping("/score")
    public AjaxResult changeScore(@RequestBody MemberInfoChangeVo memberInfoChangeVo) {

        return toAjax(faUserService.changScore(memberInfoChangeVo));

    }

    /**
     * 调整余额
     */
    @PreAuthorize("@ss.hasPermi('member:manage:edit')")
    @Log(title = "调整余额", businessType = BusinessType.UPDATE)
    @PostMapping("/money")
    public AjaxResult changeMoney(@RequestBody MemberInfoChangeVo memberInfoChangeVo) {
        return toAjax(faUserService.changeMoney(memberInfoChangeVo));

    }

    /**
     * 调整储值卡余额
     */
    @PreAuthorize("@ss.hasPermi('member:manage:edit')")
    @Log(title = "调整储值卡余额", businessType = BusinessType.UPDATE)
    @PostMapping("/card")
    public AjaxResult changeCardMoney(@RequestBody MemberInfoChangeVo memberInfoChangeVo) {

        return toAjax(faUserService.changeCardMoney(memberInfoChangeVo));

    }

    /**
     * 冻结储值卡余额
     */
    @PreAuthorize("@ss.hasPermi('member:manage:edit')")
    @Log(title = "冻结储值卡余额", businessType = BusinessType.UPDATE)
    @PostMapping("/freeze")
    public AjaxResult freezeCardMoney(@RequestBody MemberInfoChangeVo memberInfoChangeVo) {

        return toAjax(faUserService.freezeCardMoney(memberInfoChangeVo));
    }

    /**
     * 调整会员等级
     */
    @PreAuthorize("@ss.hasPermi('member:manage:edit')")
    @Log(title = "调整会员等级", businessType = BusinessType.UPDATE)
    @PostMapping("/level")
    public AjaxResult changLevel(@RequestBody LevelChangeVo levelChangeVo) {
        return toAjax(faUserService.changeLevel(levelChangeVo));
    }


    /**
     * 发送消息
     */
    @PreAuthorize("@ss.hasPermi('member:manage:edit')")
    @Log(title = "发送消息", businessType = BusinessType.UPDATE)
    @PostMapping("/msg")
    public AjaxResult sendMsg(@RequestBody MsgVo msgVo) {
        return toAjax(faUserService.sendMsg(msgVo));
    }


}
