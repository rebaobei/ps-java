package com.ruoyi.web.controller.article;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGoodArticle;
import com.ruoyi.system.service.IFaGoodArticleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 动保公益Controller
 *
 * @author ruoyi
 * @date 2021-12-04
 */
@RestController
@RequestMapping("/article/goodarticle")
public class FaGoodArticleController extends BaseController
{
    @Autowired
    private IFaGoodArticleService faGoodArticleService;

    /**
     * 查询动保公益列表
     */
    @PreAuthorize("@ss.hasPermi('article:goodarticle:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGoodArticle faGoodArticle)
    {
        startPage();
        List<FaGoodArticle> list = faGoodArticleService.selectFaGoodArticleList(faGoodArticle);
        return getDataTable(list);
    }

    /**
     * 导出动保公益列表
     */
    @PreAuthorize("@ss.hasPermi('article:goodarticle:export')")
    @Log(title = "动保公益", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGoodArticle faGoodArticle)
    {
        List<FaGoodArticle> list = faGoodArticleService.selectFaGoodArticleList(faGoodArticle);
        ExcelUtil<FaGoodArticle> util = new ExcelUtil<FaGoodArticle>(FaGoodArticle.class);
        return util.exportExcel(list, "动保公益数据");
    }

    /**
     * 获取动保公益详细信息
     */
    @PreAuthorize("@ss.hasPermi('article:goodarticle:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGoodArticleService.selectFaGoodArticleById(id));
    }

    /**
     * 新增动保公益
     */
    @PreAuthorize("@ss.hasPermi('article:goodarticle:add')")
    @Log(title = "动保公益", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGoodArticle faGoodArticle)
    {
        return toAjax(faGoodArticleService.insertFaGoodArticle(faGoodArticle));
    }

    /**
     * 修改动保公益
     */
    @PreAuthorize("@ss.hasPermi('article:goodarticle:edit')")
    @Log(title = "动保公益", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGoodArticle faGoodArticle)
    {
        return toAjax(faGoodArticleService.updateFaGoodArticle(faGoodArticle));
    }

    /**
     * 删除动保公益
     */
    @PreAuthorize("@ss.hasPermi('article:goodarticle:remove')")
    @Log(title = "动保公益", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGoodArticleService.deleteFaGoodArticleByIds(ids));
    }
}
