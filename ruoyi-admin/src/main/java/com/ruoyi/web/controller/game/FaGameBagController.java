package com.ruoyi.web.controller.game;

import java.util.List;

import com.ruoyi.system.domain.vo.GameBagVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameBag;
import com.ruoyi.system.service.IFaGameBagService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 背包管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/bag")
public class FaGameBagController extends BaseController
{
    @Autowired
    private IFaGameBagService faGameBagService;

    /**
     * 查询背包管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:bag:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameBag faGameBag)
    {
        startPage();
        List<FaGameBag> list = faGameBagService.selectFaGameBagList(faGameBag);
        return getDataTable(list);
    }

    /**
     * 导出背包管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:bag:export')")
    @Log(title = "背包管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameBag faGameBag)
    {
        List<FaGameBag> list = faGameBagService.selectFaGameBagList(faGameBag);
        ExcelUtil<FaGameBag> util = new ExcelUtil<FaGameBag>(FaGameBag.class);
        return util.exportExcel(list, "背包管理数据");
    }

    /**
     * 获取背包管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:bag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faGameBagService.selectFaGameBagById(id));
    }

    /**
     * 新增背包管理
     */
    @PreAuthorize("@ss.hasPermi('game:bag:add')")
    @Log(title = "背包管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameBag faGameBag)
    {
        return toAjax(faGameBagService.insertFaGameBag(faGameBag));
    }

    /**
     * 修改背包管理
     */
    @PreAuthorize("@ss.hasPermi('game:bag:edit')")
    @Log(title = "背包管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameBag faGameBag)
    {
        return toAjax(faGameBagService.updateFaGameBag(faGameBag));
    }

    /**
     * 删除背包管理
     */
    @PreAuthorize("@ss.hasPermi('game:bag:remove')")
    @Log(title = "背包管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faGameBagService.deleteFaGameBagByIds(ids));
    }

    /**
     * 查询背包管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:bag:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(GameBagVo faGameBag)
    {
        startPage();
        List<GameBagVo> list = faGameBagService.listGameBagVo(faGameBag);
        return getDataTable(list);
    }
}
