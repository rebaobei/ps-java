package com.ruoyi.web.controller.orderlist;

import java.util.List;

import com.ruoyi.system.domain.vo.YuyueOrderDetailVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaYuyueOrderDetail;
import com.ruoyi.system.service.IFaYuyueOrderDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 预约订单明细Controller
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/orderlist/orderdetail")
public class FaYuyueOrderDetailController extends BaseController
{
    @Autowired
    private IFaYuyueOrderDetailService faYuyueOrderDetailService;

    /**
     * 查询预约订单明细列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderdetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaYuyueOrderDetail faYuyueOrderDetail)
    {
        startPage();
        List<FaYuyueOrderDetail> list = faYuyueOrderDetailService.selectFaYuyueOrderDetailList(faYuyueOrderDetail);
        return getDataTable(list);
    }

    /**
     * 导出预约订单明细列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderdetail:export')")
    @Log(title = "预约订单明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaYuyueOrderDetail faYuyueOrderDetail)
    {
        List<FaYuyueOrderDetail> list = faYuyueOrderDetailService.selectFaYuyueOrderDetailList(faYuyueOrderDetail);
        ExcelUtil<FaYuyueOrderDetail> util = new ExcelUtil<FaYuyueOrderDetail>(FaYuyueOrderDetail.class);
        return util.exportExcel(list, "预约订单明细数据");
    }

    /**
     * 获取预约订单明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderdetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faYuyueOrderDetailService.selectFaYuyueOrderDetailById(id));
    }

    /**
     * 新增预约订单明细
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderdetail:add')")
    @Log(title = "预约订单明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaYuyueOrderDetail faYuyueOrderDetail)
    {
        return toAjax(faYuyueOrderDetailService.insertFaYuyueOrderDetail(faYuyueOrderDetail));
    }

    /**
     * 修改预约订单明细
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderdetail:edit')")
    @Log(title = "预约订单明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaYuyueOrderDetail faYuyueOrderDetail)
    {
        return toAjax(faYuyueOrderDetailService.updateFaYuyueOrderDetail(faYuyueOrderDetail));
    }

    /**
     * 删除预约订单明细
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderdetail:remove')")
    @Log(title = "预约订单明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faYuyueOrderDetailService.deleteFaYuyueOrderDetailByIds(ids));
    }

    /**
     * 查询预约订单明细列表
     */
    @PreAuthorize("@ss.hasPermi('orderlist:orderdetail:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(YuyueOrderDetailVo faYuyueOrderDetail)
    {
        startPage();
        List<YuyueOrderDetailVo> list = faYuyueOrderDetailService.listYuyueOrderDetailVo(faYuyueOrderDetail);
        return getDataTable(list);
    }
}
