package com.ruoyi.web.controller.reserve;

import java.util.List;

import com.ruoyi.system.domain.vo.ProjectTypeVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaProjectsType;
import com.ruoyi.system.service.IFaProjectsTypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物大小管理Controller
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@RestController
@RequestMapping("/reserve/project_type")
public class FaProjectsTypeController extends BaseController
{
    @Autowired
    private IFaProjectsTypeService faProjectsTypeService;

    /**
     * 查询宠物大小管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project_type:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaProjectsType faProjectsType)
    {
        startPage();
        List<FaProjectsType> list = faProjectsTypeService.selectFaProjectsTypeList(faProjectsType);
        return getDataTable(list);
    }

    /**
     * 导出宠物大小管理列表
     */
    @PreAuthorize("@ss.hasPermi('reserve:project_type:export')")
    @Log(title = "宠物大小管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaProjectsType faProjectsType)
    {
        List<FaProjectsType> list = faProjectsTypeService.selectFaProjectsTypeList(faProjectsType);
        ExcelUtil<FaProjectsType> util = new ExcelUtil<FaProjectsType>(FaProjectsType.class);
        return util.exportExcel(list, "宠物大小管理数据");
    }

    /**
     * 获取宠物大小管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('reserve:project_type:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faProjectsTypeService.selectFaProjectsTypeById(id));
    }

    /**
     * 新增宠物大小管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:project_type:add')")
    @Log(title = "宠物大小管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaProjectsType faProjectsType)
    {
        return toAjax(faProjectsTypeService.insertFaProjectsType(faProjectsType));
    }

    /**
     * 修改宠物大小管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:project_type:edit')")
    @Log(title = "宠物大小管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaProjectsType faProjectsType)
    {
        return toAjax(faProjectsTypeService.updateFaProjectsType(faProjectsType));
    }

    /**
     * 删除宠物大小管理
     */
    @PreAuthorize("@ss.hasPermi('reserve:project_type:remove')")
    @Log(title = "宠物大小管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faProjectsTypeService.deleteFaProjectsTypeByIds(ids));
    }


    /**
     * 宠物大小列表
     */
    //@PreAuthorize("@ss.hasPermi('reserve:project_type:list')")
    @GetMapping("/all_list")
    public List<FaProjectsType> allList(FaProjectsType faProjectsType)
    {
        return faProjectsTypeService.list(faProjectsType);
    }

    /**
     * 查询宠物大小管理列表有分页的
     */
    @PreAuthorize("@ss.hasPermi('reserve:project_type:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaProjectsType faProjectsType)
    {
        startPage();
        List<ProjectTypeVo> list = faProjectsTypeService.listProjectTypeVo(faProjectsType);
        return getDataTable(list);
    }


}
