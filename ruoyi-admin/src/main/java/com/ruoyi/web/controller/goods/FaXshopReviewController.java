package com.ruoyi.web.controller.goods;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.ProductReviewVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopReview;
import com.ruoyi.system.service.IFaXshopReviewService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品评价Controller
 *
 * @author ruoyi
 * @date 2021-07-19
 */
@RestController
@RequestMapping("/goods/review")
public class FaXshopReviewController extends BaseController
{
    @Autowired
    private IFaXshopReviewService faXshopReviewService;

    /**
     * 查询商品评价列表
     */
    @PreAuthorize("@ss.hasPermi('goods:review:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopReview faXshopReview)
    {
        startPage();
        List<FaXshopReview> list = faXshopReviewService.selectFaXshopReviewList(faXshopReview);
        return getDataTable(list);
    }

    /**
     * 导出商品评价列表
     */
    @PreAuthorize("@ss.hasPermi('goods:review:export')")
    @Log(title = "商品评价", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopReview faXshopReview)
    {
        List<FaXshopReview> list = faXshopReviewService.selectFaXshopReviewList(faXshopReview);
        ExcelUtil<FaXshopReview> util = new ExcelUtil<FaXshopReview>(FaXshopReview.class);
        return util.exportExcel(list, "商品评价数据");
    }

    /**
     * 获取商品评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:review:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopReviewService.selectFaXshopReviewById(id));
    }

    /**
     * 新增商品评价
     */
    @PreAuthorize("@ss.hasPermi('goods:review:add')")
    @Log(title = "商品评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopReview faXshopReview)
    {
        return toAjax(faXshopReviewService.insertFaXshopReview(faXshopReview));
    }

    /**
     * 修改商品评价
     */
    @PreAuthorize("@ss.hasPermi('goods:review:edit')")
    @Log(title = "商品评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopReview faXshopReview)
    {
        return toAjax(faXshopReviewService.updateFaXshopReview(faXshopReview));
    }

    /**
     * 删除商品评价
     */
    @PreAuthorize("@ss.hasPermi('goods:review:remove')")
    @Log(title = "商品评价", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopReviewService.deleteFaXshopReviewByIds(ids));
    }


    /**
     * 查询商品评价列表
     */
    @PreAuthorize("@ss.hasPermi('goods:review:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(ProductReviewVo productReviewVo)
    {
        startPage();
        List<ProductReviewVo> list = faXshopReviewService.listProductReviewVo(productReviewVo);
        return getDataTable(list);
    }
    /**
     * 查询商品评价列表
     */
    @PreAuthorize("@ss.hasPermi('goods:review:list')")
    @GetMapping("/getMyList")
    public TableDataInfo getMyList(ProductReviewVo productReviewVo)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        productReviewVo.setStoreId(storeInfoId);
        startPage();
        List<ProductReviewVo> list = faXshopReviewService.listProductReviewVo(productReviewVo);
        return getDataTable(list);
    }
}
