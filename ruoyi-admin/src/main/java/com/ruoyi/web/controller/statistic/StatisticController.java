package com.ruoyi.web.controller.statistic;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.IStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/8
 **/
@RestController
@RequestMapping("/statistic")
public class StatisticController {

    @Autowired
    private IStatisticService statisticService;

    //商城金额统计
    @RequestMapping("/panel")
    public AjaxResult panelGroupInfo() {
        return AjaxResult.success(statisticService.getPanelInfo());
    }

    //折线图数据
    @GetMapping("/line")
    public AjaxResult lineChartInfo() {
        return AjaxResult.success(statisticService.getLineChartInfo());
    }

}
