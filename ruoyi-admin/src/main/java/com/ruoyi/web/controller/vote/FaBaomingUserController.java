package com.ruoyi.web.controller.vote;

import java.util.List;

import com.ruoyi.system.domain.vo.BaomingUserVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBaomingUser;
import com.ruoyi.system.service.IFaBaomingUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报名管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@RestController
@RequestMapping("/vote/baominguser")
public class FaBaomingUserController extends BaseController
{
    @Autowired
    private IFaBaomingUserService faBaomingUserService;

    /**
     * 查询报名管理列表
     */
    @PreAuthorize("@ss.hasPermi('vote:baominguser:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBaomingUser faBaomingUser)
    {
        startPage();
        List<FaBaomingUser> list = faBaomingUserService.selectFaBaomingUserList(faBaomingUser);
        return getDataTable(list);
    }

    /**
     * 导出报名管理列表
     */
    @PreAuthorize("@ss.hasPermi('vote:baominguser:export')")
    @Log(title = "报名管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBaomingUser faBaomingUser)
    {
        List<FaBaomingUser> list = faBaomingUserService.selectFaBaomingUserList(faBaomingUser);
        ExcelUtil<FaBaomingUser> util = new ExcelUtil<FaBaomingUser>(FaBaomingUser.class);
        return util.exportExcel(list, "报名管理数据");
    }

    /**
     * 获取报名管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('vote:baominguser:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faBaomingUserService.selectFaBaomingUserById(id));
    }

    /**
     * 新增报名管理
     */
    @PreAuthorize("@ss.hasPermi('vote:baominguser:add')")
    @Log(title = "报名管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBaomingUser faBaomingUser)
    {
        return toAjax(faBaomingUserService.insertFaBaomingUser(faBaomingUser));
    }

    /**
     * 修改报名管理
     */
    @PreAuthorize("@ss.hasPermi('vote:baominguser:edit')")
    @Log(title = "报名管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBaomingUser faBaomingUser)
    {
        return toAjax(faBaomingUserService.updateFaBaomingUser(faBaomingUser));
    }

    /**
     * 删除报名管理
     */
    @PreAuthorize("@ss.hasPermi('vote:baominguser:remove')")
    @Log(title = "报名管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faBaomingUserService.deleteFaBaomingUserByIds(ids));
    }

    /**
     * 查询报名管理列表
     */
    @PreAuthorize("@ss.hasPermi('vote:baominguser:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(BaomingUserVo faBaomingUser)
    {
        startPage();
        List<BaomingUserVo> list = faBaomingUserService.listBaomingUserVo(faBaomingUser);
        return getDataTable(list);
    }
}
