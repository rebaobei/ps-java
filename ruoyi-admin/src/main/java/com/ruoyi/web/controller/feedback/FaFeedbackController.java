package com.ruoyi.web.controller.feedback;

import java.util.List;

import com.ruoyi.system.domain.vo.FeedBackVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaFeedback;
import com.ruoyi.system.service.IFaFeedbackService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 反馈管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@RestController
@RequestMapping("/feedback/feedback")
public class FaFeedbackController extends BaseController
{
    @Autowired
    private IFaFeedbackService faFeedbackService;

    /**
     * 查询反馈管理列表
     */
    @PreAuthorize("@ss.hasPermi('feedback:feedback:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaFeedback faFeedback)
    {
        startPage();
        List<FaFeedback> list = faFeedbackService.selectFaFeedbackList(faFeedback);
        return getDataTable(list);
    }

    /**
     * 导出反馈管理列表
     */
    @PreAuthorize("@ss.hasPermi('feedback:feedback:export')")
    @Log(title = "反馈管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaFeedback faFeedback)
    {
        List<FaFeedback> list = faFeedbackService.selectFaFeedbackList(faFeedback);
        ExcelUtil<FaFeedback> util = new ExcelUtil<FaFeedback>(FaFeedback.class);
        return util.exportExcel(list, "反馈管理数据");
    }

    /**
     * 获取反馈管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('feedback:feedback:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faFeedbackService.selectFaFeedbackById(id));
    }

    /**
     * 新增反馈管理
     */
    @PreAuthorize("@ss.hasPermi('feedback:feedback:add')")
    @Log(title = "反馈管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaFeedback faFeedback)
    {
        return toAjax(faFeedbackService.insertFaFeedback(faFeedback));
    }

    /**
     * 修改反馈管理
     */
    @PreAuthorize("@ss.hasPermi('feedback:feedback:edit')")
    @Log(title = "反馈管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaFeedback faFeedback)
    {
        return toAjax(faFeedbackService.updateFaFeedback(faFeedback));
    }

    /**
     * 删除反馈管理
     */
    @PreAuthorize("@ss.hasPermi('feedback:feedback:remove')")
    @Log(title = "反馈管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faFeedbackService.deleteFaFeedbackByIds(ids));
    }

    /**
     * 查询反馈管理列表
     */
    @PreAuthorize("@ss.hasPermi('feedback:feedback:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FeedBackVo faFeedback)
    {
        startPage();
        List<FeedBackVo> list = faFeedbackService.listFeedbackVo(faFeedback);
        return getDataTable(list);
    }
}
