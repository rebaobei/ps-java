package com.ruoyi.web.controller.goods;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopExpress;
import com.ruoyi.system.service.IFaXshopExpressService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 快递公司Controller
 *
 * @author ruoyi
 * @date 2021-07-19
 */
@RestController
@RequestMapping("/goods/express")
public class FaXshopExpressController extends BaseController
{
    @Autowired
    private IFaXshopExpressService faXshopExpressService;

    /**
     * 查询快递公司列表
     */
    @PreAuthorize("@ss.hasPermi('goods:express:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopExpress faXshopExpress)
    {
        startPage();
        List<FaXshopExpress> list = faXshopExpressService.selectFaXshopExpressList(faXshopExpress);
        return getDataTable(list);
    }

    /**
     * 导出快递公司列表
     */
    @PreAuthorize("@ss.hasPermi('goods:express:export')")
    @Log(title = "快递公司", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopExpress faXshopExpress)
    {
        List<FaXshopExpress> list = faXshopExpressService.selectFaXshopExpressList(faXshopExpress);
        ExcelUtil<FaXshopExpress> util = new ExcelUtil<FaXshopExpress>(FaXshopExpress.class);
        return util.exportExcel(list, "快递公司数据");
    }

    /**
     * 获取快递公司详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:express:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopExpressService.selectFaXshopExpressById(id));
    }

    /**
     * 新增快递公司
     */
    @PreAuthorize("@ss.hasPermi('goods:express:add')")
    @Log(title = "快递公司", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopExpress faXshopExpress)
    {
        return toAjax(faXshopExpressService.insertFaXshopExpress(faXshopExpress));
    }

    /**
     * 修改快递公司
     */
    @PreAuthorize("@ss.hasPermi('goods:express:edit')")
    @Log(title = "快递公司", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopExpress faXshopExpress)
    {
        return toAjax(faXshopExpressService.updateFaXshopExpress(faXshopExpress));
    }

    /**
     * 删除快递公司
     */
    @PreAuthorize("@ss.hasPermi('goods:express:remove')")
    @Log(title = "快递公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopExpressService.deleteFaXshopExpressByIds(ids));
    }

    /**
     * 查询所有快递公司列表
     */
    @GetMapping("/all_list")
    public List<FaXshopExpress> allList(FaXshopExpress faXshopExpress)
    {
        return faXshopExpressService.selectFaXshopExpressList(faXshopExpress);
    }
}
