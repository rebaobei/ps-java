package com.ruoyi.web.controller.store;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStorePerson;
import com.ruoyi.system.service.IFaStorePersonService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺服务团队Controller
 *
 * @author ruoyi
 * @date 2021-10-20
 */
@RestController
@RequestMapping("/storeinfo/person")
public class FaStorePersonController extends BaseController
{
    @Autowired
    private IFaStorePersonService faStorePersonService;

    /**
     * 查询店铺服务团队列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStorePerson faStorePerson)
    {
        startPage();
        List<FaStorePerson> list = faStorePersonService.selectFaStorePersonList(faStorePerson);
        return getDataTable(list);
    }
    /**
     * 查询店铺服务团队列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:list')")
    @GetMapping("/myList")
    public TableDataInfo myList(FaStorePerson faStorePerson)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStorePerson.setStoreId(storeInfoId);
        startPage();
        List<FaStorePerson> list = faStorePersonService.selectFaStorePersonList(faStorePerson);
        return getDataTable(list);
    }
    /**
     * 导出店铺服务团队列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:export')")
    @Log(title = "店铺服务团队", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStorePerson faStorePerson)
    {
        List<FaStorePerson> list = faStorePersonService.selectFaStorePersonList(faStorePerson);
        ExcelUtil<FaStorePerson> util = new ExcelUtil<FaStorePerson>(FaStorePerson.class);
        return util.exportExcel(list, "店铺服务团队数据");
    }

    /**
     * 获取店铺服务团队详细信息
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStorePersonService.selectFaStorePersonById(id));
    }

    /**
     * 新增店铺服务团队
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:add')")
    @Log(title = "店铺服务团队", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaStorePerson faStorePerson)
    {
        return toAjax(faStorePersonService.insertFaStorePerson(faStorePerson));
    }

    /**
     * 修改店铺服务团队
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:edit')")
    @Log(title = "店铺服务团队", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStorePerson faStorePerson)
    {
        faStorePerson.setStatus(0);
        return toAjax(faStorePersonService.updateFaStorePerson(faStorePerson));
    }
    /**
     * 修改店铺服务团队
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:edit')")
    @Log(title = "店铺服务团队", businessType = BusinessType.UPDATE)
    @PutMapping("/check")
    public AjaxResult check(@RequestBody FaStorePerson faStorePerson)
    {
        return toAjax(faStorePersonService.updateFaStorePerson(faStorePerson));
    }
    /**
     * 删除店铺服务团队
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:remove')")
    @Log(title = "店铺服务团队", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStorePersonService.deleteFaStorePersonByIds(ids));
    }

    /**
     * 查询所有服务团队列表
     */
    @PreAuthorize("@ss.hasPermi('storeinfo:person:list')")
    @GetMapping("/all_list")
    public List<Map<String,Object>> allList() {
        return faStorePersonService.listForSelect();
    }
}
