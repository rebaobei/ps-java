package com.ruoyi.web.controller.goods;

import java.util.List;

import io.swagger.models.auth.In;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopProductSku;
import com.ruoyi.system.service.IFaXshopProductSkuService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品属性Controller
 *
 * @author ruoyi
 * @date 2021-07-19
 */
@RestController
@RequestMapping("/goods/sku")
public class FaXshopProductSkuController extends BaseController
{
    @Autowired
    private IFaXshopProductSkuService faXshopProductSkuService;

    /**
     * 查询商品属性列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FaXshopProductSku faXshopProductSku)
    {
        startPage();
        List<FaXshopProductSku> list = faXshopProductSkuService.selectFaXshopProductSkuList(faXshopProductSku);
        return getDataTable(list);
    }

    /**
     * 导出商品属性列表
     */
    @PreAuthorize("@ss.hasPermi('goods:sku:export')")
    @Log(title = "商品属性", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopProductSku faXshopProductSku)
    {
        List<FaXshopProductSku> list = faXshopProductSkuService.selectFaXshopProductSkuList(faXshopProductSku);
        ExcelUtil<FaXshopProductSku> util = new ExcelUtil<FaXshopProductSku>(FaXshopProductSku.class);
        return util.exportExcel(list, "商品属性数据");
    }

    /**
     * 获取商品属性详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        Integer integer = Integer.valueOf(id);
        return AjaxResult.success(faXshopProductSkuService.selectFaXshopProductSkuById(integer));
    }

    /**
     * 新增商品属性
     */
    @Log(title = "商品属性", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopProductSku faXshopProductSku)
    {
        return toAjax(faXshopProductSkuService.insertFaXshopProductSku(faXshopProductSku));
    }

    /**
     * 修改商品属性
     */
    @Log(title = "商品属性", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopProductSku faXshopProductSku)
    {
        return toAjax(faXshopProductSkuService.updateFaXshopProductSku(faXshopProductSku));
    }

    /**
     * 删除商品属性
     */
    @Log(title = "商品属性", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopProductSkuService.deleteFaXshopProductSkuByIds(ids));
    }

    /**
     * 获取商品属性详细信息
     */
    @GetMapping(value = "list/{id}")
    public AjaxResult getListInfo(@PathVariable("id") String id)
    {
        Integer pId = Integer.valueOf(id);
        return AjaxResult.success(faXshopProductSkuService.listFaXshopProductSkuById(pId));
    }
}
