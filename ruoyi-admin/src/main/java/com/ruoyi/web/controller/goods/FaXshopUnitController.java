package com.ruoyi.web.controller.goods;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaXshopUnit;
import com.ruoyi.system.service.IFaXshopUnitService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 计量单位Controller
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
@RestController
@RequestMapping("/goods/unit")
public class FaXshopUnitController extends BaseController
{
    @Autowired
    private IFaXshopUnitService faXshopUnitService;

    /**
     * 查询计量单位列表
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaXshopUnit faXshopUnit)
    {
        startPage();
        List<FaXshopUnit> list = faXshopUnitService.selectFaXshopUnitList(faXshopUnit);
        return getDataTable(list);
    }

    /**
     * 导出计量单位列表
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:export')")
    @Log(title = "计量单位", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaXshopUnit faXshopUnit)
    {
        List<FaXshopUnit> list = faXshopUnitService.selectFaXshopUnitList(faXshopUnit);
        ExcelUtil<FaXshopUnit> util = new ExcelUtil<FaXshopUnit>(FaXshopUnit.class);
        return util.exportExcel(list, "计量单位数据");
    }

    /**
     * 获取计量单位详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(faXshopUnitService.selectFaXshopUnitById(id));
    }

    /**
     * 新增计量单位
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:add')")
    @Log(title = "计量单位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaXshopUnit faXshopUnit)
    {
        return toAjax(faXshopUnitService.insertFaXshopUnit(faXshopUnit));
    }

    /**
     * 修改计量单位
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:edit')")
    @Log(title = "计量单位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaXshopUnit faXshopUnit)
    {
        return toAjax(faXshopUnitService.updateFaXshopUnit(faXshopUnit));
    }

    /**
     * 删除计量单位
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:remove')")
    @Log(title = "计量单位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(faXshopUnitService.deleteFaXshopUnitByIds(ids));
    }

    /**
     * 修改默认计量单位
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:edit')")
    @Log(title = "修改默认计量单位", businessType = BusinessType.UPDATE)
    @PostMapping("/default")
    public AjaxResult changeDefault(@RequestBody FaXshopUnit unit)
    {
        return toAjax(faXshopUnitService.setDefaultUnit(unit));
    }

    /**
     * 查询计量所有单位列表
     */
    @PreAuthorize("@ss.hasPermi('goods:unit:list')")
    @GetMapping("/all_list")
    public List<FaXshopUnit> allList(FaXshopUnit faXshopUnit)
    {
        return  faXshopUnitService.selectFaXshopUnitList(faXshopUnit);
    }
}
