package com.ruoyi.web.controller.apple;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaAppleGoods;
import com.ruoyi.system.service.IFaAppleGoodsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/apple/goods")
public class  FaAppleGoodsController extends BaseController
{
    @Autowired
    private IFaAppleGoodsService faAppleGoodsService;

    /**
     * 查询商品管理列表
     */
    // 权限判断 验证用户是否具有list权限
    @PreAuthorize("@ss.hasPermi('apple:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaAppleGoods faAppleGoods)
    {
        startPage();
        List<FaAppleGoods> list = faAppleGoodsService.selectFaAppleGoodsList(faAppleGoods);
        return getDataTable(list);
    }

    /**
     * 导出商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('apple:goods:export')")
    //自定义日志记录
    @Log(title = "商品管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaAppleGoods faAppleGoods)
    {
        List<FaAppleGoods> list = faAppleGoodsService.selectFaAppleGoodsList(faAppleGoods);
        ExcelUtil<FaAppleGoods> util = new ExcelUtil<FaAppleGoods>(FaAppleGoods.class);
        return util.exportExcel(list, "商品管理数据");
    }

    /**
     * 获取商品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('apple:goods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faAppleGoodsService.selectFaAppleGoodsById(id));
    }

    /**
     * 新增商品管理
     */
    @PreAuthorize("@ss.hasPermi('apple:goods:add')")
    @Log(title = "商品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaAppleGoods faAppleGoods)
    {
        return toAjax(faAppleGoodsService.insertFaAppleGoods(faAppleGoods));
    }

    /**
     * 修改商品管理
     */
    @PreAuthorize("@ss.hasPermi('apple:goods:edit')")
    @Log(title = "商品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaAppleGoods faAppleGoods)
    {
        return toAjax(faAppleGoodsService.updateFaAppleGoods(faAppleGoods));
    }

    /**
     * 删除商品管理
     */
    @PreAuthorize("@ss.hasPermi('apple:goods:remove')")
    @Log(title = "商品管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faAppleGoodsService.deleteFaAppleGoodsByIds(ids));
    }
}
