package com.ruoyi.web.controller.dynamic;

import java.util.List;

import com.ruoyi.system.domain.vo.UserArticleCommentVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaBallUserarticleComment;
import com.ruoyi.system.service.IFaBallUserarticleCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 朋友圈评论Controller
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@RestController
@RequestMapping("/dynamic/comment")
public class FaBallUserarticleCommentController extends BaseController
{
    @Autowired
    private IFaBallUserarticleCommentService faBallUserarticleCommentService;

    /**
     * 查询朋友圈评论列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaBallUserarticleComment faBallUserarticleComment)
    {
        startPage();
        List<FaBallUserarticleComment> list = faBallUserarticleCommentService.selectFaBallUserarticleCommentList(faBallUserarticleComment);
        return getDataTable(list);
    }

    /**
     * 导出朋友圈评论列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:comment:export')")
    @Log(title = "朋友圈评论", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaBallUserarticleComment faBallUserarticleComment)
    {
        List<FaBallUserarticleComment> list = faBallUserarticleCommentService.selectFaBallUserarticleCommentList(faBallUserarticleComment);
        ExcelUtil<FaBallUserarticleComment> util = new ExcelUtil<FaBallUserarticleComment>(FaBallUserarticleComment.class);
        return util.exportExcel(list, "朋友圈评论数据");
    }

    /**
     * 获取朋友圈评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('dynamic:comment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faBallUserarticleCommentService.selectFaBallUserarticleCommentById(id));
    }

    /**
     * 新增朋友圈评论
     */
    @PreAuthorize("@ss.hasPermi('dynamic:comment:add')")
    @Log(title = "朋友圈评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaBallUserarticleComment faBallUserarticleComment)
    {
        return toAjax(faBallUserarticleCommentService.insertFaBallUserarticleComment(faBallUserarticleComment));
    }

    /**
     * 修改朋友圈评论
     */
    @PreAuthorize("@ss.hasPermi('dynamic:comment:edit')")
    @Log(title = "朋友圈评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaBallUserarticleComment faBallUserarticleComment)
    {
        return toAjax(faBallUserarticleCommentService.updateFaBallUserarticleComment(faBallUserarticleComment));
    }

    /**
     * 删除朋友圈评论
     */
    @PreAuthorize("@ss.hasPermi('dynamic:comment:remove')")
    @Log(title = "朋友圈评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faBallUserarticleCommentService.deleteFaBallUserarticleCommentByIds(ids));
    }

    /**
     * 查询朋友圈评论列表
     */
    @PreAuthorize("@ss.hasPermi('dynamic:comment:list')")
    @GetMapping("/get_list")
    public TableDataInfo myList(FaBallUserarticleComment faBallUserarticleComment)
    {
        startPage();
        List<UserArticleCommentVo> list = faBallUserarticleCommentService.listUserArticleCommentVo(faBallUserarticleComment);
        return getDataTable(list);
    }
}
