package com.ruoyi.web.controller.task;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaUserSign;
import com.ruoyi.system.service.IFaUserSignService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 签到设置积分Controller
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@RestController
@RequestMapping("/task/signsetting")
public class FaUserSignController extends BaseController
{
    @Autowired
    private IFaUserSignService faUserSignService;

    /**
     * 查询签到设置积分列表
     */
    @PreAuthorize("@ss.hasPermi('task:signsetting:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaUserSign faUserSign)
    {
        startPage();
        List<FaUserSign> list = faUserSignService.selectFaUserSignList(faUserSign);
        return getDataTable(list);
    }

    /**
     * 导出签到设置积分列表
     */
    @PreAuthorize("@ss.hasPermi('task:signsetting:export')")
    @Log(title = "签到设置积分", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaUserSign faUserSign)
    {
        List<FaUserSign> list = faUserSignService.selectFaUserSignList(faUserSign);
        ExcelUtil<FaUserSign> util = new ExcelUtil<FaUserSign>(FaUserSign.class);
        return util.exportExcel(list, "签到设置积分数据");
    }

    /**
     * 获取签到设置积分详细信息
     */
    @PreAuthorize("@ss.hasPermi('task:signsetting:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faUserSignService.selectFaUserSignById(id));
    }

    /**
     * 新增签到设置积分
     */
    @PreAuthorize("@ss.hasPermi('task:signsetting:add')")
    @Log(title = "签到设置积分", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaUserSign faUserSign)
    {
        return toAjax(faUserSignService.insertFaUserSign(faUserSign));
    }

    /**
     * 修改签到设置积分
     */
    @PreAuthorize("@ss.hasPermi('task:signsetting:edit')")
    @Log(title = "签到设置积分", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaUserSign faUserSign)
    {
        return toAjax(faUserSignService.updateFaUserSign(faUserSign));
    }

    /**
     * 删除签到设置积分
     */
    @PreAuthorize("@ss.hasPermi('task:signsetting:remove')")
    @Log(title = "签到设置积分", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faUserSignService.deleteFaUserSignByIds(ids));
    }
}
