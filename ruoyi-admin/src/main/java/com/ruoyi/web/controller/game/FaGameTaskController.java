package com.ruoyi.web.controller.game;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGameTask;
import com.ruoyi.system.service.IFaGameTaskService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 每日任务Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/daytask")
public class FaGameTaskController extends BaseController
{
    @Autowired
    private IFaGameTaskService faGameTaskService;

    /**
     * 查询每日任务列表
     */
    @PreAuthorize("@ss.hasPermi('game:daytask:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGameTask faGameTask)
    {
        startPage();
        List<FaGameTask> list = faGameTaskService.selectFaGameTaskList(faGameTask);
        return getDataTable(list);
    }

    /**
     * 导出每日任务列表
     */
    @PreAuthorize("@ss.hasPermi('game:daytask:export')")
    @Log(title = "每日任务", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGameTask faGameTask)
    {
        List<FaGameTask> list = faGameTaskService.selectFaGameTaskList(faGameTask);
        ExcelUtil<FaGameTask> util = new ExcelUtil<FaGameTask>(FaGameTask.class);
        return util.exportExcel(list, "每日任务数据");
    }

    /**
     * 获取每日任务详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:daytask:query')")
    @GetMapping(value = "/{dayTaskId}")
    public AjaxResult getInfo(@PathVariable("dayTaskId") Long dayTaskId)
    {
        return AjaxResult.success(faGameTaskService.selectFaGameTaskById(dayTaskId));
    }

    /**
     * 新增每日任务
     */
    @PreAuthorize("@ss.hasPermi('game:daytask:add')")
    @Log(title = "每日任务", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGameTask faGameTask)
    {
        return toAjax(faGameTaskService.insertFaGameTask(faGameTask));
    }

    /**
     * 修改每日任务
     */
    @PreAuthorize("@ss.hasPermi('game:daytask:edit')")
    @Log(title = "每日任务", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGameTask faGameTask)
    {
        return toAjax(faGameTaskService.updateFaGameTask(faGameTask));
    }

    /**
     * 删除每日任务
     */
    @PreAuthorize("@ss.hasPermi('game:daytask:remove')")
    @Log(title = "每日任务", businessType = BusinessType.DELETE)
	@DeleteMapping("/{dayTaskIds}")
    public AjaxResult remove(@PathVariable Long[] dayTaskIds)
    {
        return toAjax(faGameTaskService.deleteFaGameTaskByIds(dayTaskIds));
    }

    /**
     * 查询每日任务列表 不分页
     */
    @PreAuthorize("@ss.hasPermi('game:daytask:list')")
    @GetMapping("/all_list")
    public List<FaGameTask> allList(FaGameTask faGameTask)
    {
       return faGameTaskService.selectFaGameTaskList(faGameTask);
    }
}
