package com.ruoyi.web.controller.game;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaGamePets;
import com.ruoyi.system.service.IFaGamePetsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 宠物管理Controller
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@RestController
@RequestMapping("/game/pets")
public class FaGamePetsController extends BaseController
{
    @Autowired
    private IFaGamePetsService faGamePetsService;

    /**
     * 查询宠物管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:pets:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaGamePets faGamePets)
    {
        startPage();
        List<FaGamePets> list = faGamePetsService.selectFaGamePetsList(faGamePets);
        return getDataTable(list);
    }

    /**
     * 导出宠物管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:pets:export')")
    @Log(title = "宠物管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaGamePets faGamePets)
    {
        List<FaGamePets> list = faGamePetsService.selectFaGamePetsList(faGamePets);
        ExcelUtil<FaGamePets> util = new ExcelUtil<FaGamePets>(FaGamePets.class);
        return util.exportExcel(list, "宠物管理数据");
    }

    /**
     * 获取宠物管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('game:pets:query')")
    @GetMapping(value = "/{petId}")
    public AjaxResult getInfo(@PathVariable("petId") Long petId)
    {
        return AjaxResult.success(faGamePetsService.selectFaGamePetsById(petId));
    }

    /**
     * 新增宠物管理
     */
    @PreAuthorize("@ss.hasPermi('game:pets:add')")
    @Log(title = "宠物管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FaGamePets faGamePets)
    {
        return toAjax(faGamePetsService.insertFaGamePets(faGamePets));
    }

    /**
     * 修改宠物管理
     */
    @PreAuthorize("@ss.hasPermi('game:pets:edit')")
    @Log(title = "宠物管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaGamePets faGamePets)
    {
        return toAjax(faGamePetsService.updateFaGamePets(faGamePets));
    }

    /**
     * 删除宠物管理
     */
    @PreAuthorize("@ss.hasPermi('game:pets:remove')")
    @Log(title = "宠物管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{petIds}")
    public AjaxResult remove(@PathVariable Long[] petIds)
    {
        return toAjax(faGamePetsService.deleteFaGamePetsByIds(petIds));
    }

    /**
     * 查询宠物管理列表
     */
    @PreAuthorize("@ss.hasPermi('game:pets:list')")
    @GetMapping("/all_list")
    public List<FaGamePets> allList(FaGamePets faGamePets)
    {
        return faGamePetsService.selectFaGamePetsList(faGamePets);
    }
}
