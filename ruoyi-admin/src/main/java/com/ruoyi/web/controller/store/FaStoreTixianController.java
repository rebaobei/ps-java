package com.ruoyi.web.controller.store;

import java.beans.Transient;
import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FaStoreTixian;
import com.ruoyi.system.service.IFaStoreTixianService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 提现管理Controller
 * 
 * @author ruoyi
 * @date 2022-02-09
 */
@RestController
@RequestMapping("/store/tixian")
public class FaStoreTixianController extends BaseController
{
    @Autowired
    private IFaStoreTixianService faStoreTixianService;

    /**
     * 查询提现管理列表
     */
    @PreAuthorize("@ss.hasPermi('store:tixian:list')")
    @GetMapping("/list")
    public TableDataInfo list(FaStoreTixian faStoreTixian)
    {
        startPage();
        List<FaStoreTixian> list = faStoreTixianService.selectFaStoreTixianList(faStoreTixian);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('store:tixian:list')")
    @GetMapping("/getMylist")
    public TableDataInfo getMylist(FaStoreTixian faStoreTixian)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreTixian.setManystorePid(storeInfoId);
        startPage();
        List<FaStoreTixian> list = faStoreTixianService.selectFaStoreTixianList(faStoreTixian);
        return getDataTable(list);
    }

    /**
     * 导出提现管理列表
     */
    @PreAuthorize("@ss.hasPermi('store:tixian:export')")
    @Log(title = "提现管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FaStoreTixian faStoreTixian)
    {
        List<FaStoreTixian> list = faStoreTixianService.selectFaStoreTixianList(faStoreTixian);
        ExcelUtil<FaStoreTixian> util = new ExcelUtil<FaStoreTixian>(FaStoreTixian.class);
        return util.exportExcel(list, "提现管理数据");
    }

    /**
     * 获取提现管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:tixian:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(faStoreTixianService.selectFaStoreTixianById(id));
    }

    /**
     * 新增提现管理
     */
    @PreAuthorize("@ss.hasPermi('store:tixian:add')")
    @Log(title = "提现管理", businessType = BusinessType.INSERT)
    @PostMapping
    @Transactional
    public AjaxResult add(@RequestBody FaStoreTixian faStoreTixian)
    {   Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreTixian.setManystorePid(storeInfoId);
        return toAjax(faStoreTixianService.insertFaStoreTixian(faStoreTixian));
    }

    /**
     * 修改提现管理
     */
    @PreAuthorize("@ss.hasPermi('store:tixian:edit')")
    @Log(title = "提现管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FaStoreTixian faStoreTixian)
    {
        return toAjax(faStoreTixianService.updateFaStoreTixian(faStoreTixian));
    }

    /**
     * 删除提现管理
     */
    @PreAuthorize("@ss.hasPermi('store:tixian:remove')")
    @Log(title = "提现管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(faStoreTixianService.deleteFaStoreTixianByIds(ids));
    }
}
