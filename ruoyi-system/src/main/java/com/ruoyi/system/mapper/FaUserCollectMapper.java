package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserCollect;

/**
 * 会员收藏Mapper接口
 * 
 * @author fan
 * @date 2021-07-14
 */
public interface FaUserCollectMapper 
{
    /**
     * 查询会员收藏
     * 
     * @param id 会员收藏ID
     * @return 会员收藏
     */
    public FaUserCollect selectFaUserCollectById(Long id);

    /**
     * 查询会员收藏列表
     * 
     * @param faUserCollect 会员收藏
     * @return 会员收藏集合
     */
    public List<FaUserCollect> selectFaUserCollectList(FaUserCollect faUserCollect);

    /**
     * 新增会员收藏
     * 
     * @param faUserCollect 会员收藏
     * @return 结果
     */
    public int insertFaUserCollect(FaUserCollect faUserCollect);

    /**
     * 修改会员收藏
     * 
     * @param faUserCollect 会员收藏
     * @return 结果
     */
    public int updateFaUserCollect(FaUserCollect faUserCollect);

    /**
     * 删除会员收藏
     * 
     * @param id 会员收藏ID
     * @return 结果
     */
    public int deleteFaUserCollectById(Long id);

    /**
     * 批量删除会员收藏
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserCollectByIds(Long[] ids);
}
