package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGamePetsImage;
import com.ruoyi.system.domain.vo.GamePetsImageVo;

/**
 * 装扮管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface FaGamePetsImageMapper 
{
    /**
     * 查询装扮管理
     * 
     * @param id 装扮管理ID
     * @return 装扮管理
     */
    public FaGamePetsImage selectFaGamePetsImageById(Long id);

    /**
     * 查询装扮管理列表
     * 
     * @param faGamePetsImage 装扮管理
     * @return 装扮管理集合
     */
    public List<FaGamePetsImage> selectFaGamePetsImageList(FaGamePetsImage faGamePetsImage);

    /**
     * 新增装扮管理
     * 
     * @param faGamePetsImage 装扮管理
     * @return 结果
     */
    public int insertFaGamePetsImage(FaGamePetsImage faGamePetsImage);

    /**
     * 修改装扮管理
     * 
     * @param faGamePetsImage 装扮管理
     * @return 结果
     */
    public int updateFaGamePetsImage(FaGamePetsImage faGamePetsImage);

    /**
     * 删除装扮管理
     * 
     * @param id 装扮管理ID
     * @return 结果
     */
    public int deleteFaGamePetsImageById(Long id);

    /**
     * 批量删除装扮管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGamePetsImageByIds(Long[] ids);

    public List<GamePetsImageVo> listPetsImageVo(FaGamePetsImage faGamePetsImage);

}
