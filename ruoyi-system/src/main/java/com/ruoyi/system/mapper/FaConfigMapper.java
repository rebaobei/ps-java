package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaConfig;
import com.ruoyi.system.domain.vo.CommonSettingConfigVo;

/**
 * App配置Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-14
 */
public interface FaConfigMapper 
{
    /**
     * 查询App配置
     * 
     * @param id App配置ID
     * @return App配置
     */
    public FaConfig selectFaConfigById(Integer id);

    /**
     * 查询App配置列表
     * 
     * @param faConfig App配置
     * @return App配置集合
     */
    public List<FaConfig> selectFaConfigList(FaConfig faConfig);

    /**
     * 新增App配置
     * 
     * @param faConfig App配置
     * @return 结果
     */
    public int insertFaConfig(FaConfig faConfig);

    /**
     * 修改App配置
     * 
     * @param faConfig App配置
     * @return 结果
     */
    public int updateFaConfig(FaConfig faConfig);

    /**
     * 删除App配置
     * 
     * @param id App配置ID
     * @return 结果
     */
    public int deleteFaConfigById(Integer id);

    /**
     * 批量删除App配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaConfigByIds(Integer[] ids);

    List<CommonSettingConfigVo> listCommonSettingConfigVo(String type);
}
