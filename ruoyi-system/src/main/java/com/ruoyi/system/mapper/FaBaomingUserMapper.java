package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaBaomingUser;
import com.ruoyi.system.domain.vo.BaomingUserVo;

/**
 * 报名管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface FaBaomingUserMapper 
{
    /**
     * 查询报名管理
     * 
     * @param id 报名管理ID
     * @return 报名管理
     */
    public FaBaomingUser selectFaBaomingUserById(Long id);

    /**
     * 查询报名管理列表
     * 
     * @param faBaomingUser 报名管理
     * @return 报名管理集合
     */
    public List<FaBaomingUser> selectFaBaomingUserList(FaBaomingUser faBaomingUser);

    /**
     * 新增报名管理
     * 
     * @param faBaomingUser 报名管理
     * @return 结果
     */
    public int insertFaBaomingUser(FaBaomingUser faBaomingUser);

    /**
     * 修改报名管理
     * 
     * @param faBaomingUser 报名管理
     * @return 结果
     */
    public int updateFaBaomingUser(FaBaomingUser faBaomingUser);

    /**
     * 删除报名管理
     * 
     * @param id 报名管理ID
     * @return 结果
     */
    public int deleteFaBaomingUserById(Long id);

    /**
     * 批量删除报名管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaBaomingUserByIds(Long[] ids);

    List<BaomingUserVo> listBaomingUserVo(BaomingUserVo baomingUser);
}
