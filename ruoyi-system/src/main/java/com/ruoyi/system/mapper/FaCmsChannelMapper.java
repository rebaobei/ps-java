package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaCmsChannel;

/**
 * 分类管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
public interface FaCmsChannelMapper 
{
    /**
     * 查询分类管理
     * 
     * @param id 分类管理ID
     * @return 分类管理
     */
    public FaCmsChannel selectFaCmsChannelById(Integer id);

    /**
     * 查询分类管理列表
     * 
     * @param faCmsChannel 分类管理
     * @return 分类管理集合
     */
    public List<FaCmsChannel> selectFaCmsChannelList(FaCmsChannel faCmsChannel);

    /**
     * 新增分类管理
     * 
     * @param faCmsChannel 分类管理
     * @return 结果
     */
    public int insertFaCmsChannel(FaCmsChannel faCmsChannel);

    /**
     * 修改分类管理
     * 
     * @param faCmsChannel 分类管理
     * @return 结果
     */
    public int updateFaCmsChannel(FaCmsChannel faCmsChannel);

    /**
     * 删除分类管理
     * 
     * @param id 分类管理ID
     * @return 结果
     */
    public int deleteFaCmsChannelById(Integer id);

    /**
     * 批量删除分类管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaCmsChannelByIds(Integer[] ids);
}
