package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserSignlog;

/**
 * 签到明细Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface FaUserSignlogMapper 
{
    /**
     * 查询签到明细
     * 
     * @param id 签到明细ID
     * @return 签到明细
     */
    public FaUserSignlog selectFaUserSignlogById(Long id);

    /**
     * 查询签到明细列表
     * 
     * @param faUserSignlog 签到明细
     * @return 签到明细集合
     */
    public List<FaUserSignlog> selectFaUserSignlogList(FaUserSignlog faUserSignlog);

    /**
     * 新增签到明细
     * 
     * @param faUserSignlog 签到明细
     * @return 结果
     */
    public int insertFaUserSignlog(FaUserSignlog faUserSignlog);

    /**
     * 修改签到明细
     * 
     * @param faUserSignlog 签到明细
     * @return 结果
     */
    public int updateFaUserSignlog(FaUserSignlog faUserSignlog);

    /**
     * 删除签到明细
     * 
     * @param id 签到明细ID
     * @return 结果
     */
    public int deleteFaUserSignlogById(Long id);

    /**
     * 批量删除签到明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserSignlogByIds(Long[] ids);
}
