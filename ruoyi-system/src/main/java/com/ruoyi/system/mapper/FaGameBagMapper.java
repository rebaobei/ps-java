package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGameBag;
import com.ruoyi.system.domain.vo.GameBagVo;

/**
 * 背包管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface FaGameBagMapper 
{
    /**
     * 查询背包管理
     * 
     * @param id 背包管理ID
     * @return 背包管理
     */
    public FaGameBag selectFaGameBagById(Long id);

    /**
     * 查询背包管理列表
     * 
     * @param faGameBag 背包管理
     * @return 背包管理集合
     */
    public List<FaGameBag> selectFaGameBagList(FaGameBag faGameBag);

    /**
     * 新增背包管理
     * 
     * @param faGameBag 背包管理
     * @return 结果
     */
    public int insertFaGameBag(FaGameBag faGameBag);

    /**
     * 修改背包管理
     * 
     * @param faGameBag 背包管理
     * @return 结果
     */
    public int updateFaGameBag(FaGameBag faGameBag);

    /**
     * 删除背包管理
     * 
     * @param id 背包管理ID
     * @return 结果
     */
    public int deleteFaGameBagById(Long id);

    /**
     * 批量删除背包管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGameBagByIds(Long[] ids);

    List<GameBagVo> listGameBagVo(GameBagVo faGameBag);
}
