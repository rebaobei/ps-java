package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaAppleGoods;

/**
 * 商品管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaAppleGoodsMapper 
{
    /**
     * 查询商品管理
     * 
     * @param id 商品管理ID
     * @return 商品管理
     */
    public FaAppleGoods selectFaAppleGoodsById(Long id);

    /**
     * 查询商品管理列表
     * 
     * @param faAppleGoods 商品管理
     * @return 商品管理集合
     */
    public List<FaAppleGoods> selectFaAppleGoodsList(FaAppleGoods faAppleGoods);

    /**
     * 新增商品管理
     * 
     * @param faAppleGoods 商品管理
     * @return 结果
     */
    public int insertFaAppleGoods(FaAppleGoods faAppleGoods);

    /**
     * 修改商品管理
     * 
     * @param faAppleGoods 商品管理
     * @return 结果
     */
    public int updateFaAppleGoods(FaAppleGoods faAppleGoods);

    /**
     * 删除商品管理
     * 
     * @param id 商品管理ID
     * @return 结果
     */
    public int deleteFaAppleGoodsById(Long id);

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaAppleGoodsByIds(Long[] ids);
}
