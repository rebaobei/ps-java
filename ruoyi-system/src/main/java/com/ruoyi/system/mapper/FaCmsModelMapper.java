package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaCmsModel;

/**
 * 内容模型Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
public interface FaCmsModelMapper 
{
    /**
     * 查询内容模型
     * 
     * @param id 内容模型ID
     * @return 内容模型
     */
    public FaCmsModel selectFaCmsModelById(Integer id);

    /**
     * 查询内容模型列表
     * 
     * @param faCmsModel 内容模型
     * @return 内容模型集合
     */
    public List<FaCmsModel> selectFaCmsModelList(FaCmsModel faCmsModel);

    /**
     * 新增内容模型
     * 
     * @param faCmsModel 内容模型
     * @return 结果
     */
    public int insertFaCmsModel(FaCmsModel faCmsModel);

    /**
     * 修改内容模型
     * 
     * @param faCmsModel 内容模型
     * @return 结果
     */
    public int updateFaCmsModel(FaCmsModel faCmsModel);

    /**
     * 删除内容模型
     * 
     * @param id 内容模型ID
     * @return 结果
     */
    public int deleteFaCmsModelById(Integer id);

    /**
     * 批量删除内容模型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaCmsModelByIds(Integer[] ids);
}
