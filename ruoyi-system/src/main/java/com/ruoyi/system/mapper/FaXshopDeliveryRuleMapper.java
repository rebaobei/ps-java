package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopDeliveryRule;
import org.apache.ibatis.annotations.Param;

/**
 * 运费规则Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-20
 */
public interface FaXshopDeliveryRuleMapper 
{
    /**
     * 查询运费规则
     * 
     * @param id 运费规则ID
     * @return 运费规则
     */
    public FaXshopDeliveryRule selectFaXshopDeliveryRuleById(Integer id);

    /**
     * 查询运费规则列表
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 运费规则集合
     */
    public List<FaXshopDeliveryRule> selectFaXshopDeliveryRuleList(FaXshopDeliveryRule faXshopDeliveryRule);

    /**
     * 新增运费规则
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 结果
     */
    public int insertFaXshopDeliveryRule(FaXshopDeliveryRule faXshopDeliveryRule);

    /**
     * 修改运费规则
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 结果
     */
    public int updateFaXshopDeliveryRule(FaXshopDeliveryRule faXshopDeliveryRule);

    /**
     * 删除运费规则
     * 
     * @param id 运费规则ID
     * @return 结果
     */
    public int deleteFaXshopDeliveryRuleById(Integer id);

    /**
     * 批量删除运费规则
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopDeliveryRuleByIds(Integer[] ids);


    List<FaXshopDeliveryRule> listRuleByTemplateId(@Param("id") Integer id);
}
