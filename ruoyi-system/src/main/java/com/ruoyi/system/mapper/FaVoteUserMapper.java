package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaVoteUser;
import com.ruoyi.system.domain.vo.VoteUserVo;

/**
 * 参赛人员Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface FaVoteUserMapper 
{
    /**
     * 查询参赛人员
     * 
     * @param id 参赛人员ID
     * @return 参赛人员
     */
    public FaVoteUser selectFaVoteUserById(Long id);

    /**
     * 查询参赛人员列表
     * 
     * @param faVoteUser 参赛人员
     * @return 参赛人员集合
     */
    public List<FaVoteUser> selectFaVoteUserList(FaVoteUser faVoteUser);

    /**
     * 新增参赛人员
     * 
     * @param faVoteUser 参赛人员
     * @return 结果
     */
    public int insertFaVoteUser(FaVoteUser faVoteUser);

    /**
     * 修改参赛人员
     * 
     * @param faVoteUser 参赛人员
     * @return 结果
     */
    public int updateFaVoteUser(FaVoteUser faVoteUser);

    /**
     * 删除参赛人员
     * 
     * @param id 参赛人员ID
     * @return 结果
     */
    public int deleteFaVoteUserById(Long id);

    /**
     * 批量删除参赛人员
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaVoteUserByIds(Long[] ids);

    List<VoteUserVo> listVoteUserVo(VoteUserVo voteUser);
}
