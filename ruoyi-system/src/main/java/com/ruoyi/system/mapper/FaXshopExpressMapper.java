package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopExpress;

/**
 * 快递公司Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
public interface FaXshopExpressMapper 
{
    /**
     * 查询快递公司
     * 
     * @param id 快递公司ID
     * @return 快递公司
     */
    public FaXshopExpress selectFaXshopExpressById(Integer id);

    /**
     * 查询快递公司列表
     * 
     * @param faXshopExpress 快递公司
     * @return 快递公司集合
     */
    public List<FaXshopExpress> selectFaXshopExpressList(FaXshopExpress faXshopExpress);

    /**
     * 新增快递公司
     * 
     * @param faXshopExpress 快递公司
     * @return 结果
     */
    public int insertFaXshopExpress(FaXshopExpress faXshopExpress);

    /**
     * 修改快递公司
     * 
     * @param faXshopExpress 快递公司
     * @return 结果
     */
    public int updateFaXshopExpress(FaXshopExpress faXshopExpress);

    /**
     * 删除快递公司
     * 
     * @param id 快递公司ID
     * @return 结果
     */
    public int deleteFaXshopExpressById(Integer id);

    /**
     * 批量删除快递公司
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopExpressByIds(Integer[] ids);
}
