package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopProduct;
import com.ruoyi.system.domain.vo.ProductVo;
import com.ruoyi.system.domain.vo.XshopOrderProductVo;

/**
 * 商品Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-16
 */
public interface FaXshopProductMapper 
{
    /**
     * 查询商品
     * 
     * @param id 商品ID
     * @return 商品
     */
    public FaXshopProduct selectFaXshopProductById(Integer id);

    /**
     * 查询商品列表
     * 
     * @param faXshopProduct 商品
     * @return 商品集合
     */
    public List<FaXshopProduct> selectFaXshopProductList(FaXshopProduct faXshopProduct);

    /**
     * 新增商品
     * 
     * @param faXshopProduct 商品
     * @return 结果
     */
    public int insertFaXshopProduct(FaXshopProduct faXshopProduct);

    /**
     * 修改商品
     * 
     * @param faXshopProduct 商品
     * @return 结果
     */
    public int updateFaXshopProduct(FaXshopProduct faXshopProduct);

    /**
     * 删除商品
     * 
     * @param id 商品ID
     * @return 结果
     */
    public int deleteFaXshopProductById(Integer id);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopProductByIds(Integer[] ids);

    List<ProductVo> listProductVo(FaXshopProduct product);
}
