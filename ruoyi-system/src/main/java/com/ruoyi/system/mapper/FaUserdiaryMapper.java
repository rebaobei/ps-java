package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserdiary;
import com.ruoyi.system.domain.vo.UserDiaryVo;

/**
 * 宠物日记Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface FaUserdiaryMapper 
{
    /**
     * 查询宠物日记
     * 
     * @param id 宠物日记ID
     * @return 宠物日记
     */
    public FaUserdiary selectFaUserdiaryById(Long id);

    /**
     * 查询宠物日记列表
     * 
     * @param faUserdiary 宠物日记
     * @return 宠物日记集合
     */
    public List<FaUserdiary> selectFaUserdiaryList(FaUserdiary faUserdiary);

    /**
     * 新增宠物日记
     * 
     * @param faUserdiary 宠物日记
     * @return 结果
     */
    public int insertFaUserdiary(FaUserdiary faUserdiary);

    /**
     * 修改宠物日记
     * 
     * @param faUserdiary 宠物日记
     * @return 结果
     */
    public int updateFaUserdiary(FaUserdiary faUserdiary);

    /**
     * 删除宠物日记
     * 
     * @param id 宠物日记ID
     * @return 结果
     */
    public int deleteFaUserdiaryById(Long id);

    /**
     * 批量删除宠物日记
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserdiaryByIds(Long[] ids);

    List<UserDiaryVo> listUserDiaryVo(UserDiaryVo vo);
}
