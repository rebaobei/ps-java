package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaPaymentlist;

/**
 * 支付类型Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaPaymentlistMapper 
{
    /**
     * 查询支付类型
     * 
     * @param id 支付类型ID
     * @return 支付类型
     */
    public FaPaymentlist selectFaPaymentlistById(Long id);

    /**
     * 查询支付类型列表
     * 
     * @param faPaymentlist 支付类型
     * @return 支付类型集合
     */
    public List<FaPaymentlist> selectFaPaymentlistList(FaPaymentlist faPaymentlist);

    /**
     * 新增支付类型
     * 
     * @param faPaymentlist 支付类型
     * @return 结果
     */
    public int insertFaPaymentlist(FaPaymentlist faPaymentlist);

    /**
     * 修改支付类型
     * 
     * @param faPaymentlist 支付类型
     * @return 结果
     */
    public int updateFaPaymentlist(FaPaymentlist faPaymentlist);

    /**
     * 删除支付类型
     * 
     * @param id 支付类型ID
     * @return 结果
     */
    public int deleteFaPaymentlistById(Long id);

    /**
     * 批量删除支付类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaPaymentlistByIds(Long[] ids);
}
