package com.ruoyi.system.mapper;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/8
 **/
public interface StatisticMapper {

    /**
     * 预约订单金额
     */
    public BigDecimal yuyueOrderMoney(@Param("startTime") Long startTime);


    /**
     * 升级订单金额
     */
    public BigDecimal levelUpOrderMoney(@Param("startTime") Long startTime);


    /**
     * 充值用户数量
     */
    public Integer chargeUserCount(@Param("startTime") Long startTime);

    /**
     * 充值金额
     */
    public BigDecimal chargeMoney(@Param("startTime") Long startTime);

    /**
     *  余额等第三方产品消费总额
     */
    public BigDecimal consumeMoney(@Param("startTime") Long startTime);


    /**
     * 消费用户数量
     */
    public Integer consumeUserCount(@Param("startTime") Long startTime);

    /**
     * 注册用户留存百分比
     */
    // public double saveUserPercent();

    // /**
    //  * 老用户流失数据
    //  * @return
    //  */
    // public double saveUserPercent();






}
