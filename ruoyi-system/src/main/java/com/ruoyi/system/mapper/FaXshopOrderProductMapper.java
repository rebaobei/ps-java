package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopOrderProduct;
import com.ruoyi.system.domain.vo.XshopOrderProductVo;

/**
 * 订单商品Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface FaXshopOrderProductMapper 
{
    /**
     * 查询订单商品
     * 
     * @param id 订单商品ID
     * @return 订单商品
     */
    public FaXshopOrderProduct selectFaXshopOrderProductById(Integer id);

    /**
     * 查询订单商品列表
     * 
     * @param faXshopOrderProduct 订单商品
     * @return 订单商品集合
     */
    public List<FaXshopOrderProduct> selectFaXshopOrderProductList(FaXshopOrderProduct faXshopOrderProduct);

    /**
     * 新增订单商品
     * 
     * @param faXshopOrderProduct 订单商品
     * @return 结果
     */
    public int insertFaXshopOrderProduct(FaXshopOrderProduct faXshopOrderProduct);

    /**
     * 修改订单商品
     * 
     * @param faXshopOrderProduct 订单商品
     * @return 结果
     */
    public int updateFaXshopOrderProduct(FaXshopOrderProduct faXshopOrderProduct);

    /**
     * 删除订单商品
     * 
     * @param id 订单商品ID
     * @return 结果
     */
    public int deleteFaXshopOrderProductById(Integer id);

    /**
     * 批量删除订单商品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopOrderProductByIds(Integer[] ids);

    List<XshopOrderProductVo> listXshopOrderProductVo(XshopOrderProductVo faXshopOrderProduct);
}
