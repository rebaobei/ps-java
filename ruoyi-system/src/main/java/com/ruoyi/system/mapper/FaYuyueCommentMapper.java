package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaYuyueComment;
import com.ruoyi.system.domain.vo.YuYueCommentVo;

/**
 * 预约订单评论Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
public interface FaYuyueCommentMapper 
{
    /**
     * 查询预约订单评论
     * 
     * @param id 预约订单评论ID
     * @return 预约订单评论
     */
    public FaYuyueComment selectFaYuyueCommentById(Long id);

    /**
     * 查询预约订单评论列表
     * 
     * @param faYuyueComment 预约订单评论
     * @return 预约订单评论集合
     */
    public List<FaYuyueComment> selectFaYuyueCommentList(FaYuyueComment faYuyueComment);

    /**
     * 新增预约订单评论
     * 
     * @param faYuyueComment 预约订单评论
     * @return 结果
     */
    public int insertFaYuyueComment(FaYuyueComment faYuyueComment);

    /**
     * 修改预约订单评论
     * 
     * @param faYuyueComment 预约订单评论
     * @return 结果
     */
    public int updateFaYuyueComment(FaYuyueComment faYuyueComment);

    /**
     * 删除预约订单评论
     * 
     * @param id 预约订单评论ID
     * @return 结果
     */
    public int deleteFaYuyueCommentById(Long id);

    /**
     * 批量删除预约订单评论
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaYuyueCommentByIds(Long[] ids);

    public List<YuYueCommentVo> listCommentVo(FaYuyueComment faYuyueComment);
}
