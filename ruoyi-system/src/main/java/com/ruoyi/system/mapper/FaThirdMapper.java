package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaThird;
import com.ruoyi.system.domain.vo.ThirdLogWithUserInfo;

/**
 * 第三方登录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface FaThirdMapper 
{
    /**
     * 查询第三方登录
     * 
     * @param id 第三方登录ID
     * @return 第三方登录
     */
    public FaThird selectFaThirdById(Integer id);

    /**
     * 查询第三方登录列表
     * 
     * @param faThird 第三方登录
     * @return 第三方登录集合
     */
    public List<FaThird> selectFaThirdList(FaThird faThird);

    /**
     * 新增第三方登录
     * 
     * @param faThird 第三方登录
     * @return 结果
     */
    public int insertFaThird(FaThird faThird);

    /**
     * 修改第三方登录
     * 
     * @param faThird 第三方登录
     * @return 结果
     */
    public int updateFaThird(FaThird faThird);

    /**
     * 删除第三方登录
     * 
     * @param id 第三方登录ID
     * @return 结果
     */
    public int deleteFaThirdById(Integer id);

    /**
     * 批量删除第三方登录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaThirdByIds(Integer[] ids);

    List<ThirdLogWithUserInfo> listThirdLog(FaThird third);
}
