package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopmsgMessagesType;

/**
 * 会员消息类型Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaXshopmsgMessagesTypeMapper 
{
    /**
     * 查询会员消息类型
     * 
     * @param id 会员消息类型ID
     * @return 会员消息类型
     */
    public FaXshopmsgMessagesType selectFaXshopmsgMessagesTypeById(Long id);

    /**
     * 查询会员消息类型列表
     * 
     * @param faXshopmsgMessagesType 会员消息类型
     * @return 会员消息类型集合
     */
    public List<FaXshopmsgMessagesType> selectFaXshopmsgMessagesTypeList(FaXshopmsgMessagesType faXshopmsgMessagesType);

    /**
     * 新增会员消息类型
     * 
     * @param faXshopmsgMessagesType 会员消息类型
     * @return 结果
     */
    public int insertFaXshopmsgMessagesType(FaXshopmsgMessagesType faXshopmsgMessagesType);

    /**
     * 修改会员消息类型
     * 
     * @param faXshopmsgMessagesType 会员消息类型
     * @return 结果
     */
    public int updateFaXshopmsgMessagesType(FaXshopmsgMessagesType faXshopmsgMessagesType);

    /**
     * 删除会员消息类型
     * 
     * @param id 会员消息类型ID
     * @return 结果
     */
    public int deleteFaXshopmsgMessagesTypeById(Long id);

    /**
     * 批量删除会员消息类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopmsgMessagesTypeByIds(Long[] ids);
}
