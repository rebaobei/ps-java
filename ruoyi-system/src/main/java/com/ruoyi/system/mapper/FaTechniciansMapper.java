package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaTechnicians;

/**
 * 宠物分类Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
public interface FaTechniciansMapper 
{
    /**
     * 查询宠物分类
     * 
     * @param id 宠物分类ID
     * @return 宠物分类
     */
    public FaTechnicians selectFaTechniciansById(Long id);

    /**
     * 查询宠物分类列表
     * 
     * @param faTechnicians 宠物分类
     * @return 宠物分类集合
     */
    public List<FaTechnicians> selectFaTechniciansList(FaTechnicians faTechnicians);

    /**
     * 新增宠物分类
     * 
     * @param faTechnicians 宠物分类
     * @return 结果
     */
    public int insertFaTechnicians(FaTechnicians faTechnicians);

    /**
     * 修改宠物分类
     * 
     * @param faTechnicians 宠物分类
     * @return 结果
     */
    public int updateFaTechnicians(FaTechnicians faTechnicians);

    /**
     * 删除宠物分类
     * 
     * @param id 宠物分类ID
     * @return 结果
     */
    public int deleteFaTechniciansById(Long id);

    /**
     * 批量删除宠物分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaTechniciansByIds(Long[] ids);
}
