package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaStoreBrand;

/**
 * 品牌信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
public interface FaStoreBrandMapper 
{
    /**
     * 查询品牌信息
     * 
     * @param id 品牌信息ID
     * @return 品牌信息
     */
    public FaStoreBrand selectFaStoreBrandById(Long id);

    /**
     * 查询品牌信息列表
     * 
     * @param faStoreBrand 品牌信息
     * @return 品牌信息集合
     */
    public List<FaStoreBrand> selectFaStoreBrandList(FaStoreBrand faStoreBrand);

    /**
     * 新增品牌信息
     * 
     * @param faStoreBrand 品牌信息
     * @return 结果
     */
    public int insertFaStoreBrand(FaStoreBrand faStoreBrand);

    /**
     * 修改品牌信息
     * 
     * @param faStoreBrand 品牌信息
     * @return 结果
     */
    public int updateFaStoreBrand(FaStoreBrand faStoreBrand);

    /**
     * 删除品牌信息
     * 
     * @param id 品牌信息ID
     * @return 结果
     */
    public int deleteFaStoreBrandById(Long id);

    /**
     * 批量删除品牌信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaStoreBrandByIds(Long[] ids);
}
