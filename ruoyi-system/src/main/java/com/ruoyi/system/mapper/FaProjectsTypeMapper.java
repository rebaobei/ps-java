package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaProjectsType;
import com.ruoyi.system.domain.vo.ProjectTypeVo;

/**
 * 宠物大小管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
public interface FaProjectsTypeMapper 
{
    /**
     * 查询宠物大小管理
     * 
     * @param id 宠物大小管理ID
     * @return 宠物大小管理
     */
    public FaProjectsType selectFaProjectsTypeById(Long id);

    /**
     * 查询宠物大小管理列表
     * 
     * @param faProjectsType 宠物大小管理
     * @return 宠物大小管理集合
     */
    public List<FaProjectsType> selectFaProjectsTypeList(FaProjectsType faProjectsType);

    /**
     * 新增宠物大小管理
     * 
     * @param faProjectsType 宠物大小管理
     * @return 结果
     */
    public int insertFaProjectsType(FaProjectsType faProjectsType);

    /**
     * 修改宠物大小管理
     * 
     * @param faProjectsType 宠物大小管理
     * @return 结果
     */
    public int updateFaProjectsType(FaProjectsType faProjectsType);

    /**
     * 删除宠物大小管理
     * 
     * @param id 宠物大小管理ID
     * @return 结果
     */
    public int deleteFaProjectsTypeById(Long id);

    /**
     * 批量删除宠物大小管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaProjectsTypeByIds(Long[] ids);

    public List<FaProjectsType> list(FaProjectsType faProjectsType);

    List<ProjectTypeVo> listProjectTypeVo(FaProjectsType faProjectsType);
}
