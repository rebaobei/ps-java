package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserRank;

/**
 * 等级管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface FaUserRankMapper 
{
    /**
     * 查询等级管理
     * 
     * @param id 等级管理ID
     * @return 等级管理
     */
    public FaUserRank selectFaUserRankById(Long id);

    /**
     * 查询等级管理列表
     * 
     * @param faUserRank 等级管理
     * @return 等级管理集合
     */
    public List<FaUserRank> selectFaUserRankList(FaUserRank faUserRank);

    /**
     * 新增等级管理
     * 
     * @param faUserRank 等级管理
     * @return 结果
     */
    public int insertFaUserRank(FaUserRank faUserRank);

    /**
     * 修改等级管理
     * 
     * @param faUserRank 等级管理
     * @return 结果
     */
    public int updateFaUserRank(FaUserRank faUserRank);

    /**
     * 删除等级管理
     * 
     * @param id 等级管理ID
     * @return 结果
     */
    public int deleteFaUserRankById(Long id);

    /**
     * 批量删除等级管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserRankByIds(Long[] ids);
}
