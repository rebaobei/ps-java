package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserSite;

/**
 * 会员其他信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface FaUserSiteMapper 
{
    /**
     * 查询会员其他信息
     * 
     * @param id 会员其他信息ID
     * @return 会员其他信息
     */
    public FaUserSite selectFaUserSiteById(Long id);

    /**
     * 查询会员其他信息列表
     * 
     * @param faUserSite 会员其他信息
     * @return 会员其他信息集合
     */
    public List<FaUserSite> selectFaUserSiteList(FaUserSite faUserSite);

    /**
     * 新增会员其他信息
     * 
     * @param faUserSite 会员其他信息
     * @return 结果
     */
    public int insertFaUserSite(FaUserSite faUserSite);

    /**
     * 修改会员其他信息
     * 
     * @param faUserSite 会员其他信息
     * @return 结果
     */
    public int updateFaUserSite(FaUserSite faUserSite);

    /**
     * 删除会员其他信息
     * 
     * @param id 会员其他信息ID
     * @return 结果
     */
    public int deleteFaUserSiteById(Long id);

    /**
     * 批量删除会员其他信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserSiteByIds(Long[] ids);
}
