package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaVoteUserDetail;
import com.ruoyi.system.domain.vo.UserVoteDetailVo;

/**
 * 投票明细Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface FaVoteUserDetailMapper 
{
    /**
     * 查询投票明细
     * 
     * @param id 投票明细ID
     * @return 投票明细
     */
    public FaVoteUserDetail selectFaVoteUserDetailById(Long id);

    /**
     * 查询投票明细列表
     * 
     * @param faVoteUserDetail 投票明细
     * @return 投票明细集合
     */
    public List<FaVoteUserDetail> selectFaVoteUserDetailList(FaVoteUserDetail faVoteUserDetail);

    /**
     * 新增投票明细
     * 
     * @param faVoteUserDetail 投票明细
     * @return 结果
     */
    public int insertFaVoteUserDetail(FaVoteUserDetail faVoteUserDetail);

    /**
     * 修改投票明细
     * 
     * @param faVoteUserDetail 投票明细
     * @return 结果
     */
    public int updateFaVoteUserDetail(FaVoteUserDetail faVoteUserDetail);

    /**
     * 删除投票明细
     * 
     * @param id 投票明细ID
     * @return 结果
     */
    public int deleteFaVoteUserDetailById(Long id);

    /**
     * 批量删除投票明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaVoteUserDetailByIds(Long[] ids);

    List<UserVoteDetailVo> listUserVoteDetailVo(UserVoteDetailVo faVoteUserDetail);
}
