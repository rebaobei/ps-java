package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaProjects;
import com.ruoyi.system.domain.vo.ProjectVo;

/**
 * 预约项目Mapper接口
 *
 * @author ruoyi
 * @date 2021-07-26
 */
public interface FaProjectsMapper
{
    /**
     * 查询预约项目
     *
     * @param id 预约项目ID
     * @return 预约项目
     */
    public FaProjects selectFaProjectsById(Long id);

    /**
     * 查询预约项目列表
     *
     * @param faProjects 预约项目
     * @return 预约项目集合
     */
    public List<FaProjects> selectFaProjectsList(FaProjects faProjects);

    /**
     * 新增预约项目
     *
     * @param faProjects 预约项目
     * @return 结果
     */
    public int insertFaProjects(FaProjects faProjects);

    /**
     * 修改预约项目
     *
     * @param faProjects 预约项目
     * @return 结果
     */
    public int updateFaProjects(FaProjects faProjects);

    /**
     * 删除预约项目
     *
     * @param id 预约项目ID
     * @return 结果
     */
    public int deleteFaProjectsById(Long id);

    /**
     * 批量删除预约项目
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaProjectsByIds(Long[] ids);

    List<FaProjects> list();

    public List<ProjectVo> listProjectVo(FaProjects projects);

    public List<FaProjects> selectCount(Long storeInfoId);
}
