package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserMoneyLog;
import com.ruoyi.system.domain.vo.MoneyLogWithUserInfoVo;

/**
 * 余额变动Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface FaUserMoneyLogMapper 
{
    /**
     * 查询余额变动
     * 
     * @param id 余额变动ID
     * @return 余额变动
     */
    public FaUserMoneyLog selectFaUserMoneyLogById(Integer id);

    /**
     * 查询余额变动列表
     * 
     * @param faUserMoneyLog 余额变动
     * @return 余额变动集合
     */
    public List<FaUserMoneyLog> selectFaUserMoneyLogList(FaUserMoneyLog faUserMoneyLog);

    /**
     * 新增余额变动
     * 
     * @param faUserMoneyLog 余额变动
     * @return 结果
     */
    public int insertFaUserMoneyLog(FaUserMoneyLog faUserMoneyLog);

    /**
     * 修改余额变动
     * 
     * @param faUserMoneyLog 余额变动
     * @return 结果
     */
    public int updateFaUserMoneyLog(FaUserMoneyLog faUserMoneyLog);

    /**
     * 删除余额变动
     * 
     * @param id 余额变动ID
     * @return 结果
     */
    public int deleteFaUserMoneyLogById(Integer id);

    /**
     * 批量删除余额变动
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserMoneyLogByIds(Integer[] ids);

    List<MoneyLogWithUserInfoVo> listMoneyLogWithUserInfo(FaUserMoneyLog faUserMoneyLog);
}
