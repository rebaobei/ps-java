package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaOrderInvoice;
import com.ruoyi.system.domain.vo.OrderInvoiceVo;

/**
 * 订单发票Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface FaOrderInvoiceMapper 
{
    /**
     * 查询订单发票
     * 
     * @param id 订单发票ID
     * @return 订单发票
     */
    public FaOrderInvoice selectFaOrderInvoiceById(Long id);

    /**
     * 查询订单发票列表
     * 
     * @param faOrderInvoice 订单发票
     * @return 订单发票集合
     */
    public List<FaOrderInvoice> selectFaOrderInvoiceList(FaOrderInvoice faOrderInvoice);

    /**
     * 新增订单发票
     * 
     * @param faOrderInvoice 订单发票
     * @return 结果
     */
    public int insertFaOrderInvoice(FaOrderInvoice faOrderInvoice);

    /**
     * 修改订单发票
     * 
     * @param faOrderInvoice 订单发票
     * @return 结果
     */
    public int updateFaOrderInvoice(FaOrderInvoice faOrderInvoice);

    /**
     * 删除订单发票
     * 
     * @param id 订单发票ID
     * @return 结果
     */
    public int deleteFaOrderInvoiceById(Long id);

    /**
     * 批量删除订单发票
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaOrderInvoiceByIds(Long[] ids);

    List<OrderInvoiceVo> listOrderInvoiceVo(OrderInvoiceVo faOrderInvoice);
}
