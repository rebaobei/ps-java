package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGoodArticle;

/**
 * 动保公益Mapper接口
 * 
 * @author ruoyi
 * @date 2021-12-04
 */
public interface FaGoodArticleMapper 
{
    /**
     * 查询动保公益
     * 
     * @param id 动保公益ID
     * @return 动保公益
     */
    public FaGoodArticle selectFaGoodArticleById(Long id);

    /**
     * 查询动保公益列表
     * 
     * @param faGoodArticle 动保公益
     * @return 动保公益集合
     */
    public List<FaGoodArticle> selectFaGoodArticleList(FaGoodArticle faGoodArticle);

    /**
     * 新增动保公益
     * 
     * @param faGoodArticle 动保公益
     * @return 结果
     */
    public int insertFaGoodArticle(FaGoodArticle faGoodArticle);

    /**
     * 修改动保公益
     * 
     * @param faGoodArticle 动保公益
     * @return 结果
     */
    public int updateFaGoodArticle(FaGoodArticle faGoodArticle);

    /**
     * 删除动保公益
     * 
     * @param id 动保公益ID
     * @return 结果
     */
    public int deleteFaGoodArticleById(Long id);

    /**
     * 批量删除动保公益
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGoodArticleByIds(Long[] ids);
}
