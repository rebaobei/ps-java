package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGameThingsCategory;

/**
 * 物品分类Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface FaGameThingsCategoryMapper 
{
    /**
     * 查询物品分类
     * 
     * @param id 物品分类ID
     * @return 物品分类
     */
    public FaGameThingsCategory selectFaGameThingsCategoryById(Long id);

    /**
     * 查询物品分类列表
     * 
     * @param faGameThingsCategory 物品分类
     * @return 物品分类集合
     */
    public List<FaGameThingsCategory> selectFaGameThingsCategoryList(FaGameThingsCategory faGameThingsCategory);

    /**
     * 新增物品分类
     * 
     * @param faGameThingsCategory 物品分类
     * @return 结果
     */
    public int insertFaGameThingsCategory(FaGameThingsCategory faGameThingsCategory);

    /**
     * 修改物品分类
     * 
     * @param faGameThingsCategory 物品分类
     * @return 结果
     */
    public int updateFaGameThingsCategory(FaGameThingsCategory faGameThingsCategory);

    /**
     * 删除物品分类
     * 
     * @param id 物品分类ID
     * @return 结果
     */
    public int deleteFaGameThingsCategoryById(Long id);

    /**
     * 批量删除物品分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGameThingsCategoryByIds(Long[] ids);
}
