package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopProductSku;
import org.apache.ibatis.annotations.Param;

/**
 * 商品属性Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
public interface FaXshopProductSkuMapper 
{
    /**
     * 查询商品属性
     * 
     * @param id 商品属性ID
     * @return 商品属性
     */
    public FaXshopProductSku selectFaXshopProductSkuById(Integer id);

    /**
     * 查询商品属性列表
     * 
     * @param faXshopProductSku 商品属性
     * @return 商品属性集合
     */
    public List<FaXshopProductSku> selectFaXshopProductSkuList(FaXshopProductSku faXshopProductSku);

    /**
     * 新增商品属性
     * 
     * @param faXshopProductSku 商品属性
     * @return 结果
     */
    public int insertFaXshopProductSku(FaXshopProductSku faXshopProductSku);

    /**
     * 修改商品属性
     * 
     * @param faXshopProductSku 商品属性
     * @return 结果
     */
    public int updateFaXshopProductSku(FaXshopProductSku faXshopProductSku);

    /**
     * 删除商品属性
     * 
     * @param id 商品属性ID
     * @return 结果
     */
    public int deleteFaXshopProductSkuById(Integer id);

    /**
     * 批量删除商品属性
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopProductSkuByIds(Integer[] ids);

    public List<FaXshopProductSku> listFaXshopProductSkuById(@Param("id") Integer id);
}
