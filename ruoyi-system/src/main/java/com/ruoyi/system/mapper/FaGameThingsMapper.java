package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGameThings;

/**
 * 物品管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface FaGameThingsMapper 
{
    /**
     * 查询物品管理
     * 
     * @param id 物品管理ID
     * @return 物品管理
     */
    public FaGameThings selectFaGameThingsById(Long id);

    /**
     * 查询物品管理列表
     * 
     * @param faGameThings 物品管理
     * @return 物品管理集合
     */
    public List<FaGameThings> selectFaGameThingsList(FaGameThings faGameThings);

    /**
     * 新增物品管理
     * 
     * @param faGameThings 物品管理
     * @return 结果
     */
    public int insertFaGameThings(FaGameThings faGameThings);

    /**
     * 修改物品管理
     * 
     * @param faGameThings 物品管理
     * @return 结果
     */
    public int updateFaGameThings(FaGameThings faGameThings);

    /**
     * 删除物品管理
     * 
     * @param id 物品管理ID
     * @return 结果
     */
    public int deleteFaGameThingsById(Long id);

    /**
     * 批量删除物品管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGameThingsByIds(Long[] ids);
}
