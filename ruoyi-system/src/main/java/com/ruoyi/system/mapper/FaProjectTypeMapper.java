package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaProjectType;
import org.apache.ibatis.annotations.Param;

/**
 * 店铺服务分类Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-13
 */
public interface FaProjectTypeMapper 
{
    /**
     * 查询店铺服务分类
     * 
     * @param id 店铺服务分类ID
     * @return 店铺服务分类
     */
    public FaProjectType selectFaProjectTypeById(@Param("id") Long id,@Param("storeId")String StoreId);

    /**
     * 查询店铺服务分类列表
     * 
     * @param faProjectType 店铺服务分类
     * @return 店铺服务分类集合
     */
    public List<FaProjectType> selectFaProjectTypeList(FaProjectType faProjectType);

    /**
     * 新增店铺服务分类
     * 
     * @param faProjectType 店铺服务分类
     * @return 结果
     */
    public int insertFaProjectType(FaProjectType faProjectType);

    /**
     * 修改店铺服务分类
     * 
     * @param faProjectType 店铺服务分类
     * @return 结果
     */
    public int updateFaProjectType(FaProjectType faProjectType);

    /**
     * 删除店铺服务分类
     * 
     * @param id 店铺服务分类ID
     * @return 结果
     */
    public int deleteFaProjectTypeById(Long id);

    /**
     * 批量删除店铺服务分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaProjectTypeByIds(Long[] ids);
}
