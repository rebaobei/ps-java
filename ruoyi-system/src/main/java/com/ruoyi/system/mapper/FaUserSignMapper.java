package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserSign;

/**
 * 签到设置积分Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaUserSignMapper 
{
    /**
     * 查询签到设置积分
     * 
     * @param id 签到设置积分ID
     * @return 签到设置积分
     */
    public FaUserSign selectFaUserSignById(Long id);

    /**
     * 查询签到设置积分列表
     * 
     * @param faUserSign 签到设置积分
     * @return 签到设置积分集合
     */
    public List<FaUserSign> selectFaUserSignList(FaUserSign faUserSign);

    /**
     * 新增签到设置积分
     * 
     * @param faUserSign 签到设置积分
     * @return 结果
     */
    public int insertFaUserSign(FaUserSign faUserSign);

    /**
     * 修改签到设置积分
     * 
     * @param faUserSign 签到设置积分
     * @return 结果
     */
    public int updateFaUserSign(FaUserSign faUserSign);

    /**
     * 删除签到设置积分
     * 
     * @param id 签到设置积分ID
     * @return 结果
     */
    public int deleteFaUserSignById(Long id);

    /**
     * 批量删除签到设置积分
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserSignByIds(Long[] ids);
}
