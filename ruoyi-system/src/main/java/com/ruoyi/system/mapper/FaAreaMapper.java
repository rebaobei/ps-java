package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaArea;
import com.ruoyi.system.domain.vo.CascadeProvinceVo;

/**
 * 地区Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-20
 */
public interface FaAreaMapper 
{
    /**
     * 查询地区
     * 
     * @param id 地区ID
     * @return 地区
     */
    public FaArea selectFaAreaById(Long id);

    /**
     * 查询地区列表
     * 
     * @param faArea 地区
     * @return 地区集合
     */
    public List<FaArea> selectFaAreaList(FaArea faArea);

    /**
     * 新增地区
     * 
     * @param faArea 地区
     * @return 结果
     */
    public int insertFaArea(FaArea faArea);

    /**
     * 修改地区
     * 
     * @param faArea 地区
     * @return 结果
     */
    public int updateFaArea(FaArea faArea);

    /**
     * 删除地区
     * 
     * @param id 地区ID
     * @return 结果
     */
    public int deleteFaAreaById(Long id);

    /**
     * 批量删除地区
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaAreaByIds(Long[] ids);

    /**
     * 查询省市两级
     * @return
     */
    List<FaArea> listCity();

    List<FaArea> listProvince();
}
