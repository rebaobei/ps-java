package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopUnit;
import org.apache.ibatis.annotations.Param;

/**
 * 计量单位Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
public interface FaXshopUnitMapper 
{
    /**
     * 查询计量单位
     * 
     * @param id 计量单位ID
     * @return 计量单位
     */
    public FaXshopUnit selectFaXshopUnitById(Integer id);

    /**
     * 查询计量单位列表
     * 
     * @param faXshopUnit 计量单位
     * @return 计量单位集合
     */
    public List<FaXshopUnit> selectFaXshopUnitList(FaXshopUnit faXshopUnit);

    /**
     * 新增计量单位
     * 
     * @param faXshopUnit 计量单位
     * @return 结果
     */
    public int insertFaXshopUnit(FaXshopUnit faXshopUnit);

    /**
     * 修改计量单位
     * 
     * @param faXshopUnit 计量单位
     * @return 结果
     */
    public int updateFaXshopUnit(FaXshopUnit faXshopUnit);

    /**
     * 删除计量单位
     * 
     * @param id 计量单位ID
     * @return 结果
     */
    public int deleteFaXshopUnitById(Integer id);

    /**
     * 批量删除计量单位
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopUnitByIds(Integer[] ids);

    int cancelDefault(@Param("id") Integer id);
}
