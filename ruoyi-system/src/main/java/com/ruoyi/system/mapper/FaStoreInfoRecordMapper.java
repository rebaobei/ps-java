package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaStoreInfoRecord;

/**
 * 店铺信息记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-10-30
 */
public interface FaStoreInfoRecordMapper 
{
    /**
     * 查询店铺信息记录
     * 
     * @param id 店铺信息记录ID
     * @return 店铺信息记录
     */
    public FaStoreInfoRecord selectFaStoreInfoRecordById(Long id);

    /**
     * 查询店铺信息记录列表
     * 
     * @param faStoreInfoRecord 店铺信息记录
     * @return 店铺信息记录集合
     */
    public List<FaStoreInfoRecord> selectFaStoreInfoRecordList(FaStoreInfoRecord faStoreInfoRecord);

    /**
     * 新增店铺信息记录
     * 
     * @param faStoreInfoRecord 店铺信息记录
     * @return 结果
     */
    public int insertFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord);

    /**
     * 修改店铺信息记录
     * 
     * @param faStoreInfoRecord 店铺信息记录
     * @return 结果
     */
    public int updateFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord);

    /**
     * 删除店铺信息记录
     * 
     * @param id 店铺信息记录ID
     * @return 结果
     */
    public int deleteFaStoreInfoRecordById(Long id);

    /**
     * 批量删除店铺信息记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaStoreInfoRecordByIds(Long[] ids);
}
