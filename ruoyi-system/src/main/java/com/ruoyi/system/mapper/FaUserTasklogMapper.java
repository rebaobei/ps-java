package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserTasklog;

/**
 * 完成任务记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaUserTasklogMapper 
{
    /**
     * 查询完成任务记录
     * 
     * @param id 完成任务记录ID
     * @return 完成任务记录
     */
    public FaUserTasklog selectFaUserTasklogById(Long id);

    /**
     * 查询完成任务记录列表
     * 
     * @param faUserTasklog 完成任务记录
     * @return 完成任务记录集合
     */
    public List<FaUserTasklog> selectFaUserTasklogList(FaUserTasklog faUserTasklog);

    /**
     * 新增完成任务记录
     * 
     * @param faUserTasklog 完成任务记录
     * @return 结果
     */
    public int insertFaUserTasklog(FaUserTasklog faUserTasklog);

    /**
     * 修改完成任务记录
     * 
     * @param faUserTasklog 完成任务记录
     * @return 结果
     */
    public int updateFaUserTasklog(FaUserTasklog faUserTasklog);

    /**
     * 删除完成任务记录
     * 
     * @param id 完成任务记录ID
     * @return 结果
     */
    public int deleteFaUserTasklogById(Long id);

    /**
     * 批量删除完成任务记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserTasklogByIds(Long[] ids);
}
