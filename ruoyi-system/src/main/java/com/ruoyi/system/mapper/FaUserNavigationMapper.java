package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserNavigation;

/**
 * 用户中心导航Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface FaUserNavigationMapper 
{
    /**
     * 查询用户中心导航
     * 
     * @param nId 用户中心导航ID
     * @return 用户中心导航
     */
    public FaUserNavigation selectFaUserNavigationById(Integer nId);

    /**
     * 查询用户中心导航列表
     * 
     * @param faUserNavigation 用户中心导航
     * @return 用户中心导航集合
     */
    public List<FaUserNavigation> selectFaUserNavigationList(FaUserNavigation faUserNavigation);

    /**
     * 新增用户中心导航
     * 
     * @param faUserNavigation 用户中心导航
     * @return 结果
     */
    public int insertFaUserNavigation(FaUserNavigation faUserNavigation);

    /**
     * 修改用户中心导航
     * 
     * @param faUserNavigation 用户中心导航
     * @return 结果
     */
    public int updateFaUserNavigation(FaUserNavigation faUserNavigation);

    /**
     * 删除用户中心导航
     * 
     * @param nId 用户中心导航ID
     * @return 结果
     */
    public int deleteFaUserNavigationById(Integer nId);

    /**
     * 批量删除用户中心导航
     * 
     * @param nIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserNavigationByIds(Integer[] nIds);
}
