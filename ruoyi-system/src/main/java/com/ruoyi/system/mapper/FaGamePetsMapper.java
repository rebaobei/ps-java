package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGamePets;

/**
 * 宠物管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface FaGamePetsMapper 
{
    /**
     * 查询宠物管理
     * 
     * @param petId 宠物管理ID
     * @return 宠物管理
     */
    public FaGamePets selectFaGamePetsById(Long petId);

    /**
     * 查询宠物管理列表
     * 
     * @param faGamePets 宠物管理
     * @return 宠物管理集合
     */
    public List<FaGamePets> selectFaGamePetsList(FaGamePets faGamePets);

    /**
     * 新增宠物管理
     * 
     * @param faGamePets 宠物管理
     * @return 结果
     */
    public int insertFaGamePets(FaGamePets faGamePets);

    /**
     * 修改宠物管理
     * 
     * @param faGamePets 宠物管理
     * @return 结果
     */
    public int updateFaGamePets(FaGamePets faGamePets);

    /**
     * 删除宠物管理
     * 
     * @param petId 宠物管理ID
     * @return 结果
     */
    public int deleteFaGamePetsById(Long petId);

    /**
     * 批量删除宠物管理
     * 
     * @param petIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGamePetsByIds(Long[] petIds);
}
