package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGamePetsHasOwner;
import com.ruoyi.system.domain.vo.PetHasOwnerVo;

/**
 * 已领养宠物Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface FaGamePetsHasOwnerMapper 
{
    /**
     * 查询已领养宠物
     * 
     * @param id 已领养宠物ID
     * @return 已领养宠物
     */
    public FaGamePetsHasOwner selectFaGamePetsHasOwnerById(Long id);

    /**
     * 查询已领养宠物列表
     * 
     * @param faGamePetsHasOwner 已领养宠物
     * @return 已领养宠物集合
     */
    public List<FaGamePetsHasOwner> selectFaGamePetsHasOwnerList(FaGamePetsHasOwner faGamePetsHasOwner);

    /**
     * 新增已领养宠物
     * 
     * @param faGamePetsHasOwner 已领养宠物
     * @return 结果
     */
    public int insertFaGamePetsHasOwner(FaGamePetsHasOwner faGamePetsHasOwner);

    /**
     * 修改已领养宠物
     * 
     * @param faGamePetsHasOwner 已领养宠物
     * @return 结果
     */
    public int updateFaGamePetsHasOwner(FaGamePetsHasOwner faGamePetsHasOwner);

    /**
     * 删除已领养宠物
     * 
     * @param id 已领养宠物ID
     * @return 结果
     */
    public int deleteFaGamePetsHasOwnerById(Long id);

    /**
     * 批量删除已领养宠物
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGamePetsHasOwnerByIds(Long[] ids);

    List<PetHasOwnerVo> listPetHasOwnerVo(PetHasOwnerVo faGamePetsHasOwner);
}
