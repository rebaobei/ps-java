package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaCarOrder;
import com.ruoyi.system.domain.vo.CarOrderVo;

/**
 * 同城接送订单Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface FaCarOrderMapper 
{
    /**
     * 查询同城接送订单
     * 
     * @param id 同城接送订单ID
     * @return 同城接送订单
     */
    public FaCarOrder selectFaCarOrderById(Long id);

    /**
     * 查询同城接送订单列表
     * 
     * @param faCarOrder 同城接送订单
     * @return 同城接送订单集合
     */
    public List<FaCarOrder> selectFaCarOrderList(FaCarOrder faCarOrder);

    /**
     * 新增同城接送订单
     * 
     * @param faCarOrder 同城接送订单
     * @return 结果
     */
    public int insertFaCarOrder(FaCarOrder faCarOrder);

    /**
     * 修改同城接送订单
     * 
     * @param faCarOrder 同城接送订单
     * @return 结果
     */
    public int updateFaCarOrder(FaCarOrder faCarOrder);

    /**
     * 删除同城接送订单
     * 
     * @param id 同城接送订单ID
     * @return 结果
     */
    public int deleteFaCarOrderById(Long id);

    /**
     * 批量删除同城接送订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaCarOrderByIds(Long[] ids);

    List<CarOrderVo> listCarOrderVo(CarOrderVo faCarOrder);
}
