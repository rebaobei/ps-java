package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaCmsAddonnews;

/**
 * 新闻Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-12
 */
public interface FaCmsAddonnewsMapper 
{
    /**
     * 查询新闻
     * 
     * @param id 新闻ID
     * @return 新闻
     */
    public FaCmsAddonnews selectFaCmsAddonnewsById(Long id);

    /**
     * 查询新闻列表
     * 
     * @param faCmsAddonnews 新闻
     * @return 新闻集合
     */
    public List<FaCmsAddonnews> selectFaCmsAddonnewsList(FaCmsAddonnews faCmsAddonnews);

    /**
     * 新增新闻
     * 
     * @param faCmsAddonnews 新闻
     * @return 结果
     */
    public int insertFaCmsAddonnews(FaCmsAddonnews faCmsAddonnews);

    /**
     * 修改新闻
     * 
     * @param faCmsAddonnews 新闻
     * @return 结果
     */
    public int updateFaCmsAddonnews(FaCmsAddonnews faCmsAddonnews);

    /**
     * 删除新闻
     * 
     * @param id 新闻ID
     * @return 结果
     */
    public int deleteFaCmsAddonnewsById(Long id);

    /**
     * 批量删除新闻
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaCmsAddonnewsByIds(Long[] ids);
}
