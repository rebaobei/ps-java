package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaVideoList;

/**
 * 视频管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaVideoListMapper 
{
    /**
     * 查询视频管理
     * 
     * @param id 视频管理ID
     * @return 视频管理
     */
    public FaVideoList selectFaVideoListById(Long id);

    /**
     * 查询视频管理列表
     * 
     * @param faVideoList 视频管理
     * @return 视频管理集合
     */
    public List<FaVideoList> selectFaVideoListList(FaVideoList faVideoList);

    /**
     * 新增视频管理
     * 
     * @param faVideoList 视频管理
     * @return 结果
     */
    public int insertFaVideoList(FaVideoList faVideoList);

    /**
     * 修改视频管理
     * 
     * @param faVideoList 视频管理
     * @return 结果
     */
    public int updateFaVideoList(FaVideoList faVideoList);

    /**
     * 删除视频管理
     * 
     * @param id 视频管理ID
     * @return 结果
     */
    public int deleteFaVideoListById(Long id);

    /**
     * 批量删除视频管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaVideoListByIds(Long[] ids);
}
