package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaBallUserarticle;
import com.ruoyi.system.domain.vo.UserArticleVo;

/**
 * 朋友圈明细Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public interface FaBallUserarticleMapper 
{
    /**
     * 查询朋友圈明细
     * 
     * @param id 朋友圈明细ID
     * @return 朋友圈明细
     */
    public FaBallUserarticle selectFaBallUserarticleById(Long id);

    /**
     * 查询朋友圈明细列表
     * 
     * @param faBallUserarticle 朋友圈明细
     * @return 朋友圈明细集合
     */
    public List<FaBallUserarticle> selectFaBallUserarticleList(FaBallUserarticle faBallUserarticle);

    /**
     * 新增朋友圈明细
     * 
     * @param faBallUserarticle 朋友圈明细
     * @return 结果
     */
    public int insertFaBallUserarticle(FaBallUserarticle faBallUserarticle);

    /**
     * 修改朋友圈明细
     * 
     * @param faBallUserarticle 朋友圈明细
     * @return 结果
     */
    public int updateFaBallUserarticle(FaBallUserarticle faBallUserarticle);

    /**
     * 删除朋友圈明细
     * 
     * @param id 朋友圈明细ID
     * @return 结果
     */
    public int deleteFaBallUserarticleById(Long id);

    /**
     * 批量删除朋友圈明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaBallUserarticleByIds(Long[] ids);

    List<UserArticleVo> listUserArticleVo(FaBallUserarticle faBallUserarticle);
}
