package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.FaXshopOrder;
import com.ruoyi.system.domain.vo.XshopOrderVo;

/**
 * 商城订单Mapper接口
 *
 * @author ruoyi
 * @date 2021-08-03
 */
public interface FaXshopOrderMapper
{
    /**
     * 查询商城订单
     *
     * @param id 商城订单ID
     * @return 商城订单
     */
    public FaXshopOrder selectFaXshopOrderById(Integer id);

    public FaXshopOrder selectFaXshopOrderByOrderCode(String orderCode);

    /**
     * 查询商城订单列表
     *
     * @param faXshopOrder 商城订单
     * @return 商城订单集合
     */
    public List<FaXshopOrder> selectFaXshopOrderList(FaXshopOrder faXshopOrder);

    /**
     * 新增商城订单
     *
     * @param faXshopOrder 商城订单
     * @return 结果
     */
    public int insertFaXshopOrder(FaXshopOrder faXshopOrder);

    /**
     * 修改商城订单
     *
     * @param faXshopOrder 商城订单
     * @return 结果
     */
    public int updateFaXshopOrder(FaXshopOrder faXshopOrder);

    /**
     * 删除商城订单
     *
     * @param id 商城订单ID
     * @return 结果
     */
    public int deleteFaXshopOrderById(Integer id);

    /**
     * 批量删除商城订单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopOrderByIds(Integer[] ids);

    List<XshopOrderVo> listXshopOrderVo(XshopOrderVo faXshopOrder);


    Integer getOrderCount(Map<String,Long> map);
}
