package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaRoom;
import com.ruoyi.system.domain.vo.RoomVo;

/**
 * 房间管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
public interface FaRoomMapper 
{
    /**
     * 查询房间管理
     * 
     * @param id 房间管理ID
     * @return 房间管理
     */
    public FaRoom selectFaRoomById(Long id);

    /**
     * 查询房间管理列表
     * 
     * @param faRoom 房间管理
     * @return 房间管理集合
     */
    public List<FaRoom> selectFaRoomList(FaRoom faRoom);

    /**
     * 新增房间管理
     * 
     * @param faRoom 房间管理
     * @return 结果
     */
    public int insertFaRoom(FaRoom faRoom);

    /**
     * 修改房间管理
     * 
     * @param faRoom 房间管理
     * @return 结果
     */
    public int updateFaRoom(FaRoom faRoom);

    /**
     * 删除房间管理
     * 
     * @param id 房间管理ID
     * @return 结果
     */
    public int deleteFaRoomById(Long id);

    /**
     * 批量删除房间管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaRoomByIds(Long[] ids);


    List<RoomVo> listRoomVo(FaRoom room);
}
