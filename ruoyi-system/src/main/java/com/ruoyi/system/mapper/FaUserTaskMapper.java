package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserTask;

/**
 * 任务系统设置Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaUserTaskMapper 
{
    /**
     * 查询任务系统设置
     * 
     * @param id 任务系统设置ID
     * @return 任务系统设置
     */
    public FaUserTask selectFaUserTaskById(Long id);

    /**
     * 查询任务系统设置列表
     * 
     * @param faUserTask 任务系统设置
     * @return 任务系统设置集合
     */
    public List<FaUserTask> selectFaUserTaskList(FaUserTask faUserTask);

    /**
     * 新增任务系统设置
     * 
     * @param faUserTask 任务系统设置
     * @return 结果
     */
    public int insertFaUserTask(FaUserTask faUserTask);

    /**
     * 修改任务系统设置
     * 
     * @param faUserTask 任务系统设置
     * @return 结果
     */
    public int updateFaUserTask(FaUserTask faUserTask);

    /**
     * 删除任务系统设置
     * 
     * @param id 任务系统设置ID
     * @return 结果
     */
    public int deleteFaUserTaskById(Long id);

    /**
     * 批量删除任务系统设置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserTaskByIds(Long[] ids);
}
