package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

/**
 * 会员管理111Mapper接口
 *
 * @author ruoyi
 * @date 2021-07-05
 */
public interface FaUserMapper
{
    /**
     * 查询会员管理111
     *
     * @param id 会员管理111ID
     * @return 会员管理111
     */
    public FaUser selectFaUserById(Integer id);

    public FaUser selectFaUserByMobile(@Param("mobile") String mobile);

    /**
     * 查询会员管理111列表
     *
     * @param faUser 会员管理111
     * @return 会员管理111集合
     */
    public List<FaUser> selectFaUserList(FaUser faUser);

    /**
     * 新增会员管理111
     *
     * @param faUser 会员管理111
     * @return 结果
     */
    public int insertFaUser(FaUser faUser);

    /**
     * 修改会员管理111
     *
     * @param faUser 会员管理111
     * @return 结果
     */
    public int updateFaUser(FaUser faUser);

    /**
     * 删除会员管理111
     *
     * @param id 会员管理111ID
     * @return 结果
     */
    public int deleteFaUserById(Integer id);

    /**
     * 批量删除会员管理111
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserByIds(Integer[] ids);

    public Integer getCount(@Param("createTime") Long createTime);

    List<String> getPushCid();

    Integer getActiveUserCount(@Param("loginTime") Long todayZero);
}
