package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaGameGoods;
import com.ruoyi.system.domain.vo.GameGoodsVo;
import org.apache.ibatis.annotations.Param;

/**
 * 商品管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface FaGameGoodsMapper 
{
    /**
     * 查询商品管理
     * 
     * @param id 商品管理ID
     * @return 商品管理
     */
    public GameGoodsVo selectFaGameGoodsById(Long id);

    /**
     * 查询商品管理列表
     * 
     * @param faGameGoods 商品管理
     * @return 商品管理集合
     */
    public List<FaGameGoods> selectFaGameGoodsList(FaGameGoods faGameGoods);

    /**
     * 新增商品管理
     * 
     * @param faGameGoods 商品管理
     * @return 结果
     */
    public int insertFaGameGoods(FaGameGoods faGameGoods);

    /**
     * 修改商品管理
     * 
     * @param faGameGoods 商品管理
     * @return 结果
     */
    public int updateFaGameGoods(FaGameGoods faGameGoods);

    /**
     * 删除商品管理
     * 
     * @param id 商品管理ID
     * @return 结果
     */
    public int deleteFaGameGoodsById(Long id);

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaGameGoodsByIds(Long[] ids);

    List<GameGoodsVo> listGameGoodsVo(GameGoodsVo faGameGoods);

    FaGameGoods selectGoodsByThingsId(@Param("id")Long thingsId);
}
