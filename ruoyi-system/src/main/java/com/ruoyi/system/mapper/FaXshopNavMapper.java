package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaXshopNav;

/**
 * 首页设置Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface FaXshopNavMapper 
{
    /**
     * 查询首页设置
     * 
     * @param id 首页设置ID
     * @return 首页设置
     */
    public FaXshopNav selectFaXshopNavById(Integer id);

    /**
     * 查询首页设置列表
     * 
     * @param faXshopNav 首页设置
     * @return 首页设置集合
     */
    public List<FaXshopNav> selectFaXshopNavList(FaXshopNav faXshopNav);

    /**
     * 新增首页设置
     * 
     * @param faXshopNav 首页设置
     * @return 结果
     */
    public int insertFaXshopNav(FaXshopNav faXshopNav);

    /**
     * 修改首页设置
     * 
     * @param faXshopNav 首页设置
     * @return 结果
     */
    public int updateFaXshopNav(FaXshopNav faXshopNav);

    /**
     * 删除首页设置
     * 
     * @param id 首页设置ID
     * @return 结果
     */
    public int deleteFaXshopNavById(Integer id);

    /**
     * 批量删除首页设置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaXshopNavByIds(Integer[] ids);
}
