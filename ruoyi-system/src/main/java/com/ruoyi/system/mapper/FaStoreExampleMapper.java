package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaStoreExample;

/**
 * 店铺服务案Mapper接口
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
public interface FaStoreExampleMapper 
{
    /**
     * 查询店铺服务案
     * 
     * @param id 店铺服务案ID
     * @return 店铺服务案
     */
    public FaStoreExample selectFaStoreExampleById(Long id);

    /**
     * 查询店铺服务案列表
     * 
     * @param faStoreExample 店铺服务案
     * @return 店铺服务案集合
     */
    public List<FaStoreExample> selectFaStoreExampleList(FaStoreExample faStoreExample);

    /**
     * 新增店铺服务案
     * 
     * @param faStoreExample 店铺服务案
     * @return 结果
     */
    public int insertFaStoreExample(FaStoreExample faStoreExample);

    /**
     * 修改店铺服务案
     * 
     * @param faStoreExample 店铺服务案
     * @return 结果
     */
    public int updateFaStoreExample(FaStoreExample faStoreExample);

    /**
     * 删除店铺服务案
     * 
     * @param id 店铺服务案ID
     * @return 结果
     */
    public int deleteFaStoreExampleById(Long id);

    /**
     * 批量删除店铺服务案
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaStoreExampleByIds(Long[] ids);
}
