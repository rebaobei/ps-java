package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaStoreBrandRecord;

/**
 * 品牌审核记录Mapper接口
 *
 * @author ruoyi
 * @date 2021-11-02
 */
public interface FaStoreBrandRecordMapper
{
    /**
     * 查询品牌审核记录
     *
     * @param id 品牌审核记录ID
     * @return 品牌审核记录
     */
    public FaStoreBrandRecord selectFaStoreBrandRecordById(Long id);

    /**
     * 查询品牌审核记录列表
     *
     * @param faStoreBrandRecord 品牌审核记录
     * @return 品牌审核记录集合
     */
    public List<FaStoreBrandRecord> selectFaStoreBrandRecordList(FaStoreBrandRecord faStoreBrandRecord);

    /**
     * 新增品牌审核记录
     *
     * @param faStoreBrandRecord 品牌审核记录
     * @return 结果
     */
    public int insertFaStoreBrandRecord(FaStoreBrandRecord faStoreBrandRecord);

    /**
     * 修改品牌审核记录
     *
     * @param faStoreBrandRecord 品牌审核记录
     * @return 结果
     */
    public int updateFaStoreBrandRecord(FaStoreBrandRecord faStoreBrandRecord);

    /**
     * 删除品牌审核记录
     *
     * @param id 品牌审核记录ID
     * @return 结果
     */
    public int deleteFaStoreBrandRecordById(Long id);

    /**
     * 批量删除品牌审核记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaStoreBrandRecordByIds(Long[] ids);
}
