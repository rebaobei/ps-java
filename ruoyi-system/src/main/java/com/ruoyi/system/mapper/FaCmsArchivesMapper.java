package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaCmsArchives;
import com.ruoyi.system.domain.vo.CmsArchivesVo;

/**
 * 文章管理Mapper接口
 * 
 * @author zzp
 * @date 2021-08-11
 */
public interface FaCmsArchivesMapper 
{
    /**
     * 查询文章管理
     * 
     * @param id 文章管理ID
     * @return 文章管理
     */
    public FaCmsArchives selectFaCmsArchivesById(Integer id);

    /**
     * 查询文章管理列表
     * 
     * @param faCmsArchives 文章管理
     * @return 文章管理集合
     */
    public List<FaCmsArchives> selectFaCmsArchivesList(FaCmsArchives faCmsArchives);

    /**
     * 新增文章管理
     * 
     * @param faCmsArchives 文章管理
     * @return 结果
     */
    public int insertFaCmsArchives(FaCmsArchives faCmsArchives);

    /**
     * 修改文章管理
     * 
     * @param faCmsArchives 文章管理
     * @return 结果
     */
    public int updateFaCmsArchives(FaCmsArchives faCmsArchives);

    /**
     * 删除文章管理
     * 
     * @param id 文章管理ID
     * @return 结果
     */
    public int deleteFaCmsArchivesById(Integer id);

    /**
     * 批量删除文章管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaCmsArchivesByIds(Integer[] ids);

    public CmsArchivesVo selectFaCmsArchivesVoById(Integer id);

    Integer insertArticleVo(CmsArchivesVo vo);

    Integer updateArticleVo(CmsArchivesVo vo);
}
