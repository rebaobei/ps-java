package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaUserToken;

/**
 * 会员TokenMapper接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface FaUserTokenMapper 
{
    /**
     * 查询会员Token
     * 
     * @param token 会员TokenID
     * @return 会员Token
     */
    public FaUserToken selectFaUserTokenById(String token);

    /**
     * 查询会员Token列表
     * 
     * @param faUserToken 会员Token
     * @return 会员Token集合
     */
    public List<FaUserToken> selectFaUserTokenList(FaUserToken faUserToken);

    /**
     * 新增会员Token
     * 
     * @param faUserToken 会员Token
     * @return 结果
     */
    public int insertFaUserToken(FaUserToken faUserToken);

    /**
     * 修改会员Token
     * 
     * @param faUserToken 会员Token
     * @return 结果
     */
    public int updateFaUserToken(FaUserToken faUserToken);

    /**
     * 删除会员Token
     * 
     * @param token 会员TokenID
     * @return 结果
     */
    public int deleteFaUserTokenById(String token);

    /**
     * 批量删除会员Token
     * 
     * @param tokens 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaUserTokenByIds(String[] tokens);
}
