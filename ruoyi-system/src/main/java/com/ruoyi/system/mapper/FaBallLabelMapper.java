package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaBallLabel;

/**
 * 标签详情Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public interface FaBallLabelMapper 
{
    /**
     * 查询标签详情
     * 
     * @param id 标签详情ID
     * @return 标签详情
     */
    public FaBallLabel selectFaBallLabelById(Long id);

    /**
     * 查询标签详情列表
     * 
     * @param faBallLabel 标签详情
     * @return 标签详情集合
     */
    public List<FaBallLabel> selectFaBallLabelList(FaBallLabel faBallLabel);

    /**
     * 新增标签详情
     * 
     * @param faBallLabel 标签详情
     * @return 结果
     */
    public int insertFaBallLabel(FaBallLabel faBallLabel);

    /**
     * 修改标签详情
     * 
     * @param faBallLabel 标签详情
     * @return 结果
     */
    public int updateFaBallLabel(FaBallLabel faBallLabel);

    /**
     * 删除标签详情
     * 
     * @param id 标签详情ID
     * @return 结果
     */
    public int deleteFaBallLabelById(Long id);

    /**
     * 批量删除标签详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaBallLabelByIds(Long[] ids);
}
