package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaYuyueOrderDetail;
import com.ruoyi.system.domain.vo.YuyueOrderDetailVo;

/**
 * 预约订单明细Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface FaYuyueOrderDetailMapper 
{
    /**
     * 查询预约订单明细
     * 
     * @param id 预约订单明细ID
     * @return 预约订单明细
     */
    public FaYuyueOrderDetail selectFaYuyueOrderDetailById(Long id);

    /**
     * 查询预约订单明细列表
     * 
     * @param faYuyueOrderDetail 预约订单明细
     * @return 预约订单明细集合
     */
    public List<FaYuyueOrderDetail> selectFaYuyueOrderDetailList(FaYuyueOrderDetail faYuyueOrderDetail);

    /**
     * 新增预约订单明细
     * 
     * @param faYuyueOrderDetail 预约订单明细
     * @return 结果
     */
    public int insertFaYuyueOrderDetail(FaYuyueOrderDetail faYuyueOrderDetail);

    /**
     * 修改预约订单明细
     * 
     * @param faYuyueOrderDetail 预约订单明细
     * @return 结果
     */
    public int updateFaYuyueOrderDetail(FaYuyueOrderDetail faYuyueOrderDetail);

    /**
     * 删除预约订单明细
     * 
     * @param id 预约订单明细ID
     * @return 结果
     */
    public int deleteFaYuyueOrderDetailById(Long id);

    /**
     * 批量删除预约订单明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaYuyueOrderDetailByIds(Long[] ids);

    List<YuyueOrderDetailVo> listYuyueOrderDetailVo(YuyueOrderDetailVo faYuyueOrderDetail);
}
