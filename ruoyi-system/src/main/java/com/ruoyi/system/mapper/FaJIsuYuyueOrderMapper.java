package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaJIsuYuyueOrder;
import com.ruoyi.system.domain.vo.JisuYuyueOrderVo;

/**
 * 寄宿预约订单Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-06
 */
public interface FaJIsuYuyueOrderMapper 
{
    /**
     * 查询寄宿预约订单
     * 
     * @param id 寄宿预约订单ID
     * @return 寄宿预约订单
     */
    public FaJIsuYuyueOrder selectFaJIsuYuyueOrderById(Long id);

    /**
     * 查询寄宿预约订单列表
     * 
     * @param faJIsuYuyueOrder 寄宿预约订单
     * @return 寄宿预约订单集合
     */
    public List<FaJIsuYuyueOrder> selectFaJIsuYuyueOrderList(JisuYuyueOrderVo faJIsuYuyueOrder);

    /**
     * 新增寄宿预约订单
     * 
     * @param faJIsuYuyueOrder 寄宿预约订单
     * @return 结果
     */
    public int insertFaJIsuYuyueOrder(FaJIsuYuyueOrder faJIsuYuyueOrder);

    /**
     * 修改寄宿预约订单
     * 
     * @param faJIsuYuyueOrder 寄宿预约订单
     * @return 结果
     */
    public int updateFaJIsuYuyueOrder(FaJIsuYuyueOrder faJIsuYuyueOrder);

    /**
     * 删除寄宿预约订单
     * 
     * @param id 寄宿预约订单ID
     * @return 结果
     */
    public int deleteFaJIsuYuyueOrderById(Long id);

    /**
     * 批量删除寄宿预约订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaJIsuYuyueOrderByIds(Long[] ids);
}
