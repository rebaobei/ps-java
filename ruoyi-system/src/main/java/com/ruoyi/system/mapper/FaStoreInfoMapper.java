package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.FaStoreInfo;

/**
 * 店铺信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-10-30
 */
public interface FaStoreInfoMapper 
{
    /**
     * 查询店铺信息
     * 
     * @param id 店铺信息ID
     * @return 店铺信息
     */
    public FaStoreInfo selectFaStoreInfoById(Long id);

    /**
     * 查询店铺信息列表
     * 
     * @param faStoreInfo 店铺信息
     * @return 店铺信息集合
     */
    public List<FaStoreInfo> selectFaStoreInfoList(FaStoreInfo faStoreInfo);

    /**
     * 新增店铺信息
     * 
     * @param faStoreInfo 店铺信息
     * @return 结果
     */
    public int insertFaStoreInfo(FaStoreInfo faStoreInfo);

    /**
     * 修改店铺信息
     * 
     * @param faStoreInfo 店铺信息
     * @return 结果
     */
    public int updateFaStoreInfo(FaStoreInfo faStoreInfo);

    /**
     * 删除店铺信息
     * 
     * @param id 店铺信息ID
     * @return 结果
     */
    public int deleteFaStoreInfoById(Long id);

    /**
     * 批量删除店铺信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaStoreInfoByIds(Long[] ids);
}
