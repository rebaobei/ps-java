package com.ruoyi.system.tool;

import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class SendMail {
    @Autowired
    JavaMailSender javaMailSender;

    public AjaxResult sendMail(String mail, String password){
        SimpleMailMessage message = new SimpleMailMessage();
        //邮件设置
        message.setSubject("培森家园");
        message.setText("店铺管理地址: http://localhost:8013/UserStore/example  " +
                "账号"+ mail+
                "密码: "+password);
        message.setTo(mail);
        message.setFrom("psjy-cxy@cpapri.top");
        javaMailSender.send(message);
        return AjaxResult.success("账号邮件发送成功");
    }
}
