package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物品分类对象 fa_game_things_category
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameThingsCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物品分类id，背包商城通用 */
    private Long id;

    /** 分类名 */
    @Excel(name = "分类名")
    private String categoryName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCategoryName(String categoryName) 
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName() 
    {
        return categoryName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("categoryName", getCategoryName())
            .toString();
    }
}
