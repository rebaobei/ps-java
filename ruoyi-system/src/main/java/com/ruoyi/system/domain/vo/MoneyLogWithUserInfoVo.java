package com.ruoyi.system.domain.vo;

import java.math.BigDecimal;

public class MoneyLogWithUserInfoVo {
    private Integer id;
    private String username;
    private String nickname;
    private String mobile;
    private BigDecimal money;
    private String memo;
    private Long createtime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public MoneyLogWithUserInfoVo(Integer id, String username, String nickname, String mobile, BigDecimal money, String memo, Long createtime) {
        this.id = id;
        this.username = username;
        this.nickname = nickname;
        this.mobile = mobile;
        this.money = money;
        this.memo = memo;
        this.createtime = createtime;
    }

    public MoneyLogWithUserInfoVo() {
    }
}
