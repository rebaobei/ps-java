package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 每日任务完成记录对象 fa_game_day_task_log
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameDayTaskLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 玩家id */
    @Excel(name = "玩家id")
    private Long playerId;

    /** 任务开始时间，时间戳 */
    @Excel(name = "任务开始时间，时间戳")
    private Long startTime;

    /** 任务状态  去完成 已完成 领取 */
    @Excel(name = "任务状态  去完成 已完成 领取")
    private String status;

    /** 任务类型 */
    @Excel(name = "任务类型")
    private Long taskId;

    /** 需要完成的次数 */
    @Excel(name = "需要完成的次数")
    private Long requireTimes;

    /** 已完成次数 */
    @Excel(name = "已完成次数")
    private Long successTimes;

    /** 完成时间 */
    @Excel(name = "完成时间")
    private Long endTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlayerId(Long playerId) 
    {
        this.playerId = playerId;
    }

    public Long getPlayerId() 
    {
        return playerId;
    }
    public void setStartTime(Long startTime) 
    {
        this.startTime = startTime;
    }

    public Long getStartTime() 
    {
        return startTime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setTaskId(Long taskId) 
    {
        this.taskId = taskId;
    }

    public Long getTaskId() 
    {
        return taskId;
    }
    public void setRequireTimes(Long requireTimes) 
    {
        this.requireTimes = requireTimes;
    }

    public Long getRequireTimes() 
    {
        return requireTimes;
    }
    public void setSuccessTimes(Long successTimes) 
    {
        this.successTimes = successTimes;
    }

    public Long getSuccessTimes() 
    {
        return successTimes;
    }
    public void setEndTime(Long endTime) 
    {
        this.endTime = endTime;
    }

    public Long getEndTime() 
    {
        return endTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("playerId", getPlayerId())
            .append("startTime", getStartTime())
            .append("status", getStatus())
            .append("taskId", getTaskId())
            .append("requireTimes", getRequireTimes())
            .append("successTimes", getSuccessTimes())
            .append("endTime", getEndTime())
            .toString();
    }
}
