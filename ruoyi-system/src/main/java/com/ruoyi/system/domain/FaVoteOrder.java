package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 打赏订单对象 fa_vote_order
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public class FaVoteOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 订单号 */
    @Excel(name = "订单号")
    private String code;

    /** 选手id */
    @Excel(name = "选手id")
    private Long voteId;

    /** 购买人id */
    @Excel(name = "购买人id")
    private Long userId;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 投票数量 */
    @Excel(name = "投票数量")
    private Long voteCount;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private String paytype;

    /** 支付时间 */
    @Excel(name = "支付时间")
    private Long paytime;

    /** 支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款 */
    @Excel(name = "支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款")
    private String isPay;

    /** 投票活动id */
    @Excel(name = "投票活动id")
    private Long mainId;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setVoteId(Long voteId) 
    {
        this.voteId = voteId;
    }

    public Long getVoteId() 
    {
        return voteId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setVoteCount(Long voteCount) 
    {
        this.voteCount = voteCount;
    }

    public Long getVoteCount() 
    {
        return voteCount;
    }
    public void setPaytype(String paytype) 
    {
        this.paytype = paytype;
    }

    public String getPaytype() 
    {
        return paytype;
    }
    public void setPaytime(Long paytime) 
    {
        this.paytime = paytime;
    }

    public Long getPaytime() 
    {
        return paytime;
    }
    public void setIsPay(String isPay) 
    {
        this.isPay = isPay;
    }

    public String getIsPay() 
    {
        return isPay;
    }
    public void setMainId(Long mainId) 
    {
        this.mainId = mainId;
    }

    public Long getMainId() 
    {
        return mainId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("voteId", getVoteId())
            .append("userId", getUserId())
            .append("price", getPrice())
            .append("voteCount", getVoteCount())
            .append("paytype", getPaytype())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("paytime", getPaytime())
            .append("isPay", getIsPay())
            .append("mainId", getMainId())
            .toString();
    }
}
