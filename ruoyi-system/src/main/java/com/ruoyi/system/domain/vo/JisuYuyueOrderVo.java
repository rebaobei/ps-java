package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/6
 **/
public class JisuYuyueOrderVo {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单类型:0=SPA订单,1=房间订单,2=接送订单 */
    private String ordertype;

    /** 订单号 */
    private String ordercode;

    /** 预约电话 */
    private String tel;

    /** 用户id */
    private Long userId;

    /** 技师id */
    private Long techId;

    /** 分类id */
    private Long fenleiId;

    /** 房间id */
    private Long roomId;

    /** 项目id */
    private Long proId;

    /** 预约时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date yuyueTime;

    /** 总价格 */
    private BigDecimal price;

    /** 优惠金额 */
    private BigDecimal discountPrice;

    /** 数量 */
    private Long quty;

    /** 单价 */
    private BigDecimal danprice;

    /** 支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款 */
    private String isPay;

    /** 备注 */
    private String note;

    /** 状态:0=待支付,1=待核销,2=已核销,-1=作废 */
    private String status;

    /** 体重id */
    private Long sizeId;

    /** 支付时间 */
    private Long paytime;

    /** 支付方式 */
    private String paycode;

    /** 扫码人id */
    private Long saomauserId;

    /** 核销时间 */
    private Long checkdatetime;

    /** 姓名 */
    private String username;

    /** 性别0男1女 */
    private Long sex;

    /** 寄养天数 */
    private Long jiyangday;

    /** 宠主生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date userbir;

    /** 会员尾款 */
    private BigDecimal weikuanprice;

    /** 尾款编码 */
    private String weikuancode;

    /** 尾款支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款 */
    private String weikuanisPay;

    /** 申请退款时间 */
    private Long returntime;

    /** 实际退款时间 */
    private Long checkreturntime;

    /** 退款金额 */
    private BigDecimal returnmoney;

    /** 退款单号 */
    private String returncode;

    /** 寄宿预约总金额 */
    private BigDecimal yuyuesummoney;

    /** 评论次数 */
    private Long commenttimes;

    /** 是否开发票：0未开1已开 */
    private Integer invoiceType;

    /** 退款原因 */
    private String returnnote;

    /** 是否专车接送：0否1是 */
    private Integer jsType;

    /** 更新时间 */
    private Long updatetime;

    /** 创建时间 */
    private Long createtime;

    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(String ordertype) {
        this.ordertype = ordertype;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTechId() {
        return techId;
    }

    public void setTechId(Long techId) {
        this.techId = techId;
    }

    public Long getFenleiId() {
        return fenleiId;
    }

    public void setFenleiId(Long fenleiId) {
        this.fenleiId = fenleiId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public Long getProId() {
        return proId;
    }

    public void setProId(Long proId) {
        this.proId = proId;
    }

    public Date getYuyueTime() {
        return yuyueTime;
    }

    public void setYuyueTime(Date yuyueTime) {
        this.yuyueTime = yuyueTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Long getQuty() {
        return quty;
    }

    public void setQuty(Long quty) {
        this.quty = quty;
    }

    public BigDecimal getDanprice() {
        return danprice;
    }

    public void setDanprice(BigDecimal danprice) {
        this.danprice = danprice;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getSizeId() {
        return sizeId;
    }

    public void setSizeId(Long sizeId) {
        this.sizeId = sizeId;
    }

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

    public String getPaycode() {
        return paycode;
    }

    public void setPaycode(String paycode) {
        this.paycode = paycode;
    }

    public Long getSaomauserId() {
        return saomauserId;
    }

    public void setSaomauserId(Long saomauserId) {
        this.saomauserId = saomauserId;
    }

    public Long getCheckdatetime() {
        return checkdatetime;
    }

    public void setCheckdatetime(Long checkdatetime) {
        this.checkdatetime = checkdatetime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getSex() {
        return sex;
    }

    public void setSex(Long sex) {
        this.sex = sex;
    }

    public Long getJiyangday() {
        return jiyangday;
    }

    public void setJiyangday(Long jiyangday) {
        this.jiyangday = jiyangday;
    }

    public Date getUserbir() {
        return userbir;
    }

    public void setUserbir(Date userbir) {
        this.userbir = userbir;
    }

    public BigDecimal getWeikuanprice() {
        return weikuanprice;
    }

    public void setWeikuanprice(BigDecimal weikuanprice) {
        this.weikuanprice = weikuanprice;
    }

    public String getWeikuancode() {
        return weikuancode;
    }

    public void setWeikuancode(String weikuancode) {
        this.weikuancode = weikuancode;
    }

    public String getWeikuanisPay() {
        return weikuanisPay;
    }

    public void setWeikuanisPay(String weikuanisPay) {
        this.weikuanisPay = weikuanisPay;
    }

    public Long getReturntime() {
        return returntime;
    }

    public void setReturntime(Long returntime) {
        this.returntime = returntime;
    }

    public Long getCheckreturntime() {
        return checkreturntime;
    }

    public void setCheckreturntime(Long checkreturntime) {
        this.checkreturntime = checkreturntime;
    }

    public BigDecimal getReturnmoney() {
        return returnmoney;
    }

    public void setReturnmoney(BigDecimal returnmoney) {
        this.returnmoney = returnmoney;
    }

    public String getReturncode() {
        return returncode;
    }

    public void setReturncode(String returncode) {
        this.returncode = returncode;
    }

    public BigDecimal getYuyuesummoney() {
        return yuyuesummoney;
    }

    public void setYuyuesummoney(BigDecimal yuyuesummoney) {
        this.yuyuesummoney = yuyuesummoney;
    }

    public Long getCommenttimes() {
        return commenttimes;
    }

    public void setCommenttimes(Long commenttimes) {
        this.commenttimes = commenttimes;
    }

    public Integer getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(Integer invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getReturnnote() {
        return returnnote;
    }

    public void setReturnnote(String returnnote) {
        this.returnnote = returnnote;
    }

    public Integer getJsType() {
        return jsType;
    }

    public void setJsType(Integer jsType) {
        this.jsType = jsType;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }
}
