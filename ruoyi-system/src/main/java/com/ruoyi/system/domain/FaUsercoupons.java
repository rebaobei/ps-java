package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员优惠券对象 fa_usercoupons
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class FaUsercoupons extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    /** 会员 */
    @Excel(name = "会员")
    private Long userId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 类型:0=套餐券,1=活动券,2=提成券,3=注册券,4=其他券 */
    @Excel(name = "类型:0=套餐券,1=活动券,2=提成券,3=注册券,4=其他券")
    private String billtype;

    /** 订单id */
    @Excel(name = "订单id")
    private Long billid;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderid;

    /** 优惠券id */
    @Excel(name = "优惠券id")
    private Long couponsId;

    /** 开始时间 */
    @Excel(name = "开始时间")
    private Long begtime;

    /** 结束时间 */
    @Excel(name = "结束时间")
    private Long endtime;

    /** 使用时间 */
    @Excel(name = "使用时间")
    private Long usetime;

    /** 状态:0=未使用,1=已使用,-1=禁用 */
    @Excel(name = "状态:0=未使用,1=已使用,-1=禁用")
    private String status;

    /** 扫码人 */
    @Excel(name = "扫码人")
    private Long dianyuanId;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setManystorePid(Long manystorePid) 
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid() 
    {
        return manystorePid;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setBilltype(String billtype) 
    {
        this.billtype = billtype;
    }

    public String getBilltype() 
    {
        return billtype;
    }
    public void setBillid(Long billid) 
    {
        this.billid = billid;
    }

    public Long getBillid() 
    {
        return billid;
    }
    public void setOrderid(Long orderid) 
    {
        this.orderid = orderid;
    }

    public Long getOrderid() 
    {
        return orderid;
    }
    public void setCouponsId(Long couponsId) 
    {
        this.couponsId = couponsId;
    }

    public Long getCouponsId() 
    {
        return couponsId;
    }
    public void setBegtime(Long begtime) 
    {
        this.begtime = begtime;
    }

    public Long getBegtime() 
    {
        return begtime;
    }
    public void setEndtime(Long endtime) 
    {
        this.endtime = endtime;
    }

    public Long getEndtime() 
    {
        return endtime;
    }
    public void setUsetime(Long usetime) 
    {
        this.usetime = usetime;
    }

    public Long getUsetime() 
    {
        return usetime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDianyuanId(Long dianyuanId) 
    {
        this.dianyuanId = dianyuanId;
    }

    public Long getDianyuanId() 
    {
        return dianyuanId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("manystorePid", getManystorePid())
            .append("userId", getUserId())
            .append("name", getName())
            .append("billtype", getBilltype())
            .append("billid", getBillid())
            .append("orderid", getOrderid())
            .append("couponsId", getCouponsId())
            .append("begtime", getBegtime())
            .append("endtime", getEndtime())
            .append("createtime", getCreatetime())
            .append("usetime", getUsetime())
            .append("status", getStatus())
            .append("dianyuanId", getDianyuanId())
            .append("note", getNote())
            .toString();
    }
}
