package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物姿势对象 fa_game_pets_posture
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGamePetsPosture extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 宠物姿势 */
    @Excel(name = "宠物姿势")
    private String posture;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPosture(String posture) 
    {
        this.posture = posture;
    }

    public String getPosture() 
    {
        return posture;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("posture", getPosture())
            .toString();
    }
}
