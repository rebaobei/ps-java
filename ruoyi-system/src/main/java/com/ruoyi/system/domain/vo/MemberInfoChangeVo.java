package com.ruoyi.system.domain.vo;

import java.math.BigDecimal;

/**
 * 会员管理调整积分
 */
public class MemberInfoChangeVo {

    private Integer id;
    private BigDecimal count;
    private String cause;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}
