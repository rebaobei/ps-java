package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 充值订单对象 fa_recharge_order
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public class FaRechargeOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 订单号 */
    @Excel(name = "订单号")
    private String ordercode;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 充值金额 */
    @Excel(name = "充值金额")
    private BigDecimal money;

    /** 支付状态:0=未支付,1=已支付 */
    @Excel(name = "支付状态:0=未支付,1=已支付")
    private String isPay;

    /** 支付时间 */
    @Excel(name = "支付时间")
    private Long paytime;

    /** 支付方式:iap苹果支付 */
    @Excel(name = "支付方式:iap苹果支付")
    private String paycode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrdercode(String ordercode) 
    {
        this.ordercode = ordercode;
    }

    public String getOrdercode() 
    {
        return ordercode;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setIsPay(String isPay) 
    {
        this.isPay = isPay;
    }

    public String getIsPay() 
    {
        return isPay;
    }
    public void setPaytime(Long paytime) 
    {
        this.paytime = paytime;
    }

    public Long getPaytime() 
    {
        return paytime;
    }
    public void setPaycode(String paycode) 
    {
        this.paycode = paycode;
    }

    public String getPaycode() 
    {
        return paycode;
    }

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;


    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ordercode", getOrdercode())
            .append("userId", getUserId())
            .append("money", getMoney())
            .append("isPay", getIsPay())
            .append("createtime", getCreatetime())
            .append("paytime", getPaytime())
            .append("paycode", getPaycode())
            .toString();
    }
}
