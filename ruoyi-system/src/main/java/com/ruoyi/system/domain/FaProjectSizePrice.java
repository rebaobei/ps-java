package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 预约项目价格对象 fa_project_size_price
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
public class FaProjectSizePrice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private Long proId;

    /** 尺寸名称 */
    @Excel(name = "尺寸名称")
    private Long sizeId;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProId(Long proId) 
    {
        this.proId = proId;
    }

    public Long getProId() 
    {
        return proId;
    }
    public void setSizeId(Long sizeId) 
    {
        this.sizeId = sizeId;
    }

    public Long getSizeId() 
    {
        return sizeId;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proId", getProId())
            .append("sizeId", getSizeId())
            .append("price", getPrice())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .toString();
    }
}
