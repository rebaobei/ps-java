package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员Token对象 fa_user_token
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class FaUserToken extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** Token */
    private String token;

    /** 会员ID */
    @Excel(name = "会员ID")
    private Integer userId;

    /** 过期时间 */
    @Excel(name = "过期时间")
    private Long expiretime;

    public void setToken(String token) 
    {
        this.token = token;
    }

    public String getToken() 
    {
        return token;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setExpiretime(Long expiretime) 
    {
        this.expiretime = expiretime;
    }

    public Long getExpiretime() 
    {
        return expiretime;
    }

    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("token", getToken())
            .append("userId", getUserId())
            .append("createtime", getCreatetime())
            .append("expiretime", getExpiretime())
            .toString();
    }
}
