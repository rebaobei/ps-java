package com.ruoyi.system.domain.vo;

/**
 * 带有用户信息的积分变动
 */
public class ScoreLogWithUserInfo {
    private Integer id;
    private String username;
    private String nickname;
    private String mobile;
    private Long score;
    private String memo;
    private Long createtime;

    public ScoreLogWithUserInfo(Integer id, String username, String nickname, String mobile, Long score, String memo, Long createtime) {
        this.id = id;
        this.username = username;
        this.nickname = nickname;
        this.mobile = mobile;
        this.score = score;
        this.memo = memo;
        this.createtime = createtime;
    }

    public ScoreLogWithUserInfo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }
}
