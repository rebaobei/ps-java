package com.ruoyi.system.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单商品对象 fa_xshop_order_product
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public class FaXshopOrderProduct implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 订单 */
    @Excel(name = "订单")
    private Integer orderId;

    /** 商品 */
    @Excel(name = "商品")
    private Integer productId;

    /** $column.columnComment */
    @Excel(name = "商品")
    private Integer skuId;

    /** 商家 */
    @Excel(name = "商家")
    private Integer shopId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String title;

    /** 商品描述 */
    @Excel(name = "商品描述")
    private String description;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String image;

    /** 商品规格 */
    @Excel(name = "商品规格")
    private String attributes;

    /** 销售价 */
    @Excel(name = "销售价")
    private BigDecimal price;

    /** 数量 */
    @Excel(name = "数量")
    private Long quantity;

    /** 应付金额 */
    @Excel(name = "应付金额")
    private BigDecimal productPrice;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal discountPrice;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private BigDecimal orderPrice;

    /** $column.columnComment */
    @Excel(name = "订单金额")
    private Integer buyerReview;

    /** $column.columnComment */
    @Excel(name = "订单金额")
    private Long deleteTime;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private String payType;

    /** 所需积分 */
    @Excel(name = "所需积分")
    private BigDecimal goodsscore;

    /** 商品类型：1：普通商品2：1小时送货到家 */
    @Excel(name = "商品类型：1：普通商品2：1小时送货到家")
    private Integer goodstype;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createTime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updateTime;

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setOrderId(Integer orderId) 
    {
        this.orderId = orderId;
    }

    public Integer getOrderId() 
    {
        return orderId;
    }
    public void setProductId(Integer productId) 
    {
        this.productId = productId;
    }

    public Integer getProductId() 
    {
        return productId;
    }
    public void setSkuId(Integer skuId) 
    {
        this.skuId = skuId;
    }

    public Integer getSkuId() 
    {
        return skuId;
    }
    public void setShopId(Integer shopId) 
    {
        this.shopId = shopId;
    }

    public Integer getShopId() 
    {
        return shopId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setAttributes(String attributes) 
    {
        this.attributes = attributes;
    }

    public String getAttributes() 
    {
        return attributes;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setProductPrice(BigDecimal productPrice) 
    {
        this.productPrice = productPrice;
    }

    public BigDecimal getProductPrice() 
    {
        return productPrice;
    }
    public void setDiscountPrice(BigDecimal discountPrice) 
    {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getDiscountPrice() 
    {
        return discountPrice;
    }
    public void setOrderPrice(BigDecimal orderPrice) 
    {
        this.orderPrice = orderPrice;
    }

    public BigDecimal getOrderPrice() 
    {
        return orderPrice;
    }
    public void setBuyerReview(Integer buyerReview) 
    {
        this.buyerReview = buyerReview;
    }

    public Integer getBuyerReview() 
    {
        return buyerReview;
    }
    public void setDeleteTime(Long deleteTime) 
    {
        this.deleteTime = deleteTime;
    }

    public Long getDeleteTime() 
    {
        return deleteTime;
    }
    public void setManystorePid(Long manystorePid) 
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid() 
    {
        return manystorePid;
    }
    public void setPayType(String payType) 
    {
        this.payType = payType;
    }

    public String getPayType() 
    {
        return payType;
    }
    public void setGoodsscore(BigDecimal goodsscore) 
    {
        this.goodsscore = goodsscore;
    }

    public BigDecimal getGoodsscore() 
    {
        return goodsscore;
    }
    public void setGoodstype(Integer goodstype) 
    {
        this.goodstype = goodstype;
    }

    public Integer getGoodstype() 
    {
        return goodstype;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("productId", getProductId())
            .append("skuId", getSkuId())
            .append("shopId", getShopId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("image", getImage())
            .append("attributes", getAttributes())
            .append("price", getPrice())
            .append("quantity", getQuantity())
            .append("productPrice", getProductPrice())
            .append("discountPrice", getDiscountPrice())
            .append("orderPrice", getOrderPrice())
            .append("buyerReview", getBuyerReview())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("deleteTime", getDeleteTime())
            .append("manystorePid", getManystorePid())
            .append("payType", getPayType())
            .append("goodsscore", getGoodsscore())
            .append("goodstype", getGoodstype())
            .append("userId", getUserId())
            .toString();
    }
}
