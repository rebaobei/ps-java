package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 店铺信息记录对象 fa_store_info_record
 *
 * @author ruoyi
 * @date 2021-10-30
 */
public class FaStoreInfoRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 店铺名称 */
    @Excel(name = "店铺名称")
    private String storeName;
    @Excel(name = "管理后台帐号")
    private String account;
    /** 店铺描述 */
    private String storeDescribe;

    /** 店铺联系方式 */
    @Excel(name = "店铺联系方式")
    private String phone;

    /** 地址 */
    @Excel(name = "地址")
    private String address;
    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 首页轮播图 */
    private String bannerimages;

    /** 图片 */
    private String images;

    /** 营业开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "营业开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date businessHoursStr;

    /** 营业结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "营业结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date businessHoursEnd;

    /** 经度 */
    private String longitude;

    /** 纬度 */
    private String latitude;

    /** 店铺星级 */
    @Excel(name = "店铺星级")
    private Long starLevel;

    /** 店铺标签 */
    private String label;

    /** 品牌id */
    private Long brandId;

    /** 商户id */
    private Long userId;

    /** 状态 */
    private Long status;

    /** 备注 */
    @Excel(name = "备注")
    private String reamrk;

    /** 负责人名称 */
    @Excel(name = "负责人名称")
    private String principalName;

    /** 负责人联系方式 */
    @Excel(name = "负责人联系方式")
    private String principalPhone;

    /** 主营业务 */
    @Excel(name = "主营业务")
    private String business;

    /** 营业资格证 */
    @Excel(name = "营业资格证")
    private String businessImages;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkDateTime;

    /** 店铺id */
    private Long storeInfoId;

    /** 余额 */
    private BigDecimal amount;

    /** 冻结金额 */
    private BigDecimal frozenAmount;

    /** 申请类型 */
    private Integer checkType;


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setStoreName(String storeName)
    {
        this.storeName = storeName;
    }

    public String getStoreName()
    {
        return storeName;
    }
    public void setStoreDescribe(String storeDescribe)
    {
        this.storeDescribe = storeDescribe;
    }

    public String getStoreDescribe()
    {
        return storeDescribe;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public Integer getCheckType() {
        return checkType;
    }

    public void setCheckType(Integer checkType) {
        this.checkType = checkType;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }
    public void setBannerimages(String bannerimages)
    {
        this.bannerimages = bannerimages;
    }

    public String getBannerimages()
    {
        return bannerimages;
    }
    public void setImages(String images)
    {
        this.images = images;
    }

    public String getImages()
    {
        return images;
    }
    public void setBusinessHoursStr(Date businessHoursStr)
    {
        this.businessHoursStr = businessHoursStr;
    }

    public Date getBusinessHoursStr()
    {
        return businessHoursStr;
    }
    public void setBusinessHoursEnd(Date businessHoursEnd)
    {
        this.businessHoursEnd = businessHoursEnd;
    }

    public Date getBusinessHoursEnd()
    {
        return businessHoursEnd;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setStarLevel(Long starLevel)
    {
        this.starLevel = starLevel;
    }

    public Long getStarLevel()
    {
        return starLevel;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
    public void setBrandId(Long brandId)
    {
        this.brandId = brandId;
    }

    public Long getBrandId()
    {
        return brandId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setReamrk(String reamrk)
    {
        this.reamrk = reamrk;
    }

    public String getReamrk()
    {
        return reamrk;
    }
    public void setPrincipalName(String principalName)
    {
        this.principalName = principalName;
    }

    public String getPrincipalName()
    {
        return principalName;
    }
    public void setPrincipalPhone(String principalPhone)
    {
        this.principalPhone = principalPhone;
    }

    public String getPrincipalPhone()
    {
        return principalPhone;
    }
    public void setBusiness(String business)
    {
        this.business = business;
    }

    public String getBusiness()
    {
        return business;
    }
    public void setBusinessImages(String businessImages)
    {
        this.businessImages = businessImages;
    }

    public String getBusinessImages()
    {
        return businessImages;
    }
    public void setCheckDateTime(Date checkDateTime)
    {
        this.checkDateTime = checkDateTime;
    }

    public Date getCheckDateTime()
    {
        return checkDateTime;
    }
    public void setStoreInfoId(Long storeInfoId)
    {
        this.storeInfoId = storeInfoId;
    }

    public Long getStoreInfoId()
    {
        return storeInfoId;
    }
    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }
    public void setFrozenAmount(BigDecimal frozenAmount)
    {
        this.frozenAmount = frozenAmount;
    }

    public BigDecimal getFrozenAmount()
    {
        return frozenAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("storeName", getStoreName())
            .append("storeDescribe", getStoreDescribe())
            .append("phone", getPhone())
            .append("address", getAddress())
            .append("bannerimages", getBannerimages())
            .append("images", getImages())
            .append("businessHoursStr", getBusinessHoursStr())
            .append("businessHoursEnd", getBusinessHoursEnd())
            .append("longitude", getLongitude())
            .append("latitude", getLatitude())
            .append("starLevel", getStarLevel())
            .append("label", getLabel())
            .append("brandId", getBrandId())
            .append("userId", getUserId())
            .append("status", getStatus())
            .append("reamrk", getReamrk())
            .append("principalName", getPrincipalName())
            .append("principalPhone", getPrincipalPhone())
            .append("business", getBusiness())
            .append("businessImages", getBusinessImages())
            .append("checkDateTime", getCheckDateTime())
            .append("storeInfoId", getStoreInfoId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("amount", getAmount())
            .append("frozenAmount", getFrozenAmount())
            .toString();
    }
}
