package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * APP版本管理对象 fa_version
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public class FaVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 旧版本号 */
    @Excel(name = "旧版本号")
    private String oldversion;

    /** 新版本号 */
    @Excel(name = "新版本号")
    private String newversion;

    /** 包大小 */
    @Excel(name = "包大小")
    private String packagesize;

    /** 升级内容 */
    @Excel(name = "升级内容")
    private String content;

    /** 下载地址 */
    @Excel(name = "下载地址")
    private String downloadurl;

    /** 强制更新 */
    @Excel(name = "强制更新")
    private Integer enforce;

    /** 权重 */
    @Excel(name = "权重")
    private Long weigh;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOldversion(String oldversion) 
    {
        this.oldversion = oldversion;
    }

    public String getOldversion() 
    {
        return oldversion;
    }
    public void setNewversion(String newversion) 
    {
        this.newversion = newversion;
    }

    public String getNewversion() 
    {
        return newversion;
    }
    public void setPackagesize(String packagesize) 
    {
        this.packagesize = packagesize;
    }

    public String getPackagesize() 
    {
        return packagesize;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setDownloadurl(String downloadurl) 
    {
        this.downloadurl = downloadurl;
    }

    public String getDownloadurl() 
    {
        return downloadurl;
    }
    public void setEnforce(Integer enforce) 
    {
        this.enforce = enforce;
    }

    public Integer getEnforce() 
    {
        return enforce;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("oldversion", getOldversion())
            .append("newversion", getNewversion())
            .append("packagesize", getPackagesize())
            .append("content", getContent())
            .append("downloadurl", getDownloadurl())
            .append("enforce", getEnforce())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("weigh", getWeigh())
            .append("status", getStatus())
            .toString();
    }
}
