package com.ruoyi.system.domain.vo;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/14
 **/
public class CommonSettingConfigVo {

    private Integer id;

    private String name;

    private String title;

    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
