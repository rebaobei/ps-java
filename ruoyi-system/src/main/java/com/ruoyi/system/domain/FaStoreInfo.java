package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 店铺信息对象 fa_store_info
 *
 * @author ruoyi
 * @date 2021-10-30
 */
public class FaStoreInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 店铺名称
     */
    @Excel(name = "店铺名称")
    private String storeName;

    /**
     * 店铺描述
     */
    private String storeDescribe;

    /**
     * 联系方式
     */
    @Excel(name = "联系方式")
    private String phone;
    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String email;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;

    /**
     * 首页轮播图
     */
    private String bannerimages;

    /**
     * 图片
     */
    private String images;

    /**
     * 营业开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date businessHoursStr;

    /**
     * 营业结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date businessHoursEnd;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 店铺星级
     */
    @Excel(name = "店铺星级")
    private Integer starLevel;

    /**
     * 店铺标签
     */
    private String label;

    /**
     * 品牌id
     */
    private Long brandId;

    /**
     * 商户id
     */
    private Long userId;

    /**
     * 状态  0 待审核   1 待补充信息  2 已补充信息  3  已审核  -1  驳回
     */
    @Excel(name = "状态")
    private Integer status;
    /**
     * 是否展示：0=不展示，1=展示
     */
    @Excel(name = "是否展示")
    private Integer sysShow;
    /**
     * 是否首页展示：0=不展示，1=展示
     */
    @Excel(name = "是否首页展示")
    private Integer sysHomeShow;

    /**
     * 备注
     */
    private String reamrk;

    /**
     * 负责人名称
     */
    @Excel(name = "负责人名称")
    private String principalName;

    /**
     * 负责人联系方式
     */
    @Excel(name = "负责人联系方式")
    private String principalPhone;

    /**
     * 主营业务
     */
    @Excel(name = "主营业务")
    private String business;

    /**
     * 营业资格证
     */
    private String businessImages;

    /**
     * 店铺品牌
     */
    private FaStoreBrand faStoreBrand;
    private BigDecimal amount;
    private BigDecimal frozenAmount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFrozenAmount() {
        return frozenAmount;
    }

    public void setFrozenAmount(BigDecimal frozenAmount) {
        this.frozenAmount = frozenAmount;
    }

    public Integer getSysShow() {
        return sysShow;
    }

    public void setSysShow(Integer sysShow) {
        this.sysShow = sysShow;
    }

    public Integer getSysHomeShow() {
        return sysHomeShow;
    }

    public void setSysHomeShow(Integer sysHomeShow) {
        this.sysHomeShow = sysHomeShow;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public FaStoreBrand getFaStoreBrand() {
        return faStoreBrand;
    }

    public void setFaStoreBrand(FaStoreBrand faStoreBrand) {
        this.faStoreBrand = faStoreBrand;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreDescribe(String storeDescribe) {
        this.storeDescribe = storeDescribe;
    }

    public String getStoreDescribe() {
        return storeDescribe;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setBannerimages(String bannerimages) {
        this.bannerimages = bannerimages;
    }

    public String getBannerimages() {
        return bannerimages;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getImages() {
        return images;
    }

    public void setBusinessHoursStr(Date businessHoursStr) {
        this.businessHoursStr = businessHoursStr;
    }

    public Date getBusinessHoursStr() {
        return businessHoursStr;
    }

    public void setBusinessHoursEnd(Date businessHoursEnd) {
        this.businessHoursEnd = businessHoursEnd;
    }

    public Date getBusinessHoursEnd() {
        return businessHoursEnd;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setStarLevel(Integer starLevel) {
        this.starLevel = starLevel;
    }

    public Integer getStarLevel() {
        return starLevel;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    void setReamrk(String reamrk) {
        this.reamrk = reamrk;
    }

    public String getReamrk() {
        return reamrk;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalPhone(String principalPhone) {
        this.principalPhone = principalPhone;
    }

    public String getPrincipalPhone() {
        return principalPhone;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusinessImages(String businessImages) {
        this.businessImages = businessImages;
    }

    public String getBusinessImages() {
        return businessImages;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("storeName", getStoreName())
                .append("storeDescribe", getStoreDescribe())
                .append("phone", getPhone())
                .append("address", getAddress())
                .append("bannerimages", getBannerimages())
                .append("images", getImages())
                .append("businessHoursStr", getBusinessHoursStr())
                .append("businessHoursEnd", getBusinessHoursEnd())
                .append("longitude", getLongitude())
                .append("latitude", getLatitude())
                .append("starLevel", getStarLevel())
                .append("label", getLabel())
                .append("brandId", getBrandId())
                .append("userId", getUserId())
                .append("status", getStatus())
                .append("reamrk", getReamrk())
                .append("createTime", getCreateTime())
                .append("principalName", getPrincipalName())
                .append("updateTime", getUpdateTime())
                .append("principalPhone", getPrincipalPhone())
                .append("business", getBusiness())
                .append("businessImages", getBusinessImages())
                .toString();
    }
}
