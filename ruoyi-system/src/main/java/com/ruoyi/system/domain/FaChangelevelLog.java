package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 登记调整记录对象 fa_changelevel_log
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class FaChangelevelLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 原级别 */
    @Excel(name = "原级别")
    private Long oldlevel;

    /** 新级别 */
    @Excel(name = "新级别")
    private Long newlevel;

    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setOldlevel(Long oldlevel) 
    {
        this.oldlevel = oldlevel;
    }

    public Long getOldlevel() 
    {
        return oldlevel;
    }
    public void setNewlevel(Long newlevel) 
    {
        this.newlevel = newlevel;
    }

    public Long getNewlevel() 
    {
        return newlevel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("oldlevel", getOldlevel())
            .append("newlevel", getNewlevel())
            .append("createtime", getCreatetime())
            .toString();
    }
}
