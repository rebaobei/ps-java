package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 参赛人员对象 fa_vote_user
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public class FaVoteUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 主表id */
    @Excel(name = "主表id")
    private Long mainId;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 选手号 */
    @Excel(name = "选手号")
    private Long num;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 轮播图 */
    @Excel(name = "轮播图")
    private String images;

    /** 宠物寄语 */
    @Excel(name = "宠物寄语")
    private String describe;

    /** 详情 */
    @Excel(name = "详情")
    private String content;

    /** 投票数 */
    @Excel(name = "投票数")
    private Long votecount;

    /** 状态:0=隐藏,1=显示 */
    @Excel(name = "状态:0=隐藏,1=显示")
    private String status;

    /** 主人姓名 */
    @Excel(name = "主人姓名")
    private String userName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String tel;

    /** 宠物昵称 */
    @Excel(name = "宠物昵称")
    private String petName;

    /** 宠物性别 */
    @Excel(name = "宠物性别")
    private String petSex;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMainId(Long mainId) 
    {
        this.mainId = mainId;
    }

    public Long getMainId() 
    {
        return mainId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setVotecount(Long votecount) 
    {
        this.votecount = votecount;
    }

    public Long getVotecount() 
    {
        return votecount;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setPetName(String petName) 
    {
        this.petName = petName;
    }

    public String getPetName() 
    {
        return petName;
    }
    public void setPetSex(String petSex) 
    {
        this.petSex = petSex;
    }

    public String getPetSex() 
    {
        return petSex;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mainId", getMainId())
            .append("userId", getUserId())
            .append("num", getNum())
            .append("image", getImage())
            .append("images", getImages())
            .append("describe", getDescribe())
            .append("content", getContent())
            .append("votecount", getVotecount())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("userName", getUserName())
            .append("tel", getTel())
            .append("petName", getPetName())
            .append("petSex", getPetSex())
            .toString();
    }
}
