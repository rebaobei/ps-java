package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 游戏玩家对象 fa_game_player
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGamePlayer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 游戏玩家Id，与fa_user中id共用 */
    private Long id;

    /** 宠物币数量 */
    @Excel(name = "宠物币数量")
    private Integer coin;

    /** 背景图 */
    @Excel(name = "背景图")
    private String backImg;

    /** 头像 */
    @Excel(name = "头像")
    private String headerImg;

    /** 默认显示的宠物 */
    @Excel(name = "默认显示的宠物")
    private Long defaultPet;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCoin(Integer coin) 
    {
        this.coin = coin;
    }

    public Integer getCoin() 
    {
        return coin;
    }
    public void setBackImg(String backImg) 
    {
        this.backImg = backImg;
    }

    public String getBackImg() 
    {
        return backImg;
    }
    public void setHeaderImg(String headerImg) 
    {
        this.headerImg = headerImg;
    }

    public String getHeaderImg() 
    {
        return headerImg;
    }
    public void setDefaultPet(Long defaultPet) 
    {
        this.defaultPet = defaultPet;
    }

    public Long getDefaultPet() 
    {
        return defaultPet;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("coin", getCoin())
            .append("backImg", getBackImg())
            .append("headerImg", getHeaderImg())
            .append("defaultPet", getDefaultPet())
            .toString();
    }
}
