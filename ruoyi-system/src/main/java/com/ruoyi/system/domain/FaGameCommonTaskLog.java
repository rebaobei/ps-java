package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 常规任务记录对象 fa_game_common_task_log
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameCommonTaskLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 操作者 */
    @Excel(name = "操作者")
    private Long playerId;

    /** 已领养的宠物id */
    @Excel(name = "已领养的宠物id")
    private Long petId;

    /** 喂养的时间 */
    @Excel(name = "喂养的时间")
    private Long startTime;

    /** 冷却结束时间 */
    @Excel(name = "冷却结束时间")
    private Long endTime;

    /** 常规任务类型，喂水还是喂食 */
    @Excel(name = "常规任务类型，喂水还是喂食")
    private Long commonTaskType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlayerId(Long playerId) 
    {
        this.playerId = playerId;
    }

    public Long getPlayerId() 
    {
        return playerId;
    }
    public void setPetId(Long petId) 
    {
        this.petId = petId;
    }

    public Long getPetId() 
    {
        return petId;
    }
    public void setStartTime(Long startTime) 
    {
        this.startTime = startTime;
    }

    public Long getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Long endTime) 
    {
        this.endTime = endTime;
    }

    public Long getEndTime() 
    {
        return endTime;
    }
    public void setCommonTaskType(Long commonTaskType) 
    {
        this.commonTaskType = commonTaskType;
    }

    public Long getCommonTaskType() 
    {
        return commonTaskType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("playerId", getPlayerId())
            .append("petId", getPetId())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("commonTaskType", getCommonTaskType())
            .toString();
    }
}
