package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.util.List;

/**
 * @Description 朋友圈明细
 * @Author fan
 * @Date 2021/7/31
 **/
public class UserArticleVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    private Long userId;

    /** 内容 */
    private String note;

    /** 图片 */
    private String images;

    /** 文件 */
    private String files;

    /** 浏览量 */
    private Long views;

    /** 点赞数量 */
    private Long zancount;

    /** 评论数量 */
    private Long comments;

    /** 状态:0=隐藏,1=审核 */
    private String status;

    /** 经度 */
    private String lng;

    /** 维度 */
    private String lat;

    /** 话题id */
    private String topicId;

    /** 比赛id */
    private Long bisaiId;

    /** 是否置顶:0=否,1=是 */
    private String ifTop;

    /** 状态:0=私密,1=公开 */
    private String userstatus;

    /** 类型:0=动态,1=帖子,2相册 */
    private String articleType;

    /** 分享数量 */
    private Long shareCount;

    /** 更新时间 */
    private Long updatetime;
    /** 创建时间 */
    private Long createtime;

    private String nickname;

    private String avatar;

    private String topicName;

    private List<String> imageList;

    public Long getId() {
        return id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public Long getZancount() {
        return zancount;
    }

    public void setZancount(Long zancount) {
        this.zancount = zancount;
    }

    public Long getComments() {
        return comments;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public Long getBisaiId() {
        return bisaiId;
    }

    public void setBisaiId(Long bisaiId) {
        this.bisaiId = bisaiId;
    }

    public String getIfTop() {
        return ifTop;
    }

    public void setIfTop(String ifTop) {
        this.ifTop = ifTop;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    public String getArticleType() {
        return articleType;
    }

    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }

    public Long getShareCount() {
        return shareCount;
    }

    public void setShareCount(Long shareCount) {
        this.shareCount = shareCount;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }
}
