package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 门店管理对象 fa_store
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
public class FaStore extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 店铺名称 */
    @Excel(name = "店铺名称")
    private String shopname;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String tel;

    /** 省 */
    @Excel(name = "省")
    private String province;

    /** 市 */
    @Excel(name = "市")
    private String city;

    /** 县 */
    @Excel(name = "县")
    private String district;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 描述 */
    @Excel(name = "描述")
    private String describe;

    /** 详情 */
    @Excel(name = "详情")
    private String content;

    /** 图片 */
    @Excel(name = "图片")
    private String images;

    /** 经度 */
    @Excel(name = "经度")
    private String longitude;

    /** 纬度 */
    @Excel(name = "纬度")
    private String latitude;

    /** 状态:0=待审核,1=已审核,-1=驳回 */
    @Excel(name = "状态:0=待审核,1=已审核,-1=驳回")
    private String status;

    /** 二维码 */
    @Excel(name = "二维码")
    private String qrcodeurl;

    /** 余额 */
    @Excel(name = "余额")
    private BigDecimal money;

    /** 首页轮播图 */
    @Excel(name = "首页轮播图")
    private String bannerimages;

    /** 散客上限 */
    @Excel(name = "散客上限")
    private Long maxcount;

    /** 半场数 */
    @Excel(name = "半场数")
    private Long bancount;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShopname(String shopname) 
    {
        this.shopname = shopname;
    }

    public String getShopname() 
    {
        return shopname;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setProvince(String province) 
    {
        this.province = province;
    }

    public String getProvince() 
    {
        return province;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setDistrict(String district) 
    {
        this.district = district;
    }

    public String getDistrict() 
    {
        return district;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setLongitude(String longitude) 
    {
        this.longitude = longitude;
    }

    public String getLongitude() 
    {
        return longitude;
    }
    public void setLatitude(String latitude) 
    {
        this.latitude = latitude;
    }

    public String getLatitude() 
    {
        return latitude;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setQrcodeurl(String qrcodeurl) 
    {
        this.qrcodeurl = qrcodeurl;
    }

    public String getQrcodeurl() 
    {
        return qrcodeurl;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setBannerimages(String bannerimages) 
    {
        this.bannerimages = bannerimages;
    }

    public String getBannerimages() 
    {
        return bannerimages;
    }
    public void setMaxcount(Long maxcount) 
    {
        this.maxcount = maxcount;
    }

    public Long getMaxcount() 
    {
        return maxcount;
    }
    public void setBancount(Long bancount) 
    {
        this.bancount = bancount;
    }

    public Long getBancount() 
    {
        return bancount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shopname", getShopname())
            .append("tel", getTel())
            .append("province", getProvince())
            .append("city", getCity())
            .append("district", getDistrict())
            .append("address", getAddress())
            .append("describe", getDescribe())
            .append("content", getContent())
            .append("images", getImages())
            .append("longitude", getLongitude())
            .append("latitude", getLatitude())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("qrcodeurl", getQrcodeurl())
            .append("money", getMoney())
            .append("bannerimages", getBannerimages())
            .append("maxcount", getMaxcount())
            .append("bancount", getBancount())
            .toString();
    }
}
