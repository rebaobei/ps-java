package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;

/**
 * @Description 话题返回结果
 * @Author fan
 * @Date 2021/7/31
 **/
public class TopicVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 话题名称 */
    private String name;

    /** 描述 */
    private String describe;

    /** 图标 */
    private String image;

    /** 描述图片 */
    private String desImage;

    /** 数量 */
    private Long addcount;

    /** 首页是否展示:0=否,1=是 */
    private String ifShow;

    /** 排序 */
    private Long weigh;

    /** 状态:0=禁用,1=启用 */
    private String status;

    /** 申请人 */
    private Long userId;

    /** 创建时间 */
    private Long createtime;

    /**
     * 申请人昵称
     */
    private String nickname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesImage() {
        return desImage;
    }

    public void setDesImage(String desImage) {
        this.desImage = desImage;
    }

    public Long getAddcount() {
        return addcount;
    }

    public void setAddcount(Long addcount) {
        this.addcount = addcount;
    }

    public String getIfShow() {
        return ifShow;
    }

    public void setIfShow(String ifShow) {
        this.ifShow = ifShow;
    }

    public Long getWeigh() {
        return weigh;
    }

    public void setWeigh(Long weigh) {
        this.weigh = weigh;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
