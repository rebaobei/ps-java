package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 提现管理对象 fa_store_tixian
 * 
 * @author ruoyi
 * @date 2022-02-09
 */
public class FaStoreTixian extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 提现表 */
    private Long id;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    /** 提现金额 */
    @Excel(name = "提现金额")
    private BigDecimal money;

    /** 实际到账金额 */
    @Excel(name = "实际到账金额")
    private BigDecimal realMoney;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String bank;

    /** 状态:0=待审核,1=已审核,2已转账,3已驳回 */
    @Excel(name = "状态:0=待审核,1=已审核,2已转账,3已驳回")
    private Long status;

    /** 银行名称 */
    @Excel(name = "银行名称")
    private String bankname;

    /** 支行名称 */
    @Excel(name = "支行名称")
    private String banksubname;

    /** 银行卡号 */
    @Excel(name = "银行卡号")
    private Long bankcardno;

    /** 银行卡持有人姓名 */
    @Excel(name = "银行卡持有人姓名")
    private String bankcardholdername;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String realname;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    private String storeName;
    private BigDecimal storeAmount;
    private BigDecimal storeFrozenAmount;

    public BigDecimal getStoreAmount() {
        return storeAmount;
    }

    public void setStoreAmount(BigDecimal storeAmount) {
        this.storeAmount = storeAmount;
    }

    public BigDecimal getStoreFrozenAmount() {
        return storeFrozenAmount;
    }

    public void setStoreFrozenAmount(BigDecimal storeFrozenAmount) {
        this.storeFrozenAmount = storeFrozenAmount;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setManystorePid(Long manystorePid) 
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid() 
    {
        return manystorePid;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setRealMoney(BigDecimal realMoney) 
    {
        this.realMoney = realMoney;
    }

    public BigDecimal getRealMoney() 
    {
        return realMoney;
    }
    public void setBank(String bank) 
    {
        this.bank = bank;
    }

    public String getBank() 
    {
        return bank;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setBankname(String bankname) 
    {
        this.bankname = bankname;
    }

    public String getBankname() 
    {
        return bankname;
    }
    public void setBanksubname(String banksubname) 
    {
        this.banksubname = banksubname;
    }

    public String getBanksubname() 
    {
        return banksubname;
    }
    public void setBankcardno(Long bankcardno) 
    {
        this.bankcardno = bankcardno;
    }

    public Long getBankcardno() 
    {
        return bankcardno;
    }
    public void setBankcardholdername(String bankcardholdername) 
    {
        this.bankcardholdername = bankcardholdername;
    }

    public String getBankcardholdername() 
    {
        return bankcardholdername;
    }
    public void setRealname(String realname) 
    {
        this.realname = realname;
    }

    public String getRealname() 
    {
        return realname;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("manystorePid", getManystorePid())
            .append("money", getMoney())
            .append("realMoney", getRealMoney())
            .append("remark", getRemark())
            .append("bank", getBank())
            .append("status", getStatus())
            .append("createtime", getCreateTime())
            .append("updatetime", getUpdateTime())
            .append("bankname", getBankname())
            .append("banksubname", getBanksubname())
            .append("bankcardno", getBankcardno())
            .append("bankcardholdername", getBankcardholdername())
            .append("realname", getRealname())
            .append("phone", getPhone())
            .toString();
    }
}
