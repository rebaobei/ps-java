package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 海报&广告位对象 fa_adszone_zone
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public class FaAdszoneZone extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 广告位名称 */
    @Excel(name = "广告位名称")
    private String name;

    /** 广告位标记 */
    @Excel(name = "广告位标记")
    private String mark;

    /** 广告位类型:1=图片广告,2=多图&幻灯广告,3=代码广告 */
    @Excel(name = "广告位类型:1=图片广告,2=多图&幻灯广告,3=代码广告")
    private String type;

    /** 广告宽度 */
    @Excel(name = "广告宽度")
    private Long width;

    /** 广告高度 */
    @Excel(name = "广告高度")
    private Long height;

    /** 广告代码 */
    @Excel(name = "广告代码")
    private String code;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMark(String mark) 
    {
        this.mark = mark;
    }

    public String getMark() 
    {
        return mark;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setWidth(Long width) 
    {
        this.width = width;
    }

    public Long getWidth() 
    {
        return width;
    }
    public void setHeight(Long height) 
    {
        this.height = height;
    }

    public Long getHeight() 
    {
        return height;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("mark", getMark())
            .append("type", getType())
            .append("width", getWidth())
            .append("height", getHeight())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("code", getCode())
            .toString();
    }
}
