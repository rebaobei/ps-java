package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物管理对象 fa_game_pets
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGamePets extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 游戏宠物ID */
    private Long petId;

    /** 宠物头像 */
    @Excel(name = "宠物头像")
    private String petIcon;

    /** 父分类id */
    @Excel(name = "父分类id")
    private Long parentId;

    /** 宠物名 */
    @Excel(name = "宠物名")
    private String petName;

    /** 父分类名 */
    @Excel(name = "父分类名")
    private String parentName;

    /** 创建时间 */
    // @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间")
    private Long gmtCreat;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间")
    private Long gmtModified;

    public void setPetId(Long petId) 
    {
        this.petId = petId;
    }

    public Long getPetId() 
    {
        return petId;
    }
    public void setPetIcon(String petIcon) 
    {
        this.petIcon = petIcon;
    }

    public String getPetIcon() 
    {
        return petIcon;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setPetName(String petName) 
    {
        this.petName = petName;
    }

    public String getPetName() 
    {
        return petName;
    }
    public void setParentName(String parentName) 
    {
        this.parentName = parentName;
    }

    public String getParentName() 
    {
        return parentName;
    }
    public void setGmtCreat(Long gmtCreat)
    {
        this.gmtCreat = gmtCreat;
    }

    public Long getGmtCreat()
    {
        return gmtCreat;
    }
    public void setGmtModified(Long gmtModified)
    {
        this.gmtModified = gmtModified;
    }

    public Long getGmtModified()
    {
        return gmtModified;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("petId", getPetId())
            .append("petIcon", getPetIcon())
            .append("parentId", getParentId())
            .append("petName", getPetName())
            .append("parentName", getParentName())
            .append("gmtCreat", getGmtCreat())
            .append("gmtModified", getGmtModified())
            .toString();
    }
}
