package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 商品评价对象 fa_xshop_review
 *
 * @author ruoyi
 * @date 2021-07-19
 */
public class FaXshopReview extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 用户 */
    @Excel(name = "用户")
    private Integer userId;

    /** 订单 */
    @Excel(name = "订单")
    private Integer orderId;

    /** 商品 */
    @Excel(name = "商品")
    private Integer productId;

    /** SKU */
    @Excel(name = "SKU")
    private Integer skuId;

    /** 评价内容 */
    @Excel(name = "评价内容")
    private String content;

    /** 星级 */
    @Excel(name = "星级")
    private Integer star;
    /** 店铺id */
    private Long storeId;
    /**
     * 审核状态
     */
    private Integer status;
    /**
     * 图片
     */
    private String images;
    /**
     * 父id
     */
    private Integer parentId;

    /** 删除时间 */
    @Excel(name = "删除时间")
    private Long deleteTime;

    @Excel(name = "创建时间")
    private Long createtime;

    @Excel(name = "更新时间")
    private Long updatetime;


    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    public Integer getUserId()
    {
        return userId;
    }
    public void setOrderId(Integer orderId)
    {
        this.orderId = orderId;
    }

    public Integer getOrderId()
    {
        return orderId;
    }
    public void setProductId(Integer productId)
    {
        this.productId = productId;
    }

    public Integer getProductId()
    {
        return productId;
    }
    public void setSkuId(Integer skuId)
    {
        this.skuId = skuId;
    }

    public Integer getSkuId()
    {
        return skuId;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setStar(Integer star)
    {
        this.star = star;
    }

    public Integer getStar()
    {
        return star;
    }
    public void setDeleteTime(Long deleteTime)
    {
        this.deleteTime = deleteTime;
    }

    public Long getDeleteTime()
    {
        return deleteTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("orderId", getOrderId())
            .append("productId", getProductId())
            .append("skuId", getSkuId())
            .append("content", getContent())
            .append("star", getStar())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("deleteTime", getDeleteTime())
            .toString();
    }
}
