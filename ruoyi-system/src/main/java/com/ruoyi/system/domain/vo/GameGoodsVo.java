package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description 商城商品
 * @Author fan
 * @Date 2021/8/9
 **/
public class GameGoodsVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 商城内商品id */
    private Long id;

    /** 是否上架 0下架，1上架 */
    private Long show;

    /** 宠物币价格 */
    private Long coinPrice;

    /** 关联的物品id */
    private Long thingsId;

    /** 库存类型 0无限 1有限 */
    private Long storeType;

    /** 库存量，库存类型未有限时有效 */
    private Long storeCount;

    /** 商品名称 */
    private String name;

    /** 图片 */
    private String image;

    /** 物品名称 */
    private String thingsName;

    /** 物品类型 */
    private Long categoryId;

    /** 物品图片 */
    // private String image;

    /** 适用的宠物类型，默认值是0，所有类型通用 */
    private Long petType;

    /** 效果说明 */
    private String result;

    /** 修改 */
    private Long gmtModified;

    /** 创建 */
    private Long gmtCreate;

    /** 使用后增加的成长值 */
    private Long growthPoint;

    /** 装扮类型 */
    private Long decCategory;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShow() {
        return show;
    }

    public void setShow(Long show) {
        this.show = show;
    }

    public Long getCoinPrice() {
        return coinPrice;
    }

    public void setCoinPrice(Long coinPrice) {
        this.coinPrice = coinPrice;
    }

    public Long getThingsId() {
        return thingsId;
    }

    public void setThingsId(Long thingsId) {
        this.thingsId = thingsId;
    }

    public Long getStoreType() {
        return storeType;
    }

    public void setStoreType(Long storeType) {
        this.storeType = storeType;
    }

    public Long getStoreCount() {
        return storeCount;
    }

    public void setStoreCount(Long storeCount) {
        this.storeCount = storeCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThingsName() {
        return thingsName;
    }

    public void setThingsName(String thingsName) {
        this.thingsName = thingsName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getPetType() {
        return petType;
    }

    public void setPetType(Long petType) {
        this.petType = petType;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Long getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Long gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getGrowthPoint() {
        return growthPoint;
    }

    public void setGrowthPoint(Long growthPoint) {
        this.growthPoint = growthPoint;
    }

    public Long getDecCategory() {
        return decCategory;
    }

    public void setDecCategory(Long decCategory) {
        this.decCategory = decCategory;
    }
}
