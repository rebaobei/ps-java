package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品属性对象 fa_xshop_product_sku
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
public class FaXshopProductSku extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 商品 */
    @Excel(name = "商品")
    private Integer productId;

    /** 属性 */
    @Excel(name = "属性")
    private String keys;

    /** 属性值 */
    @Excel(name = "属性值")
    private String value;

    /** 销售价 */
    @Excel(name = "销售价")
    private BigDecimal price;

    /** 库存 */
    @Excel(name = "库存")
    private Long stock;

    /** 重量 */
    @Excel(name = "重量")
    private BigDecimal weight;

    /** 货号 */
    @Excel(name = "货号")
    private String sn;

    /** 市场价 */
    @Excel(name = "市场价")
    private BigDecimal marketPrice;

    /** 销量 */
    @Excel(name = "销量")
    private Long soldCount;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setProductId(Integer productId) 
    {
        this.productId = productId;
    }

    public Integer getProductId() 
    {
        return productId;
    }
    public void setKeys(String keys) 
    {
        this.keys = keys;
    }

    public String getKeys() 
    {
        return keys;
    }
    public void setValue(String value) 
    {
        this.value = value;
    }

    public String getValue() 
    {
        return value;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStock(Long stock) 
    {
        this.stock = stock;
    }

    public Long getStock() 
    {
        return stock;
    }
    public void setWeight(BigDecimal weight) 
    {
        this.weight = weight;
    }

    public BigDecimal getWeight() 
    {
        return weight;
    }
    public void setSn(String sn) 
    {
        this.sn = sn;
    }

    public String getSn() 
    {
        return sn;
    }
    public void setMarketPrice(BigDecimal marketPrice) 
    {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getMarketPrice() 
    {
        return marketPrice;
    }
    public void setSoldCount(Long soldCount) 
    {
        this.soldCount = soldCount;
    }

    public Long getSoldCount() 
    {
        return soldCount;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("keys", getKeys())
            .append("value", getValue())
            .append("price", getPrice())
            .append("stock", getStock())
            .append("weight", getWeight())
            .append("sn", getSn())
            .append("marketPrice", getMarketPrice())
            .append("soldCount", getSoldCount())
            .append("image", getImage())
            .toString();
    }
}
