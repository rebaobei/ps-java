package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 店铺视频对象 fa_store_video
 *
 * @author ruoyi
 * @date 2021-12-01
 */
public class FaStoreVideo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 视频标题 */
    @Excel(name = "视频标题")
    private String videoTitle;

    /** 视频说明 */
    private String videoExplain;

    /** 视频内容 */
    @Excel(name = "视频内容")
    private String videoContent;

    /** 视频路径 */
    @Excel(name = "视频路径")
    private String videoUrl;

    /** 视频封面 */
    @Excel(name = "视频封面")
    private String videoImg;

    /** 赞数 */
    @Excel(name = "赞数")
    private Long upvote;

    /** 店铺id */
    private Long storeId;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private Long status;

    /** 备注 */
    @Excel(name = "备注")
    private String reamrk;
    /**
     * 标签
     */
    private String label;

    /** 是否首页推荐 0 否 1推荐 */
    @Excel(name = "是否首页推荐 0 否 1推荐")
    private String homeRecommend;
    //店铺名称


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    private String storeName;

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setVideoTitle(String videoTitle)
    {
        this.videoTitle = videoTitle;
    }

    public String getVideoTitle()
    {
        return videoTitle;
    }
    public void setVideoExplain(String videoExplain)
    {
        this.videoExplain = videoExplain;
    }

    public String getVideoExplain()
    {
        return videoExplain;
    }
    public void setVideoContent(String videoContent)
    {
        this.videoContent = videoContent;
    }

    public String getVideoContent()
    {
        return videoContent;
    }
    public void setVideoUrl(String videoUrl)
    {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl()
    {
        return videoUrl;
    }
    public void setVideoImg(String videoImg)
    {
        this.videoImg = videoImg;
    }

    public String getVideoImg()
    {
        return videoImg;
    }
    public void setUpvote(Long upvote)
    {
        this.upvote = upvote;
    }

    public Long getUpvote()
    {
        return upvote;
    }
    public void setStoreId(Long storeId)
    {
        this.storeId = storeId;
    }

    public Long getStoreId()
    {
        return storeId;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setReamrk(String reamrk)
    {
        this.reamrk = reamrk;
    }

    public String getReamrk()
    {
        return reamrk;
    }
    public void setHomeRecommend(String homeRecommend)
    {
        this.homeRecommend = homeRecommend;
    }

    public String getHomeRecommend()
    {
        return homeRecommend;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("videoTitle", getVideoTitle())
            .append("videoExplain", getVideoExplain())
            .append("videoContent", getVideoContent())
            .append("videoUrl", getVideoUrl())
            .append("videoImg", getVideoImg())
            .append("upvote", getUpvote())
            .append("storeId", getStoreId())
            .append("status", getStatus())
            .append("reamrk", getReamrk())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("homeRecommend", getHomeRecommend())
            .toString();
    }
}
