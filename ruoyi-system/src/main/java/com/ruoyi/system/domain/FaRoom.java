package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 房间管理对象 fa_room
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
public class FaRoom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 分类id */
    @Excel(name = "分类id")
    private Long fenleiId;

    /** 房间名称 */
    @Excel(name = "房间名称")
    private String name;

    /** 房间照片 */
    @Excel(name = "房间照片")
    private String image;

    /** 轮播图片 */
    @Excel(name = "轮播图片")
    private String luboimages;

    /** 描述 */
    @Excel(name = "描述")
    private String describe;

    /** 详情 */
    @Excel(name = "详情")
    private String content;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 状态:0=隐藏,1=启用 */
    @Excel(name = "状态:0=隐藏,1=启用")
    private String status;

    /** 排序 */
    @Excel(name = "排序")
    private Long weigh;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFenleiId(Long fenleiId) 
    {
        this.fenleiId = fenleiId;
    }

    public Long getFenleiId() 
    {
        return fenleiId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setLuboimages(String luboimages) 
    {
        this.luboimages = luboimages;
    }

    public String getLuboimages() 
    {
        return luboimages;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("fenleiId", getFenleiId())
            .append("name", getName())
            .append("image", getImage())
            .append("luboimages", getLuboimages())
            .append("describe", getDescribe())
            .append("content", getContent())
            .append("price", getPrice())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("weigh", getWeigh())
            .toString();
    }
}
