package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 朋友圈评论对象 fa_ball_userarticle_comment
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public class FaBallUserarticleComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 文章id */
    @Excel(name = "文章id")
    private Long userarticleId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 评论内容 */
    @Excel(name = "评论内容")
    private String note;

    /** 父级 */
    @Excel(name = "父级")
    private Long pid;

    /** 喜欢数量 */
    @Excel(name = "喜欢数量")
    private Long likecount;

    /** 状态:0=隐藏,1=审核 */
    @Excel(name = "状态:0=隐藏,1=审核")
    private String status;

    /** 消息类型 */
    @Excel(name = "消息类型")
    private Long messType;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;
    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserarticleId(Long userarticleId) 
    {
        this.userarticleId = userarticleId;
    }

    public Long getUserarticleId() 
    {
        return userarticleId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setLikecount(Long likecount) 
    {
        this.likecount = likecount;
    }

    public Long getLikecount() 
    {
        return likecount;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setMessType(Long messType) 
    {
        this.messType = messType;
    }

    public Long getMessType() 
    {
        return messType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userarticleId", getUserarticleId())
            .append("userId", getUserId())
            .append("note", getNote())
            .append("pid", getPid())
            .append("createtime", getCreatetime())
            .append("likecount", getLikecount())
            .append("status", getStatus())
            .append("updatetime", getUpdatetime())
            .append("messType", getMessType())
            .toString();
    }
}
