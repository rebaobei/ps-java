package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 视频管理对象 fa_video_list
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public class FaVideoList extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 序列号 */
    @Excel(name = "序列号")
    private String xuliehao;

    /** 状态:0=禁用,1=启用 */
    @Excel(name = "状态:0=禁用,1=启用")
    private String status;

    /** 通道号 */
    @Excel(name = "通道号")
    private Long tongdaono;


    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;



    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setXuliehao(String xuliehao) 
    {
        this.xuliehao = xuliehao;
    }

    public String getXuliehao() 
    {
        return xuliehao;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setTongdaono(Long tongdaono) 
    {
        this.tongdaono = tongdaono;
    }

    public Long getTongdaono() 
    {
        return tongdaono;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("xuliehao", getXuliehao())
            .append("createtime", getCreatetime())
            .append("status", getStatus())
            .append("tongdaono", getTongdaono())
            .toString();
    }
}
