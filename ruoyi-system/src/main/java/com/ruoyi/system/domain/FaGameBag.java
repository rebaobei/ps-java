package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 背包管理对象 fa_game_bag
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameBag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id 主键，对应背包内的一项 */
    private Long id;

    /** 玩家id，也是背包id */
    @Excel(name = "玩家id，也是背包id")
    private Long playerId;

    /** 物品id */
    @Excel(name = "物品id")
    private Long thingsId;

    /** 物品拥有数量 */
    @Excel(name = "物品拥有数量")
    private Long count;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlayerId(Long playerId) 
    {
        this.playerId = playerId;
    }

    public Long getPlayerId() 
    {
        return playerId;
    }
    public void setThingsId(Long thingsId) 
    {
        this.thingsId = thingsId;
    }

    public Long getThingsId() 
    {
        return thingsId;
    }
    public void setCount(Long count) 
    {
        this.count = count;
    }

    public Long getCount() 
    {
        return count;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("playerId", getPlayerId())
            .append("thingsId", getThingsId())
            .append("count", getCount())
            .toString();
    }
}
