package com.ruoyi.system.domain.vo;

public class MsgVo {
    private String title;
    private String content;
    private Integer id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MsgVo(String title, String content, Integer id) {
        this.title = title;
        this.content = content;
        this.id = id;
    }

    public MsgVo() {
    }
}
