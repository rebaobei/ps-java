package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/9
 **/
public class GameCommonTaskLogVo {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 操作者 */
    private Long playerId;

    /** 已领养的宠物id */
    private Long petId;

    /** 喂养的时间 */
    private Long startTime;

    /** 冷却结束时间 */
    private Long endTime;

    /** 常规任务类型，喂水还是喂食 */
    private Long commonTaskType;

    private String username;

    private String nickname;

    private Long createQueryStart;
    private Long createQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getPetId() {
        return petId;
    }

    public void setPetId(Long petId) {
        this.petId = petId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getCommonTaskType() {
        return commonTaskType;
    }

    public void setCommonTaskType(Long commonTaskType) {
        this.commonTaskType = commonTaskType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
