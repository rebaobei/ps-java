package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户中心导航对象 fa_user_navigation
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaUserNavigation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增 */
    private Integer nId;

    /** 分类id */
    @Excel(name = "分类id")
    private Long cat;

    /** 图标 */
    @Excel(name = "图标")
    private String image;

    /** 颜色 */
    @Excel(name = "颜色")
    private String colors;

    /** 图标样式 */
    @Excel(name = "图标样式")
    private String cuicon;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 地址 */
    @Excel(name = "地址")
    private String navurl;

    /** 跳转地方 */
    @Excel(name = "跳转地方")
    private String navtype;

    /** 排序数字越小越在前 */
    @Excel(name = "排序数字越小越在前")
    private Long sortType;

    /** 显示不显示 */
    @Excel(name = "显示不显示")
    private String status;

    public void setnId(Integer nId) 
    {
        this.nId = nId;
    }

    public Integer getnId() 
    {
        return nId;
    }
    public void setCat(Long cat) 
    {
        this.cat = cat;
    }

    public Long getCat() 
    {
        return cat;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setColors(String colors) 
    {
        this.colors = colors;
    }

    public String getColors() 
    {
        return colors;
    }
    public void setCuicon(String cuicon) 
    {
        this.cuicon = cuicon;
    }

    public String getCuicon() 
    {
        return cuicon;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNavurl(String navurl) 
    {
        this.navurl = navurl;
    }

    public String getNavurl() 
    {
        return navurl;
    }
    public void setNavtype(String navtype) 
    {
        this.navtype = navtype;
    }

    public String getNavtype() 
    {
        return navtype;
    }
    public void setSortType(Long sortType) 
    {
        this.sortType = sortType;
    }

    public Long getSortType() 
    {
        return sortType;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("nId", getnId())
            .append("cat", getCat())
            .append("image", getImage())
            .append("colors", getColors())
            .append("cuicon", getCuicon())
            .append("name", getName())
            .append("navurl", getNavurl())
            .append("navtype", getNavtype())
            .append("sortType", getSortType())
            .append("status", getStatus())
            .toString();
    }
}
