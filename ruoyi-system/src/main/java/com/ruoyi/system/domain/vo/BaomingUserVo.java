package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.util.List;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/2
 **/
public class BaomingUserVo {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getChongName() {
        return chongName;
    }

    public void setChongName(String chongName) {
        this.chongName = chongName;
    }

    public Long getChongSix() {
        return chongSix;
    }

    public void setChongSix(Long chongSix) {
        this.chongSix = chongSix;
    }

    public String getChongRank() {
        return chongRank;
    }

    public void setChongRank(String chongRank) {
        this.chongRank = chongRank;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String username;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /** 宠物昵称 */
    @Excel(name = "宠物昵称")
    private String chongName;

    /** 宠物性别1 男2女 */
    @Excel(name = "宠物性别1 男2女")
    private Long chongSix;

    /** 备注 */
    @Excel(name = "备注")
    private String chongRank;

    /** 图 */
    @Excel(name = "图")
    private String images;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userid;

    /** 状态:0=未发布,1=已发布 */
    @Excel(name = "状态:0=未发布,1=已发布")
    private String status;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }

    private List<String> imageList;



}
