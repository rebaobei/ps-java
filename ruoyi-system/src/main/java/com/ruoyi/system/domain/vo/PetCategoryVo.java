package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description 宠物分类
 * @Author fan
 * @Date 2021/7/26
 **/
public class PetCategoryVo {

    /** id */
    private Long id;

    /** 名称 */
    private String name;

    /** 照片 */
    private List<String> images;

    /** 描述 */
    private String describe;

    /** 详情 */
    private String concat;

    /** 价格 */
    private BigDecimal price;

    /** 状态:0=隐藏,1=启用 */
    private String status;

    /** 排序 */
    private Long weigh;
    /** 创建时间 */
    private Long createtime;
    /** 更新时间 */
    private Long updatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getConcat() {
        return concat;
    }

    public void setConcat(String concat) {
        this.concat = concat;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getWeigh() {
        return weigh;
    }

    public void setWeigh(Long weigh) {
        this.weigh = weigh;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }
}
