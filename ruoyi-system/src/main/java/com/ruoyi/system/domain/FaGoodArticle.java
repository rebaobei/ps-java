package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 动保公益对象 fa_good_article
 *
 * @author ruoyi
 * @date 2021-12-04
 */
public class FaGoodArticle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 分类id */
    @Excel(name = "分类id")
    private Long categoryId;
    private String categoryName;
    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 查看次数 */
    @Excel(name = "查看次数")
    private Integer views;

    /** 点赞次数 */
    @Excel(name = "点赞次数")
    private Integer likes;

    /** 点踩次数 */
    @Excel(name = "点踩次数")
    private Integer dislikes;

    /** 缩略图 */
    @Excel(name = "缩略图")
    private String image;

    /** 轮播图 */
    @Excel(name = "轮播图")
    private String lunboImage;

    /** 作者名称 */
    @Excel(name = "作者名称")
    private String userName;


    /** 权重 */
    @Excel(name = "权重")
    private Long weigh;

    /** 描述 */
    @Excel(name = "描述")
    private String describe;

    /** 关键字 */
    private String keywords;

    /** 是否顶部 */
    @Excel(name = "是否顶部")
    private String isTop;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 是否启用 0否 1是 */
    private String status;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setViews(Integer views)
    {
        this.views = views;
    }

    public Integer getViews()
    {
        return views;
    }
    public void setLikes(Integer likes)
    {
        this.likes = likes;
    }

    public Integer getLikes()
    {
        return likes;
    }
    public void setDislikes(Integer dislikes)
    {
        this.dislikes = dislikes;
    }

    public Integer getDislikes()
    {
        return dislikes;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImage()
    {
        return image;
    }
    public void setLunboImage(String lunboImage)
    {
        this.lunboImage = lunboImage;
    }

    public String getLunboImage()
    {
        return lunboImage;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }
    public void setWeigh(Long weigh)
    {
        this.weigh = weigh;
    }

    public Long getWeigh()
    {
        return weigh;
    }
    public void setDescribe(String describe)
    {
        this.describe = describe;
    }

    public String getDescribe()
    {
        return describe;
    }
    public void setKeywords(String keywords)
    {
        this.keywords = keywords;
    }

    public String getKeywords()
    {
        return keywords;
    }
    public void setIsTop(String isTop)
    {
        this.isTop = isTop;
    }

    public String getIsTop()
    {
        return isTop;
    }
    public void setSort(Long sort)
    {
        this.sort = sort;
    }

    public Long getSort()
    {
        return sort;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("content", getContent())
            .append("views", getViews())
            .append("likes", getLikes())
            .append("dislikes", getDislikes())
            .append("image", getImage())
            .append("lunboImage", getLunboImage())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("userName", getUserName())
            .append("categoryId", getCategoryId())
            .append("weigh", getWeigh())
            .append("describe", getDescribe())
            .append("keywords", getKeywords())
            .append("isTop", getIsTop())
            .append("sort", getSort())
            .append("status", getStatus())
            .toString();
    }
}
