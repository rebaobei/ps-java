package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description 预约项目Vo
 * @Author fan
 * @Date 2021/7/29
 **/
public class ProjectVo{

    /**
     * id
     */
    private Long id;

    /**
     * 项目名称
     */
    private String name;
    /**
     * 店铺id
     */
    private Long storeInfoId;
    /**
     * 店铺id
     */
    private String storeInfoName;
    /**
     * 照片
     */
    private String image;

    /**
     * 描述
     */
    private String describe;

    /**
     * 详情
     */
    private String content;
    /**
     * 购买须知
     */
    private String explain;

    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 原始价格
     */
    private BigDecimal initPrice;

    /**
     * 状态:0=隐藏,1=启用
     */
    private String status;

    /**
     * 排序
     */
    private Long weigh;

    /**
     * 项目类型 关联projectsType
     */
    private String protype;

    /**
     * 轮播照片
     */
    private String luboimages;

    /**
     * 创建时间
     */
    private Long createtime;

    /**
     * 更新时间
     */
    private Long updatetime;

    /**
     * 轮播图
     */
    private List<String> imageList;
    /** 标签 */
    private String label;
    /**
     * 销售数量
     */
    private int totalSell;
    /**
     * 查看次数
     */
    private int showCount;

    private int auditStatus;
    private String projectTypeId;


    public String getProjectTypeId() {
        return projectTypeId;
    }

    public void setProjectTypeId(String projectTypeId) {
        this.projectTypeId = projectTypeId;
    }
    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public int getShowCount() {
        return showCount;
    }

    public void setShowCount(int showCount) {
        this.showCount = showCount;
    }

    public BigDecimal getInitPrice() {
        return initPrice;
    }

    public void setInitPrice(BigDecimal initPrice) {
        this.initPrice = initPrice;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getTotalSell() {
        return totalSell;
    }

    public void setTotalSell(int totalSell) {
        this.totalSell = totalSell;
    }

    /** 首页推荐 */
    @Excel(name = "首页推荐")
    private Integer homeRecommend;

    public Integer getHomeRecommend() {
        return homeRecommend;
    }

    public void setHomeRecommend(Integer homeRecommend) {
        this.homeRecommend = homeRecommend;
    }

    public String getStoreInfoName() {
        return storeInfoName;
    }

    public void setStoreInfoName(String storeInfoName) {
        this.storeInfoName = storeInfoName;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Long getStoreInfoId() {
        return storeInfoId;
    }

    public void setStoreInfoId(Long storeInfoId) {
        this.storeInfoId = storeInfoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getWeigh() {
        return weigh;
    }

    public void setWeigh(Long weigh) {
        this.weigh = weigh;
    }

    public String getProtype() {
        return protype;
    }

    public void setProtype(String protype) {
        this.protype = protype;
    }

    public String getLuboimages() {
        return luboimages;
    }

    public void setLuboimages(String luboimages) {
        this.luboimages = luboimages;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }
}
