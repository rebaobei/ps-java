package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 标签分类对象 fa_ball_labelfenlei
 *
 * @author ruoyi
 * @date 2021-07-31
 */
public class FaBallLabelfenlei extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 最多选择数量
     */
    @Excel(name = "最多选择数量")
    private Long maxcount;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Long weigh;

    /**
     * 类型
     */
    @Excel(name = "类型")
    private Integer fenleitype;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Long createtime;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    /**
     * 状态:0=禁用,1=启用
     */
    @Excel(name = "状态:0=禁用,1=启用")
    private String status;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMaxcount(Long maxcount) {
        this.maxcount = maxcount;
    }

    public Long getMaxcount() {
        return maxcount;
    }

    public void setWeigh(Long weigh) {
        this.weigh = weigh;
    }

    public Long getWeigh() {
        return weigh;
    }

    public void setFenleitype(Integer fenleitype) {
        this.fenleitype = fenleitype;
    }

    public Integer getFenleitype() {
        return fenleitype;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("maxcount", getMaxcount())
                .append("weigh", getWeigh())
                .append("createtime", getCreatetime())
                .append("updatetime", getUpdatetime())
                .append("fenleitype", getFenleitype())
                .append("status", getStatus())
                .toString();
    }
}
