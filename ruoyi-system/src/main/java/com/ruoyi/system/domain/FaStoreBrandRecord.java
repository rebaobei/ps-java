package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 品牌审核记录对象 fa_store_brand_record
 *
 * @author ruoyi
 * @date 2021-11-02
 */
public class FaStoreBrandRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 品牌名称 */
    @Excel(name = "品牌名称")
    private String brandName;

    /** 品牌描述 */
    @Excel(name = "品牌描述")
    private String brandDescribe;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phone;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 轮播图 */
    @Excel(name = "轮播图")
    private String bannerimages;

    /** 轮播图 */
    @Excel(name = "轮播图")
    private List<String> imageList;

    /** 图片 */
    @Excel(name = "图片")
    private String images;
    /** logo */
    @Excel(name = "logo")
    private String logo;

    /** 品牌星级 */
    @Excel(name = "品牌星级")
    private Long starLevel;

    /** 品牌标签 */
    @Excel(name = "品牌标签")
    private String label;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 备注 */
    @Excel(name = "备注")
    private String reamrk;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkDateTime;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 店铺id */
    @Excel(name = "店铺id")
    private Long storeInfoId;

    /** 品牌id */
    @Excel(name = "品牌id")
    private Long storeBrandId;

    /** 申请类型 */
    @Excel(name = "申请类型") // 0 新增 1入驻 2修改
    private Integer applicationType;
    /** 申请店铺名 */
    private String storeInfoName;


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getStoreInfoName() {
        return storeInfoName;
    }

    public void setStoreInfoName(String storeInfoName) {
        this.storeInfoName = storeInfoName;
    }

    public Long getStoreBrandId() {
        return storeBrandId;
    }

    public void setStoreBrandId(Long storeBrandId) {
        this.storeBrandId = storeBrandId;
    }

    public Integer getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(Integer applicationType) {
        this.applicationType = applicationType;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setBrandName(String brandName)
    {
        this.brandName = brandName;
    }

    public String getBrandName()
    {
        return brandName;
    }
    public void setBrandDescribe(String brandDescribe)
    {
        this.brandDescribe = brandDescribe;
    }

    public String getBrandDescribe()
    {
        return brandDescribe;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }
    public void setBannerimages(String bannerimages)
    {
        this.bannerimages = bannerimages;
    }

    public String getBannerimages()
    {
        return bannerimages;
    }
    public void setImages(String images)
    {
        this.images = images;
    }

    public String getImages()
    {
        return images;
    }
    public void setStarLevel(Long starLevel)
    {
        this.starLevel = starLevel;
    }

    public Long getStarLevel()
    {
        return starLevel;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setReamrk(String reamrk)
    {
        this.reamrk = reamrk;
    }

    public String getReamrk()
    {
        return reamrk;
    }
    public void setCheckDateTime(Date checkDateTime)
    {
        this.checkDateTime = checkDateTime;
    }

    public Date getCheckDateTime()
    {
        return checkDateTime;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setStoreInfoId(Long storeInfoId)
    {
        this.storeInfoId = storeInfoId;
    }

    public Long getStoreInfoId()
    {
        return storeInfoId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("brandName", getBrandName())
            .append("brandDescribe", getBrandDescribe())
            .append("phone", getPhone())
            .append("address", getAddress())
            .append("bannerimages", getBannerimages())
            .append("images", getImages())
            .append("starLevel", getStarLevel())
            .append("label", getLabel())
            .append("status", getStatus())
            .append("reamrk", getReamrk())
            .append("checkDateTime", getCheckDateTime())
            .append("remark", getRemark())
            .append("userId", getUserId())
            .append("storeInfoId", getStoreInfoId())
            .toString();
    }
}
