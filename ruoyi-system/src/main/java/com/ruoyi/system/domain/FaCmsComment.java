package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 评论管理对象 fa_cms_comment
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
public class FaCmsComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Integer id;

    /** 会员ID */
    @Excel(name = "会员ID")
    private Integer userId;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 关联ID */
    @Excel(name = "关联ID")
    private Integer aid;

    /** 父ID */
    @Excel(name = "父ID")
    private Long pid;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 评论数 */
    @Excel(name = "评论数")
    private Integer comments;

    /** IP */
    @Excel(name = "IP")
    private String ip;

    /** User Agent */
    @Excel(name = "User Agent")
    private String useragent;

    /** 订阅 */
    @Excel(name = "订阅")
    private Integer subscribe;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }



    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setAid(Integer aid) 
    {
        this.aid = aid;
    }

    public Integer getAid() 
    {
        return aid;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setComments(Integer comments) 
    {
        this.comments = comments;
    }

    public Integer getComments() 
    {
        return comments;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }
    public void setUseragent(String useragent) 
    {
        this.useragent = useragent;
    }

    public String getUseragent() 
    {
        return useragent;
    }
    public void setSubscribe(Integer subscribe) 
    {
        this.subscribe = subscribe;
    }

    public Integer getSubscribe() 
    {
        return subscribe;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("type", getType())
            .append("aid", getAid())
            .append("pid", getPid())
            .append("content", getContent())
            .append("comments", getComments())
            .append("ip", getIp())
            .append("useragent", getUseragent())
            .append("subscribe", getSubscribe())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .toString();
    }
}
