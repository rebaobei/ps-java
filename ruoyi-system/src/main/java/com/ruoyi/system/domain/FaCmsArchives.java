package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文章管理对象 fa_cms_archives
 * 
 * @author zzp
 * @date 2021-08-11
 */
public class FaCmsArchives extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 会员ID */
    @Excel(name = "会员ID")
    private Integer userId;

    /** 栏目ID */
    @Excel(name = "栏目ID")
    private Integer channelId;

    /** 模型ID */
    @Excel(name = "模型ID")
    private Long modelId;

    /** 专题ID */
    @Excel(name = "专题ID")
    private Long specialId;

    /** 文章标题 */
    @Excel(name = "文章标题")
    private String title;

    /** 样式 */
    @Excel(name = "样式")
    private String style;

    /** 标志 */
    @Excel(name = "标志")
    private String flag;

    /** 缩略图 */
    @Excel(name = "缩略图")
    private String image;

    /** 视频 */
    @Excel(name = "视频")
    private String file;

    /** SEO标题 */
    @Excel(name = "SEO标题")
    private String seotitle;

    /** 关键字 */
    @Excel(name = "关键字")
    private String keywords;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** TAG */
    @Excel(name = "TAG")
    private String tags;

    /** 权重 */
    @Excel(name = "权重")
    private Long weigh;

    /** 浏览次数 */
    @Excel(name = "浏览次数")
    private Integer views;

    /** 评论次数 */
    @Excel(name = "评论次数")
    private Integer comments;

    /** 点赞数 */
    @Excel(name = "点赞数")
    private Integer likes;

    /** 点踩数 */
    @Excel(name = "点踩数")
    private Integer dislikes;

    /** 自定义URL */
    @Excel(name = "自定义URL")
    private String diyname;

    /** 发布时间 */
    @Excel(name = "发布时间")
    private Long publishtime;

    /** 删除时间 */
    @Excel(name = "删除时间")
    private Long deletetime;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 限购数量 */
    @Excel(name = "限购数量")
    private Long buynum;

    /** 活动类型:0|仅查看
1|购买
2|报名 */
    @Excel(name = "活动类型:0|仅查看 1|购买 2|报名")
    private Integer buytype;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    /** 分享数量 */
    @Excel(name = "分享数量")
    private Long shareCount;

    /** 展示风格 */
    @Excel(name = "展示风格")
    private Long showType;

    /** 产品id */
    @Excel(name = "产品id")
    private Long shop1;

    /** 产品id */
    @Excel(name = "产品id")
    private Long shop2;

    /** 产品id */
    @Excel(name = "产品id")
    private Long shop3;

    /** 产品id */
    @Excel(name = "产品id")
    private Long shop4;

    /** 服务类型id */
    @Excel(name = "服务类型id")
    private Long type1;

    /** 服务id */
    @Excel(name = "服务id")
    private Long server1;

    /** 服务类型id */
    @Excel(name = "服务类型id")
    private Long type2;

    /** 服务id */
    @Excel(name = "服务id")
    private Long server2;

    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setChannelId(Integer channelId) 
    {
        this.channelId = channelId;
    }

    public Integer getChannelId() 
    {
        return channelId;
    }
    public void setModelId(Long modelId) 
    {
        this.modelId = modelId;
    }

    public Long getModelId() 
    {
        return modelId;
    }
    public void setSpecialId(Long specialId) 
    {
        this.specialId = specialId;
    }

    public Long getSpecialId() 
    {
        return specialId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setStyle(String style) 
    {
        this.style = style;
    }

    public String getStyle() 
    {
        return style;
    }
    public void setFlag(String flag) 
    {
        this.flag = flag;
    }

    public String getFlag() 
    {
        return flag;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setFile(String file) 
    {
        this.file = file;
    }

    public String getFile() 
    {
        return file;
    }
    public void setSeotitle(String seotitle) 
    {
        this.seotitle = seotitle;
    }

    public String getSeotitle() 
    {
        return seotitle;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setTags(String tags) 
    {
        this.tags = tags;
    }

    public String getTags() 
    {
        return tags;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setViews(Integer views) 
    {
        this.views = views;
    }

    public Integer getViews() 
    {
        return views;
    }
    public void setComments(Integer comments) 
    {
        this.comments = comments;
    }

    public Integer getComments() 
    {
        return comments;
    }
    public void setLikes(Integer likes) 
    {
        this.likes = likes;
    }

    public Integer getLikes() 
    {
        return likes;
    }
    public void setDislikes(Integer dislikes) 
    {
        this.dislikes = dislikes;
    }

    public Integer getDislikes() 
    {
        return dislikes;
    }
    public void setDiyname(String diyname) 
    {
        this.diyname = diyname;
    }

    public String getDiyname() 
    {
        return diyname;
    }
    public void setPublishtime(Long publishtime) 
    {
        this.publishtime = publishtime;
    }

    public Long getPublishtime() 
    {
        return publishtime;
    }
    public void setDeletetime(Long deletetime) 
    {
        this.deletetime = deletetime;
    }

    public Long getDeletetime() 
    {
        return deletetime;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setBuynum(Long buynum) 
    {
        this.buynum = buynum;
    }

    public Long getBuynum() 
    {
        return buynum;
    }
    public void setBuytype(Integer buytype) 
    {
        this.buytype = buytype;
    }

    public Integer getBuytype() 
    {
        return buytype;
    }
    public void setManystorePid(Long manystorePid) 
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid() 
    {
        return manystorePid;
    }
    public void setShareCount(Long shareCount) 
    {
        this.shareCount = shareCount;
    }

    public Long getShareCount() 
    {
        return shareCount;
    }
    public void setShowType(Long showType) 
    {
        this.showType = showType;
    }

    public Long getShowType() 
    {
        return showType;
    }
    public void setShop1(Long shop1) 
    {
        this.shop1 = shop1;
    }

    public Long getShop1() 
    {
        return shop1;
    }
    public void setShop2(Long shop2) 
    {
        this.shop2 = shop2;
    }

    public Long getShop2() 
    {
        return shop2;
    }
    public void setShop3(Long shop3) 
    {
        this.shop3 = shop3;
    }

    public Long getShop3() 
    {
        return shop3;
    }
    public void setShop4(Long shop4) 
    {
        this.shop4 = shop4;
    }

    public Long getShop4() 
    {
        return shop4;
    }
    public void setType1(Long type1) 
    {
        this.type1 = type1;
    }

    public Long getType1() 
    {
        return type1;
    }
    public void setServer1(Long server1) 
    {
        this.server1 = server1;
    }

    public Long getServer1() 
    {
        return server1;
    }
    public void setType2(Long type2) 
    {
        this.type2 = type2;
    }

    public Long getType2() 
    {
        return type2;
    }
    public void setServer2(Long server2) 
    {
        this.server2 = server2;
    }

    public Long getServer2() 
    {
        return server2;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("channelId", getChannelId())
            .append("modelId", getModelId())
            .append("specialId", getSpecialId())
            .append("title", getTitle())
            .append("style", getStyle())
            .append("flag", getFlag())
            .append("image", getImage())
            .append("file", getFile())
            .append("seotitle", getSeotitle())
            .append("keywords", getKeywords())
            .append("description", getDescription())
            .append("tags", getTags())
            .append("weigh", getWeigh())
            .append("views", getViews())
            .append("comments", getComments())
            .append("likes", getLikes())
            .append("dislikes", getDislikes())
            .append("diyname", getDiyname())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("publishtime", getPublishtime())
            .append("deletetime", getDeletetime())
            .append("memo", getMemo())
            .append("status", getStatus())
            .append("price", getPrice())
            .append("buynum", getBuynum())
            .append("buytype", getBuytype())
            .append("manystorePid", getManystorePid())
            .append("shareCount", getShareCount())
            .append("showType", getShowType())
            .append("shop1", getShop1())
            .append("shop2", getShop2())
            .append("shop3", getShop3())
            .append("shop4", getShop4())
            .append("type1", getType1())
            .append("server1", getServer1())
            .append("type2", getType2())
            .append("server2", getServer2())
            .toString();
    }
}
