package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 活动报名对象 fa_user_baoming
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaUserBaoming extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 名称 */
    @Excel(name = "名称")
    private String userName;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    /** 活动名称 */
    @Excel(name = "活动名称")
    private String huodongName;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String maill;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setHuodongName(String huodongName) 
    {
        this.huodongName = huodongName;
    }

    public String getHuodongName() 
    {
        return huodongName;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setMaill(String maill) 
    {
        this.maill = maill;
    }

    public String getMaill() 
    {
        return maill;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("tel", getTel())
            .append("huodongName", getHuodongName())
            .append("memo", getMemo())
            .append("createtime", getCreatetime())
            .append("maill", getMaill())
            .toString();
    }
}
