package com.ruoyi.system.domain.constant;

/**
 * 消息类型
 */
public class MessageConstant {

    /**
     * 系统消息
     */
    public static final Long  SYSTEM_MSG_TYPE=1L;
    /**
     * 未读
     */
    public static final Long  UNREAD=0L;
    /**
     * 已读
     */
    public static final Long  HAS_READ=1L;

}
