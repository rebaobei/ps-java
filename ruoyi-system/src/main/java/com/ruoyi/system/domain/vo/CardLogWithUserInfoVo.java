package com.ruoyi.system.domain.vo;

import java.math.BigDecimal;

/**
 * 储值卡表动返回实体类
 */
public class CardLogWithUserInfoVo {
    private Integer id;
    private String nickname;
    private String mobile;
    private String avatar;
    private BigDecimal money;
    private BigDecimal before;
    private BigDecimal after;
    private String memo;
    private Long createtime;

    public CardLogWithUserInfoVo(Integer id, String nickname, String mobile, String avatar, BigDecimal money, BigDecimal before, BigDecimal after, String memo, Long createtime) {
        this.id = id;
        this.nickname = nickname;
        this.mobile = mobile;
        this.avatar = avatar;
        this.money = money;
        this.before = before;
        this.after = after;
        this.memo = memo;
        this.createtime = createtime;
    }

    public CardLogWithUserInfoVo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getBefore() {
        return before;
    }

    public void setBefore(BigDecimal before) {
        this.before = before;
    }

    public BigDecimal getAfter() {
        return after;
    }

    public void setAfter(BigDecimal after) {
        this.after = after;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }
}
