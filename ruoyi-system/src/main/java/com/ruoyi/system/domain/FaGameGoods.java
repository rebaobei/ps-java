package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品管理对象 fa_game_goods
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商城内商品id */
    private Long id;

    /** 是否上架 0下架，1上架 */
    @Excel(name = "是否上架 0下架，1上架")
    private Long show;

    /** 宠物币价格 */
    @Excel(name = "宠物币价格")
    private Long coinPrice;

    /** 关联的物品id */
    @Excel(name = "关联的物品id")
    private Long thingsId;

    /** 库存类型 0无限 1有限 */
    @Excel(name = "库存类型 0无限 1有限")
    private Long storeType;

    /** 库存量，库存类型未有限时有效 */
    @Excel(name = "库存量，库存类型未有限时有效")
    private Long storeCount;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String name;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setShow(Long show) 
    {
        this.show = show;
    }

    public Long getShow() 
    {
        return show;
    }
    public void setCoinPrice(Long coinPrice) 
    {
        this.coinPrice = coinPrice;
    }

    public Long getCoinPrice() 
    {
        return coinPrice;
    }
    public void setThingsId(Long thingsId) 
    {
        this.thingsId = thingsId;
    }

    public Long getThingsId() 
    {
        return thingsId;
    }
    public void setStoreType(Long storeType) 
    {
        this.storeType = storeType;
    }

    public Long getStoreType() 
    {
        return storeType;
    }
    public void setStoreCount(Long storeCount) 
    {
        this.storeCount = storeCount;
    }

    public Long getStoreCount() 
    {
        return storeCount;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("show", getShow())
            .append("coinPrice", getCoinPrice())
            .append("thingsId", getThingsId())
            .append("storeType", getStoreType())
            .append("storeCount", getStoreCount())
            .append("name", getName())
            .append("image", getImage())
            .toString();
    }
}
