package com.ruoyi.system.domain.vo;

/**
 * @Description 已领养的宠物
 * @Author fan
 * @Date 2021/8/9
 **/
public class PetHasOwnerVo {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 拥有宠物的玩家 */
    private Long playerId;

    /** 宠物类型 */
    private Long petId;

    /** 性别0男1女 */
    private Long gender;

    /** 宠物昵称 */
    private String nickname;

    /** 宠物等级 */
    private Integer level;

    /** 宠物当前成长值 */
    private Long growthPoint;

    /** 总成长值 */
    private Long totalGrowthPoint;

    /** 装饰品 头部 */
    private Long dec1;

    /** 装饰品 衣服 */
    private Long dec2;

    /** 创建时间 */
    private Long gmtCreate;

    /** 修改时间 */
    private Long gmtModified;

    /** 宠物主分类Id */
    private Long petCategoryId;

    /** 宠物图片 */
    private String image;

    private String dec1Image;

    private String dec2Image;

    private String petName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getPetId() {
        return petId;
    }

    public void setPetId(Long petId) {
        this.petId = petId;
    }

    public Long getGender() {
        return gender;
    }

    public void setGender(Long gender) {
        this.gender = gender;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getGrowthPoint() {
        return growthPoint;
    }

    public void setGrowthPoint(Long growthPoint) {
        this.growthPoint = growthPoint;
    }

    public Long getTotalGrowthPoint() {
        return totalGrowthPoint;
    }

    public void setTotalGrowthPoint(Long totalGrowthPoint) {
        this.totalGrowthPoint = totalGrowthPoint;
    }

    public Long getDec1() {
        return dec1;
    }

    public void setDec1(Long dec1) {
        this.dec1 = dec1;
    }

    public Long getDec2() {
        return dec2;
    }

    public void setDec2(Long dec2) {
        this.dec2 = dec2;
    }

    public Long getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Long gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Long getPetCategoryId() {
        return petCategoryId;
    }

    public void setPetCategoryId(Long petCategoryId) {
        this.petCategoryId = petCategoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDec1Image() {
        return dec1Image;
    }

    public void setDec1Image(String dec1Image) {
        this.dec1Image = dec1Image;
    }

    public String getDec2Image() {
        return dec2Image;
    }

    public void setDec2Image(String dec2Image) {
        this.dec2Image = dec2Image;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }
}
