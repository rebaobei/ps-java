package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分类管理对象 fa_cms_channel
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
public class FaCmsChannel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 模型ID */
    private Integer modelId;

    /** 父ID */
    private Integer parentId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 标志 */
    private String flag;

    /** SEO标题 */
    private String seotitle;

    /** 关键字 */
    private String keywords;

    /** 描述 */
    private String description;

    /** 自定义名称 */
    private String diyname;

    /** 外部链接 */
    private String outlink;

    /** 文章数量 */
    private Integer items;

    /** 权重 */
    private Long weigh;

    /** 栏目页模板 */
    private String channeltpl;

    /** 列表页模板 */
    private String listtpl;

    /** 详情页模板 */
    private String showtpl;

    /** 分页大小 */
    private Integer pagesize;

    /** 是否可投稿 */
    private Integer iscontribute;

    /** 是否导航显示 */
    private Integer isnav;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 背景图片 */
    @Excel(name = "背景图片")
    private String bgimage;


    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setModelId(Integer modelId) 
    {
        this.modelId = modelId;
    }

    public Integer getModelId() 
    {
        return modelId;
    }
    public void setParentId(Integer parentId) 
    {
        this.parentId = parentId;
    }

    public Integer getParentId() 
    {
        return parentId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setFlag(String flag) 
    {
        this.flag = flag;
    }

    public String getFlag() 
    {
        return flag;
    }
    public void setSeotitle(String seotitle) 
    {
        this.seotitle = seotitle;
    }

    public String getSeotitle() 
    {
        return seotitle;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setDiyname(String diyname) 
    {
        this.diyname = diyname;
    }

    public String getDiyname() 
    {
        return diyname;
    }
    public void setOutlink(String outlink) 
    {
        this.outlink = outlink;
    }

    public String getOutlink() 
    {
        return outlink;
    }
    public void setItems(Integer items) 
    {
        this.items = items;
    }

    public Integer getItems() 
    {
        return items;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setChanneltpl(String channeltpl) 
    {
        this.channeltpl = channeltpl;
    }

    public String getChanneltpl() 
    {
        return channeltpl;
    }
    public void setListtpl(String listtpl) 
    {
        this.listtpl = listtpl;
    }

    public String getListtpl() 
    {
        return listtpl;
    }
    public void setShowtpl(String showtpl) 
    {
        this.showtpl = showtpl;
    }

    public String getShowtpl() 
    {
        return showtpl;
    }
    public void setPagesize(Integer pagesize) 
    {
        this.pagesize = pagesize;
    }

    public Integer getPagesize() 
    {
        return pagesize;
    }
    public void setIscontribute(Integer iscontribute) 
    {
        this.iscontribute = iscontribute;
    }

    public Integer getIscontribute() 
    {
        return iscontribute;
    }
    public void setIsnav(Integer isnav) 
    {
        this.isnav = isnav;
    }

    public Integer getIsnav() 
    {
        return isnav;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setBgimage(String bgimage) 
    {
        this.bgimage = bgimage;
    }

    public String getBgimage() 
    {
        return bgimage;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("modelId", getModelId())
            .append("parentId", getParentId())
            .append("name", getName())
            .append("image", getImage())
            .append("flag", getFlag())
            .append("seotitle", getSeotitle())
            .append("keywords", getKeywords())
            .append("description", getDescription())
            .append("diyname", getDiyname())
            .append("outlink", getOutlink())
            .append("items", getItems())
            .append("weigh", getWeigh())
            .append("channeltpl", getChanneltpl())
            .append("listtpl", getListtpl())
            .append("showtpl", getShowtpl())
            .append("pagesize", getPagesize())
            .append("iscontribute", getIscontribute())
            .append("isnav", getIsnav())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("bgimage", getBgimage())
            .toString();
    }
}
