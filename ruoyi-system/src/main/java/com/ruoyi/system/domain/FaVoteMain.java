package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 投票管理对象 fa_vote_main
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public class FaVoteMain extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String name;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 录播图 */
    @Excel(name = "录播图")
    private String images;

    /** 开始时间 */
    @Excel(name = "开始时间")
    private Long begtime;

    /** 结束时间 */
    @Excel(name = "结束时间")
    private Long endtime;

    /** 描述 */
    @Excel(name = "描述")
    private String describe;

    /** 活动介绍 */
    @Excel(name = "活动介绍")
    private String content;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 状态:0=隐藏,1=启用 */
    @Excel(name = "状态:0=隐藏,1=启用")
    private String status;

    /** 排序 */
    @Excel(name = "排序")
    private Long weigh;

    /** 一天一个会员最多投票次数 */
    @Excel(name = "一天一个会员最多投票次数")
    private Integer maxdaycount;

    /** 投票数量 */
    @Excel(name = "投票数量")
    private Long votecount;

    /** 参赛数量 */
    @Excel(name = "参赛数量")
    private Long joincount;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Long viewcount;

    /** 参赛名额 */
    @Excel(name = "参赛名额")
    private Long maxjoincount;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setBegtime(Long begtime) 
    {
        this.begtime = begtime;
    }

    public Long getBegtime() 
    {
        return begtime;
    }
    public void setEndtime(Long endtime) 
    {
        this.endtime = endtime;
    }

    public Long getEndtime() 
    {
        return endtime;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setMaxdaycount(Integer maxdaycount) 
    {
        this.maxdaycount = maxdaycount;
    }

    public Integer getMaxdaycount() 
    {
        return maxdaycount;
    }
    public void setVotecount(Long votecount) 
    {
        this.votecount = votecount;
    }

    public Long getVotecount() 
    {
        return votecount;
    }
    public void setJoincount(Long joincount) 
    {
        this.joincount = joincount;
    }

    public Long getJoincount() 
    {
        return joincount;
    }
    public void setViewcount(Long viewcount) 
    {
        this.viewcount = viewcount;
    }

    public Long getViewcount() 
    {
        return viewcount;
    }
    public void setMaxjoincount(Long maxjoincount) 
    {
        this.maxjoincount = maxjoincount;
    }

    public Long getMaxjoincount() 
    {
        return maxjoincount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("image", getImage())
            .append("images", getImages())
            .append("begtime", getBegtime())
            .append("endtime", getEndtime())
            .append("describe", getDescribe())
            .append("content", getContent())
            .append("price", getPrice())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("weigh", getWeigh())
            .append("maxdaycount", getMaxdaycount())
            .append("votecount", getVotecount())
            .append("joincount", getJoincount())
            .append("viewcount", getViewcount())
            .append("maxjoincount", getMaxjoincount())
            .toString();
    }
}
