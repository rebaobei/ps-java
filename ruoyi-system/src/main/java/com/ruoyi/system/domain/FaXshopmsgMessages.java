package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 消息管理对象 fa_xshopmsg_messages
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaXshopmsgMessages extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户 */
    @Excel(name = "用户")
    private Long userId;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 消息类型:1系统消息2评论和@3喜欢和赞4关注你的5私信 */
    @Excel(name = "消息类型:1系统消息2评论和@3喜欢和赞4关注你的5私信")
    private Long msgType;

    /** 跳转目标 */
    @Excel(name = "跳转目标")
    private String target;

    /** 已读 */
    @Excel(name = "已读")
    private Long isRead;

    /** 阅读时间 */
    @Excel(name = "阅读时间")
    private Long readTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setMsgType(Long msgType) 
    {
        this.msgType = msgType;
    }

    public Long getMsgType() 
    {
        return msgType;
    }
    public void setTarget(String target) 
    {
        this.target = target;
    }

    public String getTarget() 
    {
        return target;
    }
    public void setIsRead(Long isRead) 
    {
        this.isRead = isRead;
    }

    public Long getIsRead() 
    {
        return isRead;
    }
    public void setReadTime(Long readTime) 
    {
        this.readTime = readTime;
    }

    public Long getReadTime() 
    {
        return readTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("msgType", getMsgType())
            .append("target", getTarget())
            .append("isRead", getIsRead())
            .append("createtime", getCreatetime())
            .append("readTime", getReadTime())
            .toString();
    }
}
