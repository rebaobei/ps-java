package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 投票明细对象 fa_vote_user_detail
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public class FaVoteUserDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 投票人 */
    @Excel(name = "投票人")
    private Long userId;

    /** 主表id */
    @Excel(name = "主表id")
    private Long mainId;

    /** 参赛id */
    @Excel(name = "参赛id")
    private Long voteuserId;

    /** ip */
    @Excel(name = "ip")
    private String ip;

    /**
     * 创建时间
     */
    @Excel(name="createtime")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setMainId(Long mainId) 
    {
        this.mainId = mainId;
    }

    public Long getMainId() 
    {
        return mainId;
    }
    public void setVoteuserId(Long voteuserId) 
    {
        this.voteuserId = voteuserId;
    }

    public Long getVoteuserId() 
    {
        return voteuserId;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("mainId", getMainId())
            .append("voteuserId", getVoteuserId())
            .append("createtime", getCreatetime())
            .append("ip", getIp())
            .toString();
    }
}
