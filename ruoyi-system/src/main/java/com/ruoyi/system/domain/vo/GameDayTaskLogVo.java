package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

/**
 * @Description 每日任务记录
 * @Author fan
 * @Date 2021/8/10
 **/
public class GameDayTaskLogVo {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 玩家id */
    private Long playerId;

    /** 任务开始时间，时间戳 */
    private Long startTime;

    /** 任务状态  去完成 已完成 领取 */
    private String status;

    /** 任务类型 */
    private Long taskId;

    /** 需要完成的次数 */
    private Long requireTimes;

    /** 已完成次数 */
    private Long successTimes;

    /** 完成时间 */
    private Long endTime;

    private String taskName;

    private String username;

    private String nickname;

    private String avatar;

    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getRequireTimes() {
        return requireTimes;
    }

    public void setRequireTimes(Long requireTimes) {
        this.requireTimes = requireTimes;
    }

    public Long getSuccessTimes() {
        return successTimes;
    }

    public void setSuccessTimes(Long successTimes) {
        this.successTimes = successTimes;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
