package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 品牌信息对象 fa_store_brand
 *
 * @author ruoyi
 * @date 2021-10-20
 */
public class FaStoreBrand extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 品牌名称 */
    @Excel(name = "品牌名称")
    private String brandName;

    /** 品牌描述 */
    @Excel(name = "品牌描述")
    private String brandDescribe;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phone;

    /** 地址 */
    private String address;

    /** 轮播图 */
    private String bannerimages;

    /** 图片 */
    private String images;
    /** 图片 */
    private String logo;

    /** 品牌星级 */
    @Excel(name = "品牌星级")
    private Integer starLevel;

    /** 品牌标签 */
    @Excel(name = "品牌标签")
    private String label;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 备注 */
    private String reamrk;


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setBrandName(String brandName)
    {
        this.brandName = brandName;
    }

    public String getBrandName()
    {
        return brandName;
    }
    public void setBrandDescribe(String brandDescribe)
    {
        this.brandDescribe = brandDescribe;
    }

    public String getBrandDescribe()
    {
        return brandDescribe;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }
    public void setBannerimages(String bannerimages)
    {
        this.bannerimages = bannerimages;
    }

    public String getBannerimages()
    {
        return bannerimages;
    }
    public void setImages(String images)
    {
        this.images = images;
    }

    public String getImages()
    {
        return images;
    }
    public void setStarLevel(Integer starLevel)
    {
        this.starLevel = starLevel;
    }

    public Integer getStarLevel()
    {
        return starLevel;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setReamrk(String reamrk)
    {
        this.reamrk = reamrk;
    }

    public String getReamrk()
    {
        return reamrk;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("brandName", getBrandName())
            .append("brandDescribe", getBrandDescribe())
            .append("phone", getPhone())
            .append("address", getAddress())
            .append("bannerimages", getBannerimages())
            .append("images", getImages())
            .append("starLevel", getStarLevel())
            .append("label", getLabel())
            .append("status", getStatus())
            .append("reamrk", getReamrk())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
