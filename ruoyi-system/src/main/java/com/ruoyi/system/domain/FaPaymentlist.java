package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 支付类型对象 fa_paymentlist
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public class FaPaymentlist extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 支付名称 */
    @Excel(name = "支付名称")
    private String payname;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 支付编码 */
    @Excel(name = "支付编码")
    private String paycode;

    /** 配置信息 */
    @Excel(name = "配置信息")
    private String paysite;

    /** 状态:0=禁用,1=启用 */
    @Excel(name = "状态:0=禁用,1=启用")
    private String status;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 排序 */
    @Excel(name = "排序")
    private Long weigh;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPayname(String payname) 
    {
        this.payname = payname;
    }

    public String getPayname() 
    {
        return payname;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setPaycode(String paycode) 
    {
        this.paycode = paycode;
    }

    public String getPaycode() 
    {
        return paycode;
    }
    public void setPaysite(String paysite) 
    {
        this.paysite = paysite;
    }

    public String getPaysite() 
    {
        return paysite;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("payname", getPayname())
            .append("image", getImage())
            .append("paycode", getPaycode())
            .append("paysite", getPaysite())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("note", getNote())
            .append("weigh", getWeigh())
            .toString();
    }
}
