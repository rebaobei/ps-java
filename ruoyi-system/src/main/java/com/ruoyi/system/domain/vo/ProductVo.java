package com.ruoyi.system.domain.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商品Vo
 */
public class ProductVo {
    private Long id;
    private String name;
    private String storeName;
    private Integer proType;
    private String title;
    private String image;
    private Integer onSale;
    private Integer soldCount;
    private Integer reviewCount;
    private BigDecimal price;
    private BigDecimal cardPrice;
    private BigDecimal usescore;
    private Integer homeRecommend;
    private Long createTime;
    private String unit;
    private Integer auditStatus;

    private List<String> imageList;


    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProType() {
        return proType;
    }

    public void setProType(Integer proType) {
        this.proType = proType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getOnSale() {
        return onSale;
    }

    public void setOnSale(Integer onSale) {
        this.onSale = onSale;
    }

    public Integer getSoldCount() {
        return soldCount;
    }

    public void setSoldCount(Integer soldCount) {
        this.soldCount = soldCount;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getCardPrice() {
        return cardPrice;
    }

    public void setCardPrice(BigDecimal cardPrice) {
        this.cardPrice = cardPrice;
    }

    public BigDecimal getUsescore() {
        return usescore;
    }

    public void setUsescore(BigDecimal usescore) {
        this.usescore = usescore;
    }

    public Integer getHomeRecommend() {
        return homeRecommend;
    }

    public void setHomeRecommend(Integer homeRecommend) {
        this.homeRecommend = homeRecommend;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}
