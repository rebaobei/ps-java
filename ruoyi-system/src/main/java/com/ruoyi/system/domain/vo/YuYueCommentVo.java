package com.ruoyi.system.domain.vo;


/**
 * @Description
 * @Author fan
 * @Date 2021/7/28
 **/
public class YuYueCommentVo {

    /** id */
    private Long id;

    /** 订单id */
    private Long orderId;
    /** 会员id */
    private String nickname;

    /** 评论 */
    private String note;

    /** 项目id */
    private String name;

    /** 评分 */
    private Long pingfen;


    /** 创建时间 */
    private Long createtime;

    /** 店铺id */
    private Long storeId;

    /** 店铺名称 */
    private String storeName;
    /**
     * 审核状态
     */
    private Integer status;
    /**
     * 图片
     */
    private String images;
    /**
     * 父id
     */
    private Integer parentId;


    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPingfen() {
        return pingfen;
    }

    public void setPingfen(Long pingfen) {
        this.pingfen = pingfen;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }
}
