package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 积分变动对象 fa_user_score_log
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaUserScoreLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 会员ID */
    @Excel(name = "会员ID")
    private Integer userId;

    /** 变更积分 */
    @Excel(name = "变更积分")
    private Long score;

    /** 变更前积分 */
    @Excel(name = "变更前积分")
    private Long before;

    /** 变更后积分 */
    @Excel(name = "变更后积分")
    private Long after;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    /** 订单类型:-1支付抽奖订单100抽奖获得900签到 */
    @Excel(name = "订单类型:-1支付抽奖订单100抽奖获得900签到")
    private Long billType;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    /** 订单id */
    @Excel(name = "订单id")
    private Long billId;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setScore(Long score) 
    {
        this.score = score;
    }

    public Long getScore() 
    {
        return score;
    }
    public void setBefore(Long before) 
    {
        this.before = before;
    }

    public Long getBefore() 
    {
        return before;
    }
    public void setAfter(Long after) 
    {
        this.after = after;
    }

    public Long getAfter() 
    {
        return after;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setManystorePid(Long manystorePid) 
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid() 
    {
        return manystorePid;
    }
    public void setBillType(Long billType) 
    {
        this.billType = billType;
    }

    public Long getBillType() 
    {
        return billType;
    }
    public void setBillId(Long billId) 
    {
        this.billId = billId;
    }

    public Long getBillId() 
    {
        return billId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("score", getScore())
            .append("before", getBefore())
            .append("after", getAfter())
            .append("memo", getMemo())
            .append("createtime", getCreatetime())
            .append("manystorePid", getManystorePid())
            .append("billType", getBillType())
            .append("billId", getBillId())
            .toString();
    }
}
