package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 任务系统设置对象 fa_user_task
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class FaUserTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增id */
    private Long id;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String taskName;

    /** 任务链接 */
    @Excel(name = "任务链接")
    private String taskUrl;

    /** 任务图标 */
    @Excel(name = "任务图标")
    private String taskImage;

    /** 任务需完成数量 */
    @Excel(name = "任务需完成数量")
    private Long taskNum;

    /** 单次积分 */
    @Excel(name = "单次积分")
    private Long taskIntegral;

    /** 任务参数 */
    @Excel(name = "任务参数")
    private String taskCode;

    /** 跳转属性 */
    @Excel(name = "跳转属性")
    private String type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTaskName(String taskName) 
    {
        this.taskName = taskName;
    }

    public String getTaskName() 
    {
        return taskName;
    }
    public void setTaskUrl(String taskUrl) 
    {
        this.taskUrl = taskUrl;
    }

    public String getTaskUrl() 
    {
        return taskUrl;
    }
    public void setTaskImage(String taskImage) 
    {
        this.taskImage = taskImage;
    }

    public String getTaskImage() 
    {
        return taskImage;
    }
    public void setTaskNum(Long taskNum) 
    {
        this.taskNum = taskNum;
    }

    public Long getTaskNum() 
    {
        return taskNum;
    }
    public void setTaskIntegral(Long taskIntegral) 
    {
        this.taskIntegral = taskIntegral;
    }

    public Long getTaskIntegral() 
    {
        return taskIntegral;
    }
    public void setTaskCode(String taskCode) 
    {
        this.taskCode = taskCode;
    }

    public String getTaskCode() 
    {
        return taskCode;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("taskName", getTaskName())
            .append("taskUrl", getTaskUrl())
            .append("taskImage", getTaskImage())
            .append("taskNum", getTaskNum())
            .append("taskIntegral", getTaskIntegral())
            .append("taskCode", getTaskCode())
            .append("type", getType())
            .toString();
    }
}
