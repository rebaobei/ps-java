package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物等级对象 fa_game_pet_level
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGamePetLevel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 等级名 */
    @Excel(name = "等级名")
    private String levelName;

    /** 当前等级所需成长值 */
    @Excel(name = "当前等级所需成长值")
    private Long requireGrowthPoint;

    /** 等级对应的数字 */
    @Excel(name = "等级对应的数字")
    private Long level;

    /** 下一等级所需成长值 */
    @Excel(name = "下一等级所需成长值")
    private Long levelUpPoint;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLevelName(String levelName) 
    {
        this.levelName = levelName;
    }

    public String getLevelName() 
    {
        return levelName;
    }
    public void setRequireGrowthPoint(Long requireGrowthPoint) 
    {
        this.requireGrowthPoint = requireGrowthPoint;
    }

    public Long getRequireGrowthPoint() 
    {
        return requireGrowthPoint;
    }
    public void setLevel(Long level) 
    {
        this.level = level;
    }

    public Long getLevel() 
    {
        return level;
    }
    public void setLevelUpPoint(Long levelUpPoint) 
    {
        this.levelUpPoint = levelUpPoint;
    }

    public Long getLevelUpPoint() 
    {
        return levelUpPoint;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("levelName", getLevelName())
            .append("requireGrowthPoint", getRequireGrowthPoint())
            .append("level", getLevel())
            .append("levelUpPoint", getLevelUpPoint())
            .toString();
    }
}
