package com.ruoyi.system.domain.vo;

import java.util.List;

/**
 * @Description 分类的树形结构
 * @Author fan
 * @Date 2021/8/11
 **/
public class CmsChannelTreeVo {

    private String name;
    private Integer id;
    private List<CmsChannelTreeVo> children;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CmsChannelTreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<CmsChannelTreeVo> children) {
        this.children = children;
    }
}
