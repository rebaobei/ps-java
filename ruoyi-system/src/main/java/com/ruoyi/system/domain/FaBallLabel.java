package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 标签详情对象 fa_ball_label
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public class FaBallLabel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 排序 */
    @Excel(name = "排序")
    private Long weigh;

    /** 分类id */
    @Excel(name = "分类id")
    private Long fenleiId;

    /** 状态:0=禁用,1=启用 */
    @Excel(name = "状态:0=禁用,1=启用")
    private String status;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Long createtime;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setFenleiId(Long fenleiId) 
    {
        this.fenleiId = fenleiId;
    }

    public Long getFenleiId() 
    {
        return fenleiId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("image", getImage())
            .append("name", getName())
            .append("weigh", getWeigh())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("fenleiId", getFenleiId())
            .append("status", getStatus())
            .toString();
    }
}
