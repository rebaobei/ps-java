package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 完成任务记录对象 fa_user_tasklog
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public class FaUserTasklog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户完成进度josn值 */
    @Excel(name = "用户完成进度josn值")
    private String taskComment;

    /** 是否全部完成1完成0未完成 */
    @Excel(name = "是否全部完成1完成0未完成")
    private Long taskOver;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private Long addTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setTaskComment(String taskComment) 
    {
        this.taskComment = taskComment;
    }

    public String getTaskComment() 
    {
        return taskComment;
    }
    public void setTaskOver(Long taskOver) 
    {
        this.taskOver = taskOver;
    }

    public Long getTaskOver() 
    {
        return taskOver;
    }
    public void setAddTime(Long addTime) 
    {
        this.addTime = addTime;
    }

    public Long getAddTime() 
    {
        return addTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("taskComment", getTaskComment())
            .append("taskOver", getTaskOver())
            .append("addTime", getAddTime())
            .toString();
    }
}
