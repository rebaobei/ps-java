package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 店铺服务团队对象 fa_store_person
 *
 * @author ruoyi
 * @date 2021-10-20
 */
public class FaStorePerson extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String personName;

    /** 描述 */
    @Excel(name = "描述")
    private String personDescribe;

    /** 简介 */
    @Excel(name = "简介")
    private String personBrief;

    /** 领域 */
    @Excel(name = "领域")
    private String personField;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phone;

    /** 工作年份 */
    @Excel(name = "工作年份")
    private Long workYear;

    /** 赞数 */
    @Excel(name = "赞数")
    private Long upvote;

    /** 图片 */
    @Excel(name = "图片")
    private String images;

    /** 星级 */
    @Excel(name = "星级")
    private Long starLevel;

    /** 状态 */
    private Integer status;

    /** 店铺id */
    private Long storeId;
    /** 店铺名称 */
    private String storeName;
    /** 备注 */
    private String reamrk;



    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPersonBrief() {
        return personBrief;
    }

    public void setPersonBrief(String personBrief) {
        this.personBrief = personBrief;
    }

    public String getPersonField() {
        return personField;
    }

    public void setPersonField(String personField) {
        this.personField = personField;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setPersonName(String personName)
    {
        this.personName = personName;
    }

    public String getPersonName()
    {
        return personName;
    }
    public void setPersonDescribe(String personDescribe)
    {
        this.personDescribe = personDescribe;
    }

    public String getPersonDescribe()
    {
        return personDescribe;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setWorkYear(Long workYear)
    {
        this.workYear = workYear;
    }

    public Long getWorkYear()
    {
        return workYear;
    }
    public void setUpvote(Long upvote)
    {
        this.upvote = upvote;
    }

    public Long getUpvote()
    {
        return upvote;
    }
    public void setImages(String images)
    {
        this.images = images;
    }

    public String getImages()
    {
        return images;
    }
    public void setStarLevel(Long starLevel)
    {
        this.starLevel = starLevel;
    }

    public Long getStarLevel()
    {
        return starLevel;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setStoreId(Long storeId)
    {
        this.storeId = storeId;
    }

    public Long getStoreId()
    {
        return storeId;
    }
    public void setReamrk(String reamrk)
    {
        this.reamrk = reamrk;
    }

    public String getReamrk()
    {
        return reamrk;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("personName", getPersonName())
            .append("personDescribe", getPersonDescribe())
            .append("phone", getPhone())
            .append("workYear", getWorkYear())
            .append("upvote", getUpvote())
            .append("images", getImages())
            .append("starLevel", getStarLevel())
            .append("status", getStatus())
            .append("storeId", getStoreId())
            .append("reamrk", getReamrk())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
