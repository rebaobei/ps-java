package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/9
 **/
public class PlayerInfoVo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 游戏玩家Id，与fa_user中id共用 */
    private Long id;

    /** 宠物币数量 */
    private Integer coin;

    /** 背景图 */
    private String backImg;

    /** 头像 */
    private String headerImg;

    /** 默认显示的宠物 */
    private Long defaultPet;

    private String username;

    private String nickname;

    private String petNickname;

    private String level;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCoin() {
        return coin;
    }

    public void setCoin(Integer coin) {
        this.coin = coin;
    }

    public String getBackImg() {
        return backImg;
    }

    public void setBackImg(String backImg) {
        this.backImg = backImg;
    }

    public String getHeaderImg() {
        return headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }

    public Long getDefaultPet() {
        return defaultPet;
    }

    public void setDefaultPet(Long defaultPet) {
        this.defaultPet = defaultPet;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPetNickname() {
        return petNickname;
    }

    public void setPetNickname(String petNickname) {
        this.petNickname = petNickname;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
