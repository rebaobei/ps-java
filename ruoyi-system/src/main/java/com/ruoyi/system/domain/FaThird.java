package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 第三方登录对象 fa_third
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaThird extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Integer id;

    /** 会员ID */
    @Excel(name = "会员ID")
    private Integer userId;

    /** 第三方应用 */
    @Excel(name = "第三方应用")
    private String platform;

    /** 第三方唯一ID */
    @Excel(name = "第三方唯一ID")
    private String openid;

    /** 第三方会员昵称 */
    @Excel(name = "第三方会员昵称")
    private String openname;

    /** AccessToken */
    @Excel(name = "AccessToken")
    private String accessToken;

    /** 刷新token */
    @Excel(name = "刷新token")
    private String refreshToken;

    /** 有效期 */
    @Excel(name = "有效期")
    private Integer expiresIn;

    /** 登录时间 */
    @Excel(name = "登录时间")
    private Integer logintime;


    @Excel(name = "创建时间")
    private Long createtime;

    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    /** 过期时间 */
    @Excel(name = "过期时间")
    private Integer expiretime;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setPlatform(String platform) 
    {
        this.platform = platform;
    }

    public String getPlatform() 
    {
        return platform;
    }
    public void setOpenid(String openid) 
    {
        this.openid = openid;
    }

    public String getOpenid() 
    {
        return openid;
    }
    public void setOpenname(String openname) 
    {
        this.openname = openname;
    }

    public String getOpenname() 
    {
        return openname;
    }
    public void setAccessToken(String accessToken) 
    {
        this.accessToken = accessToken;
    }

    public String getAccessToken() 
    {
        return accessToken;
    }
    public void setRefreshToken(String refreshToken) 
    {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() 
    {
        return refreshToken;
    }
    public void setExpiresIn(Integer expiresIn) 
    {
        this.expiresIn = expiresIn;
    }

    public Integer getExpiresIn() 
    {
        return expiresIn;
    }
    public void setLogintime(Integer logintime) 
    {
        this.logintime = logintime;
    }

    public Integer getLogintime() 
    {
        return logintime;
    }
    public void setExpiretime(Integer expiretime) 
    {
        this.expiretime = expiretime;
    }

    public Integer getExpiretime() 
    {
        return expiretime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("platform", getPlatform())
            .append("openid", getOpenid())
            .append("openname", getOpenname())
            .append("accessToken", getAccessToken())
            .append("refreshToken", getRefreshToken())
            .append("expiresIn", getExpiresIn())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("logintime", getLogintime())
            .append("expiretime", getExpiretime())
            .toString();
    }
}
