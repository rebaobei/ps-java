package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品对象 fa_xshop_product
 *
 * @author ruoyi
 * @date 2021-07-16
 */
public class FaXshopProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 分类 */
    @Excel(name = "分类")
    private Integer categoryId;

    public Long getStoreInfoId() {
        return storeInfoId;
    }

    public void setStoreInfoId(Long storeInfoId) {
        this.storeInfoId = storeInfoId;
    }

    /** 店铺id */
    @Excel(name = "店铺id")
    private Long storeInfoId;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 是否销售 */
    @Excel(name = "是否销售")
    private Integer onSale;

    /** 折扣 */
    @Excel(name = "折扣")
    private BigDecimal rating;

    /** 销售数量 */
    @Excel(name = "销售数量")
    private Integer soldCount;

    /** 单位 */
    @Excel(name = "单位")
    private Long unitId;
    /** 单位 */
    @Excel(name = "单位")
    private String unit;
    /** 评价数量 */
    @Excel(name = "评价数量")
    private Integer reviewCount;

    /** $column.columnComment */
    @Excel(name = "评价数量")
    private String serviceTags;

    /** 首页推荐 */
    @Excel(name = "首页推荐")
    private Integer homeRecommend;

    /** 分类推荐 */
    @Excel(name = "分类推荐")
    private Integer categoryRecommend;

    /** 销售价 */
    @Excel(name = "销售价")
    private BigDecimal price;

    /** 会员价 */
    @Excel(name = "会员价")
    private BigDecimal cardPrice;

    /** $column.columnComment */
    @Excel(name = "会员价")
    private Long createUser;

    /** 运费模板 */
    @Excel(name = "运费模板")
    private Long deliveryTplId;

    /** 1：普通商品2：1小时送货到家3第三方链接 */
    @Excel(name = "1：普通商品2：1小时送货到家3第三方链接")
    private Integer proType;

    /** 所需积分 */
    @Excel(name = "所需积分")
    private BigDecimal usescore;

    /** 淘宝折扣 */
    @Excel(name = "淘宝折扣")
    private BigDecimal zhekou;

    /** 是否活动 */
    @Excel(name = "是否活动")
    private Integer activitys;
    /** 审核状态 */
    @Excel(name = "审核状态")
    private Integer auditStatus;

    private Long createtime;

    private Long updatetime;

    public String getUnit() {
        return unit;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setCategoryId(Integer categoryId)
    {
        this.categoryId = categoryId;
    }

    public Integer getCategoryId()
    {
        return categoryId;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImage()
    {
        return image;
    }
    public void setOnSale(Integer onSale)
    {
        this.onSale = onSale;
    }

    public Integer getOnSale()
    {
        return onSale;
    }
    public void setRating(BigDecimal rating)
    {
        this.rating = rating;
    }

    public BigDecimal getRating()
    {
        return rating;
    }
    public void setSoldCount(Integer soldCount)
    {
        this.soldCount = soldCount;
    }

    public Integer getSoldCount()
    {
        return soldCount;
    }
    public void setUnitId(Long unitId)
    {
        this.unitId = unitId;
    }

    public Long getUnitId()
    {
        return unitId;
    }
    public void setReviewCount(Integer reviewCount)
    {
        this.reviewCount = reviewCount;
    }

    public Integer getReviewCount()
    {
        return reviewCount;
    }
    public void setServiceTags(String serviceTags)
    {
        this.serviceTags = serviceTags;
    }

    public String getServiceTags()
    {
        return serviceTags;
    }
    public void setHomeRecommend(Integer homeRecommend)
    {
        this.homeRecommend = homeRecommend;
    }

    public Integer getHomeRecommend()
    {
        return homeRecommend;
    }
    public void setCategoryRecommend(Integer categoryRecommend)
    {
        this.categoryRecommend = categoryRecommend;
    }

    public Integer getCategoryRecommend()
    {
        return categoryRecommend;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setCardPrice(BigDecimal cardPrice)
    {
        this.cardPrice = cardPrice;
    }

    public BigDecimal getCardPrice()
    {
        return cardPrice;
    }
    public void setCreateUser(Long createUser)
    {
        this.createUser = createUser;
    }

    public Long getCreateUser()
    {
        return createUser;
    }
    public void setDeliveryTplId(Long deliveryTplId)
    {
        this.deliveryTplId = deliveryTplId;
    }

    public Long getDeliveryTplId()
    {
        return deliveryTplId;
    }
    public void setProType(Integer proType)
    {
        this.proType = proType;
    }

    public Integer getProType()
    {
        return proType;
    }
    public void setUsescore(BigDecimal usescore)
    {
        this.usescore = usescore;
    }

    public BigDecimal getUsescore()
    {
        return usescore;
    }
    public void setZhekou(BigDecimal zhekou)
    {
        this.zhekou = zhekou;
    }

    public BigDecimal getZhekou()
    {
        return zhekou;
    }
    public void setActivitys(Integer activitys)
    {
        this.activitys = activitys;
    }

    public Integer getActivitys()
    {
        return activitys;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("categoryId", getCategoryId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("content", getContent())
            .append("image", getImage())
            .append("onSale", getOnSale())
            .append("rating", getRating())
            .append("soldCount", getSoldCount())
            .append("unitId", getUnitId())
            .append("reviewCount", getReviewCount())
            .append("serviceTags", getServiceTags())
            .append("homeRecommend", getHomeRecommend())
            .append("categoryRecommend", getCategoryRecommend())
            .append("price", getPrice())
            .append("cardPrice", getCardPrice())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("createUser", getCreateUser())
            .append("deliveryTplId", getDeliveryTplId())
            .append("proType", getProType())
            .append("usescore", getUsescore())
            .append("zhekou", getZhekou())
            .append("activitys", getActivitys())
            .toString();
    }
}
