package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 内容模型对象 fa_cms_model
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
public class FaCmsModel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 模型名称 */
    @Excel(name = "模型名称")
    private String name;

    /** 表名 */
    @Excel(name = "表名")
    private String table;

    /** 字段列表 */
    @Excel(name = "字段列表")
    private String fields;

    /** 栏目页模板 */
    @Excel(name = "栏目页模板")
    private String channeltpl;

    /** 列表页模板 */
    @Excel(name = "列表页模板")
    private String listtpl;

    /** 详情页模板 */
    @Excel(name = "详情页模板")
    private String showtpl;

    /** 模型配置 */
    @Excel(name = "模型配置")
    private String setting;


    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setTable(String table) 
    {
        this.table = table;
    }

    public String getTable() 
    {
        return table;
    }
    public void setFields(String fields) 
    {
        this.fields = fields;
    }

    public String getFields() 
    {
        return fields;
    }
    public void setChanneltpl(String channeltpl) 
    {
        this.channeltpl = channeltpl;
    }

    public String getChanneltpl() 
    {
        return channeltpl;
    }
    public void setListtpl(String listtpl) 
    {
        this.listtpl = listtpl;
    }

    public String getListtpl() 
    {
        return listtpl;
    }
    public void setShowtpl(String showtpl) 
    {
        this.showtpl = showtpl;
    }

    public String getShowtpl() 
    {
        return showtpl;
    }
    public void setSetting(String setting) 
    {
        this.setting = setting;
    }

    public String getSetting() 
    {
        return setting;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("table", getTable())
            .append("fields", getFields())
            .append("channeltpl", getChanneltpl())
            .append("listtpl", getListtpl())
            .append("showtpl", getShowtpl())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("setting", getSetting())
            .toString();
    }
}
