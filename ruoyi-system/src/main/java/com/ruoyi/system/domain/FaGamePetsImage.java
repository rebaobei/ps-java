package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 装扮管理对象 fa_game_pets_image
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGamePetsImage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 宠物分类id */
    @Excel(name = "宠物分类id")
    private Long feileiId;

    /** 宠物id */
    @Excel(name = "宠物id")
    private Long petsId;

    /** 姿势(站,坐,趴) */
    @Excel(name = "姿势(站,坐,趴)")
    private Long postureId;

    /** 图片 */
    @Excel(name = "图片")
    private String images;

    /** 衣服Id */
    @Excel(name = "衣服Id")
    private Integer clothId;

    public Integer getClothId() {
        return clothId;
    }

    public void setClothId(Integer clothId) {
        this.clothId = clothId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFeileiId(Long feileiId) 
    {
        this.feileiId = feileiId;
    }

    public Long getFeileiId() 
    {
        return feileiId;
    }
    public void setPetsId(Long petsId) 
    {
        this.petsId = petsId;
    }

    public Long getPetsId() 
    {
        return petsId;
    }
    public void setPostureId(Long postureId) 
    {
        this.postureId = postureId;
    }

    public Long getPostureId() 
    {
        return postureId;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("feileiId", getFeileiId())
            .append("petsId", getPetsId())
            .append("postureId", getPostureId())
            .append("images", getImages())
            .toString();
    }
}
