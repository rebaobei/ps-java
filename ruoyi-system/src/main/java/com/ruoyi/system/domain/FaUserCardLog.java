package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 储值卡变动对象 fa_user_card_log
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaUserCardLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    private Integer id;

    /** 会员ID */
    @Excel(name = "会员ID")
    private Integer userId;

    /** 变更余额 */
    @Excel(name = "变更余额")
    private BigDecimal money;

    /** 变更前余额 */
    @Excel(name = "变更前余额")
    private BigDecimal before;

    /** 变更后余额 */
    @Excel(name = "变更后余额")
    private BigDecimal after;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    /** 订单类型:1充值，-1商品订单，-2预约订单,2后台调整 */
    @Excel(name = "订单类型:1充值，-1商品订单，-2预约订单,2后台调整")
    private Long billType;

    /** 订单id */
    @Excel(name = "订单id")
    private Long billId;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserId(Integer userId) 
    {
        this.userId = userId;
    }

    public Integer getUserId() 
    {
        return userId;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setBefore(BigDecimal before) 
    {
        this.before = before;
    }

    public BigDecimal getBefore() 
    {
        return before;
    }
    public void setAfter(BigDecimal after) 
    {
        this.after = after;
    }

    public BigDecimal getAfter() 
    {
        return after;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }
    public void setBillType(Long billType) 
    {
        this.billType = billType;
    }

    public Long getBillType() 
    {
        return billType;
    }
    public void setBillId(Long billId) 
    {
        this.billId = billId;
    }

    public Long getBillId() 
    {
        return billId;
    }
    public void setManystorePid(Long manystorePid) 
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid() 
    {
        return manystorePid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("money", getMoney())
            .append("before", getBefore())
            .append("after", getAfter())
            .append("memo", getMemo())
            .append("createtime", getCreatetime())
            .append("billType", getBillType())
            .append("billId", getBillId())
            .append("manystorePid", getManystorePid())
            .toString();
    }
}
