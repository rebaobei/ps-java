package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/2
 **/
public class VoteMainVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 标题 */
    private String name;

    /** 图片 */
    private String image;

    /** 录播图 */
    private String images;

    /** 开始时间 */
    private Long begtime;

    /** 结束时间 */
    private Long endtime;

    /** 描述 */
    private String describe;

    /** 活动介绍 */
    private String content;

    /** 价格 */
    private BigDecimal price;

    /** 状态:0=隐藏,1=启用 */
    private String status;

    /** 排序 */
    private Long weigh;

    /** 一天一个会员最多投票次数 */
    private Integer maxdaycount;

    /** 投票数量 */
    private Long votecount;

    /** 参赛数量 */
    private Long joincount;

    /** 浏览量 */
    private Long viewcount;

    /** 参赛名额 */
    private Long maxjoincount;

    /** 创建时间 */
    private Long createtime;

    /** 更新时间 */
    private Long updatetime;

    private List<String> imageList;

    private Long begTimeMillisecond;
    private Long endTimeMillisecond;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Long getBegtime() {
        return begtime;
    }

    public void setBegtime(Long begtime) {
        this.begtime = begtime;
    }

    public Long getEndtime() {
        return endtime;
    }

    public void setEndtime(Long endtime) {
        this.endtime = endtime;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getWeigh() {
        return weigh;
    }

    public void setWeigh(Long weigh) {
        this.weigh = weigh;
    }

    public Integer getMaxdaycount() {
        return maxdaycount;
    }

    public void setMaxdaycount(Integer maxdaycount) {
        this.maxdaycount = maxdaycount;
    }

    public Long getVotecount() {
        return votecount;
    }

    public void setVotecount(Long votecount) {
        this.votecount = votecount;
    }

    public Long getJoincount() {
        return joincount;
    }

    public void setJoincount(Long joincount) {
        this.joincount = joincount;
    }

    public Long getViewcount() {
        return viewcount;
    }

    public void setViewcount(Long viewcount) {
        this.viewcount = viewcount;
    }

    public Long getMaxjoincount() {
        return maxjoincount;
    }

    public void setMaxjoincount(Long maxjoincount) {
        this.maxjoincount = maxjoincount;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public Long getBegTimeMillisecond() {
        return begTimeMillisecond;
    }

    public void setBegTimeMillisecond(Long begTimeMillisecond) {
        this.begTimeMillisecond = begTimeMillisecond;
    }

    public Long getEndTimeMillisecond() {
        return endTimeMillisecond;
    }

    public void setEndTimeMillisecond(Long endTimeMillisecond) {
        this.endTimeMillisecond = endTimeMillisecond;
    }
}
