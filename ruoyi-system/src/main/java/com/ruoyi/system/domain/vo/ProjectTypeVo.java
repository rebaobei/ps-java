package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description 宠物大小
 * @Author fan
 * @Date 2021/7/29
 **/
public class ProjectTypeVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 大小名称 */
    private String name;

    /** 图片 */
    private String image;

    /** 洗澡加价金额 */
    private BigDecimal xzprice;

    /** 美容加价金额 */
    private BigDecimal mrprice;

    /** 高端SPA加价金额 */
    private BigDecimal spaprice;

    /** 状态:0=隐藏,1=显示 */
    private String status;

    /** 分类id */
    private Long feileiId;

    /** 创建时间 */
    private Long createtime;

    /**
     * 宠物分类名称
     */
    private String categoryName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public BigDecimal getXzprice() {
        return xzprice;
    }

    public void setXzprice(BigDecimal xzprice) {
        this.xzprice = xzprice;
    }

    public BigDecimal getMrprice() {
        return mrprice;
    }

    public void setMrprice(BigDecimal mrprice) {
        this.mrprice = mrprice;
    }

    public BigDecimal getSpaprice() {
        return spaprice;
    }

    public void setSpaprice(BigDecimal spaprice) {
        this.spaprice = spaprice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getFeileiId() {
        return feileiId;
    }

    public void setFeileiId(Long feileiId) {
        this.feileiId = feileiId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
