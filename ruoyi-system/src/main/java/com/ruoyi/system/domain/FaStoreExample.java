package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Arrays;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 店铺服务案对象 fa_store_example
 *
 * @author ruoyi
 * @date 2021-10-20
 */
public class FaStoreExample extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 案例名称 */
    @Excel(name = "案例名称")
    private String exampleName;

    /** 描述 */
    private String exampleDescribe;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 宠物名称 */
    @Excel(name = "宠物名称")
    private String petName;

    /** 宠物信息 */
    private String petInfo;

    /** 服务时间 */
    private Long serviceTime;

    /** 赞数 */
    @Excel(name = "赞数")
    private Long upvote;

    /** 图片 */
    private String images1;
    private String images2;

    /** 店铺id */
    private Long storeId;
    /** 店铺名称 */
    private String storeName;

    /** 商品id */
    private Long productId;

    /** 项目id */
    private Long projectId;

    /** 服务人员id */
    private String personId;
    private String[] personIds;

    /** 状态 */
    private Integer status;

    /** 备注 */
    @Excel(name = "备注")
    private String reamrk;

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String[] getPersonIds() {
        return personIds;
    }

    public void setPersonIds(String[] personIds) {
        this.personIds = personIds;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setExampleName(String exampleName)
    {
        this.exampleName = exampleName;
    }

    public String getExampleName()
    {
        return exampleName;
    }
    public void setExampleDescribe(String exampleDescribe)
    {
        this.exampleDescribe = exampleDescribe;
    }

    public String getExampleDescribe()
    {
        return exampleDescribe;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setPetName(String petName)
    {
        this.petName = petName;
    }

    public String getPetName()
    {
        return petName;
    }
    public void setPetInfo(String petInfo)
    {
        this.petInfo = petInfo;
    }

    public String getPetInfo()
    {
        return petInfo;
    }
    public void setServiceTime(Long serviceTime)
    {
        this.serviceTime = serviceTime;
    }

    public Long getServiceTime()
    {
        return serviceTime;
    }
    public void setUpvote(Long upvote)
    {
        this.upvote = upvote;
    }

    public Long getUpvote()
    {
        return upvote;
    }

    public String getImages1() {
        return images1;
    }

    public void setImages1(String images1) {
        this.images1 = images1;
    }

    public String getImages2() {
        return images2;
    }

    public void setImages2(String images2) {
        this.images2 = images2;
    }

    public void setStoreId(Long storeId)
    {
        this.storeId = storeId;
    }

    public Long getStoreId()
    {
        return storeId;
    }
    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public Long getProductId()
    {
        return productId;
    }
    public void setPersonId(String personId)
    {
        this.personId = personId;
    }

    public String getPersonId()
    {
        return personId;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setReamrk(String reamrk)
    {
        this.reamrk = reamrk;
    }

    public String getReamrk()
    {
        return reamrk;
    }

    @Override
    public String toString() {
        return "FaStoreExample{" +
                "id=" + id +
                ", exampleName='" + exampleName + '\'' +
                ", exampleDescribe='" + exampleDescribe + '\'' +
                ", price=" + price +
                ", petName='" + petName + '\'' +
                ", petInfo='" + petInfo + '\'' +
                ", serviceTime=" + serviceTime +
                ", upvote=" + upvote +
                ", images1='" + images1 + '\'' +
                ", images2='" + images2 + '\'' +
                ", storeId=" + storeId +
                ", productId=" + productId +
                ", projectId=" + projectId +
                ", personId='" + personId + '\'' +
                ", personIds=" + Arrays.toString(personIds) +
                ", status=" + status +
                ", reamrk='" + reamrk + '\'' +
                '}';
    }
}
