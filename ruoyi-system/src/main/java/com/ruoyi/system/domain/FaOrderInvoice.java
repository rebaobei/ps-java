package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单发票对象 fa_order_invoice
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public class FaOrderInvoice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 发票金额 */
    @Excel(name = "发票金额")
    private BigDecimal money;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 识别号 */
    @Excel(name = "识别号")
    private String shibieno;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /** 开户行 */
    @Excel(name = "开户行")
    private String bankname;

    /** 账号 */
    @Excel(name = "账号")
    private String zhanghao;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 状态:0=待发,1=已发 */
    @Excel(name = "状态:0=待发,1=已发")
    private String status;

    /** 订单类型:0=商城订单,1=预约订单 */
    @Excel(name = "订单类型:0=商城订单,1=预约订单")
    private String type;

    /** 开票类型:0=个人,1=企业 */
    @Excel(name = "开票类型:0=个人,1=企业")
    private String invoicetype;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setShibieno(String shibieno) 
    {
        this.shibieno = shibieno;
    }

    public String getShibieno() 
    {
        return shibieno;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setBankname(String bankname) 
    {
        this.bankname = bankname;
    }

    public String getBankname() 
    {
        return bankname;
    }
    public void setZhanghao(String zhanghao) 
    {
        this.zhanghao = zhanghao;
    }

    public String getZhanghao() 
    {
        return zhanghao;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }
    public String getStatus() 
    {
        return status;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setInvoicetype(String invoicetype) 
    {
        this.invoicetype = invoicetype;
    }

    public String getInvoicetype() 
    {
        return invoicetype;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("userId", getUserId())
            .append("money", getMoney())
            .append("name", getName())
            .append("shibieno", getShibieno())
            .append("address", getAddress())
            .append("tel", getTel())
            .append("bankname", getBankname())
            .append("zhanghao", getZhanghao())
            .append("createtime", getCreatetime())
            .append("note", getNote())
            .append("status", getStatus())
            .append("type", getType())
            .append("invoicetype", getInvoicetype())
            .toString();
    }
}
