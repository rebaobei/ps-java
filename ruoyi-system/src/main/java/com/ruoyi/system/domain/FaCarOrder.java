package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 同城接送订单对象 fa_car_order
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public class FaCarOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 预约订单id */
    @Excel(name = "预约订单id")
    private Long yyOrderid;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 状态:0=待确认,1=待支付,2=待核销,3=已核销 */
    @Excel(name = "状态:0=待确认,1=待支付,2=待核销,3=已核销")
    private String status;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 姓名 */
    @Excel(name = "姓名")
    private String username;

    /** $column.columnComment */
    @Excel(name = "姓名")
    private String paycode;

    /** 状态:0=未支付,1=已支付 */
    @Excel(name = "状态:0=未支付,1=已支付")
    private String isPay;

    /** $column.columnComment */
    @Excel(name = "状态:0=未支付,1=已支付")
    private Long paytime;

    /** 预约类型：0服务预约1寄宿预约 */
    @Excel(name = "预约类型：0服务预约1寄宿预约")
    private Integer yuyuetype;

    /** 评论次数 */
    @Excel(name = "评论次数")
    private Long commenttimes;


    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setYyOrderid(Long yyOrderid) 
    {
        this.yyOrderid = yyOrderid;
    }

    public Long getYyOrderid() 
    {
        return yyOrderid;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPaycode(String paycode) 
    {
        this.paycode = paycode;
    }

    public String getPaycode() 
    {
        return paycode;
    }
    public void setIsPay(String isPay) 
    {
        this.isPay = isPay;
    }

    public String getIsPay() 
    {
        return isPay;
    }
    public void setPaytime(Long paytime) 
    {
        this.paytime = paytime;
    }

    public Long getPaytime() 
    {
        return paytime;
    }
    public void setYuyuetype(Integer yuyuetype) 
    {
        this.yuyuetype = yuyuetype;
    }

    public Integer getYuyuetype() 
    {
        return yuyuetype;
    }
    public void setCommenttimes(Long commenttimes) 
    {
        this.commenttimes = commenttimes;
    }

    public Long getCommenttimes() 
    {
        return commenttimes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("yyOrderid", getYyOrderid())
            .append("userId", getUserId())
            .append("address", getAddress())
            .append("price", getPrice())
            .append("note", getNote())
            .append("status", getStatus())
            .append("phone", getPhone())
            .append("username", getUsername())
            .append("paycode", getPaycode())
            .append("isPay", getIsPay())
            .append("paytime", getPaytime())
            .append("yuyuetype", getYuyuetype())
            .append("createtime", getCreatetime())
            .append("commenttimes", getCommenttimes())
            .append("updatetime", getUpdatetime())
            .toString();
    }
}
