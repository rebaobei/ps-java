package com.ruoyi.system.domain.vo;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description 房间vo
 * @Author fan
 * @Date 2021/7/27
 **/
public class RoomVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 分类id */
    private Long fenleiId;

    /** 分类名 */
    private String fenleiName;

    /** 房间名称 */
    private String name;

    /** 房间照片 */
    private String image;

    /** 轮播图片 */
    private String luboimages;

    private List<String> lunboStr;

    /** 描述 */
    private String describe;

    /** 详情 */
    private String content;

    /** 价格 */
    private BigDecimal price;

    /** 状态:0=隐藏,1=启用 */
    private String status;

    /** 排序 */
    private Long weigh;

    /** 创建时间 */
    private Long createtime;

    /** 更新时间 */
    private Long updatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFenleiName() {
        return fenleiName;
    }

    public void setFenleiName(String fenleiName) {
        this.fenleiName = fenleiName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFenleiId() {
        return fenleiId;
    }

    public void setFenleiId(Long fenleiId) {
        this.fenleiId = fenleiId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLuboimages() {
        return luboimages;
    }

    public void setLuboimages(String luboimages) {
        this.luboimages = luboimages;
    }

    public List<String> getLunboStr() {
        return lunboStr;
    }

    public void setLunboStr(List<String> lunboStr) {
        this.lunboStr = lunboStr;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getWeigh() {
        return weigh;
    }

    public void setWeigh(Long weigh) {
        this.weigh = weigh;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }
}
