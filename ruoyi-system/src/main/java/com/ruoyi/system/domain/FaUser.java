package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员管理111对象 fa_user
 *
 * @author ruoyi
 * @date 2021-07-05
 */
public class FaUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Integer id;

    /** 组别ID */
    @Excel(name = "组别ID")
    private Integer groupId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickname;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 密码盐 */
    @Excel(name = "密码盐")
    private String salt;

    /** 电子邮箱 */
    @Excel(name = "电子邮箱")
    private String email;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 头像 */
    @Excel(name = "头像")
    private String avatar;

    /** 等级 */
    @Excel(name = "等级")
    private Integer level;

    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 格言 */
    @Excel(name = "格言")
    private String bio;

    /** 余额 */
    @Excel(name = "余额")
    private BigDecimal money;

    /** 积分 */
    @Excel(name = "积分")
    private Integer score;

    /** 连续登录天数 */
    @Excel(name = "连续登录天数")
    private Integer successions;

    /** 最大连续登录天数 */
    @Excel(name = "最大连续登录天数")
    private Integer maxsuccessions;

    /** 上次登录时间 */
    @Excel(name = "上次登录时间")
    private Long prevtime;

    /** 登录时间 */
    @Excel(name = "登录时间")
    private Long logintime;

    /** 登录IP */
    @Excel(name = "登录IP")
    private String loginip;

    /** 失败次数 */
    @Excel(name = "失败次数")
    private Integer loginfailure;

    /** 加入IP */
    @Excel(name = "加入IP")
    private String joinip;

    /** 加入时间 */
    @Excel(name = "加入时间")
    private Long jointime;

    /** Token */
    @Excel(name = "Token")
    private String token;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 验证 */
    @Excel(name = "验证")
    private String verification;

    /** 一级推荐人 */
    @Excel(name = "一级推荐人")
    private Long pid1;

    /** 二级推荐人 */
    @Excel(name = "二级推荐人")
    private Long pid2;

    /** 三级推荐人 */
    @Excel(name = "三级推荐人")
    private Long pid3;

    /** 推荐路径 */
    @Excel(name = "推荐路径")
    private String path;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    /** 是否店员 */
    @Excel(name = "是否店员")
    private Integer userType;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String accountholder;

    /** 姓名 */
    @Excel(name = "姓名")
    private String bankname;

    /** 账号 */
    @Excel(name = "账号")
    private String bankcard;

    /** 宠宝昵称 */
    @Excel(name = "宠宝昵称")
    private String nicheng;

    /** 是否绝育:0=否,1=是 */
    @Excel(name = "是否绝育:0=否,1=是")
    private String isJueyu;

    /** 宠物品种 */
    @Excel(name = "宠物品种")
    private String pingzhong;

    /** 病史 */
    @Excel(name = "病史")
    private String bingshi;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 累计签到天数 */
    @Excel(name = "累计签到天数")
    private Long leijiday;

    /** 应用唯一标识 */
    @Excel(name = "应用唯一标识")
    private String wxOpenid;

    /** 苹果唯一id */
    @Excel(name = "苹果唯一id")
    private String appleOpenid;

    /** 推广二维码 */
    @Excel(name = "推广二维码")
    private String tuiguangimg;

    /** 访客量 */
    @Excel(name = "访客量")
    private Long fangkecount;

    /** 推荐码 */
    @Excel(name = "推荐码")
    private String inviterCode;

    /** 总积分 */
    @Excel(name = "总积分")
    private Long sumscore;

    /** 最后充值时间 */
    @Excel(name = "最后充值时间")
    private Long lastRechargetime;

    /** 主页背景图片 */
    @Excel(name = "主页背景图片")
    private String homepageImage;

    /** 储值卡 */
    @Excel(name = "储值卡")
    private BigDecimal cardmoney;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String realName;

    /** 冻结储值卡金额 */
    @Excel(name = "冻结储值卡金额")
    private BigDecimal freezeCardmoney;

    /** 个推唯一cid */
    @Excel(name = "个推唯一cid")
    private String clientid;

    /** 登陆手机类型android ios */
    @Excel(name = "登陆手机类型android ios")
    private String appType;

    /** 0关闭1开启点赞推送 */
    @Excel(name = "0关闭1开启点赞推送")
    private Long pushZan;

    /** 0关闭1开启评论推送 */
    @Excel(name = "0关闭1开启评论推送")
    private Long pushPing;

    /** 0关闭1开启关注推送 */
    @Excel(name = "0关闭1开启关注推送")
    private Long pushGuan;

    /** 0关闭1开启收藏推送 */
    @Excel(name = "0关闭1开启收藏推送")
    private Long pushShou;

    /** 0关闭1开启签到推送 */
    @Excel(name = "0关闭1开启签到推送")
    private Long pushQian;

    /**
     * 创建时间 long 时间戳
     */
    private Long createtime;

    /**
     * 更新时间 时间戳
     */
    private Long updatetime;
    /**
     * 店铺id
     */
    private Long storeId;
    private String captcha;

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public FaUser() {
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatectime(Long createctime) {
        this.createtime = createctime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setGroupId(Integer groupId)
    {
        this.groupId = groupId;
    }

    public Integer getGroupId()
    {
        return groupId;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }
    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getNickname()
    {
        return nickname;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    public String getSalt()
    {
        return salt;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }
    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getMobile()
    {
        return mobile;
    }
    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getAvatar()
    {
        return avatar;
    }
    public void setLevel(Integer level)
    {
        this.level = level;
    }

    public Integer getLevel()
    {
        return level;
    }
    public void setGender(Integer gender)
    {
        this.gender = gender;
    }

    public Integer getGender()
    {
        return gender;
    }
    public void setBirthday(Date birthday)
    {
        this.birthday = birthday;
    }

    public Date getBirthday()
    {
        return birthday;
    }
    public void setBio(String bio)
    {
        this.bio = bio;
    }

    public String getBio()
    {
        return bio;
    }
    public void setMoney(BigDecimal money)
    {
        this.money = money;
    }

    public BigDecimal getMoney()
    {
        return money;
    }
    public void setScore(Integer score)
    {
        this.score = score;
    }

    public Integer getScore()
    {
        return score;
    }
    public void setSuccessions(Integer successions)
    {
        this.successions = successions;
    }

    public Integer getSuccessions()
    {
        return successions;
    }
    public void setMaxsuccessions(Integer maxsuccessions)
    {
        this.maxsuccessions = maxsuccessions;
    }

    public Integer getMaxsuccessions()
    {
        return maxsuccessions;
    }
    public void setPrevtime(Long prevtime)
    {
        this.prevtime = prevtime;
    }

    public Long getPrevtime()
    {
        return prevtime;
    }
    public void setLogintime(Long logintime)
    {
        this.logintime = logintime;
    }

    public Long getLogintime()
    {
        return logintime;
    }
    public void setLoginip(String loginip)
    {
        this.loginip = loginip;
    }

    public String getLoginip()
    {
        return loginip;
    }
    public void setLoginfailure(Integer loginfailure)
    {
        this.loginfailure = loginfailure;
    }

    public Integer getLoginfailure()
    {
        return loginfailure;
    }
    public void setJoinip(String joinip)
    {
        this.joinip = joinip;
    }

    public String getJoinip()
    {
        return joinip;
    }
    public void setJointime(Long jointime)
    {
        this.jointime = jointime;
    }

    public Long getJointime()
    {
        return jointime;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getToken()
    {
        return token;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setVerification(String verification)
    {
        this.verification = verification;
    }

    public String getVerification()
    {
        return verification;
    }
    public void setPid1(Long pid1)
    {
        this.pid1 = pid1;
    }

    public Long getPid1()
    {
        return pid1;
    }
    public void setPid2(Long pid2)
    {
        this.pid2 = pid2;
    }

    public Long getPid2()
    {
        return pid2;
    }
    public void setPid3(Long pid3)
    {
        this.pid3 = pid3;
    }

    public Long getPid3()
    {
        return pid3;
    }
    public void setPath(String path)
    {
        this.path = path;
    }

    public String getPath()
    {
        return path;
    }
    public void setManystorePid(Long manystorePid)
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid()
    {
        return manystorePid;
    }
    public void setUserType(Integer userType)
    {
        this.userType = userType;
    }

    public Integer getUserType()
    {
        return userType;
    }
    public void setAccountholder(String accountholder)
    {
        this.accountholder = accountholder;
    }

    public String getAccountholder()
    {
        return accountholder;
    }
    public void setBankname(String bankname)
    {
        this.bankname = bankname;
    }

    public String getBankname()
    {
        return bankname;
    }
    public void setBankcard(String bankcard)
    {
        this.bankcard = bankcard;
    }

    public String getBankcard()
    {
        return bankcard;
    }
    public void setNicheng(String nicheng)
    {
        this.nicheng = nicheng;
    }

    public String getNicheng()
    {
        return nicheng;
    }
    public void setIsJueyu(String isJueyu)
    {
        this.isJueyu = isJueyu;
    }

    public String getIsJueyu()
    {
        return isJueyu;
    }
    public void setPingzhong(String pingzhong)
    {
        this.pingzhong = pingzhong;
    }

    public String getPingzhong()
    {
        return pingzhong;
    }
    public void setBingshi(String bingshi)
    {
        this.bingshi = bingshi;
    }

    public String getBingshi()
    {
        return bingshi;
    }
    public void setAge(Long age)
    {
        this.age = age;
    }

    public Long getAge()
    {
        return age;
    }
    public void setLeijiday(Long leijiday)
    {
        this.leijiday = leijiday;
    }

    public Long getLeijiday()
    {
        return leijiday;
    }
    public void setWxOpenid(String wxOpenid)
    {
        this.wxOpenid = wxOpenid;
    }

    public String getWxOpenid()
    {
        return wxOpenid;
    }
    public void setAppleOpenid(String appleOpenid)
    {
        this.appleOpenid = appleOpenid;
    }

    public String getAppleOpenid()
    {
        return appleOpenid;
    }
    public void setTuiguangimg(String tuiguangimg)
    {
        this.tuiguangimg = tuiguangimg;
    }

    public String getTuiguangimg()
    {
        return tuiguangimg;
    }
    public void setFangkecount(Long fangkecount)
    {
        this.fangkecount = fangkecount;
    }

    public Long getFangkecount()
    {
        return fangkecount;
    }
    public void setInviterCode(String inviterCode)
    {
        this.inviterCode = inviterCode;
    }

    public String getInviterCode()
    {
        return inviterCode;
    }
    public void setSumscore(Long sumscore)
    {
        this.sumscore = sumscore;
    }

    public Long getSumscore()
    {
        return sumscore;
    }
    public void setLastRechargetime(Long lastRechargetime)
    {
        this.lastRechargetime = lastRechargetime;
    }

    public Long getLastRechargetime()
    {
        return lastRechargetime;
    }
    public void setHomepageImage(String homepageImage)
    {
        this.homepageImage = homepageImage;
    }

    public String getHomepageImage()
    {
        return homepageImage;
    }
    public void setCardmoney(BigDecimal cardmoney)
    {
        this.cardmoney = cardmoney;
    }

    public BigDecimal getCardmoney()
    {
        return cardmoney;
    }
    public void setRealName(String realName)
    {
        this.realName = realName;
    }

    public String getRealName()
    {
        return realName;
    }
    public void setFreezeCardmoney(BigDecimal freezeCardmoney)
    {
        this.freezeCardmoney = freezeCardmoney;
    }

    public BigDecimal getFreezeCardmoney()
    {
        return freezeCardmoney;
    }
    public void setClientid(String clientid)
    {
        this.clientid = clientid;
    }

    public String getClientid()
    {
        return clientid;
    }
    public void setAppType(String appType)
    {
        this.appType = appType;
    }

    public String getAppType()
    {
        return appType;
    }
    public void setPushZan(Long pushZan)
    {
        this.pushZan = pushZan;
    }

    public Long getPushZan()
    {
        return pushZan;
    }
    public void setPushPing(Long pushPing)
    {
        this.pushPing = pushPing;
    }

    public Long getPushPing()
    {
        return pushPing;
    }
    public void setPushGuan(Long pushGuan)
    {
        this.pushGuan = pushGuan;
    }

    public Long getPushGuan()
    {
        return pushGuan;
    }
    public void setPushShou(Long pushShou)
    {
        this.pushShou = pushShou;
    }

    public Long getPushShou()
    {
        return pushShou;
    }
    public void setPushQian(Long pushQian)
    {
        this.pushQian = pushQian;
    }

    public Long getPushQian()
    {
        return pushQian;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("groupId", getGroupId())
            .append("username", getUsername())
            .append("nickname", getNickname())
            .append("password", getPassword())
            .append("salt", getSalt())
            .append("email", getEmail())
            .append("mobile", getMobile())
            .append("avatar", getAvatar())
            .append("level", getLevel())
            .append("gender", getGender())
            .append("birthday", getBirthday())
            .append("bio", getBio())
            .append("money", getMoney())
            .append("score", getScore())
            .append("successions", getSuccessions())
            .append("maxsuccessions", getMaxsuccessions())
            .append("prevtime", getPrevtime())
            .append("logintime", getLogintime())
            .append("loginip", getLoginip())
            .append("loginfailure", getLoginfailure())
            .append("joinip", getJoinip())
            .append("jointime", getJointime())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("token", getToken())
            .append("status", getStatus())
            .append("verification", getVerification())
            .append("pid1", getPid1())
            .append("pid2", getPid2())
            .append("pid3", getPid3())
            .append("path", getPath())
            .append("manystorePid", getManystorePid())
            .append("userType", getUserType())
            .append("accountholder", getAccountholder())
            .append("bankname", getBankname())
            .append("bankcard", getBankcard())
            .append("nicheng", getNicheng())
            .append("isJueyu", getIsJueyu())
            .append("pingzhong", getPingzhong())
            .append("bingshi", getBingshi())
            .append("age", getAge())
            .append("leijiday", getLeijiday())
            .append("wxOpenid", getWxOpenid())
            .append("appleOpenid", getAppleOpenid())
            .append("tuiguangimg", getTuiguangimg())
            .append("fangkecount", getFangkecount())
            .append("inviterCode", getInviterCode())
            .append("sumscore", getSumscore())
            .append("lastRechargetime", getLastRechargetime())
            .append("homepageImage", getHomepageImage())
            .append("cardmoney", getCardmoney())
            .append("realName", getRealName())
            .append("freezeCardmoney", getFreezeCardmoney())
            .append("clientid", getClientid())
            .append("appType", getAppType())
            .append("pushZan", getPushZan())
            .append("pushPing", getPushPing())
            .append("pushGuan", getPushGuan())
            .append("pushShou", getPushShou())
            .append("pushQian", getPushQian())
            .toString();
    }
}
