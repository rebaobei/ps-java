package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 运费规则对象 fa_xshop_delivery_rule
 * 
 * @author ruoyi
 * @date 2021-07-20
 */
public class FaXshopDeliveryRule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 模板ID */
    @Excel(name = "模板ID")
    private Integer tplId;

    /** 首重/件价格 */
    @Excel(name = "首重/件价格")
    private BigDecimal firstPrice;

    /** 续重/件价格 */
    @Excel(name = "续重/件价格")
    private BigDecimal restPrice;

    /** 地区id */
    @Excel(name = "地区id")
    private String areaIds;

    /** 地区名 */
    @Excel(name = "地区名")
    private String areaNames;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setTplId(Integer tplId) 
    {
        this.tplId = tplId;
    }

    public Integer getTplId() 
    {
        return tplId;
    }
    public void setFirstPrice(BigDecimal firstPrice) 
    {
        this.firstPrice = firstPrice;
    }

    public BigDecimal getFirstPrice() 
    {
        return firstPrice;
    }
    public void setRestPrice(BigDecimal restPrice) 
    {
        this.restPrice = restPrice;
    }

    public BigDecimal getRestPrice() 
    {
        return restPrice;
    }
    public void setAreaIds(String areaIds) 
    {
        this.areaIds = areaIds;
    }

    public String getAreaIds() 
    {
        return areaIds;
    }
    public void setAreaNames(String areaNames) 
    {
        this.areaNames = areaNames;
    }

    public String getAreaNames() 
    {
        return areaNames;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tplId", getTplId())
            .append("firstPrice", getFirstPrice())
            .append("restPrice", getRestPrice())
            .append("areaIds", getAreaIds())
            .append("areaNames", getAreaNames())
            .toString();
    }
}
