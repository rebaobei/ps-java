package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description 预约订单详情
 * @Author fan
 * @Date 2021/8/3
 **/
public class YuyueOrderDetailVo {

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 姓名 */
    private String name;

    /** 品种 */
    private String pinzhong;

    /** 年龄 */
    private Long age;

    /** 体重 */
    private BigDecimal weight;

    /** 订单id */
    private Long orderId;

    /** 会员id */
    private Long userId;

    /** 性别 */
    private Integer sex;

    /** 疫苗注射情况 */
    private String yimiaotype;

    /** 驱虫情况 */
    private String quchongtype;

    /** 健康状况 */
    private String jiankangtype;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    /** 备注 */
    private String note;

    /** 视频id */
    private Long videoId;

    /** 状态:0=关闭视频,1=启用视频 */
    private String status;

    /**
     * 主人姓名
     */
    private String username;

    private String yuyueTime;
    private Integer gender;
    private String nickname;
    private String tel;

    private String ordercode;
    private Integer ordertype;

    private String videoName;

    private String isPay;

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public Integer getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(Integer ordertype) {
        this.ordertype = ordertype;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    /** 创建时间 */
    private Long createtime;

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPinzhong() {
        return pinzhong;
    }

    public void setPinzhong(String pinzhong) {
        this.pinzhong = pinzhong;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getYimiaotype() {
        return yimiaotype;
    }

    public void setYimiaotype(String yimiaotype) {
        this.yimiaotype = yimiaotype;
    }

    public String getQuchongtype() {
        return quchongtype;
    }

    public void setQuchongtype(String quchongtype) {
        this.quchongtype = quchongtype;
    }

    public String getJiankangtype() {
        return jiankangtype;
    }

    public void setJiankangtype(String jiankangtype) {
        this.jiankangtype = jiankangtype;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getVideoId() {
        return videoId;
    }

    public void setVideoId(Long videoId) {
        this.videoId = videoId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getYuyueTime() {
        return yuyueTime;
    }

    public void setYuyueTime(String yuyueTime) {
        this.yuyueTime = yuyueTime;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
