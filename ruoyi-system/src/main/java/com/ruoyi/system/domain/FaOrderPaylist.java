package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 支付记录对象 fa_order_paylist
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public class FaOrderPaylist extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payType;

    /** 支付金额 */
    @Excel(name = "支付金额")
    private BigDecimal price;

    /** 支付时间 */
    @Excel(name = "支付时间")
    private Long paytime;

    /** 状态:0=商城订单,1=预约订单,2=充值订单,3升级订单,4=专车接送订单 */
    @Excel(name = "状态:0=商城订单,1=预约订单,2=充值订单,3升级订单,4=专车接送订单")
    private String type;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPayType(String payType) 
    {
        this.payType = payType;
    }

    public String getPayType() 
    {
        return payType;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setPaytime(Long paytime) 
    {
        this.paytime = paytime;
    }

    public Long getPaytime() 
    {
        return paytime;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("userId", getUserId())
            .append("payType", getPayType())
            .append("price", getPrice())
            .append("paytime", getPaytime())
            .append("createtime", getCreatetime())
            .append("type", getType())
            .toString();
    }
}
