package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/16
 **/
public class GamePetsImageVo {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 宠物分类id */
    @Excel(name = "宠物分类id")
    private Long feileiId;

    /** 宠物id */
    @Excel(name = "宠物id")
    private Long petsId;

    /** 姿势(站,坐,趴) */
    @Excel(name = "姿势(站,坐,趴)")
    private Long postureId;

    /** 图片 */
    @Excel(name = "图片")
    private String images;

    /** 衣服Id */
    @Excel(name = "衣服Id")
    private Integer clothId;

    /**
     * 衣服名称
     */
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFeileiId() {
        return feileiId;
    }

    public void setFeileiId(Long feileiId) {
        this.feileiId = feileiId;
    }

    public Long getPetsId() {
        return petsId;
    }

    public void setPetsId(Long petsId) {
        this.petsId = petsId;
    }

    public Long getPostureId() {
        return postureId;
    }

    public void setPostureId(Long postureId) {
        this.postureId = postureId;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getClothId() {
        return clothId;
    }

    public void setClothId(Integer clothId) {
        this.clothId = clothId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
