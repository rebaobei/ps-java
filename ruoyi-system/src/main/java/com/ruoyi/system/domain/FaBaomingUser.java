package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报名管理对象 fa_baoming_user
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public class FaBaomingUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String username;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /** 宠物昵称 */
    @Excel(name = "宠物昵称")
    private String chongName;

    /** 宠物性别1 男2女 */
    @Excel(name = "宠物性别1 男2女")
    private Long chongSix;

    /** 备注 */
    @Excel(name = "备注")
    private String chongRank;

    /** 图 */
    @Excel(name = "图")
    private String images;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userid;

    /** 状态:0=未发布,1=已发布 */
    @Excel(name = "状态:0=未发布,1=已发布")
    private String status;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setChongName(String chongName) 
    {
        this.chongName = chongName;
    }

    public String getChongName() 
    {
        return chongName;
    }
    public void setChongSix(Long chongSix) 
    {
        this.chongSix = chongSix;
    }

    public Long getChongSix() 
    {
        return chongSix;
    }
    public void setChongRank(String chongRank) 
    {
        this.chongRank = chongRank;
    }

    public String getChongRank() 
    {
        return chongRank;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setUserid(Long userid) 
    {
        this.userid = userid;
    }

    public Long getUserid() 
    {
        return userid;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("username", getUsername())
            .append("tel", getTel())
            .append("chongName", getChongName())
            .append("chongSix", getChongSix())
            .append("chongRank", getChongRank())
            .append("images", getImages())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("userid", getUserid())
            .append("status", getStatus())
            .toString();
    }
}
