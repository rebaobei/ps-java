package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物分类对象 fa_technicians
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
public class FaTechnicians extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 照片 */
    @Excel(name = "照片")
    private String images;

    /** 描述 */
    @Excel(name = "描述")
    private String describe;

    /** 详情 */
    @Excel(name = "详情")
    private String concat;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 状态:0=隐藏,1=启用 */
    @Excel(name = "状态:0=隐藏,1=启用")
    private String status;

    /** 排序 */
    @Excel(name = "排序")
    private Long weigh;
    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;
    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setConcat(String concat) 
    {
        this.concat = concat;
    }

    public String getConcat() 
    {
        return concat;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("images", getImages())
            .append("describe", getDescribe())
            .append("concat", getConcat())
            .append("price", getPrice())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("weigh", getWeigh())
            .toString();
    }
}
