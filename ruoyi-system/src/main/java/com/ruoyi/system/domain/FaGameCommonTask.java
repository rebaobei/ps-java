package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 常规任务对象 fa_game_common_task
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameCommonTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 常规任务名称 */
    @Excel(name = "常规任务名称")
    private String name;

    /** 冷却时间 */
    @Excel(name = "冷却时间")
    private Long cd;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCd(Long cd) 
    {
        this.cd = cd;
    }

    public Long getCd() 
    {
        return cd;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("cd", getCd())
            .toString();
    }
}
