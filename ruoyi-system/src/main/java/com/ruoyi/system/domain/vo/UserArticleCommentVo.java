package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

/**
 * @Description 文章评论
 * @Author fan
 * @Date 2021/7/31
 **/
public class UserArticleCommentVo {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 文章id */
    private Long userarticleId;

    /** 用户id */
    private Long userId;

    /** 评论内容 */
    private String note;

    /** 父级 */
    private Long pid;

    /** 喜欢数量 */
    private Long likecount;

    /** 状态:0=隐藏,1=审核 */
    private String status;

    /** 消息类型 */
    private Long messType;

    /** 更新时间 */
    private Long updatetime;
    /** 创建时间 */
    private Long createtime;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 文章内容
     */
    private String articleNote;

    /**
     * 头像
     */
    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserarticleId() {
        return userarticleId;
    }

    public void setUserarticleId(Long userarticleId) {
        this.userarticleId = userarticleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getLikecount() {
        return likecount;
    }

    public void setLikecount(Long likecount) {
        this.likecount = likecount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getMessType() {
        return messType;
    }

    public void setMessType(Long messType) {
        this.messType = messType;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getArticleNote() {
        return articleNote;
    }

    public void setArticleNote(String articleNote) {
        this.articleNote = articleNote;
    }
}
