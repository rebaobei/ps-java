package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/2
 **/
public class VoteOrderVo implements Serializable {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getVoteId() {
        return voteId;
    }

    public void setVoteId(Long voteId) {
        this.voteId = voteId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Long voteCount) {
        this.voteCount = voteCount;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public Long getMainId() {
        return mainId;
    }

    public void setMainId(Long mainId) {
        this.mainId = mainId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 订单号
     */
    private String code;

    /**
     * 选手id
     */
    private Long voteId;

    /**
     * 购买人id
     */
    private Long userId;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 投票数量
     */
    private Long voteCount;

    /**
     * 支付类型
     */
    private String paytype;

    /**
     * 支付时间
     */
    private Long paytime;

    /**
     * 支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款
     */
    private String isPay;

    /**
     * 投票活动id
     */
    private Long mainId;

    /**
     * 创建时间
     */
    private Long createtime;

    /**
     * 更新时间
     */
    private Long updatetime;

    private Long payQueryStart;
    private Long payQueryEnd;

    public Long getPayQueryStart() {
        return payQueryStart;
    }

    public void setPayQueryStart(Long payQueryStart) {
        this.payQueryStart = payQueryStart;
    }

    public Long getPayQueryEnd() {
        return payQueryEnd;
    }

    public void setPayQueryEnd(Long payQueryEnd) {
        this.payQueryEnd = payQueryEnd;
    }

    private String nickname;

    private String describe;

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
