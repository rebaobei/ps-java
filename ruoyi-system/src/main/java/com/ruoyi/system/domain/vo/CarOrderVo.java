package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description 同城专车订单
 * @Author fan
 * @Date 2021/8/3
 **/
public class CarOrderVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 预约订单id */
    private Long yyOrderid;

    /** 用户id */
    private Long userId;

    /** 地址 */
    private String address;

    /** 价格 */
    private BigDecimal price;

    /** 备注 */
    private String note;

    /** 状态:0=待确认,1=待支付,2=待核销,3=已核销 */
    private String status;

    /** 手机号 */
    private String phone;

    /** 姓名 */
    private String username;

    /** $column.columnComment */
    private String paycode;

    /** 状态:0=未支付,1=已支付 */
    private String isPay;

    /** $column.columnComment */
    private Long paytime;

    /** 预约类型：0服务预约1寄宿预约 */
    private Integer yuyuetype;

    /** 评论次数 */
    private Long commenttimes;


    /** 更新时间 */
    private Long updatetime;

    /** 创建时间 */
    private Long createtime;

    private String nickname;

    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getYyOrderid() {
        return yyOrderid;
    }

    public void setYyOrderid(Long yyOrderid) {
        this.yyOrderid = yyOrderid;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPaycode() {
        return paycode;
    }

    public void setPaycode(String paycode) {
        this.paycode = paycode;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

    public Integer getYuyuetype() {
        return yuyuetype;
    }

    public void setYuyuetype(Integer yuyuetype) {
        this.yuyuetype = yuyuetype;
    }

    public Long getCommenttimes() {
        return commenttimes;
    }

    public void setCommenttimes(Long commenttimes) {
        this.commenttimes = commenttimes;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }
}
