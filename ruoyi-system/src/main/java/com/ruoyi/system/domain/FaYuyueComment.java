package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 预约订单评论对象 fa_yuyue_comment
 *
 * @author ruoyi
 * @date 2021-07-28
 */
public class FaYuyueComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 评论 */
    @Excel(name = "评论")
    private String note;

    /** 项目id */
    @Excel(name = "项目id")
    private Long proId;

    /** 评分 */
    @Excel(name = "评分")
    private Long pingfen;

    /** 订单类型 */
    @Excel(name = "订单类型")
    private Integer commentType;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 店铺id */
    private Long storeId;
    /** 店铺名称 */
    private String storeName;
    /**
     * 审核状态
     */
    private Integer status;
    /**
     * 图片
     */
    private String images;
    /**
     * 父id
     */
    private Integer parentId;


    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public Long getOrderId()
    {
        return orderId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote()
    {
        return note;
    }
    public void setProId(Long proId)
    {
        this.proId = proId;
    }

    public Long getProId()
    {
        return proId;
    }
    public void setPingfen(Long pingfen)
    {
        this.pingfen = pingfen;
    }

    public Long getPingfen()
    {
        return pingfen;
    }
    public void setCommentType(Integer commentType)
    {
        this.commentType = commentType;
    }

    public Integer getCommentType()
    {
        return commentType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("userId", getUserId())
            .append("note", getNote())
            .append("proId", getProId())
            .append("createtime", getCreatetime())
            .append("pingfen", getPingfen())
            .append("commentType", getCommentType())
            .toString();
    }
}
