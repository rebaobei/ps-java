package com.ruoyi.system.domain.vo;


import java.io.Serializable;
import java.util.List;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/2
 **/
public class VoteUserVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 主表id */
    private Long mainId;

    /** 会员id */
    private Long userId;

    /** 选手号 */
    private Long num;

    /** 图片 */
    private String image;

    /** 轮播图 */
    private String images;

    /** 宠物寄语 */
    private String describe;

    /** 详情 */
    private String content;

    /** 投票数 */
    private Long votecount;

    /** 状态:0=隐藏,1=显示 */
    private String status;

    /** 主人姓名 */
    private String userName;

    /** 联系电话 */
    private String tel;

    /** 宠物昵称 */
    private String petName;

    /** 宠物性别 */
    private String petSex;

    /** 创建时间 */
    private Long createtime;

    /** 更新时间 */
    private Long updatetime;

    private String avatar;

    private String nickname;

    private String name;

    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    private List<String> imageList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMainId() {
        return mainId;
    }

    public void setMainId(Long mainId) {
        this.mainId = mainId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getVotecount() {
        return votecount;
    }

    public void setVotecount(Long votecount) {
        this.votecount = votecount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetSex() {
        return petSex;
    }

    public void setPetSex(String petSex) {
        this.petSex = petSex;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
