package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 等级管理对象 fa_user_rank
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaUserRank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 级别名称 */
    @Excel(name = "级别名称")
    private String name;

    /** 级别 */
    @Excel(name = "级别")
    private Long grade;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 赠送积分 */
    @Excel(name = "赠送积分")
    private Long leijiscore;

    /** 充值金额 */
    @Excel(name = "充值金额")
    private BigDecimal chzhmoney;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 状态:0=隐藏,1=正常 */
    @Excel(name = "状态:0=隐藏,1=正常")
    private String status;

    /** 造型服务预约折扣 */
    @Excel(name = "造型服务预约折扣")
    private BigDecimal spaDiscount;

    /** 七星级寄宿折扣 */
    @Excel(name = "七星级寄宿折扣")
    private BigDecimal stayDiscount;

    /** 1小时送货到家折扣 */
    @Excel(name = "1小时送货到家折扣")
    private BigDecimal fastsendDiscount;

    /** 普通商品折扣 */
    @Excel(name = "普通商品折扣")
    private BigDecimal goodsDiscount;

    /** 专车接送折扣 */
    @Excel(name = "专车接送折扣")
    private BigDecimal jiesongDiscount;

    /** 商城会员价:0=关闭,1=开启 */
    @Excel(name = "商城会员价:0=关闭,1=开启")
    private String goodsZhekoutype;

    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGrade(Long grade) 
    {
        this.grade = grade;
    }

    public Long getGrade() 
    {
        return grade;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setLeijiscore(Long leijiscore) 
    {
        this.leijiscore = leijiscore;
    }

    public Long getLeijiscore() 
    {
        return leijiscore;
    }
    public void setChzhmoney(BigDecimal chzhmoney) 
    {
        this.chzhmoney = chzhmoney;
    }

    public BigDecimal getChzhmoney() 
    {
        return chzhmoney;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setSpaDiscount(BigDecimal spaDiscount) 
    {
        this.spaDiscount = spaDiscount;
    }

    public BigDecimal getSpaDiscount() 
    {
        return spaDiscount;
    }
    public void setStayDiscount(BigDecimal stayDiscount) 
    {
        this.stayDiscount = stayDiscount;
    }

    public BigDecimal getStayDiscount() 
    {
        return stayDiscount;
    }
    public void setFastsendDiscount(BigDecimal fastsendDiscount) 
    {
        this.fastsendDiscount = fastsendDiscount;
    }

    public BigDecimal getFastsendDiscount() 
    {
        return fastsendDiscount;
    }
    public void setGoodsDiscount(BigDecimal goodsDiscount) 
    {
        this.goodsDiscount = goodsDiscount;
    }

    public BigDecimal getGoodsDiscount() 
    {
        return goodsDiscount;
    }
    public void setJiesongDiscount(BigDecimal jiesongDiscount) 
    {
        this.jiesongDiscount = jiesongDiscount;
    }

    public BigDecimal getJiesongDiscount() 
    {
        return jiesongDiscount;
    }
    public void setGoodsZhekoutype(String goodsZhekoutype) 
    {
        this.goodsZhekoutype = goodsZhekoutype;
    }

    public String getGoodsZhekoutype() 
    {
        return goodsZhekoutype;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("grade", getGrade())
            .append("image", getImage())
            .append("leijiscore", getLeijiscore())
            .append("chzhmoney", getChzhmoney())
            .append("note", getNote())
            .append("status", getStatus())
            .append("spaDiscount", getSpaDiscount())
            .append("stayDiscount", getStayDiscount())
            .append("fastsendDiscount", getFastsendDiscount())
            .append("goodsDiscount", getGoodsDiscount())
            .append("createtime", getCreatetime())
            .append("jiesongDiscount", getJiesongDiscount())
            .append("goodsZhekoutype", getGoodsZhekoutype())
            .toString();
    }
}
