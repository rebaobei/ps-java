package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 预约项目对象 fa_projects
 *
 * @author ruoyi
 * @date 2021-07-26
 */
public class FaProjects extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String name;

    /** 店铺ID */
    @Excel(name = "店铺ID")
    private Long storeInfoId;

    /** 照片 */
    @Excel(name = "照片")
    private String image;

    /** 描述 */
    @Excel(name = "描述")
    private String describe;

    /** 详情 */
    @Excel(name = "详情")
    private String content;
    /** 购买须知 */
    @Excel(name = "购买须知")
    private String explain;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;
    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal initPrice;

    /** 状态:0=隐藏,1=启用 */
    @Excel(name = "状态:0=隐藏,1=启用")
    private String status;

    /** 排序 */
    @Excel(name = "排序")
    private Long weigh;

    /** 项目类型:0=犬宝服务项目,1=专车接送项目,2=猫宝服务项目 */
    @Excel(name = "项目类型:0=犬宝服务项目,1=专车接送项目,2=猫宝服务项目")
    private String protype;

    /** 轮播照片 */
    @Excel(name = "轮播照片")
    private String luboimages;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;
    /** 首页推荐 */
    @Excel(name = "首页推荐")
    private Integer homeRecommend;
    /** 标签 */
    private String label;
    /**
     * 销售数量
     */
    private int totalSell;
    /**
     * 查看次数
     */
    private int showCount;
    /**
     * 审核状态
     */
    private Integer auditStatus;
    private String projectTypeId;


    public String getProjectTypeId() {
        return projectTypeId;
    }

    public void setProjectTypeId(String projectTypeId) {
        this.projectTypeId = projectTypeId;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public int getShowCount() {
        return showCount;
    }

    public void setShowCount(int showCount) {
        this.showCount = showCount;
    }

    public BigDecimal getInitPrice() {
        return initPrice;
    }

    public void setInitPrice(BigDecimal initPrice) {
        this.initPrice = initPrice;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getTotalSell() {
        return totalSell;
    }

    public void setTotalSell(int totalSell) {
        this.totalSell = totalSell;
    }

    public Integer getHomeRecommend() {
        return homeRecommend;
    }

    public void setHomeRecommend(Integer homeRecommend) {
        this.homeRecommend = homeRecommend;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Long getStoreInfoId() {
        return storeInfoId;
    }

    public void setStoreInfoId(Long storeInfoId) {
        this.storeInfoId = storeInfoId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImage()
    {
        return image;
    }
    public void setDescribe(String describe)
    {
        this.describe = describe;
    }

    public String getDescribe()
    {
        return describe;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setWeigh(Long weigh)
    {
        this.weigh = weigh;
    }

    public Long getWeigh()
    {
        return weigh;
    }
    public void setProtype(String protype)
    {
        this.protype = protype;
    }

    public String getProtype()
    {
        return protype;
    }
    public void setLuboimages(String luboimages)
    {
        this.luboimages = luboimages;
    }

    public String getLuboimages()
    {
        return luboimages;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("image", getImage())
            .append("describe", getDescribe())
            .append("content", getContent())
            .append("price", getPrice())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("status", getStatus())
            .append("weigh", getWeigh())
            .append("protype", getProtype())
            .append("luboimages", getLuboimages())
            .toString();
    }
}
