package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物品管理对象 fa_game_things
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameThings extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物品id */
    private Long id;

    /** 物品名称 */
    @Excel(name = "物品名称")
    private String name;

    /** 物品类型 */
    @Excel(name = "物品类型")
    private Long categoryId;

    /** 物品图片 */
    @Excel(name = "物品图片")
    private String image;

    /** 适用的宠物类型，默认值是0，所有类型通用 */
    @Excel(name = "适用的宠物类型，默认值是0，所有类型通用")
    private Long petType;

    /** 效果说明 */
    @Excel(name = "效果说明")
    private String result;

    /** 修改 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /** 创建 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** 使用后增加的成长值 */
    @Excel(name = "使用后增加的成长值")
    private Long growthPoint;

    /** 装扮类型 */
    @Excel(name = "装扮类型")
    private Long decCategory;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setPetType(Long petType) 
    {
        this.petType = petType;
    }

    public Long getPetType() 
    {
        return petType;
    }
    public void setResult(String result) 
    {
        this.result = result;
    }

    public String getResult() 
    {
        return result;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }
    public void setGmtCreate(Date gmtCreate) 
    {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCreate() 
    {
        return gmtCreate;
    }
    public void setGrowthPoint(Long growthPoint) 
    {
        this.growthPoint = growthPoint;
    }

    public Long getGrowthPoint() 
    {
        return growthPoint;
    }
    public void setDecCategory(Long decCategory) 
    {
        this.decCategory = decCategory;
    }

    public Long getDecCategory() 
    {
        return decCategory;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("categoryId", getCategoryId())
            .append("image", getImage())
            .append("petType", getPetType())
            .append("result", getResult())
            .append("gmtModified", getGmtModified())
            .append("gmtCreate", getGmtCreate())
            .append("growthPoint", getGrowthPoint())
            .append("decCategory", getDecCategory())
            .toString();
    }
}
