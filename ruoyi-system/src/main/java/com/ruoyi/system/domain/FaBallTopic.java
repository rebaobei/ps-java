package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 话题管理对象 fa_ball_topic
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public class FaBallTopic extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 话题名称 */
    @Excel(name = "话题名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String describe;

    /** 图标 */
    @Excel(name = "图标")
    private String image;

    /** 描述图片 */
    @Excel(name = "描述图片")
    private String desImage;

    /** 数量 */
    @Excel(name = "数量")
    private Long addcount;

    /** 首页是否展示:0=否,1=是 */
    @Excel(name = "首页是否展示:0=否,1=是")
    private String ifShow;

    /** 排序 */
    @Excel(name = "排序")
    private Long weigh;

    /** 状态:0=禁用,1=启用 */
    @Excel(name = "状态:0=禁用,1=启用")
    private String status;

    /** 申请人 */
    @Excel(name = "申请人")
    private Long userId;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setDesImage(String desImage) 
    {
        this.desImage = desImage;
    }

    public String getDesImage() 
    {
        return desImage;
    }
    public void setAddcount(Long addcount) 
    {
        this.addcount = addcount;
    }

    public Long getAddcount() 
    {
        return addcount;
    }
    public void setIfShow(String ifShow) 
    {
        this.ifShow = ifShow;
    }

    public String getIfShow() 
    {
        return ifShow;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("describe", getDescribe())
            .append("image", getImage())
            .append("desImage", getDesImage())
            .append("addcount", getAddcount())
            .append("createtime", getCreatetime())
            .append("ifShow", getIfShow())
            .append("weigh", getWeigh())
            .append("status", getStatus())
            .append("userId", getUserId())
            .toString();
    }
}
