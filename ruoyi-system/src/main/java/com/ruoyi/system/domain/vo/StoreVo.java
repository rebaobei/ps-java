package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description 门店Vo
 * @author fan
 * @Date 2021/7/29
 **/
public class StoreVo {

    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 店铺名称 */
    private String shopname;

    /** 联系电话 */
    private String tel;

    /** 省 */
    private String province;

    /** 市 */
    private String city;

    /** 县 */
    private String district;

    /** 地址 */
    private String address;

    /** 描述 */
    private String describe;

    /** 详情 */
    private String content;

    /** 图片 */
    private String images;

    /** 经度 */
    private String longitude;

    /** 纬度 */
    private String latitude;

    /** 状态:0=待审核,1=已审核,-1=驳回 */
    private String status;

    /** 二维码 */
    private String qrcodeurl;

    /** 余额 */
    private BigDecimal money;

    /** 首页轮播图 */
    private String bannerimages;

    /**
     * 轮播图列表
     */
    private List<String> lunboImage;

    /** 散客上限 */
    private Long maxcount;

    /** 半场数 */
    private Long bancount;

    /** 创建时间 */
    private Long createtime;

    /**
     * 更新时间
     */
    private Long updatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQrcodeurl() {
        return qrcodeurl;
    }

    public void setQrcodeurl(String qrcodeurl) {
        this.qrcodeurl = qrcodeurl;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getBannerimages() {
        return bannerimages;
    }

    public void setBannerimages(String bannerimages) {
        this.bannerimages = bannerimages;
    }

    public List<String> getLunboImage() {
        return lunboImage;
    }

    public void setLunboImage(List<String> lunboImage) {
        this.lunboImage = lunboImage;
    }

    public Long getMaxcount() {
        return maxcount;
    }

    public void setMaxcount(Long maxcount) {
        this.maxcount = maxcount;
    }

    public Long getBancount() {
        return bancount;
    }

    public void setBancount(Long bancount) {
        this.bancount = bancount;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }
}
