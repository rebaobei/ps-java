package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物大小管理对象 fa_projects_type
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
public class FaProjectsType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 大小名称 */
    @Excel(name = "大小名称")
    private String name;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 洗澡加价金额 */
    @Excel(name = "洗澡加价金额")
    private BigDecimal xzprice;

    /** 美容加价金额 */
    @Excel(name = "美容加价金额")
    private BigDecimal mrprice;

    /** 高端SPA加价金额 */
    @Excel(name = "高端SPA加价金额")
    private BigDecimal spaprice;

    /** 状态:0=隐藏,1=显示 */
    @Excel(name = "状态:0=隐藏,1=显示")
    private String status;

    /** 分类id */
    @Excel(name = "分类id")
    private Long feileiId;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;


    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setXzprice(BigDecimal xzprice) 
    {
        this.xzprice = xzprice;
    }

    public BigDecimal getXzprice() 
    {
        return xzprice;
    }
    public void setMrprice(BigDecimal mrprice) 
    {
        this.mrprice = mrprice;
    }

    public BigDecimal getMrprice() 
    {
        return mrprice;
    }
    public void setSpaprice(BigDecimal spaprice) 
    {
        this.spaprice = spaprice;
    }

    public BigDecimal getSpaprice() 
    {
        return spaprice;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setFeileiId(Long feileiId) 
    {
        this.feileiId = feileiId;
    }

    public Long getFeileiId() 
    {
        return feileiId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("image", getImage())
            .append("xzprice", getXzprice())
            .append("mrprice", getMrprice())
            .append("spaprice", getSpaprice())
            .append("status", getStatus())
            .append("createtime", getCreatetime())
            .append("feileiId", getFeileiId())
            .toString();
    }
}
