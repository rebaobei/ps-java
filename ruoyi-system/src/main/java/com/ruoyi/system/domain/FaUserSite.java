package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员其他信息对象 fa_user_site
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class FaUserSite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 登录次数 */
    @Excel(name = "登录次数")
    private Long logintimes;

    /** 是否发送签到提醒 */
    @Excel(name = "是否发送签到提醒")
    private Long isRemind;

    /** 第一次加入购物车 */
    @Excel(name = "第一次加入购物车")
    private Integer addcartTimes;

    /** 第一次开启签到提醒 */
    @Excel(name = "第一次开启签到提醒")
    private Integer remindTimes;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setLogintimes(Long logintimes) 
    {
        this.logintimes = logintimes;
    }

    public Long getLogintimes() 
    {
        return logintimes;
    }
    public void setIsRemind(Long isRemind) 
    {
        this.isRemind = isRemind;
    }

    public Long getIsRemind() 
    {
        return isRemind;
    }
    public void setAddcartTimes(Integer addcartTimes) 
    {
        this.addcartTimes = addcartTimes;
    }

    public Integer getAddcartTimes() 
    {
        return addcartTimes;
    }
    public void setRemindTimes(Integer remindTimes) 
    {
        this.remindTimes = remindTimes;
    }

    public Integer getRemindTimes() 
    {
        return remindTimes;
    }

    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("logintimes", getLogintimes())
            .append("isRemind", getIsRemind())
            .append("addcartTimes", getAddcartTimes())
            .append("remindTimes", getRemindTimes())
            .append("createtime", getCreatetime())
            .toString();
    }
}
