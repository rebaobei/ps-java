package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物日记对象 fa_userdiary
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public class FaUserdiary extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 心情 */
    @Excel(name = "心情")
    private Long mood;

    /** 互动 */
    @Excel(name = "互动")
    private Long interaction;

    /** 日记 */
    @Excel(name = "日记")
    private String note;

    /** 图片 */
    @Excel(name = "图片")
    private String images;

    /** 文件 */
    @Excel(name = "文件")
    private String files;

    /** 症状 */
    @Excel(name = "症状")
    private String symptom;

    /** 其他 */
    @Excel(name = "其他")
    private String othernote;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long cretatetime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 宠物心情 */
    @Excel(name = "宠物心情")
    private Long petmood;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setMood(Long mood) 
    {
        this.mood = mood;
    }

    public Long getMood() 
    {
        return mood;
    }
    public void setInteraction(Long interaction) 
    {
        this.interaction = interaction;
    }

    public Long getInteraction() 
    {
        return interaction;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setFiles(String files) 
    {
        this.files = files;
    }

    public String getFiles() 
    {
        return files;
    }
    public void setSymptom(String symptom) 
    {
        this.symptom = symptom;
    }

    public String getSymptom() 
    {
        return symptom;
    }
    public void setOthernote(String othernote) 
    {
        this.othernote = othernote;
    }

    public String getOthernote() 
    {
        return othernote;
    }
    public void setCretatetime(Long cretatetime) 
    {
        this.cretatetime = cretatetime;
    }

    public Long getCretatetime() 
    {
        return cretatetime;
    }
    public void setPetmood(Long petmood) 
    {
        this.petmood = petmood;
    }

    public Long getPetmood() 
    {
        return petmood;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("mood", getMood())
            .append("interaction", getInteraction())
            .append("note", getNote())
            .append("images", getImages())
            .append("files", getFiles())
            .append("symptom", getSymptom())
            .append("othernote", getOthernote())
            .append("cretatetime", getCretatetime())
            .append("updatetime", getUpdatetime())
            .append("petmood", getPetmood())
            .toString();
    }
}
