package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description 购买等级订单
 * @Author fan
 * @Date 2021/8/3
 **/
public class BuyLevelOrderVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 订单号 */
    private String ordercode;

    /** 会员id */
    private Long userId;

    /** 购买级别 */
    private Long buylevel;

    /** 支付金额 */
    private BigDecimal money;

    /** 支付状态:0=未支付,1=已支付 */
    private String isPay;

    /** 支付时间 */
    private Long paytime;

    /** 支付方式 */
    private String paycode;

    /** 创建时间 */
    private Long createtime;

    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }

    private String nickname;

    private String avatar;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBuylevel() {
        return buylevel;
    }

    public void setBuylevel(Long buylevel) {
        this.buylevel = buylevel;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

    public String getPaycode() {
        return paycode;
    }

    public void setPaycode(String paycode) {
        this.paycode = paycode;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
