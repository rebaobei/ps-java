package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员规则对象 fa_user_rule
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class FaUserRule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 父ID */
    @Excel(name = "父ID")
    private Long pid;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 是否菜单 */
    @Excel(name = "是否菜单")
    private Integer ismenu;

    /** 权重 */
    @Excel(name = "权重")
    private Long weigh;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setIsmenu(Integer ismenu) 
    {
        this.ismenu = ismenu;
    }

    public Integer getIsmenu() 
    {
        return ismenu;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    private Long createtime;

    private Long updatetime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pid", getPid())
            .append("name", getName())
            .append("title", getTitle())
            .append("remark", getRemark())
            .append("ismenu", getIsmenu())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("weigh", getWeigh())
            .append("status", getStatus())
            .toString();
    }
}
