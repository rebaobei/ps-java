package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

/**
 * @Description 游戏背包
 * @Author fan
 * @Date 2021/8/9
 **/
public class GameBagVo {

    private static final long serialVersionUID = 1L;

    /** id 主键，对应背包内的一项 */
    private Long id;

    /** 玩家id，也是背包id */
    private Long playerId;

    /** 物品id */
    private Long thingsId;

    /** 物品拥有数量 */
    private Long count;

    private String name;

    private Long categoryId;

    private String image;

    private String petType;

    private String result;

    private Long decCategory;

    private Long growthPoint;

    public Long getGrowthPoint() {
        return growthPoint;
    }

    public void setGrowthPoint(Long growthPoint) {
        this.growthPoint = growthPoint;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getThingsId() {
        return thingsId;
    }

    public void setThingsId(Long thingsId) {
        this.thingsId = thingsId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getDecCategory() {
        return decCategory;
    }

    public void setDecCategory(Long decCategory) {
        this.decCategory = decCategory;
    }
}
