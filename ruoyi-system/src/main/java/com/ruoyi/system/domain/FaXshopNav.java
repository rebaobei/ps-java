package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 首页设置对象 fa_xshop_nav
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public class FaXshopNav implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Integer id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 导航分类 */
    @Excel(name = "导航分类")
    private Integer navType;

    /** 跳转类型 */
    @Excel(name = "跳转类型")
    private Integer type;

    /** 跳转目标 */
    @Excel(name = "跳转目标")
    private String target;

    /** 参数 */
    @Excel(name = "参数")
    private String params;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setNavType(Integer navType) 
    {
        this.navType = navType;
    }

    public Integer getNavType() 
    {
        return navType;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setTarget(String target) 
    {
        this.target = target;
    }

    public String getTarget() 
    {
        return target;
    }
    public void setParams(String params) 
    {
        this.params = params;
    }

    public String getParams() 
    {
        return params;
    }
    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public Integer getSort() 
    {
        return sort;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setManystorePid(Long manystorePid) 
    {
        this.manystorePid = manystorePid;
    }

    public Long getManystorePid() 
    {
        return manystorePid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("description", getDescription())
            .append("navType", getNavType())
            .append("type", getType())
            .append("target", getTarget())
            .append("params", getParams())
            .append("sort", getSort())
            .append("status", getStatus())
            .append("image", getImage())
            .append("manystorePid", getManystorePid())
            .toString();
    }
}
