package com.ruoyi.system.domain.vo;

public class LevelChangeVo {
    private Integer level;
    private Integer id;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LevelChangeVo(Integer level, Integer id) {
        this.level = level;
        this.id = id;
    }

    public LevelChangeVo() {
    }
}
