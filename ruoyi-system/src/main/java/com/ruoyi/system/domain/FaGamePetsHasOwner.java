package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 已领养宠物对象 fa_game_pets_has_owner
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGamePetsHasOwner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 拥有宠物的玩家 */
    @Excel(name = "拥有宠物的玩家")
    private Long playerId;

    /** 宠物类型 */
    @Excel(name = "宠物类型")
    private Long petId;

    /** 性别0男1女 */
    @Excel(name = "性别0男1女")
    private Long gender;

    /** 宠物昵称 */
    @Excel(name = "宠物昵称")
    private String nickname;

    /** 宠物等级 */
    @Excel(name = "宠物等级")
    private Integer level;

    /** 宠物当前成长值 */
    @Excel(name = "宠物当前成长值")
    private Long growthPoint;

    /** 总成长值 */
    @Excel(name = "总成长值")
    private Long totalGrowthPoint;

    /** 装饰品 头部 */
    @Excel(name = "装饰品 头部")
    private Long dec1;

    /** 装饰品 衣服 */
    @Excel(name = "装饰品 衣服")
    private Long dec2;

    /** 创建时间 */
    // @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Long gmtCreate;

    /** 修改时间 */
    // @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Long gmtModified;

    /** 宠物主分类Id */
    @Excel(name = "宠物主分类Id")
    private Long petCategoryId;

    /** 宠物图片 */
    @Excel(name = "宠物图片")
    private String image;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPlayerId(Long playerId) 
    {
        this.playerId = playerId;
    }

    public Long getPlayerId() 
    {
        return playerId;
    }
    public void setPetId(Long petId) 
    {
        this.petId = petId;
    }

    public Long getPetId() 
    {
        return petId;
    }
    public void setGender(Long gender) 
    {
        this.gender = gender;
    }

    public Long getGender() 
    {
        return gender;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return nickname;
    }
    public void setLevel(Integer level) 
    {
        this.level = level;
    }

    public Integer getLevel() 
    {
        return level;
    }
    public void setGrowthPoint(Long growthPoint) 
    {
        this.growthPoint = growthPoint;
    }

    public Long getGrowthPoint() 
    {
        return growthPoint;
    }
    public void setTotalGrowthPoint(Long totalGrowthPoint) 
    {
        this.totalGrowthPoint = totalGrowthPoint;
    }

    public Long getTotalGrowthPoint() 
    {
        return totalGrowthPoint;
    }
    public void setDec1(Long dec1) 
    {
        this.dec1 = dec1;
    }

    public Long getDec1() 
    {
        return dec1;
    }
    public void setDec2(Long dec2) 
    {
        this.dec2 = dec2;
    }

    public Long getDec2() 
    {
        return dec2;
    }
    public void setGmtCreate(Long gmtCreate)
    {
        this.gmtCreate = gmtCreate;
    }

    public Long getGmtCreate()
    {
        return gmtCreate;
    }
    public void setGmtModified(Long gmtModified)
    {
        this.gmtModified = gmtModified;
    }

    public Long getGmtModified()
    {
        return gmtModified;
    }
    public void setPetCategoryId(Long petCategoryId) 
    {
        this.petCategoryId = petCategoryId;
    }

    public Long getPetCategoryId() 
    {
        return petCategoryId;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("playerId", getPlayerId())
            .append("petId", getPetId())
            .append("gender", getGender())
            .append("nickname", getNickname())
            .append("level", getLevel())
            .append("growthPoint", getGrowthPoint())
            .append("totalGrowthPoint", getTotalGrowthPoint())
            .append("dec1", getDec1())
            .append("dec2", getDec2())
            .append("gmtCreate", getGmtCreate())
            .append("gmtModified", getGmtModified())
            .append("petCategoryId", getPetCategoryId())
            .append("image", getImage())
            .toString();
    }
}
