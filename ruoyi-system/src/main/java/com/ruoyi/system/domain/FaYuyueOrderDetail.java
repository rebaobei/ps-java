package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 预约订单明细对象 fa_yuyue_order_detail
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public class FaYuyueOrderDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 品种 */
    @Excel(name = "品种")
    private String pinzhong;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long age;

    /** 体重 */
    @Excel(name = "体重")
    private BigDecimal weight;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 会员id */
    @Excel(name = "会员id")
    private Long userId;

    /** 性别 */
    @Excel(name = "性别")
    private Integer sex;

    /** 疫苗注射情况 */
    @Excel(name = "疫苗注射情况")
    private String yimiaotype;

    /** 驱虫情况 */
    @Excel(name = "驱虫情况")
    private String quchongtype;

    /** 健康状况 */
    @Excel(name = "健康状况")
    private String jiankangtype;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 视频id */
    @Excel(name = "视频id")
    private Long videoId;

    /** 状态:0=关闭视频,1=启用视频 */
    @Excel(name = "状态:0=关闭视频,1=启用视频")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPinzhong(String pinzhong) 
    {
        this.pinzhong = pinzhong;
    }

    public String getPinzhong() 
    {
        return pinzhong;
    }
    public void setAge(Long age) 
    {
        this.age = age;
    }

    public Long getAge() 
    {
        return age;
    }
    public void setWeight(BigDecimal weight) 
    {
        this.weight = weight;
    }

    public BigDecimal getWeight() 
    {
        return weight;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setSex(Integer sex) 
    {
        this.sex = sex;
    }

    public Integer getSex() 
    {
        return sex;
    }
    public void setYimiaotype(String yimiaotype) 
    {
        this.yimiaotype = yimiaotype;
    }

    public String getYimiaotype() 
    {
        return yimiaotype;
    }
    public void setQuchongtype(String quchongtype) 
    {
        this.quchongtype = quchongtype;
    }

    public String getQuchongtype() 
    {
        return quchongtype;
    }
    public void setJiankangtype(String jiankangtype) 
    {
        this.jiankangtype = jiankangtype;
    }

    public String getJiankangtype() 
    {
        return jiankangtype;
    }
    public void setBirthday(Date birthday) 
    {
        this.birthday = birthday;
    }

    public Date getBirthday() 
    {
        return birthday;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }



    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("pinzhong", getPinzhong())
            .append("age", getAge())
            .append("weight", getWeight())
            .append("orderId", getOrderId())
            .append("createtime", getCreatetime())
            .append("userId", getUserId())
            .append("sex", getSex())
            .append("yimiaotype", getYimiaotype())
            .append("quchongtype", getQuchongtype())
            .append("jiankangtype", getJiankangtype())
            .append("birthday", getBirthday())
            .append("note", getNote())
            .append("videoId", getVideoId())
            .append("status", getStatus())
            .toString();
    }
}
