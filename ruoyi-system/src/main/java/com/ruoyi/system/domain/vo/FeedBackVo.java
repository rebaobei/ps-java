package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;

/**
 * @Description 反馈信息
 * @Author fan
 * @Date 2021/8/3
 **/
public class FeedBackVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    private Long userId;

    /** 反馈内容 */
    private String note;


    /** 创建时间 */
    private Long createtime;

    private String username;
    private String nickname;
    private String mobile;
    private String avatar;

    private Long createQueryStart;
    private Long createQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
