package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 宠物分类对象 fa_game_pets_catogory
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGamePetsCatogory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 宠物主分类ID */
    private Long mainCategoryId;

    /** 主分类名称 */
    @Excel(name = "主分类名称")
    private String categoryName;

    public void setMainCategoryId(Long mainCategoryId) 
    {
        this.mainCategoryId = mainCategoryId;
    }

    public Long getMainCategoryId() 
    {
        return mainCategoryId;
    }
    public void setCategoryName(String categoryName) 
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName() 
    {
        return categoryName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("mainCategoryId", getMainCategoryId())
            .append("categoryName", getCategoryName())
            .toString();
    }
}
