package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.util.List;

/**
 * @Description 宠物日记
 * @Author fan
 * @Date 2021/7/31
 **/
public class UserDiaryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 会员id */
    private Long userId;

    /** 心情 */
    private Long mood;

    /** 互动 */
    private Long interaction;

    /** 日记 */
    private String note;

    /** 图片 */
    private String images;

    /** 文件 */
    private String files;

    /** 症状 */
    private String symptom;

    /** 其他 */
    private String othernote;

    /** 创建时间 */
    private Long cretatetime;

    /** 更新时间 */
    private Long updatetime;

    /** 宠物心情 */
    private Long petmood;

    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }

    private String nickname;

    private List<String> imageList;

    private String avatar;

    private String mobile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMood() {
        return mood;
    }

    public void setMood(Long mood) {
        this.mood = mood;
    }

    public Long getInteraction() {
        return interaction;
    }

    public void setInteraction(Long interaction) {
        this.interaction = interaction;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getOthernote() {
        return othernote;
    }

    public void setOthernote(String othernote) {
        this.othernote = othernote;
    }

    public Long getCretatetime() {
        return cretatetime;
    }

    public void setCretatetime(Long cretatetime) {
        this.cretatetime = cretatetime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public Long getPetmood() {
        return petmood;
    }

    public void setPetmood(Long petmood) {
        this.petmood = petmood;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
