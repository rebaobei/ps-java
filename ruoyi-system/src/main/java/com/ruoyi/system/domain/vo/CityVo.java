package com.ruoyi.system.domain.vo;


import java.util.List;

public class CityVo{
    private Long id;
    private String name;

    private List<CityVo> cityVos;

    public CityVo() {
    }

    public CityVo(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CityVo(Long id, String name, List<CityVo> cityVos) {
        this.id = id;
        this.name = name;
        this.cityVos = cityVos;
    }

    public List<CityVo> getCityVos() {
        return cityVos;
    }

    public void setCityVos(List<CityVo> cityVos) {
        this.cityVos = cityVos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}