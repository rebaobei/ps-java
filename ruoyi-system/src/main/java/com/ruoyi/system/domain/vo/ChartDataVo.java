package com.ruoyi.system.domain.vo;

/**
 * 首页图表数据实体类
 */
public class ChartDataVo {

    private Long totalMember;
    private Long todayRegister;
    private Long monthRegister;
    private Long totalOrderPrice;
    private Long todayOrderPrice;
    private Long totalBookingOder;
    private Long todayBookingOrder;


    public ChartDataVo() {
    }

    public ChartDataVo(Long totalMember, Long todayRegister, Long monthRegister, Long totalOrderPrice, Long todayOrderPrice, Long totalBookingOder, Long todayBookingOrder) {
        this.totalMember = totalMember;
        this.todayRegister = todayRegister;
        this.monthRegister = monthRegister;
        this.totalOrderPrice = totalOrderPrice;
        this.todayOrderPrice = todayOrderPrice;
        this.totalBookingOder = totalBookingOder;
        this.todayBookingOrder = todayBookingOrder;
    }

    public Long getTotalMember() {
        return totalMember;
    }

    public void setTotalMember(Long totalMember) {
        this.totalMember = totalMember;
    }

    public Long getTodayRegister() {
        return todayRegister;
    }

    public void setTodayRegister(Long todayRegister) {
        this.todayRegister = todayRegister;
    }

    public Long getMonthRegister() {
        return monthRegister;
    }

    public void setMonthRegister(Long monthRegister) {
        this.monthRegister = monthRegister;
    }

    public Long getTotalOrderPrice() {
        return totalOrderPrice;
    }

    public void setTotalOrderPrice(Long totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
    }

    public Long getTodayOrderPrice() {
        return todayOrderPrice;
    }

    public void setTodayOrderPrice(Long todayOrderPrice) {
        this.todayOrderPrice = todayOrderPrice;
    }

    public Long getTotalBookingOder() {
        return totalBookingOder;
    }

    public void setTotalBookingOder(Long totalBookingOder) {
        this.totalBookingOder = totalBookingOder;
    }

    public Long getTodayBookingOrder() {
        return todayBookingOrder;
    }

    public void setTodayBookingOrder(Long todayBookingOrder) {
        this.todayBookingOrder = todayBookingOrder;
    }
}
