package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 签到设置积分对象 fa_user_sign
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class FaUserSign extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 天数 */
    @Excel(name = "天数")
    private Long dayData;

    /** 签到获取积分 */
    @Excel(name = "签到获取积分")
    private Long getIntegral;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDayData(Long dayData) 
    {
        this.dayData = dayData;
    }

    public Long getDayData() 
    {
        return dayData;
    }
    public void setGetIntegral(Long getIntegral) 
    {
        this.getIntegral = getIntegral;
    }

    public Long getGetIntegral() 
    {
        return getIntegral;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dayData", getDayData())
            .append("getIntegral", getGetIntegral())
            .toString();
    }
}
