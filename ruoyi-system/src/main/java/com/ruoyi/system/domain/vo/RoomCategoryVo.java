package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.util.List;

/**
 * @Description
 * @Author fan
 * @Date 2021/7/27
 **/
public class RoomCategoryVo implements Serializable{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 分类 */
    private String name;

    /** 图片 */
    private List<String> images;

    /** 状态:0=隐藏,1=启用 */
    private String status;

    /** 排序 */
    private Long weigh;

    /** 创建时间 */
    private Long createtime;

    /** 更新时间 */
    private Long updatetime;


        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Long getWeigh() {
            return weigh;
        }

        public void setWeigh(Long weigh) {
            this.weigh = weigh;
        }

        public Long getCreatetime() {
            return createtime;
        }

        public void setCreatetime(Long createtime) {
            this.createtime = createtime;
        }

        public Long getUpdatetime() {
            return updatetime;
        }

        public void setUpdatetime(Long updatetime) {
            this.updatetime = updatetime;
        }
}
