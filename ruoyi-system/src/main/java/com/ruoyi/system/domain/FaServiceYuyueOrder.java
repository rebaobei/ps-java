package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 服务预约订单对象 fa_yuyue_order
 *
 * @author ruoyi
 * @date 2021-08-06
 */
public class FaServiceYuyueOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单类型:0=SPA订单,1=房间订单,2=接送订单 */
    @Excel(name = "订单类型:0=SPA订单,1=房间订单,2=接送订单")
    private String ordertype;

    /** 订单号 */
    @Excel(name = "订单号")
    private String ordercode;

    /** 预约电话 */
    @Excel(name = "预约电话")
    private String tel;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 技师id */
    @Excel(name = "技师id")
    private Long techId;

    /** 分类id */
    @Excel(name = "分类id")
    private Long fenleiId;

    /** 房间id */
    @Excel(name = "房间id")
    private Long roomId;

    /** 项目id */
    @Excel(name = "项目id")
    private Long proId;
    /**
     * 项目名称
     */
    private String proName;
    /**
     * 店铺id
     */
    private Long storeId;
    /**
     * 店铺名称
     */
    private String storeName;
    /** 预约时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预约时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date yuyueTime;

    /** 总价格 */
    @Excel(name = "总价格")
    private BigDecimal price;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal discountPrice;

    /** 数量 */
    @Excel(name = "数量")
    private Long quty;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal danprice;

    /** 支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款 */
    @Excel(name = "支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款")
    private String isPay;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 状态:0=待支付,1=待核销,2=已核销,-1=作废 */
    @Excel(name = "状态:0=待支付,1=待核销,2=已核销,-1=作废")
    private String status;

    /** 体重id */
    @Excel(name = "体重id")
    private Long sizeId;

    /** 支付时间 */
    @Excel(name = "支付时间")
    private Long paytime;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String paycode;

    /** 扫码人id */
    @Excel(name = "扫码人id")
    private Long saomauserId;

    /** 核销时间 */
    @Excel(name = "核销时间")
    private Long checkdatetime;

    /** 姓名 */
    @Excel(name = "姓名")
    private String username;

    /** 性别0男1女 */
    @Excel(name = "性别0男1女")
    private Long sex;

    /** 寄养天数 */
    @Excel(name = "寄养天数")
    private Long jiyangday;

    /** 宠主生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "宠主生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date userbir;

    /** 会员尾款 */
    @Excel(name = "会员尾款")
    private BigDecimal weikuanprice;

    /** 尾款编码 */
    @Excel(name = "尾款编码")
    private String weikuancode;

    /** 尾款支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款 */
    @Excel(name = "尾款支付状态:0=未支付,1=已支付,-1=已退款,-2=申请退款")
    private String weikuanisPay;

    /** 申请退款时间 */
    @Excel(name = "申请退款时间")
    private Long returntime;

    /** 实际退款时间 */
    @Excel(name = "实际退款时间")
    private Long checkreturntime;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private BigDecimal returnmoney;

    /** 退款单号 */
    @Excel(name = "退款单号")
    private String returncode;

    /** 寄宿预约总金额 */
    @Excel(name = "寄宿预约总金额")
    private BigDecimal yuyuesummoney;

    /** 评论次数 */
    @Excel(name = "评论次数")
    private Long commenttimes;

    /** 是否开发票：0未开1已开 */
    @Excel(name = "是否开发票：0未开1已开")
    private Integer invoiceType;

    /** 退款原因 */
    @Excel(name = "退款原因")
    private String returnnote;

    /** 是否专车接送：0否1是 */
    @Excel(name = "是否专车接送：0否1是")
    private Integer jsType;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    private Integer videoId;
    private Integer videoStatus;


    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public Integer getVideoStatus() {
        return videoStatus;
    }

    public void setVideoStatus(Integer videoStatus) {
        this.videoStatus = videoStatus;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrdertype(String ordertype)
    {
        this.ordertype = ordertype;
    }

    public String getOrdertype()
    {
        return ordertype;
    }
    public void setOrdercode(String ordercode)
    {
        this.ordercode = ordercode;
    }

    public String getOrdercode()
    {
        return ordercode;
    }
    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public String getTel()
    {
        return tel;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setTechId(Long techId)
    {
        this.techId = techId;
    }

    public Long getTechId()
    {
        return techId;
    }
    public void setFenleiId(Long fenleiId)
    {
        this.fenleiId = fenleiId;
    }

    public Long getFenleiId()
    {
        return fenleiId;
    }
    public void setRoomId(Long roomId)
    {
        this.roomId = roomId;
    }

    public Long getRoomId()
    {
        return roomId;
    }
    public void setProId(Long proId)
    {
        this.proId = proId;
    }

    public Long getProId()
    {
        return proId;
    }
    public void setYuyueTime(Date yuyueTime)
    {
        this.yuyueTime = yuyueTime;
    }

    public Date getYuyueTime()
    {
        return yuyueTime;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setDiscountPrice(BigDecimal discountPrice)
    {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getDiscountPrice()
    {
        return discountPrice;
    }
    public void setQuty(Long quty)
    {
        this.quty = quty;
    }

    public Long getQuty()
    {
        return quty;
    }
    public void setDanprice(BigDecimal danprice)
    {
        this.danprice = danprice;
    }

    public BigDecimal getDanprice()
    {
        return danprice;
    }
    public void setIsPay(String isPay)
    {
        this.isPay = isPay;
    }

    public String getIsPay()
    {
        return isPay;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote()
    {
        return note;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setSizeId(Long sizeId)
    {
        this.sizeId = sizeId;
    }

    public Long getSizeId()
    {
        return sizeId;
    }
    public void setPaytime(Long paytime)
    {
        this.paytime = paytime;
    }

    public Long getPaytime()
    {
        return paytime;
    }
    public void setPaycode(String paycode)
    {
        this.paycode = paycode;
    }

    public String getPaycode()
    {
        return paycode;
    }
    public void setSaomauserId(Long saomauserId)
    {
        this.saomauserId = saomauserId;
    }

    public Long getSaomauserId()
    {
        return saomauserId;
    }
    public void setCheckdatetime(Long checkdatetime)
    {
        this.checkdatetime = checkdatetime;
    }

    public Long getCheckdatetime()
    {
        return checkdatetime;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }
    public void setSex(Long sex)
    {
        this.sex = sex;
    }

    public Long getSex()
    {
        return sex;
    }
    public void setJiyangday(Long jiyangday)
    {
        this.jiyangday = jiyangday;
    }

    public Long getJiyangday()
    {
        return jiyangday;
    }
    public void setUserbir(Date userbir)
    {
        this.userbir = userbir;
    }

    public Date getUserbir()
    {
        return userbir;
    }
    public void setWeikuanprice(BigDecimal weikuanprice)
    {
        this.weikuanprice = weikuanprice;
    }

    public BigDecimal getWeikuanprice()
    {
        return weikuanprice;
    }
    public void setWeikuancode(String weikuancode)
    {
        this.weikuancode = weikuancode;
    }

    public String getWeikuancode()
    {
        return weikuancode;
    }
    public void setWeikuanisPay(String weikuanisPay)
    {
        this.weikuanisPay = weikuanisPay;
    }

    public String getWeikuanisPay()
    {
        return weikuanisPay;
    }
    public void setReturntime(Long returntime)
    {
        this.returntime = returntime;
    }

    public Long getReturntime()
    {
        return returntime;
    }
    public void setCheckreturntime(Long checkreturntime)
    {
        this.checkreturntime = checkreturntime;
    }

    public Long getCheckreturntime()
    {
        return checkreturntime;
    }
    public void setReturnmoney(BigDecimal returnmoney)
    {
        this.returnmoney = returnmoney;
    }

    public BigDecimal getReturnmoney()
    {
        return returnmoney;
    }
    public void setReturncode(String returncode)
    {
        this.returncode = returncode;
    }

    public String getReturncode()
    {
        return returncode;
    }
    public void setYuyuesummoney(BigDecimal yuyuesummoney)
    {
        this.yuyuesummoney = yuyuesummoney;
    }

    public BigDecimal getYuyuesummoney()
    {
        return yuyuesummoney;
    }
    public void setCommenttimes(Long commenttimes)
    {
        this.commenttimes = commenttimes;
    }

    public Long getCommenttimes()
    {
        return commenttimes;
    }
    public void setInvoiceType(Integer invoiceType)
    {
        this.invoiceType = invoiceType;
    }

    public Integer getInvoiceType()
    {
        return invoiceType;
    }
    public void setReturnnote(String returnnote)
    {
        this.returnnote = returnnote;
    }

    public String getReturnnote()
    {
        return returnnote;
    }
    public void setJsType(Integer jsType)
    {
        this.jsType = jsType;
    }

    public Integer getJsType()
    {
        return jsType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ordertype", getOrdertype())
            .append("ordercode", getOrdercode())
            .append("tel", getTel())
            .append("userId", getUserId())
            .append("techId", getTechId())
            .append("fenleiId", getFenleiId())
            .append("roomId", getRoomId())
            .append("proId", getProId())
            .append("yuyueTime", getYuyueTime())
            .append("price", getPrice())
            .append("discountPrice", getDiscountPrice())
            .append("quty", getQuty())
            .append("danprice", getDanprice())
            .append("isPay", getIsPay())
            .append("note", getNote())
            .append("status", getStatus())
            .append("createtime", getCreatetime())
            .append("sizeId", getSizeId())
            .append("updatetime", getUpdatetime())
            .append("paytime", getPaytime())
            .append("paycode", getPaycode())
            .append("saomauserId", getSaomauserId())
            .append("checkdatetime", getCheckdatetime())
            .append("username", getUsername())
            .append("sex", getSex())
            .append("jiyangday", getJiyangday())
            .append("userbir", getUserbir())
            .append("weikuanprice", getWeikuanprice())
            .append("weikuancode", getWeikuancode())
            .append("weikuanisPay", getWeikuanisPay())
            .append("returntime", getReturntime())
            .append("checkreturntime", getCheckreturntime())
            .append("returnmoney", getReturnmoney())
            .append("returncode", getReturncode())
            .append("yuyuesummoney", getYuyuesummoney())
            .append("commenttimes", getCommenttimes())
            .append("invoiceType", getInvoiceType())
            .append("returnnote", getReturnnote())
            .append("jsType", getJsType())
            .toString();
    }
}
