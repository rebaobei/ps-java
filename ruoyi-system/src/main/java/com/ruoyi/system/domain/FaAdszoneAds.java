package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 广告项目对象 fa_adszone_ads
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public class FaAdszoneAds extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String imageurl;

    /** 链接地址 */
    @Excel(name = "链接地址")
    private String linkurl;

    /** URL打开方式 */
    @Excel(name = "URL打开方式")
    private String target;

    /** 广告代码 */
    @Excel(name = "广告代码")
    private String code;

    /** 到期时间 */
    @Excel(name = "到期时间")
    private Long expiretime;

    /** 权重 */
    @Excel(name = "权重")
    private Long weigh;

    /** 广告位ID */
    @Excel(name = "广告位ID")
    private Long zoneId;

    /** 生效时间 */
    @Excel(name = "生效时间")
    private Long effectime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getCreatetime() {
        return createtime;
    }


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setImageurl(String imageurl) 
    {
        this.imageurl = imageurl;
    }

    public String getImageurl() 
    {
        return imageurl;
    }
    public void setLinkurl(String linkurl) 
    {
        this.linkurl = linkurl;
    }

    public String getLinkurl() 
    {
        return linkurl;
    }
    public void setTarget(String target) 
    {
        this.target = target;
    }

    public String getTarget() 
    {
        return target;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setExpiretime(Long expiretime) 
    {
        this.expiretime = expiretime;
    }

    public Long getExpiretime() 
    {
        return expiretime;
    }
    public void setWeigh(Long weigh) 
    {
        this.weigh = weigh;
    }

    public Long getWeigh() 
    {
        return weigh;
    }
    public void setZoneId(Long zoneId) 
    {
        this.zoneId = zoneId;
    }

    public Long getZoneId() 
    {
        return zoneId;
    }
    public void setEffectime(Long effectime) 
    {
        this.effectime = effectime;
    }

    public Long getEffectime() 
    {
        return effectime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("imageurl", getImageurl())
            .append("linkurl", getLinkurl())
            .append("target", getTarget())
            .append("code", getCode())
            .append("expiretime", getExpiretime())
            .append("weigh", getWeigh())
            .append("createtime", getCreatetime())
            .append("updatetime", getUpdatetime())
            .append("zoneId", getZoneId())
            .append("effectime", getEffectime())
            .toString();
    }
}
