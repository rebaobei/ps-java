package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description 商城订单
 * @Author fan
 * @Date 2021/8/4
 **/
public class XshopOrderVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderSn;

    /** 用户 */
    @Excel(name = "用户")
    private Integer userId;

    /** 是否已支付 */
    @Excel(name = "是否已支付")
    private Integer isPay;

    /** 支付时间 */
    @Excel(name = "支付时间")
    private Integer payTime;

    /** 是否发货 */
    @Excel(name = "是否发货")
    private Integer isDelivery;

    /** 发货时间 */
    @Excel(name = "发货时间")
    private Integer delivery;

    /** $column.columnComment */
    @Excel(name = "发货时间")
    private Integer isReceived;

    /** $column.columnComment */
    @Excel(name = "发货时间")
    private Long receivedTime;

    /** 快递公司编号 */
    @Excel(name = "快递公司编号")
    private String expressCode;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String expressNo;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private Integer status;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactor;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactorPhone;

    /** 送货地址 */
    @Excel(name = "送货地址")
    private String address;

    /** 运费 */
    @Excel(name = "运费")
    private BigDecimal deliveryPrice;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private BigDecimal orderPrice;

    /** 应付金额 */
    @Excel(name = "应付金额")
    private BigDecimal payPrice;

    /** 支付平台 */
    @Excel(name = "支付平台")
    private String payType;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payMethod;

    /** 商品总额 */
    @Excel(name = "商品总额")
    private BigDecimal productsPrice;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal discountPrice;

    /** 已付金额 */
    @Excel(name = "已付金额")
    private BigDecimal payedPrice;

    /** 买家是否评价 */
    @Excel(name = "买家是否评价")
    private Integer buyerReview;

    /** 卖家是否评价 */
    @Excel(name = "卖家是否评价")
    private Integer salerReview;

    /** 买家备注 */
    @Excel(name = "买家备注")
    private String salerRemark;

    /** 删除时间 */
    @Excel(name = "删除时间")
    private Long deleteTime;

    /** 订单类型：0普通订单1小时送货 */
    @Excel(name = "订单类型：0普通订单1小时送货")
    private Integer orderType;

    /** 团购状态 */
    @Excel(name = "团购状态")
    private Integer grouponStatus;

    /** out_trade_no */
    @Excel(name = "out_trade_no")
    private String orderSnRe;

    /** 售后买家留言 */
    @Excel(name = "售后买家留言")
    private String afterBuyerRemark;

    /** 售后卖家留言 */
    @Excel(name = "售后卖家留言")
    private String afterSalerRemark;

    /** 售后状态 */
    @Excel(name = "售后状态")
    private Integer afterSaleStatus;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private BigDecimal refundFee;

    /** 归属店铺 */
    @Excel(name = "归属店铺")
    private Long manystorePid;

    /** 优惠券 */
    @Excel(name = "优惠券")
    private Long couponsId;

    /** 取货状态:0=普通快递,1=1小时配送 */
    @Excel(name = "取货状态:0=普通快递,1=1小时配送")
    private String sendType;

    /** 扫码人id */
    @Excel(name = "扫码人id")
    private Long saomauserId;

    /** 总积分 */
    @Excel(name = "总积分")
    private BigDecimal sumscore;

    /** 储值卡金额 */
    @Excel(name = "储值卡金额")
    private BigDecimal cardmoney;

    /** 余额 */
    @Excel(name = "余额")
    private BigDecimal balance;

    /** 会员等级 */
    @Excel(name = "会员等级")
    private Long userlevel;

    /** 是否开发票：0未开1已开 */
    @Excel(name = "是否开发票：0未开1已开")
    private Integer invoiceType;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createTime;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updateTime;

    /** 更新时间 */
    private String remark;

    private String username;

    private String nickname;


    private Long createQueryStart;
    private Long createQueryEnd;
    private Long updateQueryStart;
    private Long updateQueryEnd;

    public Long getCreateQueryStart() {
        return createQueryStart;
    }

    public void setCreateQueryStart(Long createQueryStart) {
        this.createQueryStart = createQueryStart;
    }

    public Long getCreateQueryEnd() {
        return createQueryEnd;
    }

    public void setCreateQueryEnd(Long createQueryEnd) {
        this.createQueryEnd = createQueryEnd;
    }

    public Long getUpdateQueryStart() {
        return updateQueryStart;
    }

    public void setUpdateQueryStart(Long updateQueryStart) {
        this.updateQueryStart = updateQueryStart;
    }

    public Long getUpdateQueryEnd() {
        return updateQueryEnd;
    }

    public void setUpdateQueryEnd(Long updateQueryEnd) {
        this.updateQueryEnd = updateQueryEnd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIsPay() {
        return isPay;
    }

    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }

    public Integer getPayTime() {
        return payTime;
    }

    public void setPayTime(Integer payTime) {
        this.payTime = payTime;
    }

    public Integer getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(Integer isDelivery) {
        this.isDelivery = isDelivery;
    }

    public Integer getDelivery() {
        return delivery;
    }

    public void setDelivery(Integer delivery) {
        this.delivery = delivery;
    }

    public Integer getIsReceived() {
        return isReceived;
    }

    public void setIsReceived(Integer isReceived) {
        this.isReceived = isReceived;
    }

    public Long getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(Long receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getExpressCode() {
        return expressCode;
    }

    public void setExpressCode(String expressCode) {
        this.expressCode = expressCode;
    }

    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContactor() {
        return contactor;
    }

    public void setContactor(String contactor) {
        this.contactor = contactor;
    }

    public String getContactorPhone() {
        return contactorPhone;
    }

    public void setContactorPhone(String contactorPhone) {
        this.contactorPhone = contactorPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(BigDecimal deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    public BigDecimal getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(BigDecimal payPrice) {
        this.payPrice = payPrice;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    public BigDecimal getProductsPrice() {
        return productsPrice;
    }

    public void setProductsPrice(BigDecimal productsPrice) {
        this.productsPrice = productsPrice;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getPayedPrice() {
        return payedPrice;
    }

    public void setPayedPrice(BigDecimal payedPrice) {
        this.payedPrice = payedPrice;
    }

    public Integer getBuyerReview() {
        return buyerReview;
    }

    public void setBuyerReview(Integer buyerReview) {
        this.buyerReview = buyerReview;
    }

    public Integer getSalerReview() {
        return salerReview;
    }

    public void setSalerReview(Integer salerReview) {
        this.salerReview = salerReview;
    }

    public String getSalerRemark() {
        return salerRemark;
    }

    public void setSalerRemark(String salerRemark) {
        this.salerRemark = salerRemark;
    }

    public Long getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Long deleteTime) {
        this.deleteTime = deleteTime;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getGrouponStatus() {
        return grouponStatus;
    }

    public void setGrouponStatus(Integer grouponStatus) {
        this.grouponStatus = grouponStatus;
    }

    public String getOrderSnRe() {
        return orderSnRe;
    }

    public void setOrderSnRe(String orderSnRe) {
        this.orderSnRe = orderSnRe;
    }

    public String getAfterBuyerRemark() {
        return afterBuyerRemark;
    }

    public void setAfterBuyerRemark(String afterBuyerRemark) {
        this.afterBuyerRemark = afterBuyerRemark;
    }

    public String getAfterSalerRemark() {
        return afterSalerRemark;
    }

    public void setAfterSalerRemark(String afterSalerRemark) {
        this.afterSalerRemark = afterSalerRemark;
    }

    public Integer getAfterSaleStatus() {
        return afterSaleStatus;
    }

    public void setAfterSaleStatus(Integer afterSaleStatus) {
        this.afterSaleStatus = afterSaleStatus;
    }

    public BigDecimal getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(BigDecimal refundFee) {
        this.refundFee = refundFee;
    }

    public Long getManystorePid() {
        return manystorePid;
    }

    public void setManystorePid(Long manystorePid) {
        this.manystorePid = manystorePid;
    }

    public Long getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(Long couponsId) {
        this.couponsId = couponsId;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public Long getSaomauserId() {
        return saomauserId;
    }

    public void setSaomauserId(Long saomauserId) {
        this.saomauserId = saomauserId;
    }

    public BigDecimal getSumscore() {
        return sumscore;
    }

    public void setSumscore(BigDecimal sumscore) {
        this.sumscore = sumscore;
    }

    public BigDecimal getCardmoney() {
        return cardmoney;
    }

    public void setCardmoney(BigDecimal cardmoney) {
        this.cardmoney = cardmoney;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Long getUserlevel() {
        return userlevel;
    }

    public void setUserlevel(Long userlevel) {
        this.userlevel = userlevel;
    }

    public Integer getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(Integer invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
