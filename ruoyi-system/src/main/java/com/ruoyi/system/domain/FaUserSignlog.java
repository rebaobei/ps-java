package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 签到明细对象 fa_user_signlog
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public class FaUserSignlog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户签到 */
    @Excel(name = "用户签到")
    private String userSign;

    /** 0未完成1完成 */
    @Excel(name = "0未完成1完成")
    private Long signOver;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private Long addTime;

    /** 完成时间 */
    @Excel(name = "完成时间")
    private Long overTime;

    /** 续签次数 */
    @Excel(name = "续签次数")
    private Long xuqianNum;

    /** 续签时间 */
    @Excel(name = "续签时间")
    private Long xuqianTime;

    @Excel(name = "更新时间")
    private Long updatetime;

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserSign(String userSign) 
    {
        this.userSign = userSign;
    }

    public String getUserSign() 
    {
        return userSign;
    }
    public void setSignOver(Long signOver) 
    {
        this.signOver = signOver;
    }

    public Long getSignOver() 
    {
        return signOver;
    }
    public void setAddTime(Long addTime) 
    {
        this.addTime = addTime;
    }

    public Long getAddTime() 
    {
        return addTime;
    }
    public void setOverTime(Long overTime) 
    {
        this.overTime = overTime;
    }

    public Long getOverTime() 
    {
        return overTime;
    }
    public void setXuqianNum(Long xuqianNum) 
    {
        this.xuqianNum = xuqianNum;
    }

    public Long getXuqianNum() 
    {
        return xuqianNum;
    }
    public void setXuqianTime(Long xuqianTime) 
    {
        this.xuqianTime = xuqianTime;
    }

    public Long getXuqianTime() 
    {
        return xuqianTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userSign", getUserSign())
            .append("signOver", getSignOver())
            .append("addTime", getAddTime())
            .append("overTime", getOverTime())
            .append("updatetime", getUpdatetime())
            .append("xuqianNum", getXuqianNum())
            .append("xuqianTime", getXuqianTime())
            .toString();
    }
}
