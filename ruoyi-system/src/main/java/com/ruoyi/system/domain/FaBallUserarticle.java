package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 朋友圈明细对象 fa_ball_userarticle
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public class FaBallUserarticle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 内容 */
    @Excel(name = "内容")
    private String note;

    /** 图片 */
    @Excel(name = "图片")
    private String images;

    /** 文件 */
    @Excel(name = "文件")
    private String files;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Long views;

    /** 点赞数量 */
    @Excel(name = "点赞数量")
    private Long zancount;

    /** 评论数量 */
    @Excel(name = "评论数量")
    private Long comments;

    /** 状态:0=隐藏,1=审核 */
    @Excel(name = "状态:0=隐藏,1=审核")
    private String status;

    /** 经度 */
    @Excel(name = "经度")
    private String lng;

    /** 维度 */
    @Excel(name = "维度")
    private String lat;

    /** 话题id */
    @Excel(name = "话题id")
    private String topicId;

    /** 比赛id */
    @Excel(name = "比赛id")
    private Long bisaiId;

    /** 是否置顶:0=否,1=是 */
    @Excel(name = "是否置顶:0=否,1=是")
    private String ifTop;

    /** 状态:0=私密,1=公开 */
    @Excel(name = "状态:0=私密,1=公开")
    private String userstatus;

    /** 类型:0=动态,1=帖子,2相册 */
    @Excel(name = "类型:0=动态,1=帖子,2相册")
    private String articleType;

    /** 分享数量 */
    @Excel(name = "分享数量")
    private Long shareCount;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private Long updatetime;
    /** 创建时间 */
    @Excel(name = "创建时间")
    private Long createtime;

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setFiles(String files) 
    {
        this.files = files;
    }

    public String getFiles() 
    {
        return files;
    }
    public void setViews(Long views) 
    {
        this.views = views;
    }

    public Long getViews() 
    {
        return views;
    }
    public void setZancount(Long zancount) 
    {
        this.zancount = zancount;
    }

    public Long getZancount() 
    {
        return zancount;
    }
    public void setComments(Long comments) 
    {
        this.comments = comments;
    }

    public Long getComments() 
    {
        return comments;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setLng(String lng) 
    {
        this.lng = lng;
    }

    public String getLng() 
    {
        return lng;
    }
    public void setLat(String lat) 
    {
        this.lat = lat;
    }

    public String getLat() 
    {
        return lat;
    }
    public void setTopicId(String topicId) 
    {
        this.topicId = topicId;
    }

    public String getTopicId() 
    {
        return topicId;
    }
    public void setBisaiId(Long bisaiId) 
    {
        this.bisaiId = bisaiId;
    }

    public Long getBisaiId() 
    {
        return bisaiId;
    }
    public void setIfTop(String ifTop) 
    {
        this.ifTop = ifTop;
    }

    public String getIfTop() 
    {
        return ifTop;
    }
    public void setUserstatus(String userstatus) 
    {
        this.userstatus = userstatus;
    }

    public String getUserstatus() 
    {
        return userstatus;
    }
    public void setArticleType(String articleType) 
    {
        this.articleType = articleType;
    }

    public String getArticleType() 
    {
        return articleType;
    }
    public void setShareCount(Long shareCount) 
    {
        this.shareCount = shareCount;
    }

    public Long getShareCount() 
    {
        return shareCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("note", getNote())
            .append("images", getImages())
            .append("files", getFiles())
            .append("createtime", getCreatetime())
            .append("views", getViews())
            .append("zancount", getZancount())
            .append("comments", getComments())
            .append("status", getStatus())
            .append("lng", getLng())
            .append("lat", getLat())
            .append("topicId", getTopicId())
            .append("bisaiId", getBisaiId())
            .append("ifTop", getIfTop())
            .append("updatetime", getUpdatetime())
            .append("userstatus", getUserstatus())
            .append("articleType", getArticleType())
            .append("shareCount", getShareCount())
            .toString();
    }
}
