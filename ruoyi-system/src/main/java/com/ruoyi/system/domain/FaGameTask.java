package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 每日任务对象 fa_game_task
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public class FaGameTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 任务ID */
    private Long dayTaskId;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String dayTaskName;

    /** 奖励积分, 与已有交叉 */
    @Excel(name = "奖励积分, 与已有交叉")
    private Long score;

    /** 奖励宠物币 */
    @Excel(name = "奖励宠物币")
    private Long coin;

    /** 需要执行次数 */
    @Excel(name = "需要执行次数")
    private Long requireTimes;

    /** 任务图片 */
    @Excel(name = "任务图片")
    private String taskImage;

    /** 任务地址 */
    @Excel(name = "任务地址")
    private String taskPath;

    /** 任务状态0禁用1启用 */
    @Excel(name = "任务状态")
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setDayTaskId(Long dayTaskId)
    {
        this.dayTaskId = dayTaskId;
    }

    public Long getDayTaskId() 
    {
        return dayTaskId;
    }
    public void setDayTaskName(String dayTaskName) 
    {
        this.dayTaskName = dayTaskName;
    }

    public String getDayTaskName() 
    {
        return dayTaskName;
    }
    public void setScore(Long score) 
    {
        this.score = score;
    }

    public Long getScore() 
    {
        return score;
    }
    public void setCoin(Long coin) 
    {
        this.coin = coin;
    }

    public Long getCoin() 
    {
        return coin;
    }
    public void setRequireTimes(Long requireTimes) 
    {
        this.requireTimes = requireTimes;
    }

    public Long getRequireTimes() 
    {
        return requireTimes;
    }
    public void setTaskImage(String taskImage) 
    {
        this.taskImage = taskImage;
    }

    public String getTaskImage() 
    {
        return taskImage;
    }
    public void setTaskPath(String taskPath) 
    {
        this.taskPath = taskPath;
    }

    public String getTaskPath() 
    {
        return taskPath;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dayTaskId", getDayTaskId())
            .append("dayTaskName", getDayTaskName())
            .append("score", getScore())
            .append("coin", getCoin())
            .append("requireTimes", getRequireTimes())
            .append("taskImage", getTaskImage())
            .append("taskPath", getTaskPath())
            .toString();
    }
}
