package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.OrderInvoiceVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaOrderInvoiceMapper;
import com.ruoyi.system.domain.FaOrderInvoice;
import com.ruoyi.system.service.IFaOrderInvoiceService;

/**
 * 订单发票Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaOrderInvoiceServiceImpl implements IFaOrderInvoiceService 
{
    @Autowired
    private FaOrderInvoiceMapper faOrderInvoiceMapper;

    /**
     * 查询订单发票
     * 
     * @param id 订单发票ID
     * @return 订单发票
     */
    @Override
    public FaOrderInvoice selectFaOrderInvoiceById(Long id)
    {
        return faOrderInvoiceMapper.selectFaOrderInvoiceById(id);
    }

    /**
     * 查询订单发票列表
     * 
     * @param faOrderInvoice 订单发票
     * @return 订单发票
     */
    @Override
    public List<FaOrderInvoice> selectFaOrderInvoiceList(FaOrderInvoice faOrderInvoice)
    {
        return faOrderInvoiceMapper.selectFaOrderInvoiceList(faOrderInvoice);
    }

    /**
     * 新增订单发票
     * 
     * @param faOrderInvoice 订单发票
     * @return 结果
     */
    @Override
    public int insertFaOrderInvoice(FaOrderInvoice faOrderInvoice)
    {
        faOrderInvoice.setCreatetime(MyDateUtils.nowTimestamp());
        return faOrderInvoiceMapper.insertFaOrderInvoice(faOrderInvoice);
    }

    /**
     * 修改订单发票
     * 
     * @param faOrderInvoice 订单发票
     * @return 结果
     */
    @Override
    public int updateFaOrderInvoice(FaOrderInvoice faOrderInvoice)
    {
        return faOrderInvoiceMapper.updateFaOrderInvoice(faOrderInvoice);
    }

    /**
     * 批量删除订单发票
     * 
     * @param ids 需要删除的订单发票ID
     * @return 结果
     */
    @Override
    public int deleteFaOrderInvoiceByIds(Long[] ids)
    {
        return faOrderInvoiceMapper.deleteFaOrderInvoiceByIds(ids);
    }

    /**
     * 删除订单发票信息
     * 
     * @param id 订单发票ID
     * @return 结果
     */
    @Override
    public int deleteFaOrderInvoiceById(Long id)
    {
        return faOrderInvoiceMapper.deleteFaOrderInvoiceById(id);
    }

    @Override
    public List<OrderInvoiceVo> listOrderInvoiceVo(OrderInvoiceVo faOrderInvoice) {
        return faOrderInvoiceMapper.listOrderInvoiceVo(faOrderInvoice);
    }
}
