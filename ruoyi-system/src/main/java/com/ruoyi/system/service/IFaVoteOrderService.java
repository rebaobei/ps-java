package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaVoteOrder;
import com.ruoyi.system.domain.vo.VoteOrderVo;

/**
 * 打赏订单Service接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface IFaVoteOrderService 
{
    /**
     * 查询打赏订单
     * 
     * @param id 打赏订单ID
     * @return 打赏订单
     */
    public FaVoteOrder selectFaVoteOrderById(Long id);

    /**
     * 查询打赏订单列表
     * 
     * @param faVoteOrder 打赏订单
     * @return 打赏订单集合
     */
    public List<FaVoteOrder> selectFaVoteOrderList(FaVoteOrder faVoteOrder);

    /**
     * 新增打赏订单
     * 
     * @param faVoteOrder 打赏订单
     * @return 结果
     */
    public int insertFaVoteOrder(FaVoteOrder faVoteOrder);

    /**
     * 修改打赏订单
     * 
     * @param faVoteOrder 打赏订单
     * @return 结果
     */
    public int updateFaVoteOrder(FaVoteOrder faVoteOrder);

    /**
     * 批量删除打赏订单
     * 
     * @param ids 需要删除的打赏订单ID
     * @return 结果
     */
    public int deleteFaVoteOrderByIds(Long[] ids);

    /**
     * 删除打赏订单信息
     * 
     * @param id 打赏订单ID
     * @return 结果
     */
    public int deleteFaVoteOrderById(Long id);

    List<VoteOrderVo> listVoteOrderVo(VoteOrderVo faVoteOrder);
}
