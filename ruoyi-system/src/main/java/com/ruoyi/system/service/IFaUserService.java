package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.FaUser;
import com.ruoyi.system.domain.vo.LevelChangeVo;
import com.ruoyi.system.domain.vo.MemberInfoChangeVo;
import com.ruoyi.system.domain.vo.MsgVo;

/**
 * 会员管理111Service接口
 *
 * @author ruoyi
 * @date 2021-07-05
 */
public interface IFaUserService
{
    /**
     * 查询会员管理111
     *
     * @param id 会员管理111ID
     * @return 会员管理111
     */
    public FaUser selectFaUserById(Integer id);

    public FaUser selectFaUserByMobile(String mobile);

    public AjaxResult getCaptchaByMobile(String mobile);

    /**
     * 查询会员管理111列表
     *
     * @param faUser 会员管理111
     * @return 会员管理111集合
     */
    public List<FaUser> selectFaUserList(FaUser faUser);

    /**
     * 新增会员管理111
     *
     * @param faUser 会员管理111
     * @return 结果
     */
    public int insertFaUser(FaUser faUser);

    /**
     * 修改会员管理111
     *
     * @param faUser 会员管理111
     * @return 结果
     */
    public int updateFaUser(FaUser faUser);

    public AjaxResult addStore(FaUser faUser);

    /**
     * 批量删除会员管理111
     *
     * @param ids 需要删除的会员管理111ID
     * @return 结果
     */
    public int deleteFaUserByIds(Integer[] ids);

    /**
     * 删除会员管理111信息
     *
     * @param id 会员管理111ID
     * @return 结果
     */
    public int deleteFaUserById(Integer id);


    int changScore(MemberInfoChangeVo memberInfoChangeVo);

    int changeMoney(MemberInfoChangeVo memberInfoChangeVo);

    int changeCardMoney(MemberInfoChangeVo memberInfoChangeVo);

    int freezeCardMoney(MemberInfoChangeVo memberInfoChangeVo);

    int changeLevel(LevelChangeVo levelChangeVo);

    int sendMsg(MsgVo msgVo);

    List<String> getPushCid();


}
