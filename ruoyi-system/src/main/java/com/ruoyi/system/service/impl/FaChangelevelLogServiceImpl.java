package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaChangelevelLogMapper;
import com.ruoyi.system.domain.FaChangelevelLog;
import com.ruoyi.system.service.IFaChangelevelLogService;

/**
 * 登记调整记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaChangelevelLogServiceImpl implements IFaChangelevelLogService 
{
    @Autowired
    private FaChangelevelLogMapper faChangelevelLogMapper;

    /**
     * 查询登记调整记录
     * 
     * @param id 登记调整记录ID
     * @return 登记调整记录
     */
    @Override
    public FaChangelevelLog selectFaChangelevelLogById(Long id)
    {
        return faChangelevelLogMapper.selectFaChangelevelLogById(id);
    }

    /**
     * 查询登记调整记录列表
     * 
     * @param faChangelevelLog 登记调整记录
     * @return 登记调整记录
     */
    @Override
    public List<FaChangelevelLog> selectFaChangelevelLogList(FaChangelevelLog faChangelevelLog)
    {
        return faChangelevelLogMapper.selectFaChangelevelLogList(faChangelevelLog);
    }

    /**
     * 新增登记调整记录
     * 
     * @param faChangelevelLog 登记调整记录
     * @return 结果
     */
    @Override
    public int insertFaChangelevelLog(FaChangelevelLog faChangelevelLog)
    {
        return faChangelevelLogMapper.insertFaChangelevelLog(faChangelevelLog);
    }

    /**
     * 修改登记调整记录
     * 
     * @param faChangelevelLog 登记调整记录
     * @return 结果
     */
    @Override
    public int updateFaChangelevelLog(FaChangelevelLog faChangelevelLog)
    {
        return faChangelevelLogMapper.updateFaChangelevelLog(faChangelevelLog);
    }

    /**
     * 批量删除登记调整记录
     * 
     * @param ids 需要删除的登记调整记录ID
     * @return 结果
     */
    @Override
    public int deleteFaChangelevelLogByIds(Long[] ids)
    {
        return faChangelevelLogMapper.deleteFaChangelevelLogByIds(ids);
    }

    /**
     * 删除登记调整记录信息
     * 
     * @param id 登记调整记录ID
     * @return 结果
     */
    @Override
    public int deleteFaChangelevelLogById(Long id)
    {
        return faChangelevelLogMapper.deleteFaChangelevelLogById(id);
    }
}
