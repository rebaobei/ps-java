package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaBallUserarticleComment;
import com.ruoyi.system.domain.vo.UserArticleCommentVo;

/**
 * 朋友圈评论Service接口
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public interface IFaBallUserarticleCommentService 
{
    /**
     * 查询朋友圈评论
     * 
     * @param id 朋友圈评论ID
     * @return 朋友圈评论
     */
    public FaBallUserarticleComment selectFaBallUserarticleCommentById(Long id);

    /**
     * 查询朋友圈评论列表
     * 
     * @param faBallUserarticleComment 朋友圈评论
     * @return 朋友圈评论集合
     */
    public List<FaBallUserarticleComment> selectFaBallUserarticleCommentList(FaBallUserarticleComment faBallUserarticleComment);

    /**
     * 新增朋友圈评论
     * 
     * @param faBallUserarticleComment 朋友圈评论
     * @return 结果
     */
    public int insertFaBallUserarticleComment(FaBallUserarticleComment faBallUserarticleComment);

    /**
     * 修改朋友圈评论
     * 
     * @param faBallUserarticleComment 朋友圈评论
     * @return 结果
     */
    public int updateFaBallUserarticleComment(FaBallUserarticleComment faBallUserarticleComment);

    /**
     * 批量删除朋友圈评论
     * 
     * @param ids 需要删除的朋友圈评论ID
     * @return 结果
     */
    public int deleteFaBallUserarticleCommentByIds(Long[] ids);

    /**
     * 删除朋友圈评论信息
     * 
     * @param id 朋友圈评论ID
     * @return 结果
     */
    public int deleteFaBallUserarticleCommentById(Long id);

    List<UserArticleCommentVo> listUserArticleCommentVo(FaBallUserarticleComment faBallUserarticleComment);
}
