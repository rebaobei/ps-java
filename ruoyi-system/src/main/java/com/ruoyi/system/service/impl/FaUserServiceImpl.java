package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.constant.MessageConstant;
import com.ruoyi.system.domain.vo.LevelChangeVo;
import com.ruoyi.system.domain.vo.MemberInfoChangeVo;
import com.ruoyi.system.domain.vo.MsgVo;
import com.ruoyi.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import com.ruoyi.system.service.IFaUserService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * 会员管理111Service业务层处理
 *
 * @author ruoyi
 * @date 2021-07-05
 */
@Service
public class FaUserServiceImpl implements IFaUserService {
    @Autowired
    private FaUserMapper faUserMapper;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private FaUserScoreLogMapper userScoreLogMapper;

    @Autowired
    private FaUserMoneyLogMapper moneyLogMapper;

    @Autowired
    private FaUserCardLogMapper cardLogMapper;

    @Autowired
    private FaChangelevelLogMapper changelevelLogMapper;

    @Autowired
    private FaXshopmsgMessagesMapper messagesMapper;
    @Value("${custom.sendLink}")
    private String SEND_LINK;
    /**
     * 查询会员管理111
     *
     * @param id 会员管理111ID
     * @return 会员管理111
     */
    @Override
    public FaUser selectFaUserById(Integer id) {
        return faUserMapper.selectFaUserById(id);
    }
   @Override
    public FaUser selectFaUserByMobile(String mobile) {

        return faUserMapper.selectFaUserByMobile(mobile);
    }

    @Override
    public AjaxResult getCaptchaByMobile(String mobile) {
        FaUser faUser = faUserMapper.selectFaUserByMobile(mobile);
        if(StringUtils.isNull(faUser)){
            return AjaxResult.error("手机号不存在【账号不存在】,请在app端绑定手机号");
        }else {
            RestTemplate restTemplate=new RestTemplate();
            Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
            String uri=this.SEND_LINK+"?userId="+faUser.getId()+"&storeInfoId="+storeInfoId;
            HttpHeaders headers = new HttpHeaders();
            HttpEntity<String> entity = new HttpEntity<>(headers);
            String strbody=restTemplate.exchange(uri, HttpMethod.GET, entity,String.class).getBody();
            JSONObject jsonObject = JSONObject.parseObject(strbody);
            Object msg = jsonObject.get("msg");
            return AjaxResult.success(msg);
        }
    }

    /**
     * 查询会员管理111列表
     *
     * @param faUser 会员管理111
     * @return 会员管理111
     */
    @Override
    public List<FaUser> selectFaUserList(FaUser faUser) {
        return faUserMapper.selectFaUserList(faUser);
    }

    /**
     * 新增会员管理111
     *
     * @param faUser 会员管理111
     * @return 结果
     */
    @Override
    public int insertFaUser(FaUser faUser) {
        return faUserMapper.insertFaUser(faUser);
    }

    /**
     * 修改会员管理111
     *
     * @param faUser 会员管理111
     * @return 结果
     */
    @Override
    public int updateFaUser(FaUser faUser) {
        return faUserMapper.updateFaUser(faUser);
    }
    @Override
    public AjaxResult addStore(FaUser faUser) {
        String mobile = faUser.getMobile();
        if(StringUtils.isEmpty(mobile)){
            return AjaxResult.error("请输入手机号");
        }
        FaUser user = faUserMapper.selectFaUserByMobile(mobile);
        if(StringUtils.isNull(user)){
            return AjaxResult.error("手机号不存在【账号不存在】,请在app端绑定手机号");
        }
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        String cacheObject = redisCache.getCacheObject("short_message" + user.getId()+"store_id"+storeInfoId);

        String captcha = faUser.getCaptcha();
        if(!captcha.equals(cacheObject)){
            return AjaxResult.error("验证码错误");
        }
        if(user.getStoreId()!=null){
            return AjaxResult.error("已绑定");
        }

        user.setUserType(1);
        user.setStoreId(storeInfoId);
        faUserMapper.updateFaUser(user);
        return AjaxResult.success();
    }

    /**
     * 批量删除会员管理111
     *
     * @param ids 需要删除的会员管理111ID
     * @return 结果
     */
    @Override
    public int deleteFaUserByIds(Integer[] ids) {
        return faUserMapper.deleteFaUserByIds(ids);
    }

    /**
     * 删除会员管理111信息
     *
     * @param id 会员管理111ID
     * @return 结果
     */
    @Override
    public int deleteFaUserById(Integer id) {
        return faUserMapper.deleteFaUserById(id);
    }

    @Override
    @Transactional
    public int changScore(MemberInfoChangeVo memberInfoChangeVo) {
        FaUser faUser = faUserMapper.selectFaUserById(memberInfoChangeVo.getId());
        faUser.setScore(faUser.getScore() + memberInfoChangeVo.getCount().intValue());
        faUser.setSumscore(faUser.getSumscore() + memberInfoChangeVo.getCount().intValue());
        faUserMapper.updateFaUser(faUser);
        // todo 需要根据业务逻辑调整
        FaUserScoreLog faUserScoreLog = new FaUserScoreLog();
        faUserScoreLog.setUserId(memberInfoChangeVo.getId());
        faUserScoreLog.setScore(Long.parseLong(memberInfoChangeVo.getCount().toString()));
        faUserScoreLog.setBefore(0L);
        faUserScoreLog.setAfter(0L);
        faUserScoreLog.setCreatetime(System.currentTimeMillis() / 1000);
        faUserScoreLog.setMemo(memberInfoChangeVo.getCause());
        faUserScoreLog.setManystorePid(0L);
        faUserScoreLog.setBillId(0L);
        faUserScoreLog.setBillType(0L);
        return userScoreLogMapper.insertFaUserScoreLog(faUserScoreLog);
    }

    /**
     * 修改余额
     */
    @Override
    @Transactional
    public int changeMoney(MemberInfoChangeVo memberInfoChangeVo) {
        FaUser faUser = faUserMapper.selectFaUserById(memberInfoChangeVo.getId());
        faUser.setMoney(faUser.getMoney().add(memberInfoChangeVo.getCount()));
        faUserMapper.updateFaUser(faUser);
        FaUserMoneyLog faUserMoneyLog = new FaUserMoneyLog();
        faUserMoneyLog.setUserId(memberInfoChangeVo.getId());
        faUserMoneyLog.setMoney(memberInfoChangeVo.getCount());
        faUserMoneyLog.setBefore(BigDecimal.valueOf(0L));
        faUserMoneyLog.setAfter(BigDecimal.valueOf(0L));
        faUserMoneyLog.setMemo(memberInfoChangeVo.getCause());
        faUserMoneyLog.setCreatetime(System.currentTimeMillis() / 1000);
        faUserMoneyLog.setBillType(1L);
        faUserMoneyLog.setBillId(1L);
        faUserMoneyLog.setManystorePid(0L);
        return moneyLogMapper.insertFaUserMoneyLog(faUserMoneyLog);
    }

    @Override
    @Transactional
    public int changeCardMoney(MemberInfoChangeVo memberInfoChangeVo) {
        FaUser faUser = faUserMapper.selectFaUserById(memberInfoChangeVo.getId());
        faUser.setCardmoney(faUser.getCardmoney().add(memberInfoChangeVo.getCount()));
        faUserMapper.updateFaUser(faUser);
        FaUserCardLog cardLog = new FaUserCardLog();
        cardLog.setUserId(memberInfoChangeVo.getId());
        cardLog.setMoney(memberInfoChangeVo.getCount());
        cardLog.setAfter(BigDecimal.valueOf(0L));
        cardLog.setBefore(BigDecimal.valueOf(0L));
        cardLog.setCreatetime(System.currentTimeMillis() / 1000);
        cardLog.setBillType(2L);
        // todo 订单类型
        cardLog.setBillId(0L);
        cardLog.setManystorePid(0L);
        return cardLogMapper.insertFaUserCardLog(cardLog);
    }

    @Override
    @Transactional
    public int freezeCardMoney(MemberInfoChangeVo memberInfoChangeVo) {
        FaUser faUser = faUserMapper.selectFaUserById(memberInfoChangeVo.getId());
        faUser.setCardmoney(faUser.getCardmoney().subtract(memberInfoChangeVo.getCount()));
        faUser.setFreezeCardmoney(memberInfoChangeVo.getCount());
        faUserMapper.updateFaUser(faUser);
        FaUserCardLog cardLog = new FaUserCardLog();
        cardLog.setUserId(memberInfoChangeVo.getId());
        cardLog.setMoney(memberInfoChangeVo.getCount());
        cardLog.setAfter(BigDecimal.valueOf(0L));
        cardLog.setBefore(BigDecimal.valueOf(0L));
        cardLog.setCreatetime(System.currentTimeMillis() / 1000);
        cardLog.setBillType(2L);
        // todo 订单类型
        cardLog.setBillId(0L);
        cardLog.setManystorePid(0L);
        return cardLogMapper.insertFaUserCardLog(cardLog);
    }

    @Override
    @Transactional
    public int changeLevel(LevelChangeVo levelChangeVo) {
        FaUser faUser = faUserMapper.selectFaUserById(levelChangeVo.getId());
        Integer oldLevel = faUser.getLevel();
        faUser.setLevel(levelChangeVo.getLevel());
        faUserMapper.updateFaUser(faUser);
        FaChangelevelLog changelevelLog = new FaChangelevelLog();
        changelevelLog.setUserId(levelChangeVo.getId().longValue());
        changelevelLog.setOldlevel(oldLevel.longValue());
        changelevelLog.setNewlevel(levelChangeVo.getLevel().longValue());
        changelevelLog.setCreatetime(System.currentTimeMillis() / 1000);
        return changelevelLogMapper.insertFaChangelevelLog(changelevelLog);
    }

    // todo 发送消息
    @Override
    public int sendMsg(MsgVo msgVo) {
        FaXshopmsgMessages faXshopmsgMessages = new FaXshopmsgMessages();
        faXshopmsgMessages.setUserId(msgVo.getId().longValue());
        faXshopmsgMessages.setTitle(msgVo.getTitle());
        faXshopmsgMessages.setDescription(msgVo.getContent());
        faXshopmsgMessages.setMsgType(MessageConstant.SYSTEM_MSG_TYPE);
        faXshopmsgMessages.setIsRead(MessageConstant.UNREAD);
        faXshopmsgMessages.setCreatetime(System.currentTimeMillis() / 1000);
        return messagesMapper.insertFaXshopmsgMessages(faXshopmsgMessages);
    }

    @Override
    public List<String> getPushCid() {
        List<String> pushCid = faUserMapper.getPushCid();
        return pushCid.stream().filter(s -> s.length() > 8).distinct().collect(Collectors.toList());
    }
}
