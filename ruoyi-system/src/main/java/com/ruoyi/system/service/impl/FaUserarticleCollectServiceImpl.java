package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserarticleCollectMapper;
import com.ruoyi.system.domain.FaUserarticleCollect;
import com.ruoyi.system.service.IFaUserarticleCollectService;

/**
 * 帖子收藏记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUserarticleCollectServiceImpl implements IFaUserarticleCollectService 
{
    @Autowired
    private FaUserarticleCollectMapper faUserarticleCollectMapper;

    /**
     * 查询帖子收藏记录
     * 
     * @param id 帖子收藏记录ID
     * @return 帖子收藏记录
     */
    @Override
    public FaUserarticleCollect selectFaUserarticleCollectById(Long id)
    {
        return faUserarticleCollectMapper.selectFaUserarticleCollectById(id);
    }

    /**
     * 查询帖子收藏记录列表
     * 
     * @param faUserarticleCollect 帖子收藏记录
     * @return 帖子收藏记录
     */
    @Override
    public List<FaUserarticleCollect> selectFaUserarticleCollectList(FaUserarticleCollect faUserarticleCollect)
    {
        return faUserarticleCollectMapper.selectFaUserarticleCollectList(faUserarticleCollect);
    }

    /**
     * 新增帖子收藏记录
     * 
     * @param faUserarticleCollect 帖子收藏记录
     * @return 结果
     */
    @Override
    public int insertFaUserarticleCollect(FaUserarticleCollect faUserarticleCollect)
    {
        return faUserarticleCollectMapper.insertFaUserarticleCollect(faUserarticleCollect);
    }

    /**
     * 修改帖子收藏记录
     * 
     * @param faUserarticleCollect 帖子收藏记录
     * @return 结果
     */
    @Override
    public int updateFaUserarticleCollect(FaUserarticleCollect faUserarticleCollect)
    {
        return faUserarticleCollectMapper.updateFaUserarticleCollect(faUserarticleCollect);
    }

    /**
     * 批量删除帖子收藏记录
     * 
     * @param ids 需要删除的帖子收藏记录ID
     * @return 结果
     */
    @Override
    public int deleteFaUserarticleCollectByIds(Long[] ids)
    {
        return faUserarticleCollectMapper.deleteFaUserarticleCollectByIds(ids);
    }

    /**
     * 删除帖子收藏记录信息
     * 
     * @param id 帖子收藏记录ID
     * @return 结果
     */
    @Override
    public int deleteFaUserarticleCollectById(Long id)
    {
        return faUserarticleCollectMapper.deleteFaUserarticleCollectById(id);
    }
}
