package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaVideoListMapper;
import com.ruoyi.system.domain.FaVideoList;
import com.ruoyi.system.service.IFaVideoListService;

/**
 * 视频管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaVideoListServiceImpl implements IFaVideoListService 
{
    @Autowired
    private FaVideoListMapper faVideoListMapper;

    /**
     * 查询视频管理
     * 
     * @param id 视频管理ID
     * @return 视频管理
     */
    @Override
    public FaVideoList selectFaVideoListById(Long id)
    {
        return faVideoListMapper.selectFaVideoListById(id);
    }

    /**
     * 查询视频管理列表
     * 
     * @param faVideoList 视频管理
     * @return 视频管理
     */
    @Override
    public List<FaVideoList> selectFaVideoListList(FaVideoList faVideoList)
    {
        return faVideoListMapper.selectFaVideoListList(faVideoList);
    }

    /**
     * 新增视频管理
     * 
     * @param faVideoList 视频管理
     * @return 结果
     */
    @Override
    public int insertFaVideoList(FaVideoList faVideoList)
    {
        faVideoList.setCreatetime(MyDateUtils.nowTimestamp());
        return faVideoListMapper.insertFaVideoList(faVideoList);
    }

    /**
     * 修改视频管理
     * 
     * @param faVideoList 视频管理
     * @return 结果
     */
    @Override
    public int updateFaVideoList(FaVideoList faVideoList)
    {
        return faVideoListMapper.updateFaVideoList(faVideoList);
    }

    /**
     * 批量删除视频管理
     * 
     * @param ids 需要删除的视频管理ID
     * @return 结果
     */
    @Override
    public int deleteFaVideoListByIds(Long[] ids)
    {
        return faVideoListMapper.deleteFaVideoListByIds(ids);
    }

    /**
     * 删除视频管理信息
     * 
     * @param id 视频管理ID
     * @return 结果
     */
    @Override
    public int deleteFaVideoListById(Long id)
    {
        return faVideoListMapper.deleteFaVideoListById(id);
    }
}
