package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaStoreTixian;

/**
 * 提现管理Service接口
 * 
 * @author ruoyi
 * @date 2022-02-09
 */
public interface IFaStoreTixianService 
{
    /**
     * 查询提现管理
     * 
     * @param id 提现管理ID
     * @return 提现管理
     */
    public FaStoreTixian selectFaStoreTixianById(Long id);

    /**
     * 查询提现管理列表
     * 
     * @param faStoreTixian 提现管理
     * @return 提现管理集合
     */
    public List<FaStoreTixian> selectFaStoreTixianList(FaStoreTixian faStoreTixian);

    /**
     * 新增提现管理
     * 
     * @param faStoreTixian 提现管理
     * @return 结果
     */
    public int insertFaStoreTixian(FaStoreTixian faStoreTixian);

    /**
     * 修改提现管理
     * 
     * @param faStoreTixian 提现管理
     * @return 结果
     */
    public int updateFaStoreTixian(FaStoreTixian faStoreTixian);

    /**
     * 批量删除提现管理
     * 
     * @param ids 需要删除的提现管理ID
     * @return 结果
     */
    public int deleteFaStoreTixianByIds(Long[] ids);

    /**
     * 删除提现管理信息
     * 
     * @param id 提现管理ID
     * @return 结果
     */
    public int deleteFaStoreTixianById(Long id);
}
