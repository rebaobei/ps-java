package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserRuleMapper;
import com.ruoyi.system.domain.FaUserRule;
import com.ruoyi.system.service.IFaUserRuleService;

/**
 * 会员规则Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUserRuleServiceImpl implements IFaUserRuleService 
{
    @Autowired
    private FaUserRuleMapper faUserRuleMapper;

    /**
     * 查询会员规则
     * 
     * @param id 会员规则ID
     * @return 会员规则
     */
    @Override
    public FaUserRule selectFaUserRuleById(Integer id)
    {
        return faUserRuleMapper.selectFaUserRuleById(id);
    }

    /**
     * 查询会员规则列表
     * 
     * @param faUserRule 会员规则
     * @return 会员规则
     */
    @Override
    public List<FaUserRule> selectFaUserRuleList(FaUserRule faUserRule)
    {
        return faUserRuleMapper.selectFaUserRuleList(faUserRule);
    }

    /**
     * 新增会员规则
     * 
     * @param faUserRule 会员规则
     * @return 结果
     */
    @Override
    public int insertFaUserRule(FaUserRule faUserRule)
    {
        return faUserRuleMapper.insertFaUserRule(faUserRule);
    }

    /**
     * 修改会员规则
     * 
     * @param faUserRule 会员规则
     * @return 结果
     */
    @Override
    public int updateFaUserRule(FaUserRule faUserRule)
    {
        return faUserRuleMapper.updateFaUserRule(faUserRule);
    }

    /**
     * 批量删除会员规则
     * 
     * @param ids 需要删除的会员规则ID
     * @return 结果
     */
    @Override
    public int deleteFaUserRuleByIds(Integer[] ids)
    {
        return faUserRuleMapper.deleteFaUserRuleByIds(ids);
    }

    /**
     * 删除会员规则信息
     * 
     * @param id 会员规则ID
     * @return 结果
     */
    @Override
    public int deleteFaUserRuleById(Integer id)
    {
        return faUserRuleMapper.deleteFaUserRuleById(id);
    }
}
