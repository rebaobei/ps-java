package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaServiceYuyueOrder;
import com.ruoyi.system.domain.vo.ServiceOrderVo;

/**
 * 服务预约订单Service接口
 * 
 * @author ruoyi
 * @date 2021-08-06
 */
public interface IFaServiceYuyueOrderService 
{
    /**
     * 查询服务预约订单
     * 
     * @param id 服务预约订单ID
     * @return 服务预约订单
     */
    public FaServiceYuyueOrder selectFaServiceYuyueOrderById(Long id);

    /**
     * 查询服务预约订单列表
     * 
     * @param faServiceYuyueOrder 服务预约订单
     * @return 服务预约订单集合
     */
    public List<FaServiceYuyueOrder> selectFaServiceYuyueOrderList(FaServiceYuyueOrder faServiceYuyueOrder);

    /**
     * 新增服务预约订单
     * 
     * @param faServiceYuyueOrder 服务预约订单
     * @return 结果
     */
    public int insertFaServiceYuyueOrder(FaServiceYuyueOrder faServiceYuyueOrder);

    /**
     * 修改服务预约订单
     * 
     * @param faServiceYuyueOrder 服务预约订单
     * @return 结果
     */
    public int updateFaServiceYuyueOrder(FaServiceYuyueOrder faServiceYuyueOrder);

    /**
     * 批量删除服务预约订单
     * 
     * @param ids 需要删除的服务预约订单ID
     * @return 结果
     */
    public int deleteFaServiceYuyueOrderByIds(Long[] ids);

    /**
     * 删除服务预约订单信息
     * 
     * @param id 服务预约订单ID
     * @return 结果
     */
    public int deleteFaServiceYuyueOrderById(Long id);

    List<ServiceOrderVo> listServiceOrderVo(ServiceOrderVo faServiceYuyueOrder);
}
