package com.ruoyi.system.service;

import org.apache.ibatis.ognl.Ognl;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/8
 **/
public interface IStatisticService {


    Map<String,Object> getPanelInfo();

    Map<String, Object> getLineChartInfo();
}
