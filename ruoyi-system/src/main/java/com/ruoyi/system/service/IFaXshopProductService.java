package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.FaXshopProduct;
import com.ruoyi.system.domain.vo.ProductVo;
import com.ruoyi.system.domain.vo.XshopOrderProductVo;

/**
 * 商品Service接口
 * 
 * @author ruoyi
 * @date 2021-07-16
 */
public interface IFaXshopProductService 
{
    /**
     * 查询商品
     * 
     * @param id 商品ID
     * @return 商品
     */
    public FaXshopProduct selectFaXshopProductById(Integer id);

    /**
     * 查询商品列表
     * 
     * @param faXshopProduct 商品
     * @return 商品集合
     */
    public List<FaXshopProduct> selectFaXshopProductList(FaXshopProduct faXshopProduct);

    /**
     * 新增商品
     * 
     * @param faXshopProduct 商品
     * @return 结果
     */
    public int insertFaXshopProduct(FaXshopProduct faXshopProduct);

    /**
     * 修改商品
     * 
     * @param faXshopProduct 商品
     * @return 结果
     */
    public int updateFaXshopProduct(FaXshopProduct faXshopProduct);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的商品ID
     * @return 结果
     */
    public int deleteFaXshopProductByIds(Integer[] ids);

    /**
     * 删除商品信息
     * 
     * @param id 商品ID
     * @return 结果
     */
    public int deleteFaXshopProductById(Integer id);

    List<ProductVo> listProductVo(FaXshopProduct product);

    List<Map<String, Object>> listProductVoForSelect();
}
