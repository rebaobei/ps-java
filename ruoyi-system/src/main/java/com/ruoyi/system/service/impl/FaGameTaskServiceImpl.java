package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameTaskMapper;
import com.ruoyi.system.domain.FaGameTask;
import com.ruoyi.system.service.IFaGameTaskService;

/**
 * 每日任务Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameTaskServiceImpl implements IFaGameTaskService 
{
    @Autowired
    private FaGameTaskMapper faGameTaskMapper;

    /**
     * 查询每日任务
     * 
     * @param dayTaskId 每日任务ID
     * @return 每日任务
     */
    @Override
    public FaGameTask selectFaGameTaskById(Long dayTaskId)
    {
        return faGameTaskMapper.selectFaGameTaskById(dayTaskId);
    }

    /**
     * 查询每日任务列表
     * 
     * @param faGameTask 每日任务
     * @return 每日任务
     */
    @Override
    public List<FaGameTask> selectFaGameTaskList(FaGameTask faGameTask)
    {
        return faGameTaskMapper.selectFaGameTaskList(faGameTask);
    }

    /**
     * 新增每日任务
     * 
     * @param faGameTask 每日任务
     * @return 结果
     */
    @Override
    public int insertFaGameTask(FaGameTask faGameTask)
    {
        return faGameTaskMapper.insertFaGameTask(faGameTask);
    }

    /**
     * 修改每日任务
     * 
     * @param faGameTask 每日任务
     * @return 结果
     */
    @Override
    public int updateFaGameTask(FaGameTask faGameTask)
    {
        return faGameTaskMapper.updateFaGameTask(faGameTask);
    }

    /**
     * 批量删除每日任务
     * 
     * @param dayTaskIds 需要删除的每日任务ID
     * @return 结果
     */
    @Override
    public int deleteFaGameTaskByIds(Long[] dayTaskIds)
    {
        return faGameTaskMapper.deleteFaGameTaskByIds(dayTaskIds);
    }

    /**
     * 删除每日任务信息
     * 
     * @param dayTaskId 每日任务ID
     * @return 结果
     */
    @Override
    public int deleteFaGameTaskById(Long dayTaskId)
    {
        return faGameTaskMapper.deleteFaGameTaskById(dayTaskId);
    }
}
