package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserCardLog;
import com.ruoyi.system.domain.vo.CardLogWithUserInfoVo;

/**
 * 储值卡变动Service接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface IFaUserCardLogService 
{
    /**
     * 查询储值卡变动
     * 
     * @param id 储值卡变动ID
     * @return 储值卡变动
     */
    public FaUserCardLog selectFaUserCardLogById(Integer id);

    /**
     * 查询储值卡变动列表
     * 
     * @param faUserCardLog 储值卡变动
     * @return 储值卡变动集合
     */
    public List<FaUserCardLog> selectFaUserCardLogList(FaUserCardLog faUserCardLog);

    /**
     * 新增储值卡变动
     * 
     * @param faUserCardLog 储值卡变动
     * @return 结果
     */
    public int insertFaUserCardLog(FaUserCardLog faUserCardLog);

    /**
     * 修改储值卡变动
     * 
     * @param faUserCardLog 储值卡变动
     * @return 结果
     */
    public int updateFaUserCardLog(FaUserCardLog faUserCardLog);

    /**
     * 批量删除储值卡变动
     * 
     * @param ids 需要删除的储值卡变动ID
     * @return 结果
     */
    public int deleteFaUserCardLogByIds(Integer[] ids);

    /**
     * 删除储值卡变动信息
     * 
     * @param id 储值卡变动ID
     * @return 结果
     */
    public int deleteFaUserCardLogById(Integer id);

    List<CardLogWithUserInfoVo> listCardLogWithUserInfo(FaUserCardLog faUserCardLog);
}
