package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGamePetLevel;

/**
 * 宠物等级Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGamePetLevelService 
{
    /**
     * 查询宠物等级
     * 
     * @param id 宠物等级ID
     * @return 宠物等级
     */
    public FaGamePetLevel selectFaGamePetLevelById(Long id);

    /**
     * 查询宠物等级列表
     * 
     * @param faGamePetLevel 宠物等级
     * @return 宠物等级集合
     */
    public List<FaGamePetLevel> selectFaGamePetLevelList(FaGamePetLevel faGamePetLevel);

    /**
     * 新增宠物等级
     * 
     * @param faGamePetLevel 宠物等级
     * @return 结果
     */
    public int insertFaGamePetLevel(FaGamePetLevel faGamePetLevel);

    /**
     * 修改宠物等级
     * 
     * @param faGamePetLevel 宠物等级
     * @return 结果
     */
    public int updateFaGamePetLevel(FaGamePetLevel faGamePetLevel);

    /**
     * 批量删除宠物等级
     * 
     * @param ids 需要删除的宠物等级ID
     * @return 结果
     */
    public int deleteFaGamePetLevelByIds(Long[] ids);

    /**
     * 删除宠物等级信息
     * 
     * @param id 宠物等级ID
     * @return 结果
     */
    public int deleteFaGamePetLevelById(Long id);
}
