package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaAdszoneAdsMapper;
import com.ruoyi.system.domain.FaAdszoneAds;
import com.ruoyi.system.service.IFaAdszoneAdsService;

/**
 * 广告项目Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaAdszoneAdsServiceImpl implements IFaAdszoneAdsService 
{
    @Autowired
    private FaAdszoneAdsMapper faAdszoneAdsMapper;

    /**
     * 查询广告项目
     * 
     * @param id 广告项目ID
     * @return 广告项目
     */
    @Override
    public FaAdszoneAds selectFaAdszoneAdsById(Long id)
    {
        return faAdszoneAdsMapper.selectFaAdszoneAdsById(id);
    }

    /**
     * 查询广告项目列表
     * 
     * @param faAdszoneAds 广告项目
     * @return 广告项目
     */
    @Override
    public List<FaAdszoneAds> selectFaAdszoneAdsList(FaAdszoneAds faAdszoneAds)
    {
        return faAdszoneAdsMapper.selectFaAdszoneAdsList(faAdszoneAds);
    }

    /**
     * 新增广告项目
     * 
     * @param faAdszoneAds 广告项目
     * @return 结果
     */
    @Override
    public int insertFaAdszoneAds(FaAdszoneAds faAdszoneAds)
    {
        return faAdszoneAdsMapper.insertFaAdszoneAds(faAdszoneAds);
    }

    /**
     * 修改广告项目
     * 
     * @param faAdszoneAds 广告项目
     * @return 结果
     */
    @Override
    public int updateFaAdszoneAds(FaAdszoneAds faAdszoneAds)
    {
        return faAdszoneAdsMapper.updateFaAdszoneAds(faAdszoneAds);
    }

    /**
     * 批量删除广告项目
     * 
     * @param ids 需要删除的广告项目ID
     * @return 结果
     */
    @Override
    public int deleteFaAdszoneAdsByIds(Long[] ids)
    {
        return faAdszoneAdsMapper.deleteFaAdszoneAdsByIds(ids);
    }

    /**
     * 删除广告项目信息
     * 
     * @param id 广告项目ID
     * @return 结果
     */
    @Override
    public int deleteFaAdszoneAdsById(Long id)
    {
        return faAdszoneAdsMapper.deleteFaAdszoneAdsById(id);
    }
}
