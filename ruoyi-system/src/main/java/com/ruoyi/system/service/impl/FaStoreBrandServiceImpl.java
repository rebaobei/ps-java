package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStoreBrandMapper;
import com.ruoyi.system.domain.FaStoreBrand;
import com.ruoyi.system.service.IFaStoreBrandService;

/**
 * 品牌信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
@Service
public class FaStoreBrandServiceImpl implements IFaStoreBrandService 
{
    @Autowired
    private FaStoreBrandMapper faStoreBrandMapper;

    /**
     * 查询品牌信息
     * 
     * @param id 品牌信息ID
     * @return 品牌信息
     */
    @Override
    public FaStoreBrand selectFaStoreBrandById(Long id)
    {
        return faStoreBrandMapper.selectFaStoreBrandById(id);
    }

    /**
     * 查询品牌信息列表
     * 
     * @param faStoreBrand 品牌信息
     * @return 品牌信息
     */
    @Override
    public List<FaStoreBrand> selectFaStoreBrandList(FaStoreBrand faStoreBrand)
    {
        return faStoreBrandMapper.selectFaStoreBrandList(faStoreBrand);
    }

    /**
     * 新增品牌信息
     * 
     * @param faStoreBrand 品牌信息
     * @return 结果
     */
    @Override
    public int insertFaStoreBrand(FaStoreBrand faStoreBrand)
    {
        faStoreBrand.setCreateTime(DateUtils.getNowDate());
        return faStoreBrandMapper.insertFaStoreBrand(faStoreBrand);
    }

    /**
     * 修改品牌信息
     * 
     * @param faStoreBrand 品牌信息
     * @return 结果
     */
    @Override
    public int updateFaStoreBrand(FaStoreBrand faStoreBrand)
    {
        faStoreBrand.setUpdateTime(DateUtils.getNowDate());
        return faStoreBrandMapper.updateFaStoreBrand(faStoreBrand);
    }

    /**
     * 批量删除品牌信息
     * 
     * @param ids 需要删除的品牌信息ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreBrandByIds(Long[] ids)
    {
        return faStoreBrandMapper.deleteFaStoreBrandByIds(ids);
    }

    /**
     * 删除品牌信息信息
     * 
     * @param id 品牌信息ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreBrandById(Long id)
    {
        return faStoreBrandMapper.deleteFaStoreBrandById(id);
    }
}
