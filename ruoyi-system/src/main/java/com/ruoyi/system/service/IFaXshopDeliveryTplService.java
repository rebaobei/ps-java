package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaXshopDeliveryTpl;

/**
 * 运费模板Service接口
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
public interface IFaXshopDeliveryTplService 
{
    /**
     * 查询运费模板
     * 
     * @param id 运费模板ID
     * @return 运费模板
     */
    public FaXshopDeliveryTpl selectFaXshopDeliveryTplById(Integer id);

    /**
     * 查询运费模板列表
     * 
     * @param faXshopDeliveryTpl 运费模板
     * @return 运费模板集合
     */
    public List<FaXshopDeliveryTpl> selectFaXshopDeliveryTplList(FaXshopDeliveryTpl faXshopDeliveryTpl);

    /**
     * 新增运费模板
     * 
     * @param faXshopDeliveryTpl 运费模板
     * @return 结果
     */
    public int insertFaXshopDeliveryTpl(FaXshopDeliveryTpl faXshopDeliveryTpl);

    /**
     * 修改运费模板
     * 
     * @param faXshopDeliveryTpl 运费模板
     * @return 结果
     */
    public int updateFaXshopDeliveryTpl(FaXshopDeliveryTpl faXshopDeliveryTpl);

    /**
     * 批量删除运费模板
     * 
     * @param ids 需要删除的运费模板ID
     * @return 结果
     */
    public int deleteFaXshopDeliveryTplByIds(Integer[] ids);

    /**
     * 删除运费模板信息
     * 
     * @param id 运费模板ID
     * @return 结果
     */
    public int deleteFaXshopDeliveryTplById(Integer id);
}
