package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.UserArticleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBallUserarticleMapper;
import com.ruoyi.system.domain.FaBallUserarticle;
import com.ruoyi.system.service.IFaBallUserarticleService;

/**
 * 朋友圈明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@Service
public class FaBallUserarticleServiceImpl implements IFaBallUserarticleService 
{
    @Autowired
    private FaBallUserarticleMapper faBallUserarticleMapper;

    /**
     * 查询朋友圈明细
     * 
     * @param id 朋友圈明细ID
     * @return 朋友圈明细
     */
    @Override
    public FaBallUserarticle selectFaBallUserarticleById(Long id)
    {
        return faBallUserarticleMapper.selectFaBallUserarticleById(id);
    }

    /**
     * 查询朋友圈明细列表
     * 
     * @param faBallUserarticle 朋友圈明细
     * @return 朋友圈明细
     */
    @Override
    public List<FaBallUserarticle> selectFaBallUserarticleList(FaBallUserarticle faBallUserarticle)
    {
        return faBallUserarticleMapper.selectFaBallUserarticleList(faBallUserarticle);
    }

    /**
     * 新增朋友圈明细
     * 
     * @param faBallUserarticle 朋友圈明细
     * @return 结果
     */
    @Override
    public int insertFaBallUserarticle(FaBallUserarticle faBallUserarticle)
    {
        faBallUserarticle.setCreatetime(MyDateUtils.nowTimestamp());
        faBallUserarticle.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBallUserarticleMapper.insertFaBallUserarticle(faBallUserarticle);
    }

    /**
     * 修改朋友圈明细
     * 
     * @param faBallUserarticle 朋友圈明细
     * @return 结果
     */
    @Override
    public int updateFaBallUserarticle(FaBallUserarticle faBallUserarticle)
    {
        faBallUserarticle.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBallUserarticleMapper.updateFaBallUserarticle(faBallUserarticle);
    }

    /**
     * 批量删除朋友圈明细
     * 
     * @param ids 需要删除的朋友圈明细ID
     * @return 结果
     */
    @Override
    public int deleteFaBallUserarticleByIds(Long[] ids)
    {
        return faBallUserarticleMapper.deleteFaBallUserarticleByIds(ids);
    }

    /**
     * 删除朋友圈明细信息
     * 
     * @param id 朋友圈明细ID
     * @return 结果
     */
    @Override
    public int deleteFaBallUserarticleById(Long id)
    {
        return faBallUserarticleMapper.deleteFaBallUserarticleById(id);
    }

    @Override
    public List<UserArticleVo> listUserArticleVo(FaBallUserarticle faBallUserarticle) {
        List<UserArticleVo> userArticleVos = faBallUserarticleMapper.listUserArticleVo(faBallUserarticle);
        if (userArticleVos != null){
            for (UserArticleVo vo : userArticleVos){
                if (vo.getImages() != null){
                    String[] split = vo.getImages().split(",");
                    vo.setImageList(Arrays.asList(split));
                }
            }
        }
        return userArticleVos;
    }
}
