package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.FaXshopProduct;
import com.ruoyi.system.domain.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStorePersonMapper;
import com.ruoyi.system.domain.FaStorePerson;
import com.ruoyi.system.service.IFaStorePersonService;

/**
 * 店铺服务团队Service业务层处理
 *
 * @author ruoyi
 * @date 2021-10-20
 */
@Service
public class FaStorePersonServiceImpl implements IFaStorePersonService
{
    @Autowired
    private FaStorePersonMapper faStorePersonMapper;

    /**
     * 查询店铺服务团队
     *
     * @param id 店铺服务团队ID
     * @return 店铺服务团队
     */
    @Override
    public FaStorePerson selectFaStorePersonById(Long id)
    {
        return faStorePersonMapper.selectFaStorePersonById(id);
    }

    /**
     * 查询店铺服务团队列表
     *
     * @param faStorePerson 店铺服务团队
     * @return 店铺服务团队
     */
    @Override
    public List<FaStorePerson> selectFaStorePersonList(FaStorePerson faStorePerson)
    {
        return faStorePersonMapper.selectFaStorePersonList(faStorePerson);
    }

    /**
     * 新增店铺服务团队
     *
     * @param faStorePerson 店铺服务团队
     * @return 结果
     */
    @Override
    public int insertFaStorePerson(FaStorePerson faStorePerson)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStorePerson.setCreateTime(DateUtils.getNowDate());
        faStorePerson.setStoreId(storeInfoId);
        return faStorePersonMapper.insertFaStorePerson(faStorePerson);
    }

    /**
     * 修改店铺服务团队
     *
     * @param faStorePerson 店铺服务团队
     * @return 结果
     */
    @Override
    public int updateFaStorePerson(FaStorePerson faStorePerson)
    {
        faStorePerson.setUpdateTime(DateUtils.getNowDate());
        return faStorePersonMapper.updateFaStorePerson(faStorePerson);
    }

    /**
     * 批量删除店铺服务团队
     *
     * @param ids 需要删除的店铺服务团队ID
     * @return 结果
     */
    @Override
    public int deleteFaStorePersonByIds(Long[] ids)
    {
        return faStorePersonMapper.deleteFaStorePersonByIds(ids);
    }

    /**
     * 删除店铺服务团队信息
     *
     * @param id 店铺服务团队ID
     * @return 结果
     */
    @Override
    public int deleteFaStorePersonById(Long id)
    {
        return faStorePersonMapper.deleteFaStorePersonById(id);
    }



    @Override
    public List<Map<String, Object>> listForSelect() {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        FaStorePerson faStorePerson = new FaStorePerson();
        faStorePerson.setStoreId(storeInfoId);
        List<FaStorePerson> faStorePeople = faStorePersonMapper.selectFaStorePersonList(faStorePerson);
        List<Map<String,Object>> result = new ArrayList<>();
        for (FaStorePerson vo:faStorePeople){
            Map<String,Object> map = new HashMap<>();
            map.put("id",String.valueOf(vo.getId()));
            map.put("name",vo.getPersonName());
            result.add(map);
        }
        return result;
    }
}
