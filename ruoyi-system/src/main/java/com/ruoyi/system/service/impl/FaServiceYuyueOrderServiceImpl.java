package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.ServiceOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaServiceYuyueOrderMapper;
import com.ruoyi.system.domain.FaServiceYuyueOrder;
import com.ruoyi.system.service.IFaServiceYuyueOrderService;

/**
 * 服务预约订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-06
 */
@Service
public class FaServiceYuyueOrderServiceImpl implements IFaServiceYuyueOrderService 
{
    @Autowired
    private FaServiceYuyueOrderMapper faServiceYuyueOrderMapper;

    /**
     * 查询服务预约订单
     * 
     * @param id 服务预约订单ID
     * @return 服务预约订单
     */
    @Override
    public FaServiceYuyueOrder selectFaServiceYuyueOrderById(Long id)
    {
        return faServiceYuyueOrderMapper.selectFaServiceYuyueOrderById(id);
    }

    /**
     * 查询服务预约订单列表
     * 
     * @param faServiceYuyueOrder 服务预约订单
     * @return 服务预约订单
     */
    @Override
    public List<FaServiceYuyueOrder> selectFaServiceYuyueOrderList(FaServiceYuyueOrder faServiceYuyueOrder)
    {
        return faServiceYuyueOrderMapper.selectFaServiceYuyueOrderList(faServiceYuyueOrder);
    }

    /**
     * 新增服务预约订单
     * 
     * @param faServiceYuyueOrder 服务预约订单
     * @return 结果
     */
    @Override
    public int insertFaServiceYuyueOrder(FaServiceYuyueOrder faServiceYuyueOrder)
    {
        return faServiceYuyueOrderMapper.insertFaServiceYuyueOrder(faServiceYuyueOrder);
    }

    /**
     * 修改服务预约订单
     * 
     * @param faServiceYuyueOrder 服务预约订单
     * @return 结果
     */
    @Override
    public int updateFaServiceYuyueOrder(FaServiceYuyueOrder faServiceYuyueOrder)
    {
        return faServiceYuyueOrderMapper.updateFaServiceYuyueOrder(faServiceYuyueOrder);
    }

    /**
     * 批量删除服务预约订单
     * 
     * @param ids 需要删除的服务预约订单ID
     * @return 结果
     */
    @Override
    public int deleteFaServiceYuyueOrderByIds(Long[] ids)
    {
        return faServiceYuyueOrderMapper.deleteFaServiceYuyueOrderByIds(ids);
    }

    /**
     * 删除服务预约订单信息
     * 
     * @param id 服务预约订单ID
     * @return 结果
     */
    @Override
    public int deleteFaServiceYuyueOrderById(Long id)
    {
        return faServiceYuyueOrderMapper.deleteFaServiceYuyueOrderById(id);
    }

    @Override
    public List<ServiceOrderVo> listServiceOrderVo(ServiceOrderVo faServiceYuyueOrder) {
        return faServiceYuyueOrderMapper.listServiceOrderVo(faServiceYuyueOrder);
    }
}
