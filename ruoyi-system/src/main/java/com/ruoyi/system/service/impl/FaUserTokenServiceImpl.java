package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserTokenMapper;
import com.ruoyi.system.domain.FaUserToken;
import com.ruoyi.system.service.IFaUserTokenService;

/**
 * 会员TokenService业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUserTokenServiceImpl implements IFaUserTokenService 
{
    @Autowired
    private FaUserTokenMapper faUserTokenMapper;

    /**
     * 查询会员Token
     * 
     * @param token 会员TokenID
     * @return 会员Token
     */
    @Override
    public FaUserToken selectFaUserTokenById(String token)
    {
        return faUserTokenMapper.selectFaUserTokenById(token);
    }

    /**
     * 查询会员Token列表
     * 
     * @param faUserToken 会员Token
     * @return 会员Token
     */
    @Override
    public List<FaUserToken> selectFaUserTokenList(FaUserToken faUserToken)
    {
        return faUserTokenMapper.selectFaUserTokenList(faUserToken);
    }

    /**
     * 新增会员Token
     * 
     * @param faUserToken 会员Token
     * @return 结果
     */
    @Override
    public int insertFaUserToken(FaUserToken faUserToken)
    {
        return faUserTokenMapper.insertFaUserToken(faUserToken);
    }

    /**
     * 修改会员Token
     * 
     * @param faUserToken 会员Token
     * @return 结果
     */
    @Override
    public int updateFaUserToken(FaUserToken faUserToken)
    {
        return faUserTokenMapper.updateFaUserToken(faUserToken);
    }

    /**
     * 批量删除会员Token
     * 
     * @param tokens 需要删除的会员TokenID
     * @return 结果
     */
    @Override
    public int deleteFaUserTokenByIds(String[] tokens)
    {
        return faUserTokenMapper.deleteFaUserTokenByIds(tokens);
    }

    /**
     * 删除会员Token信息
     * 
     * @param token 会员TokenID
     * @return 结果
     */
    @Override
    public int deleteFaUserTokenById(String token)
    {
        return faUserTokenMapper.deleteFaUserTokenById(token);
    }
}
