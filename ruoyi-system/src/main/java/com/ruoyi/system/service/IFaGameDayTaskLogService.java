package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGameDayTaskLog;
import com.ruoyi.system.domain.vo.GameDayTaskLogVo;

/**
 * 每日任务完成记录Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGameDayTaskLogService 
{
    /**
     * 查询每日任务完成记录
     * 
     * @param id 每日任务完成记录ID
     * @return 每日任务完成记录
     */
    public FaGameDayTaskLog selectFaGameDayTaskLogById(Long id);

    /**
     * 查询每日任务完成记录列表
     * 
     * @param faGameDayTaskLog 每日任务完成记录
     * @return 每日任务完成记录集合
     */
    public List<FaGameDayTaskLog> selectFaGameDayTaskLogList(FaGameDayTaskLog faGameDayTaskLog);

    /**
     * 新增每日任务完成记录
     * 
     * @param faGameDayTaskLog 每日任务完成记录
     * @return 结果
     */
    public int insertFaGameDayTaskLog(FaGameDayTaskLog faGameDayTaskLog);

    /**
     * 修改每日任务完成记录
     * 
     * @param faGameDayTaskLog 每日任务完成记录
     * @return 结果
     */
    public int updateFaGameDayTaskLog(FaGameDayTaskLog faGameDayTaskLog);

    /**
     * 批量删除每日任务完成记录
     * 
     * @param ids 需要删除的每日任务完成记录ID
     * @return 结果
     */
    public int deleteFaGameDayTaskLogByIds(Long[] ids);

    /**
     * 删除每日任务完成记录信息
     * 
     * @param id 每日任务完成记录ID
     * @return 结果
     */
    public int deleteFaGameDayTaskLogById(Long id);

    List<GameDayTaskLogVo> listGameDayTaskLogVo(GameDayTaskLogVo faGameDayTaskLog);
}
