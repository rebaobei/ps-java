package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.system.domain.vo.CmsChannelTreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaCmsChannelMapper;
import com.ruoyi.system.domain.FaCmsChannel;
import com.ruoyi.system.service.IFaCmsChannelService;

/**
 * 分类管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-11
 */
@Service
public class FaCmsChannelServiceImpl implements IFaCmsChannelService {
    @Autowired
    private FaCmsChannelMapper faCmsChannelMapper;

    /**
     * 查询分类管理
     *
     * @param id 分类管理ID
     * @return 分类管理
     */
    @Override
    public FaCmsChannel selectFaCmsChannelById(Integer id) {
        return faCmsChannelMapper.selectFaCmsChannelById(id);
    }

    /**
     * 查询分类管理列表
     *
     * @param faCmsChannel 分类管理
     * @return 分类管理
     */
    @Override
    public List<FaCmsChannel> selectFaCmsChannelList(FaCmsChannel faCmsChannel) {
        return faCmsChannelMapper.selectFaCmsChannelList(faCmsChannel);
    }

    /**
     * 新增分类管理
     *
     * @param faCmsChannel 分类管理
     * @return 结果
     */
    @Override
    public int insertFaCmsChannel(FaCmsChannel faCmsChannel) {
        return faCmsChannelMapper.insertFaCmsChannel(faCmsChannel);
    }

    /**
     * 修改分类管理
     *
     * @param faCmsChannel 分类管理
     * @return 结果
     */
    @Override
    public int updateFaCmsChannel(FaCmsChannel faCmsChannel) {
        return faCmsChannelMapper.updateFaCmsChannel(faCmsChannel);
    }

    /**
     * 批量删除分类管理
     *
     * @param ids 需要删除的分类管理ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsChannelByIds(Integer[] ids) {
        return faCmsChannelMapper.deleteFaCmsChannelByIds(ids);
    }

    /**
     * 删除分类管理信息
     *
     * @param id 分类管理ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsChannelById(Integer id) {
        return faCmsChannelMapper.deleteFaCmsChannelById(id);
    }

    /**
     * 递归得到树形结构
     *
     * @return 树形结构
     */
    @Override
    public List<CmsChannelTreeVo> getChannelTreeData() {
        List<FaCmsChannel> faCmsChannels = faCmsChannelMapper.selectFaCmsChannelList(new FaCmsChannel());
        return generateTreeData(0, faCmsChannels);
    }


    /**
     * 生成树
     * @param pid 父id
     * @param rawData 原始数据
     * @return 结果
     */
    public List<CmsChannelTreeVo> generateTreeData(Integer pid, List<FaCmsChannel> rawData) {
        List<CmsChannelTreeVo> tree = new ArrayList<>();
        for (FaCmsChannel vo : rawData) {
            if (pid.equals(vo.getParentId())) {
                CmsChannelTreeVo node = new CmsChannelTreeVo();
                node.setId(vo.getId());
                node.setName(vo.getName());
                node.setChildren(generateTreeData(vo.getId(), rawData));
                tree.add(node);
            }
        }
        return tree;
    }


}
