package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.GameBagVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameBagMapper;
import com.ruoyi.system.domain.FaGameBag;
import com.ruoyi.system.service.IFaGameBagService;

/**
 * 背包管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameBagServiceImpl implements IFaGameBagService 
{
    @Autowired
    private FaGameBagMapper faGameBagMapper;

    /**
     * 查询背包管理
     * 
     * @param id 背包管理ID
     * @return 背包管理
     */
    @Override
    public FaGameBag selectFaGameBagById(Long id)
    {
        return faGameBagMapper.selectFaGameBagById(id);
    }

    /**
     * 查询背包管理列表
     * 
     * @param faGameBag 背包管理
     * @return 背包管理
     */
    @Override
    public List<FaGameBag> selectFaGameBagList(FaGameBag faGameBag)
    {
        return faGameBagMapper.selectFaGameBagList(faGameBag);
    }

    /**
     * 新增背包管理
     * 
     * @param faGameBag 背包管理
     * @return 结果
     */
    @Override
    public int insertFaGameBag(FaGameBag faGameBag)
    {
        return faGameBagMapper.insertFaGameBag(faGameBag);
    }

    /**
     * 修改背包管理
     * 
     * @param faGameBag 背包管理
     * @return 结果
     */
    @Override
    public int updateFaGameBag(FaGameBag faGameBag)
    {
        return faGameBagMapper.updateFaGameBag(faGameBag);
    }

    /**
     * 批量删除背包管理
     * 
     * @param ids 需要删除的背包管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGameBagByIds(Long[] ids)
    {
        return faGameBagMapper.deleteFaGameBagByIds(ids);
    }

    /**
     * 删除背包管理信息
     * 
     * @param id 背包管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGameBagById(Long id)
    {
        return faGameBagMapper.deleteFaGameBagById(id);
    }

    @Override
    public List<GameBagVo> listGameBagVo(GameBagVo faGameBag) {
        return faGameBagMapper.listGameBagVo(faGameBag);
    }
}
