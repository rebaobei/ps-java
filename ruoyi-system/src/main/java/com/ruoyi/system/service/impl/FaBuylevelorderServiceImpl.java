package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.BuyLevelOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBuylevelorderMapper;
import com.ruoyi.system.domain.FaBuylevelorder;
import com.ruoyi.system.service.IFaBuylevelorderService;

/**
 * 购买等级Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaBuylevelorderServiceImpl implements IFaBuylevelorderService 
{
    @Autowired
    private FaBuylevelorderMapper faBuylevelorderMapper;

    /**
     * 查询购买等级
     * 
     * @param id 购买等级ID
     * @return 购买等级
     */
    @Override
    public FaBuylevelorder selectFaBuylevelorderById(Long id)
    {
        return faBuylevelorderMapper.selectFaBuylevelorderById(id);
    }

    /**
     * 查询购买等级列表
     * 
     * @param faBuylevelorder 购买等级
     * @return 购买等级
     */
    @Override
    public List<FaBuylevelorder> selectFaBuylevelorderList(FaBuylevelorder faBuylevelorder)
    {
        return faBuylevelorderMapper.selectFaBuylevelorderList(faBuylevelorder);
    }

    /**
     * 新增购买等级
     * 
     * @param faBuylevelorder 购买等级
     * @return 结果
     */
    @Override
    public int insertFaBuylevelorder(FaBuylevelorder faBuylevelorder)
    {
        faBuylevelorder.setCreatetime(MyDateUtils.nowTimestamp());
        return faBuylevelorderMapper.insertFaBuylevelorder(faBuylevelorder);
    }

    /**
     * 修改购买等级
     * 
     * @param faBuylevelorder 购买等级
     * @return 结果
     */
    @Override
    public int updateFaBuylevelorder(FaBuylevelorder faBuylevelorder)
    {
        return faBuylevelorderMapper.updateFaBuylevelorder(faBuylevelorder);
    }

    /**
     * 批量删除购买等级
     * 
     * @param ids 需要删除的购买等级ID
     * @return 结果
     */
    @Override
    public int deleteFaBuylevelorderByIds(Long[] ids)
    {
        return faBuylevelorderMapper.deleteFaBuylevelorderByIds(ids);
    }

    /**
     * 删除购买等级信息
     * 
     * @param id 购买等级ID
     * @return 结果
     */
    @Override
    public int deleteFaBuylevelorderById(Long id)
    {
        return faBuylevelorderMapper.deleteFaBuylevelorderById(id);
    }

    @Override
    public List<BuyLevelOrderVo> listBuyLevelOrderVo(BuyLevelOrderVo faBuylevelorder) {
        return faBuylevelorderMapper.listBuyLevelOrderVo(faBuylevelorder);
    }
}
