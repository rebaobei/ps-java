package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopServiceTagMapper;
import com.ruoyi.system.domain.FaXshopServiceTag;
import com.ruoyi.system.service.IFaXshopServiceTagService;

/**
 * 服务标签Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
@Service
public class FaXshopServiceTagServiceImpl implements IFaXshopServiceTagService 
{
    @Autowired
    private FaXshopServiceTagMapper faXshopServiceTagMapper;

    /**
     * 查询服务标签
     * 
     * @param id 服务标签ID
     * @return 服务标签
     */
    @Override
    public FaXshopServiceTag selectFaXshopServiceTagById(Integer id)
    {
        return faXshopServiceTagMapper.selectFaXshopServiceTagById(id);
    }

    /**
     * 查询服务标签列表
     * 
     * @param faXshopServiceTag 服务标签
     * @return 服务标签
     */
    @Override
    public List<FaXshopServiceTag> selectFaXshopServiceTagList(FaXshopServiceTag faXshopServiceTag)
    {
        return faXshopServiceTagMapper.selectFaXshopServiceTagList(faXshopServiceTag);
    }

    /**
     * 新增服务标签
     * 
     * @param faXshopServiceTag 服务标签
     * @return 结果
     */
    @Override
    public int insertFaXshopServiceTag(FaXshopServiceTag faXshopServiceTag)
    {
        return faXshopServiceTagMapper.insertFaXshopServiceTag(faXshopServiceTag);
    }

    /**
     * 修改服务标签
     * 
     * @param faXshopServiceTag 服务标签
     * @return 结果
     */
    @Override
    public int updateFaXshopServiceTag(FaXshopServiceTag faXshopServiceTag)
    {
        return faXshopServiceTagMapper.updateFaXshopServiceTag(faXshopServiceTag);
    }

    /**
     * 批量删除服务标签
     * 
     * @param ids 需要删除的服务标签ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopServiceTagByIds(Integer[] ids)
    {
        return faXshopServiceTagMapper.deleteFaXshopServiceTagByIds(ids);
    }

    /**
     * 删除服务标签信息
     * 
     * @param id 服务标签ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopServiceTagById(Integer id)
    {
        return faXshopServiceTagMapper.deleteFaXshopServiceTagById(id);
    }
}
