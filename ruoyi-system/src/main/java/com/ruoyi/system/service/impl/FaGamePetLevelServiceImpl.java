package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGamePetLevelMapper;
import com.ruoyi.system.domain.FaGamePetLevel;
import com.ruoyi.system.service.IFaGamePetLevelService;

/**
 * 宠物等级Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGamePetLevelServiceImpl implements IFaGamePetLevelService 
{
    @Autowired
    private FaGamePetLevelMapper faGamePetLevelMapper;

    /**
     * 查询宠物等级
     * 
     * @param id 宠物等级ID
     * @return 宠物等级
     */
    @Override
    public FaGamePetLevel selectFaGamePetLevelById(Long id)
    {
        return faGamePetLevelMapper.selectFaGamePetLevelById(id);
    }

    /**
     * 查询宠物等级列表
     * 
     * @param faGamePetLevel 宠物等级
     * @return 宠物等级
     */
    @Override
    public List<FaGamePetLevel> selectFaGamePetLevelList(FaGamePetLevel faGamePetLevel)
    {
        return faGamePetLevelMapper.selectFaGamePetLevelList(faGamePetLevel);
    }

    /**
     * 新增宠物等级
     * 
     * @param faGamePetLevel 宠物等级
     * @return 结果
     */
    @Override
    public int insertFaGamePetLevel(FaGamePetLevel faGamePetLevel)
    {
        return faGamePetLevelMapper.insertFaGamePetLevel(faGamePetLevel);
    }

    /**
     * 修改宠物等级
     * 
     * @param faGamePetLevel 宠物等级
     * @return 结果
     */
    @Override
    public int updateFaGamePetLevel(FaGamePetLevel faGamePetLevel)
    {
        return faGamePetLevelMapper.updateFaGamePetLevel(faGamePetLevel);
    }

    /**
     * 批量删除宠物等级
     * 
     * @param ids 需要删除的宠物等级ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetLevelByIds(Long[] ids)
    {
        return faGamePetLevelMapper.deleteFaGamePetLevelByIds(ids);
    }

    /**
     * 删除宠物等级信息
     * 
     * @param id 宠物等级ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetLevelById(Long id)
    {
        return faGamePetLevelMapper.deleteFaGamePetLevelById(id);
    }
}
