package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.ruoyi.system.domain.vo.CascadeProvinceVo;
import com.ruoyi.system.domain.vo.CityVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaAreaMapper;
import com.ruoyi.system.domain.FaArea;
import com.ruoyi.system.service.IFaAreaService;

/**
 * 地区Service业务层处理
 *
 * @author ruoyi
 * @date 2021-07-20
 */
@Service
public class FaAreaServiceImpl implements IFaAreaService {
    @Autowired
    private FaAreaMapper faAreaMapper;

    /**
     * 查询地区
     *
     * @param id 地区ID
     * @return 地区
     */
    @Override
    public FaArea selectFaAreaById(Long id) {
        return faAreaMapper.selectFaAreaById(id);
    }

    /**
     * 查询地区列表
     *
     * @param faArea 地区
     * @return 地区
     */
    @Override
    public List<FaArea> selectFaAreaList(FaArea faArea) {
        return faAreaMapper.selectFaAreaList(faArea);
    }

    /**
     * 新增地区
     *
     * @param faArea 地区
     * @return 结果
     */
    @Override
    public int insertFaArea(FaArea faArea) {
        return faAreaMapper.insertFaArea(faArea);
    }

    /**
     * 修改地区
     *
     * @param faArea 地区
     * @return 结果
     */
    @Override
    public int updateFaArea(FaArea faArea) {
        return faAreaMapper.updateFaArea(faArea);
    }

    /**
     * 批量删除地区
     *
     * @param ids 需要删除的地区ID
     * @return 结果
     */
    @Override
    public int deleteFaAreaByIds(Long[] ids) {
        return faAreaMapper.deleteFaAreaByIds(ids);
    }

    /**
     * 删除地区信息
     *
     * @param id 地区ID
     * @return 结果
     */
    @Override
    public int deleteFaAreaById(Long id) {
        return faAreaMapper.deleteFaAreaById(id);
    }

    @Override
    public List<CascadeProvinceVo> listCascade() {
        // // todo 放入缓存
        // List<FaArea> provinces = faAreaMapper.listProvince();
        // List<FaArea> cities = faAreaMapper.listCity();
        // CityVo global = new CityVo(0L,"全国");
        // List<CityVo> p = new ArrayList<>();
        // List<CityVo> c = new ArrayList<>();
        // for (FaArea province : provinces){
        //     List<CityVo> cityVos = new ArrayList<>();
        //     for (FaArea city : cities){
        //         if (province.getId().equals(city.getPid())){
        //             CityVo cityVo = new CityVo(city.getId(), city.getName());
        //             cityVos.add(cityVo);
        //         }
        //     }
        //     if (cityVos.size() > 0){
        //         CascadeProvinceVo cascadeProvinceVo = new CascadeProvinceVo(province.getId(),province.getName(),cityVos);
        //         result.add(cascadeProvinceVo);
        //     }
        // }
        return null;
    }

}
