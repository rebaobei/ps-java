package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.constant.UnitConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopUnitMapper;
import com.ruoyi.system.domain.FaXshopUnit;
import com.ruoyi.system.service.IFaXshopUnitService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 计量单位Service业务层处理
 *
 * @author ruoyi
 * @date 2021-07-19
 */
@Service
public class FaXshopUnitServiceImpl implements IFaXshopUnitService {
    @Autowired
    private FaXshopUnitMapper faXshopUnitMapper;

    /**
     * 查询计量单位
     *
     * @param id 计量单位ID
     * @return 计量单位
     */
    @Override
    public FaXshopUnit selectFaXshopUnitById(Integer id) {
        return faXshopUnitMapper.selectFaXshopUnitById(id);
    }

    /**
     * 查询计量单位列表
     *
     * @param faXshopUnit 计量单位
     * @return 计量单位
     */
    @Override
    public List<FaXshopUnit> selectFaXshopUnitList(FaXshopUnit faXshopUnit) {
        return faXshopUnitMapper.selectFaXshopUnitList(faXshopUnit);
    }

    /**
     * 新增计量单位
     *
     * @param faXshopUnit 计量单位
     * @return 结果
     */
    @Override
    public int insertFaXshopUnit(FaXshopUnit faXshopUnit) {
        return faXshopUnitMapper.insertFaXshopUnit(faXshopUnit);
    }

    /**
     * 修改计量单位
     *
     * @param faXshopUnit 计量单位
     * @return 结果
     */
    @Override
    public int updateFaXshopUnit(FaXshopUnit faXshopUnit) {
        return faXshopUnitMapper.updateFaXshopUnit(faXshopUnit);
    }

    /**
     * 批量删除计量单位
     *
     * @param ids 需要删除的计量单位ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopUnitByIds(Integer[] ids) {
        return faXshopUnitMapper.deleteFaXshopUnitByIds(ids);
    }

    /**
     * 删除计量单位信息
     *
     * @param id 计量单位ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopUnitById(Integer id) {
        return faXshopUnitMapper.deleteFaXshopUnitById(id);
    }

    /**
     * 设置默认的计量单位
     *
     * @param
     * @return
     */
    @Override
    @Transactional
    public int setDefaultUnit(FaXshopUnit unit) {
        //设置当前为默认,
        if (unit.getIsDefault().equals(UnitConstant.UNIT_DEFAULT)) {
            Integer id = unit.getId();
            faXshopUnitMapper.updateFaXshopUnit(unit);
            return faXshopUnitMapper.cancelDefault(id);
        } else {
            // 取消默认设置,必须要有一个默认的
            return 0;
        }

    }
}
