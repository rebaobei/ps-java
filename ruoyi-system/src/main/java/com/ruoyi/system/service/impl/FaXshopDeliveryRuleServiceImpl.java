package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopDeliveryRuleMapper;
import com.ruoyi.system.domain.FaXshopDeliveryRule;
import com.ruoyi.system.service.IFaXshopDeliveryRuleService;

/**
 * 运费规则Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-20
 */
@Service
public class FaXshopDeliveryRuleServiceImpl implements IFaXshopDeliveryRuleService 
{
    @Autowired
    private FaXshopDeliveryRuleMapper faXshopDeliveryRuleMapper;

    /**
     * 查询运费规则
     * 
     * @param id 运费规则ID
     * @return 运费规则
     */
    @Override
    public FaXshopDeliveryRule selectFaXshopDeliveryRuleById(Integer id)
    {
        return faXshopDeliveryRuleMapper.selectFaXshopDeliveryRuleById(id);
    }

    /**
     * 查询运费规则列表
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 运费规则
     */
    @Override
    public List<FaXshopDeliveryRule> selectFaXshopDeliveryRuleList(FaXshopDeliveryRule faXshopDeliveryRule)
    {
        return faXshopDeliveryRuleMapper.selectFaXshopDeliveryRuleList(faXshopDeliveryRule);
    }

    /**
     * 新增运费规则
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 结果
     */
    @Override
    public int insertFaXshopDeliveryRule(FaXshopDeliveryRule faXshopDeliveryRule)
    {
        return faXshopDeliveryRuleMapper.insertFaXshopDeliveryRule(faXshopDeliveryRule);
    }

    /**
     * 修改运费规则
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 结果
     */
    @Override
    public int updateFaXshopDeliveryRule(FaXshopDeliveryRule faXshopDeliveryRule)
    {
        return faXshopDeliveryRuleMapper.updateFaXshopDeliveryRule(faXshopDeliveryRule);
    }

    /**
     * 批量删除运费规则
     * 
     * @param ids 需要删除的运费规则ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopDeliveryRuleByIds(Integer[] ids)
    {
        return faXshopDeliveryRuleMapper.deleteFaXshopDeliveryRuleByIds(ids);
    }

    /**
     * 删除运费规则信息
     * 
     * @param id 运费规则ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopDeliveryRuleById(Integer id)
    {
        return faXshopDeliveryRuleMapper.deleteFaXshopDeliveryRuleById(id);
    }

    @Override
    public List<FaXshopDeliveryRule> listRuleByTemplateId(Integer id) {
        return faXshopDeliveryRuleMapper.listRuleByTemplateId(id);
    }
}
