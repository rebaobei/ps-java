package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.CommonSettingConfigVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaConfigMapper;
import com.ruoyi.system.domain.FaConfig;
import com.ruoyi.system.service.IFaConfigService;

/**
 * App配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-14
 */
@Service
public class FaConfigServiceImpl implements IFaConfigService 
{
    @Autowired
    private FaConfigMapper faConfigMapper;

    /**
     * 查询App配置
     * 
     * @param id App配置ID
     * @return App配置
     */
    @Override
    public FaConfig selectFaConfigById(Integer id)
    {
        return faConfigMapper.selectFaConfigById(id);
    }

    /**
     * 查询App配置列表
     * 
     * @param faConfig App配置
     * @return App配置
     */
    @Override
    public List<FaConfig> selectFaConfigList(FaConfig faConfig)
    {
        return faConfigMapper.selectFaConfigList(faConfig);
    }

    /**
     * 新增App配置
     * 
     * @param faConfig App配置
     * @return 结果
     */
    @Override
    public int insertFaConfig(FaConfig faConfig)
    {
        return faConfigMapper.insertFaConfig(faConfig);
    }

    /**
     * 修改App配置
     * 
     * @param faConfig App配置
     * @return 结果
     */
    @Override
    public int updateFaConfig(FaConfig faConfig)
    {
        return faConfigMapper.updateFaConfig(faConfig);
    }

    /**
     * 批量删除App配置
     * 
     * @param ids 需要删除的App配置ID
     * @return 结果
     */
    @Override
    public int deleteFaConfigByIds(Integer[] ids)
    {
        return faConfigMapper.deleteFaConfigByIds(ids);
    }

    /**
     * 删除App配置信息
     * 
     * @param id App配置ID
     * @return 结果
     */
    @Override
    public int deleteFaConfigById(Integer id)
    {
        return faConfigMapper.deleteFaConfigById(id);
    }

    @Override
    public List<CommonSettingConfigVo> listCommonSettingConfigVo(String type) {
        return faConfigMapper.listCommonSettingConfigVo(type);
    }
}
