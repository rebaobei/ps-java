package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.system.domain.FaStoreBrand;
import com.ruoyi.system.domain.FaStoreBrandRecord;
import com.ruoyi.system.domain.FaStoreInfo;
import com.ruoyi.system.mapper.FaStoreBrandMapper;
import com.ruoyi.system.mapper.FaStoreBrandRecordMapper;
import com.ruoyi.system.mapper.FaStoreInfoMapper;
import com.ruoyi.system.service.IFaStoreBrandRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 品牌审核记录Service业务层处理
 *
 * @author ruoyi
 * @date 2021-11-02
 */
@Service
public class FaStoreBrandRecordServiceImpl implements IFaStoreBrandRecordService {
    @Autowired
    private FaStoreBrandRecordMapper faStoreBrandRecordMapper;
    @Autowired
    private FaStoreBrandMapper faStoreBrandMapper;
    @Autowired
    private FaStoreInfoMapper faStoreInfoMapper;

    /**
     * 查询品牌审核记录
     *
     * @param id 品牌审核记录ID
     * @return 品牌审核记录
     */
    @Override
    public FaStoreBrandRecord selectFaStoreBrandRecordById(Long id) {
        return faStoreBrandRecordMapper.selectFaStoreBrandRecordById(id);
    }

    /**
     * 查询品牌审核记录列表
     *
     * @param faStoreBrandRecord 品牌审核记录
     * @return 品牌审核记录
     */
    @Override
    public List<FaStoreBrandRecord> selectFaStoreBrandRecordList(FaStoreBrandRecord faStoreBrandRecord) {
        List<FaStoreBrandRecord> faStoreBrandRecords = faStoreBrandRecordMapper.selectFaStoreBrandRecordList(faStoreBrandRecord);
        for (FaStoreBrandRecord vo : faStoreBrandRecords) {
            if (vo.getBannerimages() != null && vo.getBannerimages().length() > 0) {
                String[] split = vo.getBannerimages().split(",");
                vo.setImageList(Arrays.asList(split));
            }
        }
        return faStoreBrandRecords;
    }

    /**
     * 新增品牌审核记录
     *
     * @param faStoreBrandRecord 品牌审核记录
     * @return 结果
     */
    @Override
    public int insertFaStoreBrandRecord(FaStoreBrandRecord faStoreBrandRecord) {
        faStoreBrandRecord.setCreateTime(DateUtils.getNowDate());
        faStoreBrandRecord.setStatus(0);
        return faStoreBrandRecordMapper.insertFaStoreBrandRecord(faStoreBrandRecord);
    }

    /**
     * 修改品牌审核记录
     *
     * @param faStoreBrandRecord 品牌审核记录
     * @return 结果
     */
    @Override
    public int updateFaStoreBrandRecord(FaStoreBrandRecord faStoreBrandRecord) {
        faStoreBrandRecord.setUpdateTime(DateUtils.getNowDate());
        return faStoreBrandRecordMapper.updateFaStoreBrandRecord(faStoreBrandRecord);
    }

    @Override
    public AjaxResult checkFaStoreBrandRecord(Long id, Integer status) {
        FaStoreBrandRecord faStoreBrandRecord = faStoreBrandRecordMapper.selectFaStoreBrandRecordById(id);
        if (faStoreBrandRecord == null) {
            return AjaxResult.error("未找到申请记录");
        }
        Integer oldStatus = faStoreBrandRecord.getStatus();
        Integer applicationType = faStoreBrandRecord.getApplicationType();
        if (oldStatus != 0) {
            return AjaxResult.error("申请记录不为待审核状态，请确认");
        }

        if (status.equals(1)  && applicationType.equals(0) ) {
            //新增品牌
            FaStoreBrand faStoreBrand = new FaStoreBrand();
            BeanUtils.copyBeanProp(faStoreBrand, faStoreBrandRecord);
            faStoreBrand.setStarLevel(1);
            faStoreBrand.setStatus(1);
            faStoreBrandMapper.insertFaStoreBrand(faStoreBrand);
        } else if (status.equals(1)  && applicationType.equals(1) ) {
            //入驻品牌
            FaStoreBrand faStoreBrand = faStoreBrandMapper.selectFaStoreBrandById(faStoreBrandRecord.getStoreBrandId());
            Long storeInfoId = faStoreBrandRecord.getStoreInfoId();
            FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(storeInfoId);
            faStoreInfo.setBrandId(faStoreBrand.getId());
            faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        } else if (status.equals(1)  && applicationType.equals(2) ) {
            //修改品牌
            Long storeBrandId = faStoreBrandRecord.getStoreBrandId();
            FaStoreBrand faStoreBrand = faStoreBrandMapper.selectFaStoreBrandById(storeBrandId);
            BeanUtils.copyProperties(faStoreBrandRecord,faStoreBrand,"id");
            faStoreBrand.setStatus(1);
            faStoreBrandMapper.updateFaStoreBrand(faStoreBrand);
        }
        faStoreBrandRecord.setStatus(status);
        faStoreBrandRecord.setCheckDateTime(DateUtils.getNowDate());
        faStoreBrandRecordMapper.updateFaStoreBrandRecord(faStoreBrandRecord);
        return AjaxResult.success();
    }

    /**
     * 入驻
     *
     * @param
     * @return 结果
     */
    @Override
    public AjaxResult joinFaStoreBrandRecord(Long id) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        Long userId = user.getUserId();
        Long storeInfoId = user.getStoreInfoId();
        FaStoreBrand faStoreBrand = faStoreBrandMapper.selectFaStoreBrandById(id);
        FaStoreBrandRecord param = new FaStoreBrandRecord();
        param.setStoreInfoId(storeInfoId);
        param.setApplicationType(1);
        List<FaStoreBrandRecord> faStoreBrandRecords = faStoreBrandRecordMapper.selectFaStoreBrandRecordList(param);
        if (faStoreBrandRecords.size() >= 1) {
            return AjaxResult.error("已申请品牌入驻,请等待审核");
        }
        FaStoreBrandRecord faStoreBrandRecord = new FaStoreBrandRecord();
        BeanUtils.copyBeanProp(faStoreBrandRecord, faStoreBrand);
        faStoreBrandRecord.setUserId(userId);
        faStoreBrandRecord.setStoreInfoId(storeInfoId);
        faStoreBrandRecord.setStoreBrandId(id);
        faStoreBrandRecord.setStatus(0);
        faStoreBrandRecord.setCreateTime(DateUtils.getNowDate());
        faStoreBrandRecord.setApplicationType(1);
        faStoreBrandRecordMapper.insertFaStoreBrandRecord(faStoreBrandRecord);
        return AjaxResult.success();
    }

    /**
     * 批量删除品牌审核记录
     *
     * @param ids 需要删除的品牌审核记录ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreBrandRecordByIds(Long[] ids) {
        return faStoreBrandRecordMapper.deleteFaStoreBrandRecordByIds(ids);
    }

    /**
     * 删除品牌审核记录信息
     *
     * @param id 品牌审核记录ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreBrandRecordById(Long id) {
        return faStoreBrandRecordMapper.deleteFaStoreBrandRecordById(id);
    }
}
