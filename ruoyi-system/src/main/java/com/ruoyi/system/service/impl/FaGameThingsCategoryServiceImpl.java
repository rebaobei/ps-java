package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameThingsCategoryMapper;
import com.ruoyi.system.domain.FaGameThingsCategory;
import com.ruoyi.system.service.IFaGameThingsCategoryService;

/**
 * 物品分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameThingsCategoryServiceImpl implements IFaGameThingsCategoryService 
{
    @Autowired
    private FaGameThingsCategoryMapper faGameThingsCategoryMapper;

    /**
     * 查询物品分类
     * 
     * @param id 物品分类ID
     * @return 物品分类
     */
    @Override
    public FaGameThingsCategory selectFaGameThingsCategoryById(Long id)
    {
        return faGameThingsCategoryMapper.selectFaGameThingsCategoryById(id);
    }

    /**
     * 查询物品分类列表
     * 
     * @param faGameThingsCategory 物品分类
     * @return 物品分类
     */
    @Override
    public List<FaGameThingsCategory> selectFaGameThingsCategoryList(FaGameThingsCategory faGameThingsCategory)
    {
        return faGameThingsCategoryMapper.selectFaGameThingsCategoryList(faGameThingsCategory);
    }

    /**
     * 新增物品分类
     * 
     * @param faGameThingsCategory 物品分类
     * @return 结果
     */
    @Override
    public int insertFaGameThingsCategory(FaGameThingsCategory faGameThingsCategory)
    {
        return faGameThingsCategoryMapper.insertFaGameThingsCategory(faGameThingsCategory);
    }

    /**
     * 修改物品分类
     * 
     * @param faGameThingsCategory 物品分类
     * @return 结果
     */
    @Override
    public int updateFaGameThingsCategory(FaGameThingsCategory faGameThingsCategory)
    {
        return faGameThingsCategoryMapper.updateFaGameThingsCategory(faGameThingsCategory);
    }

    /**
     * 批量删除物品分类
     * 
     * @param ids 需要删除的物品分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGameThingsCategoryByIds(Long[] ids)
    {
        return faGameThingsCategoryMapper.deleteFaGameThingsCategoryByIds(ids);
    }

    /**
     * 删除物品分类信息
     * 
     * @param id 物品分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGameThingsCategoryById(Long id)
    {
        return faGameThingsCategoryMapper.deleteFaGameThingsCategoryById(id);
    }
}
