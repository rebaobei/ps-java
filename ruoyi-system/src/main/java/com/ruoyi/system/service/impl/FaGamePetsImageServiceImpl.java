package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.GamePetsImageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGamePetsImageMapper;
import com.ruoyi.system.domain.FaGamePetsImage;
import com.ruoyi.system.service.IFaGamePetsImageService;

/**
 * 装扮管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGamePetsImageServiceImpl implements IFaGamePetsImageService {
    @Autowired
    private FaGamePetsImageMapper faGamePetsImageMapper;

    /**
     * 查询装扮管理
     *
     * @param id 装扮管理ID
     * @return 装扮管理
     */
    @Override
    public FaGamePetsImage selectFaGamePetsImageById(Long id) {
        return faGamePetsImageMapper.selectFaGamePetsImageById(id);
    }

    /**
     * 查询装扮管理列表
     *
     * @param faGamePetsImage 装扮管理
     * @return 装扮管理
     */
    @Override
    public List<FaGamePetsImage> selectFaGamePetsImageList(FaGamePetsImage faGamePetsImage) {
        return faGamePetsImageMapper.selectFaGamePetsImageList(faGamePetsImage);
    }

    /**
     * 新增装扮管理
     *
     * @param faGamePetsImage 装扮管理
     * @return 结果
     */
    @Override
    public int insertFaGamePetsImage(FaGamePetsImage faGamePetsImage) {
        return faGamePetsImageMapper.insertFaGamePetsImage(faGamePetsImage);
    }

    /**
     * 修改装扮管理
     *
     * @param faGamePetsImage 装扮管理
     * @return 结果
     */
    @Override
    public int updateFaGamePetsImage(FaGamePetsImage faGamePetsImage) {
        return faGamePetsImageMapper.updateFaGamePetsImage(faGamePetsImage);
    }

    /**
     * 批量删除装扮管理
     *
     * @param ids 需要删除的装扮管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsImageByIds(Long[] ids) {
        return faGamePetsImageMapper.deleteFaGamePetsImageByIds(ids);
    }

    /**
     * 删除装扮管理信息
     *
     * @param id 装扮管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsImageById(Long id) {
        return faGamePetsImageMapper.deleteFaGamePetsImageById(id);
    }

    @Override
    public List<GamePetsImageVo> listPetsImageVo(FaGamePetsImage faGamePetsImage) {
        return faGamePetsImageMapper.listPetsImageVo(faGamePetsImage);
    }


}
