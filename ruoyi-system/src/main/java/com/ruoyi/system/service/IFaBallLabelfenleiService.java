package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaBallLabelfenlei;

/**
 * 标签分类Service接口
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public interface IFaBallLabelfenleiService 
{
    /**
     * 查询标签分类
     * 
     * @param id 标签分类ID
     * @return 标签分类
     */
    public FaBallLabelfenlei selectFaBallLabelfenleiById(Long id);

    /**
     * 查询标签分类列表
     * 
     * @param faBallLabelfenlei 标签分类
     * @return 标签分类集合
     */
    public List<FaBallLabelfenlei> selectFaBallLabelfenleiList(FaBallLabelfenlei faBallLabelfenlei);

    /**
     * 新增标签分类
     * 
     * @param faBallLabelfenlei 标签分类
     * @return 结果
     */
    public int insertFaBallLabelfenlei(FaBallLabelfenlei faBallLabelfenlei);

    /**
     * 修改标签分类
     * 
     * @param faBallLabelfenlei 标签分类
     * @return 结果
     */
    public int updateFaBallLabelfenlei(FaBallLabelfenlei faBallLabelfenlei);

    /**
     * 批量删除标签分类
     * 
     * @param ids 需要删除的标签分类ID
     * @return 结果
     */
    public int deleteFaBallLabelfenleiByIds(Long[] ids);

    /**
     * 删除标签分类信息
     * 
     * @param id 标签分类ID
     * @return 结果
     */
    public int deleteFaBallLabelfenleiById(Long id);
}
