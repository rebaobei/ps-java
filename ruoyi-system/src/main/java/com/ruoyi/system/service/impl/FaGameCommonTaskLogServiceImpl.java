package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.GameCommonTaskLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameCommonTaskLogMapper;
import com.ruoyi.system.domain.FaGameCommonTaskLog;
import com.ruoyi.system.service.IFaGameCommonTaskLogService;

/**
 * 常规任务记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameCommonTaskLogServiceImpl implements IFaGameCommonTaskLogService 
{
    @Autowired
    private FaGameCommonTaskLogMapper faGameCommonTaskLogMapper;

    /**
     * 查询常规任务记录
     * 
     * @param id 常规任务记录ID
     * @return 常规任务记录
     */
    @Override
    public FaGameCommonTaskLog selectFaGameCommonTaskLogById(Long id)
    {
        return faGameCommonTaskLogMapper.selectFaGameCommonTaskLogById(id);
    }

    /**
     * 查询常规任务记录列表
     * 
     * @param faGameCommonTaskLog 常规任务记录
     * @return 常规任务记录
     */
    @Override
    public List<FaGameCommonTaskLog> selectFaGameCommonTaskLogList(FaGameCommonTaskLog faGameCommonTaskLog)
    {
        return faGameCommonTaskLogMapper.selectFaGameCommonTaskLogList(faGameCommonTaskLog);
    }

    /**
     * 新增常规任务记录
     * 
     * @param faGameCommonTaskLog 常规任务记录
     * @return 结果
     */
    @Override
    public int insertFaGameCommonTaskLog(FaGameCommonTaskLog faGameCommonTaskLog)
    {
        return faGameCommonTaskLogMapper.insertFaGameCommonTaskLog(faGameCommonTaskLog);
    }

    /**
     * 修改常规任务记录
     * 
     * @param faGameCommonTaskLog 常规任务记录
     * @return 结果
     */
    @Override
    public int updateFaGameCommonTaskLog(FaGameCommonTaskLog faGameCommonTaskLog)
    {
        return faGameCommonTaskLogMapper.updateFaGameCommonTaskLog(faGameCommonTaskLog);
    }

    /**
     * 批量删除常规任务记录
     * 
     * @param ids 需要删除的常规任务记录ID
     * @return 结果
     */
    @Override
    public int deleteFaGameCommonTaskLogByIds(Long[] ids)
    {
        return faGameCommonTaskLogMapper.deleteFaGameCommonTaskLogByIds(ids);
    }

    /**
     * 删除常规任务记录信息
     * 
     * @param id 常规任务记录ID
     * @return 结果
     */
    @Override
    public int deleteFaGameCommonTaskLogById(Long id)
    {
        return faGameCommonTaskLogMapper.deleteFaGameCommonTaskLogById(id);
    }

    @Override
    public List<GameCommonTaskLogVo> listCommonTaskLogVo(GameCommonTaskLogVo faGameCommonTaskLog) {
        return faGameCommonTaskLogMapper.listCommonTaskLogVo(faGameCommonTaskLog);
    }
}
