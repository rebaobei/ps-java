package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.UserQiandaoLogWithUserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBallUserqiandaodetailMapper;
import com.ruoyi.system.domain.FaBallUserqiandaodetail;
import com.ruoyi.system.service.IFaBallUserqiandaodetailService;

/**
 * 签到明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@Service
public class FaBallUserqiandaodetailServiceImpl implements IFaBallUserqiandaodetailService 
{
    @Autowired
    private FaBallUserqiandaodetailMapper faBallUserqiandaodetailMapper;

    /**
     * 查询签到明细
     * 
     * @param id 签到明细ID
     * @return 签到明细
     */
    @Override
    public FaBallUserqiandaodetail selectFaBallUserqiandaodetailById(Long id)
    {
        return faBallUserqiandaodetailMapper.selectFaBallUserqiandaodetailById(id);
    }

    /**
     * 查询签到明细列表
     * 
     * @param faBallUserqiandaodetail 签到明细
     * @return 签到明细
     */
    @Override
    public List<FaBallUserqiandaodetail> selectFaBallUserqiandaodetailList(FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        return faBallUserqiandaodetailMapper.selectFaBallUserqiandaodetailList(faBallUserqiandaodetail);
    }

    /**
     * 新增签到明细
     * 
     * @param faBallUserqiandaodetail 签到明细
     * @return 结果
     */
    @Override
    public int insertFaBallUserqiandaodetail(FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        return faBallUserqiandaodetailMapper.insertFaBallUserqiandaodetail(faBallUserqiandaodetail);
    }

    /**
     * 修改签到明细
     * 
     * @param faBallUserqiandaodetail 签到明细
     * @return 结果
     */
    @Override
    public int updateFaBallUserqiandaodetail(FaBallUserqiandaodetail faBallUserqiandaodetail)
    {
        return faBallUserqiandaodetailMapper.updateFaBallUserqiandaodetail(faBallUserqiandaodetail);
    }

    /**
     * 批量删除签到明细
     * 
     * @param ids 需要删除的签到明细ID
     * @return 结果
     */
    @Override
    public int deleteFaBallUserqiandaodetailByIds(Long[] ids)
    {
        return faBallUserqiandaodetailMapper.deleteFaBallUserqiandaodetailByIds(ids);
    }

    /**
     * 删除签到明细信息
     * 
     * @param id 签到明细ID
     * @return 结果
     */
    @Override
    public int deleteFaBallUserqiandaodetailById(Long id)
    {
        return faBallUserqiandaodetailMapper.deleteFaBallUserqiandaodetailById(id);
    }

    @Override
    public List<UserQiandaoLogWithUserInfoVo> listUserQianDaoLog(FaBallUserqiandaodetail faBallUserqiandaodetail) {
        return faBallUserqiandaodetailMapper.listUserQianDaoLog(faBallUserqiandaodetail);
    }
}
