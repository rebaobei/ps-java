package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaStoreIssue;

/**
 * 店铺问题Service接口
 * 
 * @author ruoyi
 * @date 2021-11-21
 */
public interface IFaStoreIssueService 
{
    /**
     * 查询店铺问题
     * 
     * @param id 店铺问题ID
     * @return 店铺问题
     */
    public FaStoreIssue selectFaStoreIssueById(Long id);

    /**
     * 查询店铺问题列表
     * 
     * @param faStoreIssue 店铺问题
     * @return 店铺问题集合
     */
    public List<FaStoreIssue> selectFaStoreIssueList(FaStoreIssue faStoreIssue);

    /**
     * 新增店铺问题
     * 
     * @param faStoreIssue 店铺问题
     * @return 结果
     */
    public int insertFaStoreIssue(FaStoreIssue faStoreIssue);

    /**
     * 修改店铺问题
     * 
     * @param faStoreIssue 店铺问题
     * @return 结果
     */
    public int updateFaStoreIssue(FaStoreIssue faStoreIssue);

    /**
     * 批量删除店铺问题
     * 
     * @param ids 需要删除的店铺问题ID
     * @return 结果
     */
    public int deleteFaStoreIssueByIds(Long[] ids);

    /**
     * 删除店铺问题信息
     * 
     * @param id 店铺问题ID
     * @return 结果
     */
    public int deleteFaStoreIssueById(Long id);
}
