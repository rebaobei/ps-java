package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.TopicVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBallTopicMapper;
import com.ruoyi.system.domain.FaBallTopic;
import com.ruoyi.system.service.IFaBallTopicService;

/**
 * 话题管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@Service
public class FaBallTopicServiceImpl implements IFaBallTopicService 
{
    @Autowired
    private FaBallTopicMapper faBallTopicMapper;

    /**
     * 查询话题管理
     * 
     * @param id 话题管理ID
     * @return 话题管理
     */
    @Override
    public FaBallTopic selectFaBallTopicById(Long id)
    {
        return faBallTopicMapper.selectFaBallTopicById(id);
    }

    /**
     * 查询话题管理列表
     * 
     * @param faBallTopic 话题管理
     * @return 话题管理
     */
    @Override
    public List<FaBallTopic> selectFaBallTopicList(FaBallTopic faBallTopic)
    {
        return faBallTopicMapper.selectFaBallTopicList(faBallTopic);
    }

    /**
     * 新增话题管理
     * 
     * @param faBallTopic 话题管理
     * @return 结果
     */
    @Override
    public int insertFaBallTopic(FaBallTopic faBallTopic)
    {
        faBallTopic.setCreatetime(MyDateUtils.nowTimestamp());
        return faBallTopicMapper.insertFaBallTopic(faBallTopic);
    }

    /**
     * 修改话题管理
     * 
     * @param faBallTopic 话题管理
     * @return 结果
     */
    @Override
    public int updateFaBallTopic(FaBallTopic faBallTopic)
    {
        return faBallTopicMapper.updateFaBallTopic(faBallTopic);
    }

    /**
     * 批量删除话题管理
     * 
     * @param ids 需要删除的话题管理ID
     * @return 结果
     */
    @Override
    public int deleteFaBallTopicByIds(Long[] ids)
    {
        return faBallTopicMapper.deleteFaBallTopicByIds(ids);
    }

    /**
     * 删除话题管理信息
     * 
     * @param id 话题管理ID
     * @return 结果
     */
    @Override
    public int deleteFaBallTopicById(Long id)
    {
        return faBallTopicMapper.deleteFaBallTopicById(id);
    }

    @Override
    public List<TopicVo> listTopicVo(FaBallTopic faBallTopic) {
        return faBallTopicMapper.listTopicVo(faBallTopic);
    }
}
