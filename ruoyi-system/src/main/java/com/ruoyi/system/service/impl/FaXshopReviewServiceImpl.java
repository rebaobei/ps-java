package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.vo.ProductReviewVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopReviewMapper;
import com.ruoyi.system.domain.FaXshopReview;
import com.ruoyi.system.service.IFaXshopReviewService;

/**
 * 商品评价Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
@Service
public class FaXshopReviewServiceImpl implements IFaXshopReviewService 
{
    @Autowired
    private FaXshopReviewMapper faXshopReviewMapper;

    /**
     * 查询商品评价
     * 
     * @param id 商品评价ID
     * @return 商品评价
     */
    @Override
    public FaXshopReview selectFaXshopReviewById(Integer id)
    {
        return faXshopReviewMapper.selectFaXshopReviewById(id);
    }

    /**
     * 查询商品评价列表
     * 
     * @param faXshopReview 商品评价
     * @return 商品评价
     */
    @Override
    public List<FaXshopReview> selectFaXshopReviewList(FaXshopReview faXshopReview)
    {
        return faXshopReviewMapper.selectFaXshopReviewList(faXshopReview);
    }

    /**
     * 新增商品评价
     * 
     * @param faXshopReview 商品评价
     * @return 结果
     */
    @Override
    public int insertFaXshopReview(FaXshopReview faXshopReview)
    {
        faXshopReview.setCreateTime(DateUtils.getNowDate());
        return faXshopReviewMapper.insertFaXshopReview(faXshopReview);
    }

    /**
     * 修改商品评价
     * 
     * @param faXshopReview 商品评价
     * @return 结果
     */
    @Override
    public int updateFaXshopReview(FaXshopReview faXshopReview)
    {
        faXshopReview.setUpdateTime(DateUtils.getNowDate());
        return faXshopReviewMapper.updateFaXshopReview(faXshopReview);
    }

    /**
     * 批量删除商品评价
     * 
     * @param ids 需要删除的商品评价ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopReviewByIds(Integer[] ids)
    {
        return faXshopReviewMapper.deleteFaXshopReviewByIds(ids);
    }

    /**
     * 删除商品评价信息
     * 
     * @param id 商品评价ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopReviewById(Integer id)
    {
        return faXshopReviewMapper.deleteFaXshopReviewById(id);
    }

    @Override
    public List<ProductReviewVo> listProductReviewVo(ProductReviewVo vo) {
        return faXshopReviewMapper.listProductReviewVo(vo);
    }
}
