package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserarticleCollect;

/**
 * 帖子收藏记录Service接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface IFaUserarticleCollectService 
{
    /**
     * 查询帖子收藏记录
     * 
     * @param id 帖子收藏记录ID
     * @return 帖子收藏记录
     */
    public FaUserarticleCollect selectFaUserarticleCollectById(Long id);

    /**
     * 查询帖子收藏记录列表
     * 
     * @param faUserarticleCollect 帖子收藏记录
     * @return 帖子收藏记录集合
     */
    public List<FaUserarticleCollect> selectFaUserarticleCollectList(FaUserarticleCollect faUserarticleCollect);

    /**
     * 新增帖子收藏记录
     * 
     * @param faUserarticleCollect 帖子收藏记录
     * @return 结果
     */
    public int insertFaUserarticleCollect(FaUserarticleCollect faUserarticleCollect);

    /**
     * 修改帖子收藏记录
     * 
     * @param faUserarticleCollect 帖子收藏记录
     * @return 结果
     */
    public int updateFaUserarticleCollect(FaUserarticleCollect faUserarticleCollect);

    /**
     * 批量删除帖子收藏记录
     * 
     * @param ids 需要删除的帖子收藏记录ID
     * @return 结果
     */
    public int deleteFaUserarticleCollectByIds(Long[] ids);

    /**
     * 删除帖子收藏记录信息
     * 
     * @param id 帖子收藏记录ID
     * @return 结果
     */
    public int deleteFaUserarticleCollectById(Long id);
}
