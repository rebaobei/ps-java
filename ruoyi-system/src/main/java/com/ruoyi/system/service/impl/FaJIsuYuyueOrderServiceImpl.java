package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.JisuYuyueOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaJIsuYuyueOrderMapper;
import com.ruoyi.system.domain.FaJIsuYuyueOrder;
import com.ruoyi.system.service.IFaJIsuYuyueOrderService;

/**
 * 寄宿预约订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-06
 */
@Service
public class FaJIsuYuyueOrderServiceImpl implements IFaJIsuYuyueOrderService 
{
    @Autowired
    private FaJIsuYuyueOrderMapper faJIsuYuyueOrderMapper;

    /**
     * 查询寄宿预约订单
     * 
     * @param id 寄宿预约订单ID
     * @return 寄宿预约订单
     */
    @Override
    public FaJIsuYuyueOrder selectFaJIsuYuyueOrderById(Long id)
    {
        return faJIsuYuyueOrderMapper.selectFaJIsuYuyueOrderById(id);
    }

    /**
     * 查询寄宿预约订单列表
     * 
     * @param faJIsuYuyueOrder 寄宿预约订单
     * @return 寄宿预约订单
     */
    @Override
    public List<FaJIsuYuyueOrder> selectFaJIsuYuyueOrderList(JisuYuyueOrderVo faJIsuYuyueOrder)
    {
        return faJIsuYuyueOrderMapper.selectFaJIsuYuyueOrderList(faJIsuYuyueOrder);
    }

    /**
     * 新增寄宿预约订单
     * 
     * @param faJIsuYuyueOrder 寄宿预约订单
     * @return 结果
     */
    @Override
    public int insertFaJIsuYuyueOrder(FaJIsuYuyueOrder faJIsuYuyueOrder)
    {
        return faJIsuYuyueOrderMapper.insertFaJIsuYuyueOrder(faJIsuYuyueOrder);
    }

    /**
     * 修改寄宿预约订单
     * 
     * @param faJIsuYuyueOrder 寄宿预约订单
     * @return 结果
     */
    @Override
    public int updateFaJIsuYuyueOrder(FaJIsuYuyueOrder faJIsuYuyueOrder)
    {
        return faJIsuYuyueOrderMapper.updateFaJIsuYuyueOrder(faJIsuYuyueOrder);
    }

    /**
     * 批量删除寄宿预约订单
     * 
     * @param ids 需要删除的寄宿预约订单ID
     * @return 结果
     */
    @Override
    public int deleteFaJIsuYuyueOrderByIds(Long[] ids)
    {
        return faJIsuYuyueOrderMapper.deleteFaJIsuYuyueOrderByIds(ids);
    }

    /**
     * 删除寄宿预约订单信息
     * 
     * @param id 寄宿预约订单ID
     * @return 结果
     */
    @Override
    public int deleteFaJIsuYuyueOrderById(Long id)
    {
        return faJIsuYuyueOrderMapper.deleteFaJIsuYuyueOrderById(id);
    }
}
