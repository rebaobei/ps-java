package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaVoteSiteMapper;
import com.ruoyi.system.domain.FaVoteSite;
import com.ruoyi.system.service.IFaVoteSiteService;

/**
 * 打赏设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@Service
public class FaVoteSiteServiceImpl implements IFaVoteSiteService 
{
    @Autowired
    private FaVoteSiteMapper faVoteSiteMapper;

    /**
     * 查询打赏设置
     * 
     * @param id 打赏设置ID
     * @return 打赏设置
     */
    @Override
    public FaVoteSite selectFaVoteSiteById(Long id)
    {
        return faVoteSiteMapper.selectFaVoteSiteById(id);
    }

    /**
     * 查询打赏设置列表
     * 
     * @param faVoteSite 打赏设置
     * @return 打赏设置
     */
    @Override
    public List<FaVoteSite> selectFaVoteSiteList(FaVoteSite faVoteSite)
    {
        return faVoteSiteMapper.selectFaVoteSiteList(faVoteSite);
    }

    /**
     * 新增打赏设置
     * 
     * @param faVoteSite 打赏设置
     * @return 结果
     */
    @Override
    public int insertFaVoteSite(FaVoteSite faVoteSite)
    {
        faVoteSite.setCreatetime(MyDateUtils.nowTimestamp());
        faVoteSite.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteSiteMapper.insertFaVoteSite(faVoteSite);
    }

    /**
     * 修改打赏设置
     * 
     * @param faVoteSite 打赏设置
     * @return 结果
     */
    @Override
    public int updateFaVoteSite(FaVoteSite faVoteSite)
    {
        faVoteSite.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteSiteMapper.updateFaVoteSite(faVoteSite);
    }

    /**
     * 批量删除打赏设置
     * 
     * @param ids 需要删除的打赏设置ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteSiteByIds(Long[] ids)
    {
        return faVoteSiteMapper.deleteFaVoteSiteByIds(ids);
    }

    /**
     * 删除打赏设置信息
     * 
     * @param id 打赏设置ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteSiteById(Long id)
    {
        return faVoteSiteMapper.deleteFaVoteSiteById(id);
    }
}
