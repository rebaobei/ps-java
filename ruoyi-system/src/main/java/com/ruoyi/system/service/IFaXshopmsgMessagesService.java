package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaXshopmsgMessages;
import com.ruoyi.system.domain.vo.MsgLogWithUserInfo;

/**
 * 消息管理Service接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface IFaXshopmsgMessagesService 
{
    /**
     * 查询消息管理
     * 
     * @param id 消息管理ID
     * @return 消息管理
     */
    public FaXshopmsgMessages selectFaXshopmsgMessagesById(Long id);

    /**
     * 查询消息管理列表
     * 
     * @param faXshopmsgMessages 消息管理
     * @return 消息管理集合
     */
    public List<FaXshopmsgMessages> selectFaXshopmsgMessagesList(FaXshopmsgMessages faXshopmsgMessages);

    /**
     * 新增消息管理
     * 
     * @param faXshopmsgMessages 消息管理
     * @return 结果
     */
    public int insertFaXshopmsgMessages(FaXshopmsgMessages faXshopmsgMessages);

    /**
     * 修改消息管理
     * 
     * @param faXshopmsgMessages 消息管理
     * @return 结果
     */
    public int updateFaXshopmsgMessages(FaXshopmsgMessages faXshopmsgMessages);

    /**
     * 批量删除消息管理
     * 
     * @param ids 需要删除的消息管理ID
     * @return 结果
     */
    public int deleteFaXshopmsgMessagesByIds(Long[] ids);

    /**
     * 删除消息管理信息
     * 
     * @param id 消息管理ID
     * @return 结果
     */
    public int deleteFaXshopmsgMessagesById(Long id);

    List<MsgLogWithUserInfo> listMsgLog(FaXshopmsgMessages messages);
}
