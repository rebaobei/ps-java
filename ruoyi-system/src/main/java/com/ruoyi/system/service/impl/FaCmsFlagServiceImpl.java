package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaCmsFlagMapper;
import com.ruoyi.system.domain.FaCmsFlag;
import com.ruoyi.system.service.IFaCmsFlagService;

/**
 * 文章标志Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-11
 */
@Service
public class FaCmsFlagServiceImpl implements IFaCmsFlagService {
    @Autowired
    private FaCmsFlagMapper faCmsFlagMapper;

    /**
     * 查询文章标志
     *
     * @param id 文章标志ID
     * @return 文章标志
     */
    @Override
    public FaCmsFlag selectFaCmsFlagById(Long id) {
        return faCmsFlagMapper.selectFaCmsFlagById(id);
    }

    /**
     * 查询文章标志列表
     *
     * @param faCmsFlag 文章标志
     * @return 文章标志
     */
    @Override
    public List<FaCmsFlag> selectFaCmsFlagList(FaCmsFlag faCmsFlag) {
        return faCmsFlagMapper.selectFaCmsFlagList(faCmsFlag);
    }

    /**
     * 新增文章标志
     *
     * @param faCmsFlag 文章标志
     * @return 结果
     */
    @Override
    public int insertFaCmsFlag(FaCmsFlag faCmsFlag) {
        faCmsFlag.setCreatetime(MyDateUtils.nowTimestamp());
        faCmsFlag.setUpdatetime(MyDateUtils.nowTimestamp());
        return faCmsFlagMapper.insertFaCmsFlag(faCmsFlag);
    }

    /**
     * 修改文章标志
     *
     * @param faCmsFlag 文章标志
     * @return 结果
     */
    @Override
    public int updateFaCmsFlag(FaCmsFlag faCmsFlag) {
        faCmsFlag.setUpdatetime(MyDateUtils.nowTimestamp());
        return faCmsFlagMapper.updateFaCmsFlag(faCmsFlag);
    }

    /**
     * 批量删除文章标志
     *
     * @param ids 需要删除的文章标志ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsFlagByIds(Long[] ids) {
        return faCmsFlagMapper.deleteFaCmsFlagByIds(ids);
    }

    /**
     * 删除文章标志信息
     *
     * @param id 文章标志ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsFlagById(Long id) {
        return faCmsFlagMapper.deleteFaCmsFlagById(id);
    }
}
