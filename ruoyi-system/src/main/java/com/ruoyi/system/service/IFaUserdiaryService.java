package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserdiary;
import com.ruoyi.system.domain.vo.UserDiaryVo;

/**
 * 宠物日记Service接口
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public interface IFaUserdiaryService 
{
    /**
     * 查询宠物日记
     * 
     * @param id 宠物日记ID
     * @return 宠物日记
     */
    public FaUserdiary selectFaUserdiaryById(Long id);

    /**
     * 查询宠物日记列表
     * 
     * @param faUserdiary 宠物日记
     * @return 宠物日记集合
     */
    public List<FaUserdiary> selectFaUserdiaryList(FaUserdiary faUserdiary);

    /**
     * 新增宠物日记
     * 
     * @param faUserdiary 宠物日记
     * @return 结果
     */
    public int insertFaUserdiary(FaUserdiary faUserdiary);

    /**
     * 修改宠物日记
     * 
     * @param faUserdiary 宠物日记
     * @return 结果
     */
    public int updateFaUserdiary(FaUserdiary faUserdiary);

    /**
     * 批量删除宠物日记
     * 
     * @param ids 需要删除的宠物日记ID
     * @return 结果
     */
    public int deleteFaUserdiaryByIds(Long[] ids);

    /**
     * 删除宠物日记信息
     * 
     * @param id 宠物日记ID
     * @return 结果
     */
    public int deleteFaUserdiaryById(Long id);

    List<UserDiaryVo> listUserDiaryVo(UserDiaryVo vo);
}
