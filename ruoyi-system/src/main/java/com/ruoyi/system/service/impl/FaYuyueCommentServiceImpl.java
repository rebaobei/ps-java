package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.YuYueCommentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaYuyueCommentMapper;
import com.ruoyi.system.domain.FaYuyueComment;
import com.ruoyi.system.service.IFaYuyueCommentService;

/**
 * 预约订单评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
@Service
public class FaYuyueCommentServiceImpl implements IFaYuyueCommentService 
{
    @Autowired
    private FaYuyueCommentMapper faYuyueCommentMapper;

    /**
     * 查询预约订单评论
     * 
     * @param id 预约订单评论ID
     * @return 预约订单评论
     */
    @Override
    public FaYuyueComment selectFaYuyueCommentById(Long id)
    {
        return faYuyueCommentMapper.selectFaYuyueCommentById(id);
    }

    /**
     * 查询预约订单评论列表
     * 
     * @param faYuyueComment 预约订单评论
     * @return 预约订单评论
     */
    @Override
    public List<FaYuyueComment> selectFaYuyueCommentList(FaYuyueComment faYuyueComment)
    {
        return faYuyueCommentMapper.selectFaYuyueCommentList(faYuyueComment);
    }

    /**
     * 新增预约订单评论
     * 
     * @param faYuyueComment 预约订单评论
     * @return 结果
     */
    @Override
    public int insertFaYuyueComment(FaYuyueComment faYuyueComment)
    {
        return faYuyueCommentMapper.insertFaYuyueComment(faYuyueComment);
    }

    /**
     * 修改预约订单评论
     * 
     * @param faYuyueComment 预约订单评论
     * @return 结果
     */
    @Override
    public int updateFaYuyueComment(FaYuyueComment faYuyueComment)
    {
        return faYuyueCommentMapper.updateFaYuyueComment(faYuyueComment);
    }

    /**
     * 批量删除预约订单评论
     * 
     * @param ids 需要删除的预约订单评论ID
     * @return 结果
     */
    @Override
    public int deleteFaYuyueCommentByIds(Long[] ids)
    {
        return faYuyueCommentMapper.deleteFaYuyueCommentByIds(ids);
    }

    /**
     * 删除预约订单评论信息
     * 
     * @param id 预约订单评论ID
     * @return 结果
     */
    @Override
    public int deleteFaYuyueCommentById(Long id)
    {
        return faYuyueCommentMapper.deleteFaYuyueCommentById(id);
    }

    @Override
    public List<YuYueCommentVo> listCommnentVo(FaYuyueComment faYuyueComment) {
        return faYuyueCommentMapper.listCommentVo(faYuyueComment);
    }
}
