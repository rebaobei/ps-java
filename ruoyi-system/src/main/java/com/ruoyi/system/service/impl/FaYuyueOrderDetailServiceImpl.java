package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.YuyueOrderDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaYuyueOrderDetailMapper;
import com.ruoyi.system.domain.FaYuyueOrderDetail;
import com.ruoyi.system.service.IFaYuyueOrderDetailService;

/**
 * 预约订单明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaYuyueOrderDetailServiceImpl implements IFaYuyueOrderDetailService 
{
    @Autowired
    private FaYuyueOrderDetailMapper faYuyueOrderDetailMapper;

    /**
     * 查询预约订单明细
     * 
     * @param id 预约订单明细ID
     * @return 预约订单明细
     */
    @Override
    public FaYuyueOrderDetail selectFaYuyueOrderDetailById(Long id)
    {
        return faYuyueOrderDetailMapper.selectFaYuyueOrderDetailById(id);
    }

    /**
     * 查询预约订单明细列表
     * 
     * @param faYuyueOrderDetail 预约订单明细
     * @return 预约订单明细
     */
    @Override
    public List<FaYuyueOrderDetail> selectFaYuyueOrderDetailList(FaYuyueOrderDetail faYuyueOrderDetail)
    {
        return faYuyueOrderDetailMapper.selectFaYuyueOrderDetailList(faYuyueOrderDetail);
    }

    /**
     * 新增预约订单明细
     * 
     * @param faYuyueOrderDetail 预约订单明细
     * @return 结果
     */
    @Override
    public int insertFaYuyueOrderDetail(FaYuyueOrderDetail faYuyueOrderDetail)
    {
        faYuyueOrderDetail.setCreatetime(MyDateUtils.nowTimestamp());
        return faYuyueOrderDetailMapper.insertFaYuyueOrderDetail(faYuyueOrderDetail);
    }

    /**
     * 修改预约订单明细
     * 
     * @param faYuyueOrderDetail 预约订单明细
     * @return 结果
     */
    @Override
    public int updateFaYuyueOrderDetail(FaYuyueOrderDetail faYuyueOrderDetail)
    {
        return faYuyueOrderDetailMapper.updateFaYuyueOrderDetail(faYuyueOrderDetail);
    }

    /**
     * 批量删除预约订单明细
     * 
     * @param ids 需要删除的预约订单明细ID
     * @return 结果
     */
    @Override
    public int deleteFaYuyueOrderDetailByIds(Long[] ids)
    {
        return faYuyueOrderDetailMapper.deleteFaYuyueOrderDetailByIds(ids);
    }

    /**
     * 删除预约订单明细信息
     * 
     * @param id 预约订单明细ID
     * @return 结果
     */
    @Override
    public int deleteFaYuyueOrderDetailById(Long id)
    {
        return faYuyueOrderDetailMapper.deleteFaYuyueOrderDetailById(id);
    }

    @Override
    public List<YuyueOrderDetailVo> listYuyueOrderDetailVo(YuyueOrderDetailVo faYuyueOrderDetail) {
        return faYuyueOrderDetailMapper.listYuyueOrderDetailVo(faYuyueOrderDetail);
    }
}
