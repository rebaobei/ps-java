package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBallLabelMapper;
import com.ruoyi.system.domain.FaBallLabel;
import com.ruoyi.system.service.IFaBallLabelService;

/**
 * 标签详情Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@Service
public class FaBallLabelServiceImpl implements IFaBallLabelService 
{
    @Autowired
    private FaBallLabelMapper faBallLabelMapper;

    /**
     * 查询标签详情
     * 
     * @param id 标签详情ID
     * @return 标签详情
     */
    @Override
    public FaBallLabel selectFaBallLabelById(Long id)
    {
        return faBallLabelMapper.selectFaBallLabelById(id);
    }

    /**
     * 查询标签详情列表
     * 
     * @param faBallLabel 标签详情
     * @return 标签详情
     */
    @Override
    public List<FaBallLabel> selectFaBallLabelList(FaBallLabel faBallLabel)
    {
        return faBallLabelMapper.selectFaBallLabelList(faBallLabel);
    }

    /**
     * 新增标签详情
     * 
     * @param faBallLabel 标签详情
     * @return 结果
     */
    @Override
    public int insertFaBallLabel(FaBallLabel faBallLabel)
    {
        faBallLabel.setCreatetime(MyDateUtils.nowTimestamp());
        faBallLabel.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBallLabelMapper.insertFaBallLabel(faBallLabel);
    }

    /**
     * 修改标签详情
     * 
     * @param faBallLabel 标签详情
     * @return 结果
     */
    @Override
    public int updateFaBallLabel(FaBallLabel faBallLabel)
    {
        faBallLabel.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBallLabelMapper.updateFaBallLabel(faBallLabel);
    }

    /**
     * 批量删除标签详情
     * 
     * @param ids 需要删除的标签详情ID
     * @return 结果
     */
    @Override
    public int deleteFaBallLabelByIds(Long[] ids)
    {
        return faBallLabelMapper.deleteFaBallLabelByIds(ids);
    }

    /**
     * 删除标签详情信息
     * 
     * @param id 标签详情ID
     * @return 结果
     */
    @Override
    public int deleteFaBallLabelById(Long id)
    {
        return faBallLabelMapper.deleteFaBallLabelById(id);
    }
}
