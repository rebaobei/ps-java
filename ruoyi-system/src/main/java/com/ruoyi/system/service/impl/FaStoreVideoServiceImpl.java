package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStoreVideoMapper;
import com.ruoyi.system.domain.FaStoreVideo;
import com.ruoyi.system.service.IFaStoreVideoService;

/**
 * 店铺视频Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-01
 */
@Service
public class FaStoreVideoServiceImpl implements IFaStoreVideoService 
{
    @Autowired
    private FaStoreVideoMapper faStoreVideoMapper;

    /**
     * 查询店铺视频
     * 
     * @param id 店铺视频ID
     * @return 店铺视频
     */
    @Override
    public FaStoreVideo selectFaStoreVideoById(Long id)
    {
        return faStoreVideoMapper.selectFaStoreVideoById(id);
    }

    /**
     * 查询店铺视频列表
     * 
     * @param faStoreVideo 店铺视频
     * @return 店铺视频
     */
    @Override
    public List<FaStoreVideo> selectFaStoreVideoList(FaStoreVideo faStoreVideo)
    {
        return faStoreVideoMapper.selectFaStoreVideoList(faStoreVideo);
    }

    /**
     * 新增店铺视频
     * 
     * @param faStoreVideo 店铺视频
     * @return 结果
     */
    @Override
    public int insertFaStoreVideo(FaStoreVideo faStoreVideo)
    {
        faStoreVideo.setCreateTime(DateUtils.getNowDate());
        return faStoreVideoMapper.insertFaStoreVideo(faStoreVideo);
    }

    /**
     * 修改店铺视频
     * 
     * @param faStoreVideo 店铺视频
     * @return 结果
     */
    @Override
    public int updateFaStoreVideo(FaStoreVideo faStoreVideo)
    {
        faStoreVideo.setUpdateTime(DateUtils.getNowDate());
        return faStoreVideoMapper.updateFaStoreVideo(faStoreVideo);
    }

    /**
     * 批量删除店铺视频
     * 
     * @param ids 需要删除的店铺视频ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreVideoByIds(Long[] ids)
    {
        return faStoreVideoMapper.deleteFaStoreVideoByIds(ids);
    }

    /**
     * 删除店铺视频信息
     * 
     * @param id 店铺视频ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreVideoById(Long id)
    {
        return faStoreVideoMapper.deleteFaStoreVideoById(id);
    }
}
