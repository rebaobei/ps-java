package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaFeedback;
import com.ruoyi.system.domain.vo.FeedBackVo;

/**
 * 反馈管理Service接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface IFaFeedbackService 
{
    /**
     * 查询反馈管理
     * 
     * @param id 反馈管理ID
     * @return 反馈管理
     */
    public FaFeedback selectFaFeedbackById(Long id);

    /**
     * 查询反馈管理列表
     * 
     * @param faFeedback 反馈管理
     * @return 反馈管理集合
     */
    public List<FaFeedback> selectFaFeedbackList(FaFeedback faFeedback);

    /**
     * 新增反馈管理
     * 
     * @param faFeedback 反馈管理
     * @return 结果
     */
    public int insertFaFeedback(FaFeedback faFeedback);

    /**
     * 修改反馈管理
     * 
     * @param faFeedback 反馈管理
     * @return 结果
     */
    public int updateFaFeedback(FaFeedback faFeedback);

    /**
     * 批量删除反馈管理
     * 
     * @param ids 需要删除的反馈管理ID
     * @return 结果
     */
    public int deleteFaFeedbackByIds(Long[] ids);

    /**
     * 删除反馈管理信息
     * 
     * @param id 反馈管理ID
     * @return 结果
     */
    public int deleteFaFeedbackById(Long id);

    List<FeedBackVo> listFeedbackVo(FeedBackVo faFeedback);
}
