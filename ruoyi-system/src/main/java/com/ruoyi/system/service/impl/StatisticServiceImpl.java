package com.ruoyi.system.service.impl;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.FaOrderPaylist;
import com.ruoyi.system.mapper.FaOrderPaylistMapper;
import com.ruoyi.system.mapper.FaUserMapper;
import com.ruoyi.system.mapper.FaXshopOrderMapper;
import com.ruoyi.system.mapper.StatisticMapper;
import com.ruoyi.system.service.IStatisticService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Description
 * @Author fan
 * @Date 2021/8/8
 **/
@Service
public class StatisticServiceImpl implements IStatisticService {

    @Autowired
    private FaUserMapper faUserMapper;

    @Autowired
    private FaOrderPaylistMapper faOrderPaylistMapper;

    @Autowired
    private FaXshopOrderMapper orderMapper;

    @Autowired
    private StatisticMapper statisticMapper;

    /**
     * 图表数据
     * @return 图表数据
     */
    @Override
    public Map<String, Object> getPanelInfo() {
        Map<String, Object> result = new HashMap<>();
        Long todayZero = MyDateUtils.getTodayZeroTime();
        Long monthZero = MyDateUtils.getMothZeroTime();
        Long weekZero = MyDateUtils.getWeekZeroTime();

        Integer sumRegCount = faUserMapper.getCount(null);
        Integer todayRegCount = faUserMapper.getCount(todayZero);
        BigDecimal sumMallMoney = faOrderPaylistMapper.getSumMallMoney(null);
        BigDecimal todayMallMoney = faOrderPaylistMapper.getSumMallMoney(todayZero);
        Integer todayActiveUser = faUserMapper.getActiveUserCount(todayZero);
        Integer weekActiveUser = faUserMapper.getActiveUserCount(weekZero);
        Integer monthActiveUser = faUserMapper.getActiveUserCount(monthZero);

        //日周月活跃用户
        result.put("todayActiveUser",todayActiveUser);
        result.put("weekActiveUser",weekActiveUser);
        result.put("monthActiveUser",monthActiveUser);

        //总会员
        result.put("sumRegCount", sumRegCount);
        //今日注册量
        result.put("todayRegCount", todayRegCount);
        //商城订单总额
        result.put("sumMallMoney", sumMallMoney);
        //今日商城订单额
        result.put("todayMallMoney", todayMallMoney);
        //本月注册用户
        result.put("monthRegCount",faUserMapper.getCount(monthZero));

        //今日充值用户数
        result.put("todayRechargeCount",statisticMapper.chargeUserCount(todayZero));
        //本周充值用户数
        result.put("weekRechargeCount",statisticMapper.chargeUserCount(weekZero));
        //本月充值用户数
        result.put("monthRechargeCount",statisticMapper.chargeUserCount(monthZero));

        //预约订单总额
        result.put("yuyueOrderMoney",statisticMapper.yuyueOrderMoney(null));
        //今日预约订单额
        result.put("todayYuyueOrderMoney",statisticMapper.yuyueOrderMoney(todayZero));

        //升级订单总额
        result.put("leverUpOrderMoney",statisticMapper.levelUpOrderMoney(null));
        //今日升级订单总额
        result.put("todayLeverUpOrderMoney",statisticMapper.levelUpOrderMoney(todayZero));


        //今日充值额
        result.put("todayRechargeMoney",statisticMapper.chargeMoney(todayZero));
        //本周充值额
        result.put("weekRechargeMoney",statisticMapper.chargeMoney(weekZero));
        //本月充值额
        result.put("monthRechargeMoney",statisticMapper.chargeMoney(monthZero));

        //消费总额
        result.put("consumeMoney",statisticMapper.consumeMoney(null));
        //消费用户数
        result.put("consumeCount",statisticMapper.consumeUserCount(null));


        return result;
    }

    /**
     * 过去7天的订单量 销售额
     *
     * @return
     */
    @Override
    public Map<String, Object> getLineChartInfo() {
        Map<String, Object> result = new HashMap<>();
        List<BigDecimal> daySumMoney = new ArrayList<>();
        List<Integer> daySumOrder = new ArrayList<>();
        List<String> day = new ArrayList<>();
        for (int i = -7; i < 0; i++) {
            Map<String, Long> map = new HashMap<>();
            map.put("startTime", MyDateUtils.getPreDayZeroTime(i));
            map.put("endTime", MyDateUtils.getPreDayZeroTime(i + 1));
            day.add(MyDateUtils.getPreDay(i));
            BigDecimal sumMallMoneyBetween = faOrderPaylistMapper.getSumMallMoneyBetween(map);
            if (sumMallMoneyBetween == null){
                sumMallMoneyBetween = BigDecimal.valueOf(0L);
            }
            daySumMoney.add(sumMallMoneyBetween);
            daySumOrder.add(orderMapper.getOrderCount(map));
        }
        result.put("date", day);
        result.put("daySumMoney", daySumMoney);
        result.put("daySumOrder", daySumOrder);
        return result;
    }
}
