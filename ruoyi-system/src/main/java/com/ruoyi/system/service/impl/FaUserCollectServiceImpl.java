package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserCollectMapper;
import com.ruoyi.system.domain.FaUserCollect;
import com.ruoyi.system.service.IFaUserCollectService;

/**
 * 会员收藏Service业务层处理
 * 
 * @author fan
 * @date 2021-07-14
 */
@Service
public class FaUserCollectServiceImpl implements IFaUserCollectService 
{
    @Autowired
    private FaUserCollectMapper faUserCollectMapper;

    /**
     * 查询会员收藏
     * 
     * @param id 会员收藏ID
     * @return 会员收藏
     */
    @Override
    public FaUserCollect selectFaUserCollectById(Long id)
    {
        return faUserCollectMapper.selectFaUserCollectById(id);
    }

    /**
     * 查询会员收藏列表
     * 
     * @param faUserCollect 会员收藏
     * @return 会员收藏
     */
    @Override
    public List<FaUserCollect> selectFaUserCollectList(FaUserCollect faUserCollect)
    {
        return faUserCollectMapper.selectFaUserCollectList(faUserCollect);
    }

    /**
     * 新增会员收藏
     * 
     * @param faUserCollect 会员收藏
     * @return 结果
     */
    @Override
    public int insertFaUserCollect(FaUserCollect faUserCollect)
    {
        return faUserCollectMapper.insertFaUserCollect(faUserCollect);
    }

    /**
     * 修改会员收藏
     * 
     * @param faUserCollect 会员收藏
     * @return 结果
     */
    @Override
    public int updateFaUserCollect(FaUserCollect faUserCollect)
    {
        return faUserCollectMapper.updateFaUserCollect(faUserCollect);
    }

    /**
     * 批量删除会员收藏
     * 
     * @param ids 需要删除的会员收藏ID
     * @return 结果
     */
    @Override
    public int deleteFaUserCollectByIds(Long[] ids)
    {
        return faUserCollectMapper.deleteFaUserCollectByIds(ids);
    }

    /**
     * 删除会员收藏信息
     * 
     * @param id 会员收藏ID
     * @return 结果
     */
    @Override
    public int deleteFaUserCollectById(Long id)
    {
        return faUserCollectMapper.deleteFaUserCollectById(id);
    }
}
