package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.vo.MsgLogWithUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopmsgMessagesMapper;
import com.ruoyi.system.domain.FaXshopmsgMessages;
import com.ruoyi.system.service.IFaXshopmsgMessagesService;

/**
 * 消息管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaXshopmsgMessagesServiceImpl implements IFaXshopmsgMessagesService 
{
    @Autowired
    private FaXshopmsgMessagesMapper faXshopmsgMessagesMapper;

    /**
     * 查询消息管理
     * 
     * @param id 消息管理ID
     * @return 消息管理
     */
    @Override
    public FaXshopmsgMessages selectFaXshopmsgMessagesById(Long id)
    {
        return faXshopmsgMessagesMapper.selectFaXshopmsgMessagesById(id);
    }

    /**
     * 查询消息管理列表
     * 
     * @param faXshopmsgMessages 消息管理
     * @return 消息管理
     */
    @Override
    public List<FaXshopmsgMessages> selectFaXshopmsgMessagesList(FaXshopmsgMessages faXshopmsgMessages)
    {
        return faXshopmsgMessagesMapper.selectFaXshopmsgMessagesList(faXshopmsgMessages);
    }

    /**
     * 新增消息管理
     * 
     * @param faXshopmsgMessages 消息管理
     * @return 结果
     */
    @Override
    public int insertFaXshopmsgMessages(FaXshopmsgMessages faXshopmsgMessages)
    {
        faXshopmsgMessages.setCreateTime(DateUtils.getNowDate());
        return faXshopmsgMessagesMapper.insertFaXshopmsgMessages(faXshopmsgMessages);
    }

    /**
     * 修改消息管理
     * 
     * @param faXshopmsgMessages 消息管理
     * @return 结果
     */
    @Override
    public int updateFaXshopmsgMessages(FaXshopmsgMessages faXshopmsgMessages)
    {
        return faXshopmsgMessagesMapper.updateFaXshopmsgMessages(faXshopmsgMessages);
    }

    /**
     * 批量删除消息管理
     * 
     * @param ids 需要删除的消息管理ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopmsgMessagesByIds(Long[] ids)
    {
        return faXshopmsgMessagesMapper.deleteFaXshopmsgMessagesByIds(ids);
    }

    /**
     * 删除消息管理信息
     * 
     * @param id 消息管理ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopmsgMessagesById(Long id)
    {
        return faXshopmsgMessagesMapper.deleteFaXshopmsgMessagesById(id);
    }

    @Override
    public List<MsgLogWithUserInfo> listMsgLog(FaXshopmsgMessages messages) {
        return faXshopmsgMessagesMapper.listMsgLog(messages);
    }
}
