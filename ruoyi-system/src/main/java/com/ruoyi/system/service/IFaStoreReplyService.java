package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaStoreReply;

/**
 * 店铺问题回复Service接口
 * 
 * @author ruoyi
 * @date 2021-11-21
 */
public interface IFaStoreReplyService 
{
    /**
     * 查询店铺问题回复
     * 
     * @param id 店铺问题回复ID
     * @return 店铺问题回复
     */
    public FaStoreReply selectFaStoreReplyById(Long id);

    /**
     * 查询店铺问题回复列表
     * 
     * @param faStoreReply 店铺问题回复
     * @return 店铺问题回复集合
     */
    public List<FaStoreReply> selectFaStoreReplyList(FaStoreReply faStoreReply);

    /**
     * 新增店铺问题回复
     * 
     * @param faStoreReply 店铺问题回复
     * @return 结果
     */
    public int insertFaStoreReply(FaStoreReply faStoreReply);

    /**
     * 修改店铺问题回复
     * 
     * @param faStoreReply 店铺问题回复
     * @return 结果
     */
    public int updateFaStoreReply(FaStoreReply faStoreReply);

    /**
     * 批量删除店铺问题回复
     * 
     * @param ids 需要删除的店铺问题回复ID
     * @return 结果
     */
    public int deleteFaStoreReplyByIds(Long[] ids);

    /**
     * 删除店铺问题回复信息
     * 
     * @param id 店铺问题回复ID
     * @return 结果
     */
    public int deleteFaStoreReplyById(Long id);
}
