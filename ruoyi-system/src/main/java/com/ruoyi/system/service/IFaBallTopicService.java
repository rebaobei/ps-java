package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaBallTopic;
import com.ruoyi.system.domain.vo.TopicVo;

/**
 * 话题管理Service接口
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
public interface IFaBallTopicService 
{
    /**
     * 查询话题管理
     * 
     * @param id 话题管理ID
     * @return 话题管理
     */
    public FaBallTopic selectFaBallTopicById(Long id);

    /**
     * 查询话题管理列表
     * 
     * @param faBallTopic 话题管理
     * @return 话题管理集合
     */
    public List<FaBallTopic> selectFaBallTopicList(FaBallTopic faBallTopic);

    /**
     * 新增话题管理
     * 
     * @param faBallTopic 话题管理
     * @return 结果
     */
    public int insertFaBallTopic(FaBallTopic faBallTopic);

    /**
     * 修改话题管理
     * 
     * @param faBallTopic 话题管理
     * @return 结果
     */
    public int updateFaBallTopic(FaBallTopic faBallTopic);

    /**
     * 批量删除话题管理
     * 
     * @param ids 需要删除的话题管理ID
     * @return 结果
     */
    public int deleteFaBallTopicByIds(Long[] ids);

    /**
     * 删除话题管理信息
     * 
     * @param id 话题管理ID
     * @return 结果
     */
    public int deleteFaBallTopicById(Long id);

    List<TopicVo> listTopicVo(FaBallTopic faBallTopic);
}
