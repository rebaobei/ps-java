package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaXshopCategory;

/**
 * 商品分类Service接口
 * 
 * @author ruoyi
 * @date 2021-07-16
 */
public interface IFaXshopCategoryService 
{
    /**
     * 查询商品分类
     * 
     * @param id 商品分类ID
     * @return 商品分类
     */
    public FaXshopCategory selectFaXshopCategoryById(Integer id);

    /**
     * 查询商品分类列表
     * 
     * @param faXshopCategory 商品分类
     * @return 商品分类集合
     */
    public List<FaXshopCategory> selectFaXshopCategoryList(FaXshopCategory faXshopCategory);

    /**
     * 新增商品分类
     * 
     * @param faXshopCategory 商品分类
     * @return 结果
     */
    public int insertFaXshopCategory(FaXshopCategory faXshopCategory);

    /**
     * 修改商品分类
     * 
     * @param faXshopCategory 商品分类
     * @return 结果
     */
    public int updateFaXshopCategory(FaXshopCategory faXshopCategory);

    /**
     * 批量删除商品分类
     * 
     * @param ids 需要删除的商品分类ID
     * @return 结果
     */
    public int deleteFaXshopCategoryByIds(Integer[] ids);

    /**
     * 删除商品分类信息
     * 
     * @param id 商品分类ID
     * @return 结果
     */
    public int deleteFaXshopCategoryById(Integer id);
}
