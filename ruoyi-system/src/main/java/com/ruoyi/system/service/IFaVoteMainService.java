package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaVoteMain;
import com.ruoyi.system.domain.vo.VoteMainVo;

/**
 * 投票管理Service接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface IFaVoteMainService 
{
    /**
     * 查询投票管理
     * 
     * @param id 投票管理ID
     * @return 投票管理
     */
    public FaVoteMain selectFaVoteMainById(Long id);

    /**
     * 查询投票管理列表
     * 
     * @param faVoteMain 投票管理
     * @return 投票管理集合
     */
    public List<FaVoteMain> selectFaVoteMainList(FaVoteMain faVoteMain);

    /**
     * 新增投票管理
     * 
     * @param faVoteMain 投票管理
     * @return 结果
     */
    public int insertFaVoteMain(FaVoteMain faVoteMain);

    /**
     * 修改投票管理
     * 
     * @param faVoteMain 投票管理
     * @return 结果
     */
    public int updateFaVoteMain(FaVoteMain faVoteMain);

    /**
     * 批量删除投票管理
     * 
     * @param ids 需要删除的投票管理ID
     * @return 结果
     */
    public int deleteFaVoteMainByIds(Long[] ids);

    /**
     * 删除投票管理信息
     * 
     * @param id 投票管理ID
     * @return 结果
     */
    public int deleteFaVoteMainById(Long id);

    List<VoteMainVo> listVoteMainVo(FaVoteMain faVoteMain);

    VoteMainVo getVoteMainVoById(Long id);
}
