package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.FeedBackVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaFeedbackMapper;
import com.ruoyi.system.domain.FaFeedback;
import com.ruoyi.system.service.IFaFeedbackService;

/**
 * 反馈管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaFeedbackServiceImpl implements IFaFeedbackService 
{
    @Autowired
    private FaFeedbackMapper faFeedbackMapper;

    /**
     * 查询反馈管理
     * 
     * @param id 反馈管理ID
     * @return 反馈管理
     */
    @Override
    public FaFeedback selectFaFeedbackById(Long id)
    {
        return faFeedbackMapper.selectFaFeedbackById(id);
    }

    /**
     * 查询反馈管理列表
     * 
     * @param faFeedback 反馈管理
     * @return 反馈管理
     */
    @Override
    public List<FaFeedback> selectFaFeedbackList(FaFeedback faFeedback)
    {
        return faFeedbackMapper.selectFaFeedbackList(faFeedback);
    }

    /**
     * 新增反馈管理
     * 
     * @param faFeedback 反馈管理
     * @return 结果
     */
    @Override
    public int insertFaFeedback(FaFeedback faFeedback)
    {
        return faFeedbackMapper.insertFaFeedback(faFeedback);
    }

    /**
     * 修改反馈管理
     * 
     * @param faFeedback 反馈管理
     * @return 结果
     */
    @Override
    public int updateFaFeedback(FaFeedback faFeedback)
    {
        return faFeedbackMapper.updateFaFeedback(faFeedback);
    }

    /**
     * 批量删除反馈管理
     * 
     * @param ids 需要删除的反馈管理ID
     * @return 结果
     */
    @Override
    public int deleteFaFeedbackByIds(Long[] ids)
    {
        return faFeedbackMapper.deleteFaFeedbackByIds(ids);
    }

    /**
     * 删除反馈管理信息
     * 
     * @param id 反馈管理ID
     * @return 结果
     */
    @Override
    public int deleteFaFeedbackById(Long id)
    {
        return faFeedbackMapper.deleteFaFeedbackById(id);
    }

    @Override
    public List<FeedBackVo> listFeedbackVo(FeedBackVo faFeedback) {
        return faFeedbackMapper.listFeedbackVo(faFeedback);
    }
}
