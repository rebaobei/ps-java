package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaCmsFlag;

/**
 * 文章标志Service接口
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
public interface IFaCmsFlagService 
{
    /**
     * 查询文章标志
     * 
     * @param id 文章标志ID
     * @return 文章标志
     */
    public FaCmsFlag selectFaCmsFlagById(Long id);

    /**
     * 查询文章标志列表
     * 
     * @param faCmsFlag 文章标志
     * @return 文章标志集合
     */
    public List<FaCmsFlag> selectFaCmsFlagList(FaCmsFlag faCmsFlag);

    /**
     * 新增文章标志
     * 
     * @param faCmsFlag 文章标志
     * @return 结果
     */
    public int insertFaCmsFlag(FaCmsFlag faCmsFlag);

    /**
     * 修改文章标志
     * 
     * @param faCmsFlag 文章标志
     * @return 结果
     */
    public int updateFaCmsFlag(FaCmsFlag faCmsFlag);

    /**
     * 批量删除文章标志
     * 
     * @param ids 需要删除的文章标志ID
     * @return 结果
     */
    public int deleteFaCmsFlagByIds(Long[] ids);

    /**
     * 删除文章标志信息
     * 
     * @param id 文章标志ID
     * @return 结果
     */
    public int deleteFaCmsFlagById(Long id);
}
