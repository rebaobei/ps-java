package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGamePetsPostureMapper;
import com.ruoyi.system.domain.FaGamePetsPosture;
import com.ruoyi.system.service.IFaGamePetsPostureService;

/**
 * 宠物姿势Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGamePetsPostureServiceImpl implements IFaGamePetsPostureService 
{
    @Autowired
    private FaGamePetsPostureMapper faGamePetsPostureMapper;

    /**
     * 查询宠物姿势
     * 
     * @param id 宠物姿势ID
     * @return 宠物姿势
     */
    @Override
    public FaGamePetsPosture selectFaGamePetsPostureById(Long id)
    {
        return faGamePetsPostureMapper.selectFaGamePetsPostureById(id);
    }

    /**
     * 查询宠物姿势列表
     * 
     * @param faGamePetsPosture 宠物姿势
     * @return 宠物姿势
     */
    @Override
    public List<FaGamePetsPosture> selectFaGamePetsPostureList(FaGamePetsPosture faGamePetsPosture)
    {
        return faGamePetsPostureMapper.selectFaGamePetsPostureList(faGamePetsPosture);
    }

    /**
     * 新增宠物姿势
     * 
     * @param faGamePetsPosture 宠物姿势
     * @return 结果
     */
    @Override
    public int insertFaGamePetsPosture(FaGamePetsPosture faGamePetsPosture)
    {
        return faGamePetsPostureMapper.insertFaGamePetsPosture(faGamePetsPosture);
    }

    /**
     * 修改宠物姿势
     * 
     * @param faGamePetsPosture 宠物姿势
     * @return 结果
     */
    @Override
    public int updateFaGamePetsPosture(FaGamePetsPosture faGamePetsPosture)
    {
        return faGamePetsPostureMapper.updateFaGamePetsPosture(faGamePetsPosture);
    }

    /**
     * 批量删除宠物姿势
     * 
     * @param ids 需要删除的宠物姿势ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsPostureByIds(Long[] ids)
    {
        return faGamePetsPostureMapper.deleteFaGamePetsPostureByIds(ids);
    }

    /**
     * 删除宠物姿势信息
     * 
     * @param id 宠物姿势ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsPostureById(Long id)
    {
        return faGamePetsPostureMapper.deleteFaGamePetsPostureById(id);
    }
}
