package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaBaomingUser;
import com.ruoyi.system.domain.vo.BaomingUserVo;

/**
 * 报名管理Service接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface IFaBaomingUserService 
{
    /**
     * 查询报名管理
     * 
     * @param id 报名管理ID
     * @return 报名管理
     */
    public FaBaomingUser selectFaBaomingUserById(Long id);

    /**
     * 查询报名管理列表
     * 
     * @param faBaomingUser 报名管理
     * @return 报名管理集合
     */
    public List<FaBaomingUser> selectFaBaomingUserList(FaBaomingUser faBaomingUser);

    /**
     * 新增报名管理
     * 
     * @param faBaomingUser 报名管理
     * @return 结果
     */
    public int insertFaBaomingUser(FaBaomingUser faBaomingUser);

    /**
     * 修改报名管理
     * 
     * @param faBaomingUser 报名管理
     * @return 结果
     */
    public int updateFaBaomingUser(FaBaomingUser faBaomingUser);

    /**
     * 批量删除报名管理
     * 
     * @param ids 需要删除的报名管理ID
     * @return 结果
     */
    public int deleteFaBaomingUserByIds(Long[] ids);

    /**
     * 删除报名管理信息
     * 
     * @param id 报名管理ID
     * @return 结果
     */
    public int deleteFaBaomingUserById(Long id);

    List<BaomingUserVo> listBaomingUserVo(BaomingUserVo faBaomingUser);
}
