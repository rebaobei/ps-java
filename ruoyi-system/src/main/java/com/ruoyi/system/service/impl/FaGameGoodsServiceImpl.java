package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.FaGameThings;
import com.ruoyi.system.domain.vo.GameGoodsVo;
import com.ruoyi.system.mapper.FaGameThingsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameGoodsMapper;
import com.ruoyi.system.domain.FaGameGoods;
import com.ruoyi.system.service.IFaGameGoodsService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商品管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameGoodsServiceImpl implements IFaGameGoodsService {

    private static Long DECORATION_THINGS = 2L;
    @Autowired
    private FaGameGoodsMapper faGameGoodsMapper;

    @Autowired
    private FaGameThingsMapper faGameThingsMapper;

    /**
     * 查询商品管理
     *
     * @param id 商品管理ID
     * @return 商品管理
     */
    @Override
    public GameGoodsVo selectFaGameGoodsById(Long id) {
        return faGameGoodsMapper.selectFaGameGoodsById(id);
    }

    /**
     * 查询商品管理列表
     *
     * @param faGameGoods 商品管理
     * @return 商品管理
     */
    @Override
    public List<FaGameGoods> selectFaGameGoodsList(FaGameGoods faGameGoods) {
        return faGameGoodsMapper.selectFaGameGoodsList(faGameGoods);
    }

    /**
     * 新增商品管理
     *
     * @return 结果
     */
    @Override
    @Transactional
    public int insertFaGameGoods(GameGoodsVo vo) {
        FaGameThings things = new FaGameThings();
        FaGameGoods goods = new FaGameGoods();
        things.setName(vo.getName());
        things.setCategoryId(vo.getCategoryId());
        things.setImage(vo.getImage());
        things.setPetType(vo.getPetType());
        things.setResult(vo.getResult());
        things.setGrowthPoint(vo.getGrowthPoint());
        things.setDecCategory(vo.getDecCategory());
        //非装扮物品 装扮分类设为空
        if (!things.getCategoryId().equals(DECORATION_THINGS)){
            things.setDecCategory(null);
        }

        faGameThingsMapper.insertFaGameThings(things);

        goods.setShow(vo.getShow());
        goods.setCoinPrice(vo.getCoinPrice());

        goods.setThingsId(things.getId());

        goods.setStoreType(vo.getStoreType());
        goods.setStoreCount(vo.getStoreCount());
        goods.setName(vo.getName());
        goods.setImage(vo.getImage());
        return faGameGoodsMapper.insertFaGameGoods(goods);
    }

    /**
     * 修改商品管理
     *
     * @param faGameGoods 商品管理
     * @return 结果
     */
    @Override
    @Transactional
    public int updateFaGameGoods(GameGoodsVo faGameGoods) {
        FaGameThings faGameThings = faGameThingsMapper.selectFaGameThingsById(faGameGoods.getThingsId());
        if (faGameThings != null) {
            faGameThings.setImage(faGameGoods.getImage());
            faGameThings.setName(faGameGoods.getName());
            faGameThings.setCategoryId(faGameGoods.getCategoryId());
            faGameThings.setPetType(faGameGoods.getPetType());
            faGameThings.setResult(faGameGoods.getResult());
            faGameThings.setGrowthPoint(faGameGoods.getGrowthPoint());
            faGameThings.setDecCategory(faGameGoods.getDecCategory());
            //非装扮物品 装扮分类设为空
            if (!faGameThings.getCategoryId().equals(DECORATION_THINGS)){
                faGameThings.setDecCategory(null);
            }
            faGameThingsMapper.updateFaGameThings(faGameThings);
        }
        FaGameGoods goods = new FaGameGoods();
        goods.setId(faGameGoods.getId());
        goods.setShow(faGameGoods.getShow());
        goods.setCoinPrice(faGameGoods.getCoinPrice());
        goods.setStoreType(faGameGoods.getStoreType());
        goods.setStoreCount(faGameGoods.getStoreCount());
        goods.setName(faGameGoods.getName());
        goods.setImage(faGameGoods.getImage());
        return faGameGoodsMapper.updateFaGameGoods(goods);
    }

    /**
     * 批量删除商品管理
     *
     * @param ids 需要删除的商品管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGameGoodsByIds(Long[] ids) {
        return faGameGoodsMapper.deleteFaGameGoodsByIds(ids);
    }

    /**
     * 删除商品管理信息
     *
     * @param id 商品管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGameGoodsById(Long id) {
        return faGameGoodsMapper.deleteFaGameGoodsById(id);
    }

    @Override
    public List<GameGoodsVo> listGameGoodsVo(GameGoodsVo faGameGoods) {
        return faGameGoodsMapper.listGameGoodsVo(faGameGoods);
    }
}
