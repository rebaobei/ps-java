package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserTaskMapper;
import com.ruoyi.system.domain.FaUserTask;
import com.ruoyi.system.service.IFaUserTaskService;

/**
 * 任务系统设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUserTaskServiceImpl implements IFaUserTaskService 
{
    @Autowired
    private FaUserTaskMapper faUserTaskMapper;

    /**
     * 查询任务系统设置
     * 
     * @param id 任务系统设置ID
     * @return 任务系统设置
     */
    @Override
    public FaUserTask selectFaUserTaskById(Long id)
    {
        return faUserTaskMapper.selectFaUserTaskById(id);
    }

    /**
     * 查询任务系统设置列表
     * 
     * @param faUserTask 任务系统设置
     * @return 任务系统设置
     */
    @Override
    public List<FaUserTask> selectFaUserTaskList(FaUserTask faUserTask)
    {
        return faUserTaskMapper.selectFaUserTaskList(faUserTask);
    }

    /**
     * 新增任务系统设置
     * 
     * @param faUserTask 任务系统设置
     * @return 结果
     */
    @Override
    public int insertFaUserTask(FaUserTask faUserTask)
    {
        return faUserTaskMapper.insertFaUserTask(faUserTask);
    }

    /**
     * 修改任务系统设置
     * 
     * @param faUserTask 任务系统设置
     * @return 结果
     */
    @Override
    public int updateFaUserTask(FaUserTask faUserTask)
    {
        return faUserTaskMapper.updateFaUserTask(faUserTask);
    }

    /**
     * 批量删除任务系统设置
     * 
     * @param ids 需要删除的任务系统设置ID
     * @return 结果
     */
    @Override
    public int deleteFaUserTaskByIds(Long[] ids)
    {
        return faUserTaskMapper.deleteFaUserTaskByIds(ids);
    }

    /**
     * 删除任务系统设置信息
     * 
     * @param id 任务系统设置ID
     * @return 结果
     */
    @Override
    public int deleteFaUserTaskById(Long id)
    {
        return faUserTaskMapper.deleteFaUserTaskById(id);
    }
}
