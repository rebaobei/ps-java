package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBallLabelfenleiMapper;
import com.ruoyi.system.domain.FaBallLabelfenlei;
import com.ruoyi.system.service.IFaBallLabelfenleiService;

/**
 * 标签分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@Service
public class FaBallLabelfenleiServiceImpl implements IFaBallLabelfenleiService 
{
    @Autowired
    private FaBallLabelfenleiMapper faBallLabelfenleiMapper;

    /**
     * 查询标签分类
     * 
     * @param id 标签分类ID
     * @return 标签分类
     */
    @Override
    public FaBallLabelfenlei selectFaBallLabelfenleiById(Long id)
    {
        return faBallLabelfenleiMapper.selectFaBallLabelfenleiById(id);
    }

    /**
     * 查询标签分类列表
     * 
     * @param faBallLabelfenlei 标签分类
     * @return 标签分类
     */
    @Override
    public List<FaBallLabelfenlei> selectFaBallLabelfenleiList(FaBallLabelfenlei faBallLabelfenlei)
    {
        return faBallLabelfenleiMapper.selectFaBallLabelfenleiList(faBallLabelfenlei);
    }

    /**
     * 新增标签分类
     * 
     * @param faBallLabelfenlei 标签分类
     * @return 结果
     */
    @Override
    public int insertFaBallLabelfenlei(FaBallLabelfenlei faBallLabelfenlei)
    {
        faBallLabelfenlei.setCreatetime(MyDateUtils.nowTimestamp());
        faBallLabelfenlei.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBallLabelfenleiMapper.insertFaBallLabelfenlei(faBallLabelfenlei);
    }

    /**
     * 修改标签分类
     * 
     * @param faBallLabelfenlei 标签分类
     * @return 结果
     */
    @Override
    public int updateFaBallLabelfenlei(FaBallLabelfenlei faBallLabelfenlei)
    {
        faBallLabelfenlei.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBallLabelfenleiMapper.updateFaBallLabelfenlei(faBallLabelfenlei);
    }

    /**
     * 批量删除标签分类
     * 
     * @param ids 需要删除的标签分类ID
     * @return 结果
     */
    @Override
    public int deleteFaBallLabelfenleiByIds(Long[] ids)
    {
        return faBallLabelfenleiMapper.deleteFaBallLabelfenleiByIds(ids);
    }

    /**
     * 删除标签分类信息
     * 
     * @param id 标签分类ID
     * @return 结果
     */
    @Override
    public int deleteFaBallLabelfenleiById(Long id)
    {
        return faBallLabelfenleiMapper.deleteFaBallLabelfenleiById(id);
    }
}
