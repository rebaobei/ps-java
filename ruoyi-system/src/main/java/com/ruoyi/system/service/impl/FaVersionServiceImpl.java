package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaVersionMapper;
import com.ruoyi.system.domain.FaVersion;
import com.ruoyi.system.service.IFaVersionService;

/**
 * APP版本管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaVersionServiceImpl implements IFaVersionService 
{
    @Autowired
    private FaVersionMapper faVersionMapper;

    /**
     * 查询APP版本管理
     * 
     * @param id APP版本管理ID
     * @return APP版本管理
     */
    @Override
    public FaVersion selectFaVersionById(Long id)
    {
        return faVersionMapper.selectFaVersionById(id);
    }

    /**
     * 查询APP版本管理列表
     * 
     * @param faVersion APP版本管理
     * @return APP版本管理
     */
    @Override
    public List<FaVersion> selectFaVersionList(FaVersion faVersion)
    {
        return faVersionMapper.selectFaVersionList(faVersion);
    }

    /**
     * 新增APP版本管理
     * 
     * @param faVersion APP版本管理
     * @return 结果
     */
    @Override
    public int insertFaVersion(FaVersion faVersion)
    {
        faVersion.setCreatetime(MyDateUtils.nowTimestamp());
        faVersion.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVersionMapper.insertFaVersion(faVersion);
    }

    /**
     * 修改APP版本管理
     * 
     * @param faVersion APP版本管理
     * @return 结果
     */
    @Override
    public int updateFaVersion(FaVersion faVersion)
    {
        faVersion.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVersionMapper.updateFaVersion(faVersion);
    }

    /**
     * 批量删除APP版本管理
     * 
     * @param ids 需要删除的APP版本管理ID
     * @return 结果
     */
    @Override
    public int deleteFaVersionByIds(Long[] ids)
    {
        return faVersionMapper.deleteFaVersionByIds(ids);
    }

    /**
     * 删除APP版本管理信息
     * 
     * @param id APP版本管理ID
     * @return 结果
     */
    @Override
    public int deleteFaVersionById(Long id)
    {
        return faVersionMapper.deleteFaVersionById(id);
    }
}
