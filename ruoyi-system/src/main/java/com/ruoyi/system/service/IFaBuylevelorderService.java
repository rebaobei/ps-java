package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaBuylevelorder;
import com.ruoyi.system.domain.vo.BuyLevelOrderVo;

/**
 * 购买等级Service接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface IFaBuylevelorderService 
{
    /**
     * 查询购买等级
     * 
     * @param id 购买等级ID
     * @return 购买等级
     */
    public FaBuylevelorder selectFaBuylevelorderById(Long id);

    /**
     * 查询购买等级列表
     * 
     * @param faBuylevelorder 购买等级
     * @return 购买等级集合
     */
    public List<FaBuylevelorder> selectFaBuylevelorderList(FaBuylevelorder faBuylevelorder);

    /**
     * 新增购买等级
     * 
     * @param faBuylevelorder 购买等级
     * @return 结果
     */
    public int insertFaBuylevelorder(FaBuylevelorder faBuylevelorder);

    /**
     * 修改购买等级
     * 
     * @param faBuylevelorder 购买等级
     * @return 结果
     */
    public int updateFaBuylevelorder(FaBuylevelorder faBuylevelorder);

    /**
     * 批量删除购买等级
     * 
     * @param ids 需要删除的购买等级ID
     * @return 结果
     */
    public int deleteFaBuylevelorderByIds(Long[] ids);

    /**
     * 删除购买等级信息
     * 
     * @param id 购买等级ID
     * @return 结果
     */
    public int deleteFaBuylevelorderById(Long id);

    List<BuyLevelOrderVo> listBuyLevelOrderVo(BuyLevelOrderVo faBuylevelorder);
}
