package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaYuyueOrderMapper;
import com.ruoyi.system.domain.FaYuyueOrder;
import com.ruoyi.system.service.IFaYuyueOrderService;

/**
 * 预约订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaYuyueOrderServiceImpl implements IFaYuyueOrderService 
{
    @Autowired
    private FaYuyueOrderMapper faYuyueOrderMapper;

    /**
     * 查询预约订单
     * 
     * @param id 预约订单ID
     * @return 预约订单
     */
    @Override
    public FaYuyueOrder selectFaYuyueOrderById(Long id)
    {
        return faYuyueOrderMapper.selectFaYuyueOrderById(id);
    }

    /**
     * 查询预约订单列表
     * 
     * @param faYuyueOrder 预约订单
     * @return 预约订单
     */
    @Override
    public List<FaYuyueOrder> selectFaYuyueOrderList(FaYuyueOrder faYuyueOrder)
    {
        return faYuyueOrderMapper.selectFaYuyueOrderList(faYuyueOrder);
    }

    /**
     * 新增预约订单
     * 
     * @param faYuyueOrder 预约订单
     * @return 结果
     */
    @Override
    public int insertFaYuyueOrder(FaYuyueOrder faYuyueOrder)
    {
        faYuyueOrder.setCreatetime(MyDateUtils.nowTimestamp());
        faYuyueOrder.setUpdatetime(MyDateUtils.nowTimestamp());
        return faYuyueOrderMapper.insertFaYuyueOrder(faYuyueOrder);
    }

    /**
     * 修改预约订单
     * 
     * @param faYuyueOrder 预约订单
     * @return 结果
     */
    @Override
    public int updateFaYuyueOrder(FaYuyueOrder faYuyueOrder)
    {
        faYuyueOrder.setUpdatetime(MyDateUtils.nowTimestamp());
        return faYuyueOrderMapper.updateFaYuyueOrder(faYuyueOrder);
    }

    /**
     * 批量删除预约订单
     * 
     * @param ids 需要删除的预约订单ID
     * @return 结果
     */
    @Override
    public int deleteFaYuyueOrderByIds(Long[] ids)
    {
        return faYuyueOrderMapper.deleteFaYuyueOrderByIds(ids);
    }

    /**
     * 删除预约订单信息
     * 
     * @param id 预约订单ID
     * @return 结果
     */
    @Override
    public int deleteFaYuyueOrderById(Long id)
    {
        return faYuyueOrderMapper.deleteFaYuyueOrderById(id);
    }
}
