package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserBaoming;

/**
 * 活动报名Service接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface IFaUserBaomingService 
{
    /**
     * 查询活动报名
     * 
     * @param id 活动报名ID
     * @return 活动报名
     */
    public FaUserBaoming selectFaUserBaomingById(Integer id);

    /**
     * 查询活动报名列表
     * 
     * @param faUserBaoming 活动报名
     * @return 活动报名集合
     */
    public List<FaUserBaoming> selectFaUserBaomingList(FaUserBaoming faUserBaoming);

    /**
     * 新增活动报名
     * 
     * @param faUserBaoming 活动报名
     * @return 结果
     */
    public int insertFaUserBaoming(FaUserBaoming faUserBaoming);

    /**
     * 修改活动报名
     * 
     * @param faUserBaoming 活动报名
     * @return 结果
     */
    public int updateFaUserBaoming(FaUserBaoming faUserBaoming);

    /**
     * 批量删除活动报名
     * 
     * @param ids 需要删除的活动报名ID
     * @return 结果
     */
    public int deleteFaUserBaomingByIds(Integer[] ids);

    /**
     * 删除活动报名信息
     * 
     * @param id 活动报名ID
     * @return 结果
     */
    public int deleteFaUserBaomingById(Integer id);
}
