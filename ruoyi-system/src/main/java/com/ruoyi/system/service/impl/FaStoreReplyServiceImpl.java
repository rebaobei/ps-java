package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStoreReplyMapper;
import com.ruoyi.system.domain.FaStoreReply;
import com.ruoyi.system.service.IFaStoreReplyService;

/**
 * 店铺问题回复Service业务层处理
 *
 * @author ruoyi
 * @date 2021-11-21
 */
@Service
public class FaStoreReplyServiceImpl implements IFaStoreReplyService
{
    @Autowired
    private FaStoreReplyMapper faStoreReplyMapper;

    /**
     * 查询店铺问题回复
     *
     * @param id 店铺问题回复ID
     * @return 店铺问题回复
     */
    @Override
    public FaStoreReply selectFaStoreReplyById(Long id)
    {
        return faStoreReplyMapper.selectFaStoreReplyById(id);
    }

    /**
     * 查询店铺问题回复列表
     *
     * @param faStoreReply 店铺问题回复
     * @return 店铺问题回复
     */
    @Override
    public List<FaStoreReply> selectFaStoreReplyList(FaStoreReply faStoreReply)
    {
        return faStoreReplyMapper.selectFaStoreReplyList(faStoreReply);
    }

    /**
     * 新增店铺问题回复
     *
     * @param faStoreReply 店铺问题回复
     * @return 结果
     */
    @Override
    public int insertFaStoreReply(FaStoreReply faStoreReply)
    {
        faStoreReply.setCreateTime(DateUtils.getNowDate());
        return faStoreReplyMapper.insertFaStoreReply(faStoreReply);
    }

    /**
     * 修改店铺问题回复
     *
     * @param faStoreReply 店铺问题回复
     * @return 结果
     */
    @Override
    public int updateFaStoreReply(FaStoreReply faStoreReply)
    {
        faStoreReply.setUpdateTime(DateUtils.getNowDate());
        faStoreReply.setCheckDateTime(DateUtils.getNowDate());
        return faStoreReplyMapper.updateFaStoreReply(faStoreReply);
    }

    /**
     * 批量删除店铺问题回复
     *
     * @param ids 需要删除的店铺问题回复ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreReplyByIds(Long[] ids)
    {
        return faStoreReplyMapper.deleteFaStoreReplyByIds(ids);
    }

    /**
     * 删除店铺问题回复信息
     *
     * @param id 店铺问题回复ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreReplyById(Long id)
    {
        return faStoreReplyMapper.deleteFaStoreReplyById(id);
    }
}
