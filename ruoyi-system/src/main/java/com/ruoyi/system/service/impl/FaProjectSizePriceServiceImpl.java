package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.ProjectPriceSizeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaProjectSizePriceMapper;
import com.ruoyi.system.domain.FaProjectSizePrice;
import com.ruoyi.system.service.IFaProjectSizePriceService;

/**
 * 预约项目价格Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
@Service
public class FaProjectSizePriceServiceImpl implements IFaProjectSizePriceService 
{
    @Autowired
    private FaProjectSizePriceMapper faProjectSizePriceMapper;

    /**
     * 查询预约项目价格
     * 
     * @param id 预约项目价格ID
     * @return 预约项目价格
     */
    @Override
    public FaProjectSizePrice selectFaProjectSizePriceById(Long id)
    {
        return faProjectSizePriceMapper.selectFaProjectSizePriceById(id);
    }

    /**
     * 查询预约项目价格列表
     * 
     * @param faProjectSizePrice 预约项目价格
     * @return 预约项目价格
     */
    @Override
    public List<FaProjectSizePrice> selectFaProjectSizePriceList(FaProjectSizePrice faProjectSizePrice)
    {
        return faProjectSizePriceMapper.selectFaProjectSizePriceList(faProjectSizePrice);
    }

    /**
     * 新增预约项目价格
     * 
     * @param faProjectSizePrice 预约项目价格
     * @return 结果
     */
    @Override
    public int insertFaProjectSizePrice(FaProjectSizePrice faProjectSizePrice)
    {
        faProjectSizePrice.setCreatetime(System.currentTimeMillis()/1000L);
        faProjectSizePrice.setUpdatetime(System.currentTimeMillis()/1000L);
        return faProjectSizePriceMapper.insertFaProjectSizePrice(faProjectSizePrice);
    }

    /**
     * 修改预约项目价格
     * 
     * @param faProjectSizePrice 预约项目价格
     * @return 结果
     */
    @Override
    public int updateFaProjectSizePrice(FaProjectSizePrice faProjectSizePrice)
    {
        faProjectSizePrice.setUpdatetime(System.currentTimeMillis()/1000L);
        return faProjectSizePriceMapper.updateFaProjectSizePrice(faProjectSizePrice);
    }

    /**
     * 批量删除预约项目价格
     * 
     * @param ids 需要删除的预约项目价格ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectSizePriceByIds(Long[] ids)
    {
        return faProjectSizePriceMapper.deleteFaProjectSizePriceByIds(ids);
    }

    /**
     * 删除预约项目价格信息
     * 
     * @param id 预约项目价格ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectSizePriceById(Long id)
    {
        return faProjectSizePriceMapper.deleteFaProjectSizePriceById(id);
    }

    @Override
    public List<ProjectPriceSizeVo> listProjectSizeVo(FaProjectSizePrice projectSizePrice) {
        return faProjectSizePriceMapper.listProjectSizeVo(projectSizePrice);
    }
}
