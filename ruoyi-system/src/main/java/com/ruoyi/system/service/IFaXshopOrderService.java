package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.FaXshopOrder;
import com.ruoyi.system.domain.vo.XshopOrderVo;

/**
 * 商城订单Service接口
 *
 * @author ruoyi
 * @date 2021-08-03
 */
public interface IFaXshopOrderService
{
    /**
     * 查询商城订单
     *
     * @param id 商城订单ID
     * @return 商城订单
     */
    public FaXshopOrder selectFaXshopOrderById(Integer id);

    /**
     * 查询商城订单列表
     *
     * @param faXshopOrder 商城订单
     * @return 商城订单集合
     */
    public List<FaXshopOrder> selectFaXshopOrderList(FaXshopOrder faXshopOrder);

    /**
     * 新增商城订单
     *
     * @param faXshopOrder 商城订单
     * @return 结果
     */
    public int insertFaXshopOrder(FaXshopOrder faXshopOrder);

    /**
     * 修改商城订单
     *
     * @param faXshopOrder 商城订单
     * @return 结果
     */
    public int updateFaXshopOrder(FaXshopOrder faXshopOrder);

    /**
     * 批量删除商城订单
     *
     * @param ids 需要删除的商城订单ID
     * @return 结果
     */
    public int deleteFaXshopOrderByIds(Integer[] ids);

    /**
     * 删除商城订单信息
     *
     * @param id 商城订单ID
     * @return 结果
     */
    public int deleteFaXshopOrderById(Integer id);

    List<XshopOrderVo> listXshopOrderVo(XshopOrderVo faXshopOrder);


    Map<String,String> refundMoney(String id);
}
