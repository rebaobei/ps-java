package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserBaomingMapper;
import com.ruoyi.system.domain.FaUserBaoming;
import com.ruoyi.system.service.IFaUserBaomingService;

/**
 * 活动报名Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaUserBaomingServiceImpl implements IFaUserBaomingService 
{
    @Autowired
    private FaUserBaomingMapper faUserBaomingMapper;

    /**
     * 查询活动报名
     * 
     * @param id 活动报名ID
     * @return 活动报名
     */
    @Override
    public FaUserBaoming selectFaUserBaomingById(Integer id)
    {
        return faUserBaomingMapper.selectFaUserBaomingById(id);
    }

    /**
     * 查询活动报名列表
     * 
     * @param faUserBaoming 活动报名
     * @return 活动报名
     */
    @Override
    public List<FaUserBaoming> selectFaUserBaomingList(FaUserBaoming faUserBaoming)
    {
        return faUserBaomingMapper.selectFaUserBaomingList(faUserBaoming);
    }

    /**
     * 新增活动报名
     * 
     * @param faUserBaoming 活动报名
     * @return 结果
     */
    @Override
    public int insertFaUserBaoming(FaUserBaoming faUserBaoming)
    {
        return faUserBaomingMapper.insertFaUserBaoming(faUserBaoming);
    }

    /**
     * 修改活动报名
     * 
     * @param faUserBaoming 活动报名
     * @return 结果
     */
    @Override
    public int updateFaUserBaoming(FaUserBaoming faUserBaoming)
    {
        return faUserBaomingMapper.updateFaUserBaoming(faUserBaoming);
    }

    /**
     * 批量删除活动报名
     * 
     * @param ids 需要删除的活动报名ID
     * @return 结果
     */
    @Override
    public int deleteFaUserBaomingByIds(Integer[] ids)
    {
        return faUserBaomingMapper.deleteFaUserBaomingByIds(ids);
    }

    /**
     * 删除活动报名信息
     * 
     * @param id 活动报名ID
     * @return 结果
     */
    @Override
    public int deleteFaUserBaomingById(Integer id)
    {
        return faUserBaomingMapper.deleteFaUserBaomingById(id);
    }
}
