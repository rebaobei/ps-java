package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaChangelevelLog;

/**
 * 登记调整记录Service接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface IFaChangelevelLogService 
{
    /**
     * 查询登记调整记录
     * 
     * @param id 登记调整记录ID
     * @return 登记调整记录
     */
    public FaChangelevelLog selectFaChangelevelLogById(Long id);

    /**
     * 查询登记调整记录列表
     * 
     * @param faChangelevelLog 登记调整记录
     * @return 登记调整记录集合
     */
    public List<FaChangelevelLog> selectFaChangelevelLogList(FaChangelevelLog faChangelevelLog);

    /**
     * 新增登记调整记录
     * 
     * @param faChangelevelLog 登记调整记录
     * @return 结果
     */
    public int insertFaChangelevelLog(FaChangelevelLog faChangelevelLog);

    /**
     * 修改登记调整记录
     * 
     * @param faChangelevelLog 登记调整记录
     * @return 结果
     */
    public int updateFaChangelevelLog(FaChangelevelLog faChangelevelLog);

    /**
     * 批量删除登记调整记录
     * 
     * @param ids 需要删除的登记调整记录ID
     * @return 结果
     */
    public int deleteFaChangelevelLogByIds(Long[] ids);

    /**
     * 删除登记调整记录信息
     * 
     * @param id 登记调整记录ID
     * @return 结果
     */
    public int deleteFaChangelevelLogById(Long id);
}
