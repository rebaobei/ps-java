package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ruoyi.system.domain.vo.PetCategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaTechniciansMapper;
import com.ruoyi.system.domain.FaTechnicians;
import com.ruoyi.system.service.IFaTechniciansService;

import javax.lang.model.element.VariableElement;

/**
 * 宠物分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@Service
public class FaTechniciansServiceImpl implements IFaTechniciansService 
{
    @Autowired
    private FaTechniciansMapper faTechniciansMapper;

    /**
     * 查询宠物分类
     * 
     * @param id 宠物分类ID
     * @return 宠物分类
     */
    @Override
    public FaTechnicians selectFaTechniciansById(Long id)
    {
        return faTechniciansMapper.selectFaTechniciansById(id);
    }

    /**
     * 查询宠物分类列表
     * 
     * @param faTechnicians 宠物分类
     * @return 宠物分类
     */
    @Override
    public List<FaTechnicians> selectFaTechniciansList(FaTechnicians faTechnicians)
    {
        return faTechniciansMapper.selectFaTechniciansList(faTechnicians);
    }

    /**
     * 新增宠物分类
     * 
     * @param faTechnicians 宠物分类
     * @return 结果
     */
    @Override
    public int insertFaTechnicians(FaTechnicians faTechnicians)
    {
        faTechnicians.setCreatetime(System.currentTimeMillis()/1000L);
        faTechnicians.setUpdatetime(System.currentTimeMillis()/1000L);
        return faTechniciansMapper.insertFaTechnicians(faTechnicians);
    }

    /**
     * 修改宠物分类
     * 
     * @param faTechnicians 宠物分类
     * @return 结果
     */
    @Override
    public int updateFaTechnicians(FaTechnicians faTechnicians)
    {
        faTechnicians.setUpdatetime(System.currentTimeMillis()/1000L);
        return faTechniciansMapper.updateFaTechnicians(faTechnicians);
    }

    /**
     * 批量删除宠物分类
     * 
     * @param ids 需要删除的宠物分类ID
     * @return 结果
     */
    @Override
    public int deleteFaTechniciansByIds(Long[] ids)
    {
        return faTechniciansMapper.deleteFaTechniciansByIds(ids);
    }

    /**
     * 删除宠物分类信息
     * 
     * @param id 宠物分类ID
     * @return 结果
     */
    @Override
    public int deleteFaTechniciansById(Long id)
    {
        return faTechniciansMapper.deleteFaTechniciansById(id);
    }

    @Override
    public List<PetCategoryVo> listPetCategoty(FaTechnicians faTechnicians) {
        List<PetCategoryVo> result = new ArrayList<>();
        List<FaTechnicians> faTechnicianList = faTechniciansMapper.selectFaTechniciansList(faTechnicians);
        for (FaTechnicians item : faTechnicianList){
            PetCategoryVo petCategoryVo = new PetCategoryVo();
            petCategoryVo.setId(item.getId());
            petCategoryVo.setName(item.getName());
            petCategoryVo.setDescribe(item.getDescribe());
            petCategoryVo.setConcat(item.getConcat());
            petCategoryVo.setPrice(item.getPrice());
            petCategoryVo.setUpdatetime(item.getUpdatetime());
            petCategoryVo.setCreatetime(item.getCreatetime());
            petCategoryVo.setStatus(item.getStatus());
            petCategoryVo.setWeigh(item.getWeigh());
            if (item.getImages() != null && !item.getImages().isEmpty()){
                String[] split = item.getImages().split(",");
                petCategoryVo.setImages(Arrays.asList(split));
            }
            result.add(petCategoryVo);
        }
        return result;
    }
}
