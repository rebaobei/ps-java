package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaCmsModelMapper;
import com.ruoyi.system.domain.FaCmsModel;
import com.ruoyi.system.service.IFaCmsModelService;

/**
 * 内容模型Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
@Service
public class FaCmsModelServiceImpl implements IFaCmsModelService 
{
    @Autowired
    private FaCmsModelMapper faCmsModelMapper;

    /**
     * 查询内容模型
     * 
     * @param id 内容模型ID
     * @return 内容模型
     */
    @Override
    public FaCmsModel selectFaCmsModelById(Integer id)
    {
        return faCmsModelMapper.selectFaCmsModelById(id);
    }

    /**
     * 查询内容模型列表
     * 
     * @param faCmsModel 内容模型
     * @return 内容模型
     */
    @Override
    public List<FaCmsModel> selectFaCmsModelList(FaCmsModel faCmsModel)
    {
        return faCmsModelMapper.selectFaCmsModelList(faCmsModel);
    }

    /**
     * 新增内容模型
     * 
     * @param faCmsModel 内容模型
     * @return 结果
     */
    @Override
    public int insertFaCmsModel(FaCmsModel faCmsModel)
    {
        return faCmsModelMapper.insertFaCmsModel(faCmsModel);
    }

    /**
     * 修改内容模型
     * 
     * @param faCmsModel 内容模型
     * @return 结果
     */
    @Override
    public int updateFaCmsModel(FaCmsModel faCmsModel)
    {
        return faCmsModelMapper.updateFaCmsModel(faCmsModel);
    }

    /**
     * 批量删除内容模型
     * 
     * @param ids 需要删除的内容模型ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsModelByIds(Integer[] ids)
    {
        return faCmsModelMapper.deleteFaCmsModelByIds(ids);
    }

    /**
     * 删除内容模型信息
     * 
     * @param id 内容模型ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsModelById(Integer id)
    {
        return faCmsModelMapper.deleteFaCmsModelById(id);
    }
}
