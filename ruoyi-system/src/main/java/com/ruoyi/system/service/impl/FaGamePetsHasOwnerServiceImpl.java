package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.PetHasOwnerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGamePetsHasOwnerMapper;
import com.ruoyi.system.domain.FaGamePetsHasOwner;
import com.ruoyi.system.service.IFaGamePetsHasOwnerService;

/**
 * 已领养宠物Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGamePetsHasOwnerServiceImpl implements IFaGamePetsHasOwnerService 
{
    @Autowired
    private FaGamePetsHasOwnerMapper faGamePetsHasOwnerMapper;

    /**
     * 查询已领养宠物
     * 
     * @param id 已领养宠物ID
     * @return 已领养宠物
     */
    @Override
    public FaGamePetsHasOwner selectFaGamePetsHasOwnerById(Long id)
    {
        return faGamePetsHasOwnerMapper.selectFaGamePetsHasOwnerById(id);
    }

    /**
     * 查询已领养宠物列表
     * 
     * @param faGamePetsHasOwner 已领养宠物
     * @return 已领养宠物
     */
    @Override
    public List<FaGamePetsHasOwner> selectFaGamePetsHasOwnerList(FaGamePetsHasOwner faGamePetsHasOwner)
    {
        return faGamePetsHasOwnerMapper.selectFaGamePetsHasOwnerList(faGamePetsHasOwner);
    }

    /**
     * 新增已领养宠物
     * 
     * @param faGamePetsHasOwner 已领养宠物
     * @return 结果
     */
    @Override
    public int insertFaGamePetsHasOwner(FaGamePetsHasOwner faGamePetsHasOwner)
    {
        return faGamePetsHasOwnerMapper.insertFaGamePetsHasOwner(faGamePetsHasOwner);
    }

    /**
     * 修改已领养宠物
     * 
     * @param faGamePetsHasOwner 已领养宠物
     * @return 结果
     */
    @Override
    public int updateFaGamePetsHasOwner(FaGamePetsHasOwner faGamePetsHasOwner)
    {
        return faGamePetsHasOwnerMapper.updateFaGamePetsHasOwner(faGamePetsHasOwner);
    }

    /**
     * 批量删除已领养宠物
     * 
     * @param ids 需要删除的已领养宠物ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsHasOwnerByIds(Long[] ids)
    {
        return faGamePetsHasOwnerMapper.deleteFaGamePetsHasOwnerByIds(ids);
    }

    /**
     * 删除已领养宠物信息
     * 
     * @param id 已领养宠物ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsHasOwnerById(Long id)
    {
        return faGamePetsHasOwnerMapper.deleteFaGamePetsHasOwnerById(id);
    }

    @Override
    public List<PetHasOwnerVo> listPetHasOwnerVo(PetHasOwnerVo faGamePetsHasOwner) {
        return faGamePetsHasOwnerMapper.listPetHasOwnerVo(faGamePetsHasOwner);
    }


}
