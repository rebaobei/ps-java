package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.FaStoreInfo;
import com.ruoyi.system.mapper.FaStoreInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStoreTixianMapper;
import com.ruoyi.system.domain.FaStoreTixian;
import com.ruoyi.system.service.IFaStoreTixianService;

/**
 * 提现管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-02-09
 */
@Service
public class FaStoreTixianServiceImpl implements IFaStoreTixianService 
{
    @Autowired
    private FaStoreTixianMapper faStoreTixianMapper;
    @Autowired
    private FaStoreInfoMapper faStoreInfoMapper;
    /**
     * 查询提现管理
     * 
     * @param id 提现管理ID
     * @return 提现管理
     */
    @Override
    public FaStoreTixian selectFaStoreTixianById(Long id)
    {
        return faStoreTixianMapper.selectFaStoreTixianById(id);
    }

    /**
     * 查询提现管理列表
     * 
     * @param faStoreTixian 提现管理
     * @return 提现管理
     */
    @Override
    public List<FaStoreTixian> selectFaStoreTixianList(FaStoreTixian faStoreTixian)
    {
        return faStoreTixianMapper.selectFaStoreTixianList(faStoreTixian);
    }

    /**
     * 新增提现管理
     * 
     * @param faStoreTixian 提现管理
     * @return 结果
     */
    @Override
    public int insertFaStoreTixian(FaStoreTixian faStoreTixian)
    {
        BigDecimal money = faStoreTixian.getMoney();
        if(money.signum()!=1){
            throw new CustomException("提现金额不正确");
        }
        FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(faStoreTixian.getManystorePid());
        BigDecimal beforeAvailableBalance = faStoreInfo.getAmount();
        BigDecimal beforeFrozenBalance = faStoreInfo.getFrozenAmount();
        BigDecimal afterAvailableBalance = beforeAvailableBalance.subtract(money);
        if(afterAvailableBalance.signum()!=1){
            throw new CustomException("提现金额超出当前账户余额，当前账户余额为："+beforeAvailableBalance);
        }
        BigDecimal afterFrozenBalance = beforeFrozenBalance.add(money);
        faStoreInfo.setAmount(afterAvailableBalance);
        faStoreInfo.setFrozenAmount(afterFrozenBalance);
        int i = faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        faStoreTixian.setCreateTime(DateUtils.getNowDate());
        faStoreTixian.setUpdateTime(DateUtils.getNowDate());
        return faStoreTixianMapper.insertFaStoreTixian(faStoreTixian);
    }

    /**
     * 修改提现管理
     * 
     * @param vo 提现管理
     * @return 结果
     */
    @Override
    public int updateFaStoreTixian(FaStoreTixian vo)
    {
        FaStoreTixian faStoreTixian = faStoreTixianMapper.selectFaStoreTixianById(vo.getId());
        if(faStoreTixian.getStatus()==1&&vo.getStatus()==2){
            FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(faStoreTixian.getManystorePid());
            BigDecimal beforeFrozenBalance = faStoreInfo.getFrozenAmount();
            BigDecimal afterFrozenBalance = beforeFrozenBalance.subtract(faStoreTixian.getMoney());
            faStoreInfo.setFrozenAmount(afterFrozenBalance);
            int i = faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        }
        if(faStoreTixian.getStatus()==2&&vo.getStatus()==3){
            FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(faStoreTixian.getManystorePid());
            BigDecimal amount = faStoreInfo.getAmount();
            BigDecimal afterAmount = amount.add(faStoreTixian.getMoney());
            faStoreInfo.setAmount(afterAmount);
            int i = faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        } else if (vo.getStatus()==3){
            FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(faStoreTixian.getManystorePid());
            BigDecimal frozenAmount = faStoreInfo.getFrozenAmount();
            BigDecimal afterFrozenAmount = frozenAmount.add(faStoreTixian.getMoney());
            faStoreInfo.setAmount(afterFrozenAmount);
            int i = faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        }
        vo.setCreateTime(DateUtils.getNowDate());
        vo.setUpdateTime(DateUtils.getNowDate());
        return faStoreTixianMapper.updateFaStoreTixian(vo);
    }

    /**
     * 批量删除提现管理
     * 
     * @param ids 需要删除的提现管理ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreTixianByIds(Long[] ids)
    {
        return faStoreTixianMapper.deleteFaStoreTixianByIds(ids);
    }

    /**
     * 删除提现管理信息
     * 
     * @param id 提现管理ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreTixianById(Long id)
    {
        return faStoreTixianMapper.deleteFaStoreTixianById(id);
    }
}
