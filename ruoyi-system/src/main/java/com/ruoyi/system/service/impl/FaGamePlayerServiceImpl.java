package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.PlayerInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGamePlayerMapper;
import com.ruoyi.system.domain.FaGamePlayer;
import com.ruoyi.system.service.IFaGamePlayerService;

/**
 * 游戏玩家Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGamePlayerServiceImpl implements IFaGamePlayerService 
{
    @Autowired
    private FaGamePlayerMapper faGamePlayerMapper;

    /**
     * 查询游戏玩家
     * 
     * @param id 游戏玩家ID
     * @return 游戏玩家
     */
    @Override
    public FaGamePlayer selectFaGamePlayerById(Long id)
    {
        return faGamePlayerMapper.selectFaGamePlayerById(id);
    }

    /**
     * 查询游戏玩家列表
     * 
     * @param faGamePlayer 游戏玩家
     * @return 游戏玩家
     */
    @Override
    public List<FaGamePlayer> selectFaGamePlayerList(FaGamePlayer faGamePlayer)
    {
        return faGamePlayerMapper.selectFaGamePlayerList(faGamePlayer);
    }

    /**
     * 新增游戏玩家
     * 
     * @param faGamePlayer 游戏玩家
     * @return 结果
     */
    @Override
    public int insertFaGamePlayer(FaGamePlayer faGamePlayer)
    {
        return faGamePlayerMapper.insertFaGamePlayer(faGamePlayer);
    }

    /**
     * 修改游戏玩家
     * 
     * @param faGamePlayer 游戏玩家
     * @return 结果
     */
    @Override
    public int updateFaGamePlayer(FaGamePlayer faGamePlayer)
    {
        return faGamePlayerMapper.updateFaGamePlayer(faGamePlayer);
    }

    /**
     * 批量删除游戏玩家
     * 
     * @param ids 需要删除的游戏玩家ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePlayerByIds(Long[] ids)
    {
        return faGamePlayerMapper.deleteFaGamePlayerByIds(ids);
    }

    /**
     * 删除游戏玩家信息
     * 
     * @param id 游戏玩家ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePlayerById(Long id)
    {
        return faGamePlayerMapper.deleteFaGamePlayerById(id);
    }

    @Override
    public List<PlayerInfoVo> lsitPlayerInfoVo(PlayerInfoVo faGamePlayer) {
        return faGamePlayerMapper.lsitPlayerInfoVo(faGamePlayer);
    }
}
