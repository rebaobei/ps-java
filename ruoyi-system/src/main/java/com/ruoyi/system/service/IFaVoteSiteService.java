package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaVoteSite;

/**
 * 打赏设置Service接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface IFaVoteSiteService 
{
    /**
     * 查询打赏设置
     * 
     * @param id 打赏设置ID
     * @return 打赏设置
     */
    public FaVoteSite selectFaVoteSiteById(Long id);

    /**
     * 查询打赏设置列表
     * 
     * @param faVoteSite 打赏设置
     * @return 打赏设置集合
     */
    public List<FaVoteSite> selectFaVoteSiteList(FaVoteSite faVoteSite);

    /**
     * 新增打赏设置
     * 
     * @param faVoteSite 打赏设置
     * @return 结果
     */
    public int insertFaVoteSite(FaVoteSite faVoteSite);

    /**
     * 修改打赏设置
     * 
     * @param faVoteSite 打赏设置
     * @return 结果
     */
    public int updateFaVoteSite(FaVoteSite faVoteSite);

    /**
     * 批量删除打赏设置
     * 
     * @param ids 需要删除的打赏设置ID
     * @return 结果
     */
    public int deleteFaVoteSiteByIds(Long[] ids);

    /**
     * 删除打赏设置信息
     * 
     * @param id 打赏设置ID
     * @return 结果
     */
    public int deleteFaVoteSiteById(Long id);
}
