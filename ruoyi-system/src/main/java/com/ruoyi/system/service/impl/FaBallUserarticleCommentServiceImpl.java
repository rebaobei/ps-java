package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.FaBallUserarticle;
import com.ruoyi.system.domain.vo.UserArticleCommentVo;
import com.ruoyi.system.mapper.FaBallUserarticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBallUserarticleCommentMapper;
import com.ruoyi.system.domain.FaBallUserarticleComment;
import com.ruoyi.system.service.IFaBallUserarticleCommentService;

/**
 * 朋友圈评论Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@Service
public class FaBallUserarticleCommentServiceImpl implements IFaBallUserarticleCommentService 
{
    @Autowired
    private FaBallUserarticleCommentMapper faBallUserarticleCommentMapper;
    @Autowired
    private FaBallUserarticleMapper faBallUserarticleMapper;

    /**
     * 查询朋友圈评论
     * 
     * @param id 朋友圈评论ID
     * @return 朋友圈评论
     */
    @Override
    public FaBallUserarticleComment selectFaBallUserarticleCommentById(Long id)
    {
        return faBallUserarticleCommentMapper.selectFaBallUserarticleCommentById(id);
    }

    /**
     * 查询朋友圈评论列表
     * 
     * @param faBallUserarticleComment 朋友圈评论
     * @return 朋友圈评论
     */
    @Override
    public List<FaBallUserarticleComment> selectFaBallUserarticleCommentList(FaBallUserarticleComment faBallUserarticleComment)
    {
        return faBallUserarticleCommentMapper.selectFaBallUserarticleCommentList(faBallUserarticleComment);
    }

    /**
     * 新增朋友圈评论
     * 
     * @param faBallUserarticleComment 朋友圈评论
     * @return 结果
     */
    @Override
    public int insertFaBallUserarticleComment(FaBallUserarticleComment faBallUserarticleComment)
    {
        faBallUserarticleComment.setUpdatetime(MyDateUtils.nowTimestamp());
        faBallUserarticleComment.setCreatetime(MyDateUtils.nowTimestamp());
        return faBallUserarticleCommentMapper.insertFaBallUserarticleComment(faBallUserarticleComment);
    }

    /**
     * 修改朋友圈评论
     * 
     * @param faBallUserarticleComment 朋友圈评论
     * @return 结果
     */
    @Override
    public int updateFaBallUserarticleComment(FaBallUserarticleComment faBallUserarticleComment)
    {
        faBallUserarticleComment.setUpdatetime(MyDateUtils.nowTimestamp());
        if("1".equals(faBallUserarticleComment.getStatus())){
            Long userarticleId = faBallUserarticleComment.getUserarticleId();
            FaBallUserarticle faBallUserarticle = faBallUserarticleMapper.selectFaBallUserarticleById(userarticleId);
            faBallUserarticle.setComments(faBallUserarticle.getComments()-1);
        }else if( "0".equals(faBallUserarticleComment.getStatus())){
            Long userarticleId = faBallUserarticleComment.getUserarticleId();
            FaBallUserarticle faBallUserarticle = faBallUserarticleMapper.selectFaBallUserarticleById(userarticleId);
            faBallUserarticle.setComments(faBallUserarticle.getComments()+1);
        }
        return faBallUserarticleCommentMapper.updateFaBallUserarticleComment(faBallUserarticleComment);
    }

    /**
     * 批量删除朋友圈评论
     * 
     * @param ids 需要删除的朋友圈评论ID
     * @return 结果
     */
    @Override
    public int deleteFaBallUserarticleCommentByIds(Long[] ids)
    {
        return faBallUserarticleCommentMapper.deleteFaBallUserarticleCommentByIds(ids);
    }

    /**
     * 删除朋友圈评论信息
     * 
     * @param id 朋友圈评论ID
     * @return 结果
     */
    @Override
    public int deleteFaBallUserarticleCommentById(Long id)
    {
        return faBallUserarticleCommentMapper.deleteFaBallUserarticleCommentById(id);
    }

    @Override
    public List<UserArticleCommentVo> listUserArticleCommentVo(FaBallUserarticleComment faBallUserarticleComment) {
        return faBallUserarticleCommentMapper.listArticleCommentVo(faBallUserarticleComment);
    }
}
