package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserScoreLog;
import com.ruoyi.system.domain.vo.ScoreLogWithUserInfo;

/**
 * 积分变动Service接口
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
public interface IFaUserScoreLogService 
{
    /**
     * 查询积分变动
     * 
     * @param id 积分变动ID
     * @return 积分变动
     */
    public FaUserScoreLog selectFaUserScoreLogById(Integer id);

    /**
     * 查询积分变动列表
     * 
     * @param faUserScoreLog 积分变动
     * @return 积分变动集合
     */
    public List<FaUserScoreLog> selectFaUserScoreLogList(FaUserScoreLog faUserScoreLog);

    /**
     * 新增积分变动
     * 
     * @param faUserScoreLog 积分变动
     * @return 结果
     */
    public int insertFaUserScoreLog(FaUserScoreLog faUserScoreLog);

    /**
     * 修改积分变动
     * 
     * @param faUserScoreLog 积分变动
     * @return 结果
     */
    public int updateFaUserScoreLog(FaUserScoreLog faUserScoreLog);

    /**
     * 批量删除积分变动
     * 
     * @param ids 需要删除的积分变动ID
     * @return 结果
     */
    public int deleteFaUserScoreLogByIds(Integer[] ids);

    /**
     * 删除积分变动信息
     * 
     * @param id 积分变动ID
     * @return 结果
     */
    public int deleteFaUserScoreLogById(Integer id);

    /**
     * 查询积分变动列表
     *
     * @param faUserScoreLog 积分变动
     * @return 积分变动集合
     */
    public List<ScoreLogWithUserInfo> listScoreLogWithUserInfo(FaUserScoreLog scoreLog);
}
