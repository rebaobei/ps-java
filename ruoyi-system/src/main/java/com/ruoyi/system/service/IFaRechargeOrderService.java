package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaRechargeOrder;
import com.ruoyi.system.domain.vo.RechargeOrderVo;

/**
 * 充值订单Service接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface IFaRechargeOrderService 
{
    /**
     * 查询充值订单
     * 
     * @param id 充值订单ID
     * @return 充值订单
     */
    public FaRechargeOrder selectFaRechargeOrderById(Long id);

    /**
     * 查询充值订单列表
     * 
     * @param faRechargeOrder 充值订单
     * @return 充值订单集合
     */
    public List<FaRechargeOrder> selectFaRechargeOrderList(FaRechargeOrder faRechargeOrder);

    /**
     * 新增充值订单
     * 
     * @param faRechargeOrder 充值订单
     * @return 结果
     */
    public int insertFaRechargeOrder(FaRechargeOrder faRechargeOrder);

    /**
     * 修改充值订单
     * 
     * @param faRechargeOrder 充值订单
     * @return 结果
     */
    public int updateFaRechargeOrder(FaRechargeOrder faRechargeOrder);

    /**
     * 批量删除充值订单
     * 
     * @param ids 需要删除的充值订单ID
     * @return 结果
     */
    public int deleteFaRechargeOrderByIds(Long[] ids);

    /**
     * 删除充值订单信息
     * 
     * @param id 充值订单ID
     * @return 结果
     */
    public int deleteFaRechargeOrderById(Long id);

    List<RechargeOrderVo> listRechargeOrderVo(RechargeOrderVo faRechargeOrder);
}
