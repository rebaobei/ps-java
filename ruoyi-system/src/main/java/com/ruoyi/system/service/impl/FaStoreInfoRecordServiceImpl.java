package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.system.domain.FaStoreInfo;
import com.ruoyi.system.domain.FaStoreInfoRecord;
import com.ruoyi.system.domain.FaUser;
import com.ruoyi.system.mapper.FaStoreInfoMapper;
import com.ruoyi.system.mapper.FaStoreInfoRecordMapper;
import com.ruoyi.system.service.IFaStoreInfoRecordService;
import com.ruoyi.system.service.IFaUserService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.tool.SendMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * 店铺信息记录Service业务层处理
 *
 * @author ruoyi
 * @date 2021-10-30
 */
@Service
public class FaStoreInfoRecordServiceImpl implements IFaStoreInfoRecordService {
    @Autowired

    private FaStoreInfoRecordMapper faStoreInfoRecordMapper;
    @Autowired
    private FaStoreInfoMapper faStoreInfoMapper;
    @Autowired
    private ISysUserService userService;
    @Autowired
    SendMail sendMail;
    @Autowired
    private IFaUserService iFaUserService;

    /**
     * 查询店铺信息记录
     *
     * @param id 店铺信息记录ID
     * @return 店铺信息记录
     */
    @Override
    public FaStoreInfoRecord selectFaStoreInfoRecordById(Long id) {
        return faStoreInfoRecordMapper.selectFaStoreInfoRecordById(id);
    }

    /**
     * 查询店铺信息记录列表
     *
     * @param faStoreInfoRecord 店铺信息记录
     * @return 店铺信息记录
     */
    @Override
    public List<FaStoreInfoRecord> selectFaStoreInfoRecordList(FaStoreInfoRecord faStoreInfoRecord) {
        return faStoreInfoRecordMapper.selectFaStoreInfoRecordList(faStoreInfoRecord);
    }

    /**
     * 新增店铺信息记录
     *
     * @param faStoreInfoRecord 店铺信息记录
     * @return 结果
     */
    @Override
    public AjaxResult insertFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord) {
        faStoreInfoRecord.setCreateTime(DateUtils.getNowDate());
        faStoreInfoRecord.setCheckType(1);
        Long storeInfoId = faStoreInfoRecord.getStoreInfoId();
        if (storeInfoId == null) {
            return AjaxResult.error("店铺不存在");
        }
        FaStoreInfo faStoreInfo = new FaStoreInfo();
        faStoreInfo.setId(storeInfoId);
        faStoreInfo.setStatus(0);
        faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        faStoreInfoRecordMapper.insertFaStoreInfoRecord(faStoreInfoRecord);
        return AjaxResult.success();
    }

    @Override
    public AjaxResult sysInsertFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord) {
        faStoreInfoRecord.setCreateTime(DateUtils.getNowDate());
        faStoreInfoRecord.setCheckType(0);
        faStoreInfoRecord.setStatus(1L);
        //
        FaStoreInfo faStoreInfo = new FaStoreInfo();
        BeanUtils.copyProperties(faStoreInfoRecord, faStoreInfo, "id");
        String account = faStoreInfoRecord.getAccount();
        String principalName = faStoreInfoRecord.getPrincipalName();
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(account))) {
            return AjaxResult.error("新增用户'" + account + "'失败，登录账号已存在");
        }
        Long userId = faStoreInfoRecord.getUserId();

        faStoreInfo.setBrandId(null);
        faStoreInfo.setStatus(1);
        faStoreInfo.setStarLevel(1);
        faStoreInfo.setAmount(BigDecimal.ZERO);
        faStoreInfo.setFrozenAmount(BigDecimal.ZERO);
        //创建店铺
        faStoreInfoMapper.insertFaStoreInfo(faStoreInfo);
        Long id = faStoreInfo.getId();
        //app用户表绑定
        if(userId!=null){
            FaUser faUser = iFaUserService.selectFaUserById(userId.intValue());
            faUser.setStoreId(id);
            iFaUserService.updateFaUser(faUser);
        }
        SysUser user = new SysUser();
        user.setUserName(account);
        user.setNickName(principalName);
        user.setStoreInfoId(id);
        user.setAppUserId(userId);
        String password = "peisenjiayuan123456";
        user.setPassword(SecurityUtils.encryptPassword(password));
        //创建系统用户
        createUser(user);
        Long backUserId = user.getUserId();
        faStoreInfo.setUserId(backUserId);
        faStoreInfo.setAmount(null);
        faStoreInfo.setFrozenAmount(null);
        //修改店铺和系统用户id绑定
        faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        //绑定店铺信息
        faStoreInfoRecord.setStoreInfoId(id);
        faStoreInfoRecordMapper.insertFaStoreInfoRecord(faStoreInfoRecord);
        return AjaxResult.success(password);
    }

    /**
     * 修改店铺信息记录
     *
     * @param faStoreInfoRecord 店铺信息记录
     * @return 结果
     */
    @Override
    @Transactional
    public AjaxResult updateFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord) {
        faStoreInfoRecord.setUpdateTime(DateUtils.getNowDate());
        Long status = faStoreInfoRecord.getStatus();
        if (status == null) {
            return AjaxResult.error("审核状态为空");
        }
        String email = faStoreInfoRecord.getEmail();
        String principalName = faStoreInfoRecord.getPrincipalName();
        Long storeInfoId = faStoreInfoRecord.getStoreInfoId();

        if (status == 1) {
            if (storeInfoId == null && faStoreInfoRecord.getCheckType() == 0) {
                if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(email))) {
                    return AjaxResult.error("新增用户'" + email + "'失败，登录账号已存在");
                }
                Long userId = faStoreInfoRecord.getUserId();
                FaUser faUser = iFaUserService.selectFaUserById(userId.intValue());
                FaStoreInfo faStoreInfo = new FaStoreInfo();
                BeanUtils.copyProperties(faStoreInfoRecord, faStoreInfo, "id");
                faStoreInfo.setBrandId(null);
                faStoreInfo.setStatus(0);
                faStoreInfo.setStarLevel(1);
                faStoreInfo.setAmount(BigDecimal.ZERO);
                faStoreInfo.setFrozenAmount(BigDecimal.ZERO);
                //创建店铺
                faStoreInfoMapper.insertFaStoreInfo(faStoreInfo);
                Long id = faStoreInfo.getId();
                //app用户表绑定
                faUser.setStoreId(id);
                iFaUserService.updateFaUser(faUser);
                SysUser user = new SysUser();
                user.setUserName(email);
                user.setNickName(principalName);
                user.setEmail(email);
                user.setStoreInfoId(id);
                user.setAppUserId(userId);
                String password = radomString();
                user.setPassword(SecurityUtils.encryptPassword(password));
                //创建系统用户
                createUser(user);
                Long backUserId = user.getUserId();
                faStoreInfo.setUserId(backUserId);
                //修改店铺和系统用户id绑定
                faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
                sendMail.sendMail(email, password);
                //申请记录绑定店铺信息
                faStoreInfoRecord.setStoreInfoId(id);
            } else {
                FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(storeInfoId);
                Long userId = faStoreInfo.getUserId();
                BeanUtils.copyProperties(faStoreInfoRecord, faStoreInfo, "id","userId");
                faStoreInfo.setStatus(2);
                faStoreInfo.setUserId(null);
                faStoreInfo.setAmount(null);
                faStoreInfo.setFrozenAmount(null);
                faStoreInfo.setBrandId(null);
                faStoreInfo.setId(storeInfoId);
                updateUser(userId);
                faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
            }
        } else if (status == -1) {
            if (storeInfoId != null && faStoreInfoRecord.getCheckType() == 0) {
                FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(storeInfoId);
                faStoreInfo.setStatus(-1);
                faStoreInfo.setUserId(null);
                faStoreInfo.setAmount(null);
                faStoreInfo.setFrozenAmount(null);
                faStoreInfo.setBrandId(null);
                faStoreInfo.setId(storeInfoId);
//                userService.insertUserAuth()
                faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
            }
        }
        faStoreInfoRecordMapper.updateFaStoreInfoRecord(faStoreInfoRecord);
        return AjaxResult.success();
    }

    private SysUser createUser(SysUser user) {
        user.setCreateBy(SecurityUtils.getUsername());
        Long[] roleIds = new Long[1];
        roleIds[0] = 4L;
        user.setRoleIds(roleIds);
        user.setUserType("10");
        userService.insertUser(user);
        return user;
    }

    private SysUser updateUser(Long userId) {
        SysUser sysUser = userService.selectUserById(userId);
        Long[] roleIds = new Long[1];
        roleIds[0] = 3L;
        sysUser.setRoleIds(roleIds);
        userService.updateUser(sysUser);
        return sysUser;
    }

    private String radomString() {
        String result = "";
        for (int i = 0; i < 6; i++) {
            int intVal = (int) (Math.random() * 26 + 97);
            result = result + (char) intVal;
        }
        return result;
    }

    /**
     * 批量删除店铺信息记录
     *
     * @param ids 需要删除的店铺信息记录ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreInfoRecordByIds(Long[] ids) {
        return faStoreInfoRecordMapper.deleteFaStoreInfoRecordByIds(ids);
    }

    /**
     * 删除店铺信息记录信息
     *
     * @param id 店铺信息记录ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreInfoRecordById(Long id) {
        return faStoreInfoRecordMapper.deleteFaStoreInfoRecordById(id);
    }
}
