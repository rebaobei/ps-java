package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGamePetsCatogory;

/**
 * 宠物分类Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGamePetsCatogoryService 
{
    /**
     * 查询宠物分类
     * 
     * @param mainCategoryId 宠物分类ID
     * @return 宠物分类
     */
    public FaGamePetsCatogory selectFaGamePetsCatogoryById(Long mainCategoryId);

    /**
     * 查询宠物分类列表
     * 
     * @param faGamePetsCatogory 宠物分类
     * @return 宠物分类集合
     */
    public List<FaGamePetsCatogory> selectFaGamePetsCatogoryList(FaGamePetsCatogory faGamePetsCatogory);

    /**
     * 新增宠物分类
     * 
     * @param faGamePetsCatogory 宠物分类
     * @return 结果
     */
    public int insertFaGamePetsCatogory(FaGamePetsCatogory faGamePetsCatogory);

    /**
     * 修改宠物分类
     * 
     * @param faGamePetsCatogory 宠物分类
     * @return 结果
     */
    public int updateFaGamePetsCatogory(FaGamePetsCatogory faGamePetsCatogory);

    /**
     * 批量删除宠物分类
     * 
     * @param mainCategoryIds 需要删除的宠物分类ID
     * @return 结果
     */
    public int deleteFaGamePetsCatogoryByIds(Long[] mainCategoryIds);

    /**
     * 删除宠物分类信息
     * 
     * @param mainCategoryId 宠物分类ID
     * @return 结果
     */
    public int deleteFaGamePetsCatogoryById(Long mainCategoryId);
}
