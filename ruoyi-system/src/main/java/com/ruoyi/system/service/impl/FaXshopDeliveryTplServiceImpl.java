package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopDeliveryTplMapper;
import com.ruoyi.system.domain.FaXshopDeliveryTpl;
import com.ruoyi.system.service.IFaXshopDeliveryTplService;

/**
 * 运费模板Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
@Service
public class FaXshopDeliveryTplServiceImpl implements IFaXshopDeliveryTplService 
{
    @Autowired
    private FaXshopDeliveryTplMapper faXshopDeliveryTplMapper;

    /**
     * 查询运费模板
     * 
     * @param id 运费模板ID
     * @return 运费模板
     */
    @Override
    public FaXshopDeliveryTpl selectFaXshopDeliveryTplById(Integer id)
    {
        return faXshopDeliveryTplMapper.selectFaXshopDeliveryTplById(id);
    }

    /**
     * 查询运费模板列表
     * 
     * @param faXshopDeliveryTpl 运费模板
     * @return 运费模板
     */
    @Override
    public List<FaXshopDeliveryTpl> selectFaXshopDeliveryTplList(FaXshopDeliveryTpl faXshopDeliveryTpl)
    {
        return faXshopDeliveryTplMapper.selectFaXshopDeliveryTplList(faXshopDeliveryTpl);
    }

    /**
     * 新增运费模板
     * 
     * @param faXshopDeliveryTpl 运费模板
     * @return 结果
     */
    @Override
    public int insertFaXshopDeliveryTpl(FaXshopDeliveryTpl faXshopDeliveryTpl)
    {
        return faXshopDeliveryTplMapper.insertFaXshopDeliveryTpl(faXshopDeliveryTpl);
    }

    /**
     * 修改运费模板
     * 
     * @param faXshopDeliveryTpl 运费模板
     * @return 结果
     */
    @Override
    public int updateFaXshopDeliveryTpl(FaXshopDeliveryTpl faXshopDeliveryTpl)
    {
        return faXshopDeliveryTplMapper.updateFaXshopDeliveryTpl(faXshopDeliveryTpl);
    }

    /**
     * 批量删除运费模板
     * 
     * @param ids 需要删除的运费模板ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopDeliveryTplByIds(Integer[] ids)
    {
        return faXshopDeliveryTplMapper.deleteFaXshopDeliveryTplByIds(ids);
    }

    /**
     * 删除运费模板信息
     * 
     * @param id 运费模板ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopDeliveryTplById(Integer id)
    {
        return faXshopDeliveryTplMapper.deleteFaXshopDeliveryTplById(id);
    }
}
