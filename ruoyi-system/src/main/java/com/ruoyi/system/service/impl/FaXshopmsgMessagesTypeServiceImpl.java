package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopmsgMessagesTypeMapper;
import com.ruoyi.system.domain.FaXshopmsgMessagesType;
import com.ruoyi.system.service.IFaXshopmsgMessagesTypeService;

/**
 * 会员消息类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaXshopmsgMessagesTypeServiceImpl implements IFaXshopmsgMessagesTypeService 
{
    @Autowired
    private FaXshopmsgMessagesTypeMapper faXshopmsgMessagesTypeMapper;

    /**
     * 查询会员消息类型
     * 
     * @param id 会员消息类型ID
     * @return 会员消息类型
     */
    @Override
    public FaXshopmsgMessagesType selectFaXshopmsgMessagesTypeById(Long id)
    {
        return faXshopmsgMessagesTypeMapper.selectFaXshopmsgMessagesTypeById(id);
    }

    /**
     * 查询会员消息类型列表
     * 
     * @param faXshopmsgMessagesType 会员消息类型
     * @return 会员消息类型
     */
    @Override
    public List<FaXshopmsgMessagesType> selectFaXshopmsgMessagesTypeList(FaXshopmsgMessagesType faXshopmsgMessagesType)
    {
        return faXshopmsgMessagesTypeMapper.selectFaXshopmsgMessagesTypeList(faXshopmsgMessagesType);
    }

    /**
     * 新增会员消息类型
     * 
     * @param faXshopmsgMessagesType 会员消息类型
     * @return 结果
     */
    @Override
    public int insertFaXshopmsgMessagesType(FaXshopmsgMessagesType faXshopmsgMessagesType)
    {
        faXshopmsgMessagesType.setCreatetime(MyDateUtils.nowTimestamp());
        faXshopmsgMessagesType.setUpdatetime(MyDateUtils.nowTimestamp());
        return faXshopmsgMessagesTypeMapper.insertFaXshopmsgMessagesType(faXshopmsgMessagesType);
    }

    /**
     * 修改会员消息类型
     * 
     * @param faXshopmsgMessagesType 会员消息类型
     * @return 结果
     */
    @Override
    public int updateFaXshopmsgMessagesType(FaXshopmsgMessagesType faXshopmsgMessagesType)
    {
        faXshopmsgMessagesType.setUpdatetime(MyDateUtils.nowTimestamp());
        return faXshopmsgMessagesTypeMapper.updateFaXshopmsgMessagesType(faXshopmsgMessagesType);
    }

    /**
     * 批量删除会员消息类型
     * 
     * @param ids 需要删除的会员消息类型ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopmsgMessagesTypeByIds(Long[] ids)
    {
        return faXshopmsgMessagesTypeMapper.deleteFaXshopmsgMessagesTypeByIds(ids);
    }

    /**
     * 删除会员消息类型信息
     * 
     * @param id 会员消息类型ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopmsgMessagesTypeById(Long id)
    {
        return faXshopmsgMessagesTypeMapper.deleteFaXshopmsgMessagesTypeById(id);
    }
}
