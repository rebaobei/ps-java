package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaCmsComment;
import com.ruoyi.system.domain.vo.CmsCommentVo;

/**
 * 评论管理Service接口
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
public interface IFaCmsCommentService 
{
    /**
     * 查询评论管理
     * 
     * @param id 评论管理ID
     * @return 评论管理
     */
    public FaCmsComment selectFaCmsCommentById(Integer id);

    /**
     * 查询评论管理列表
     * 
     * @param faCmsComment 评论管理
     * @return 评论管理集合
     */
    public List<FaCmsComment> selectFaCmsCommentList(FaCmsComment faCmsComment);

    /**
     * 新增评论管理
     * 
     * @param faCmsComment 评论管理
     * @return 结果
     */
    public int insertFaCmsComment(FaCmsComment faCmsComment);

    /**
     * 修改评论管理
     * 
     * @param faCmsComment 评论管理
     * @return 结果
     */
    public int updateFaCmsComment(FaCmsComment faCmsComment);

    /**
     * 批量删除评论管理
     * 
     * @param ids 需要删除的评论管理ID
     * @return 结果
     */
    public int deleteFaCmsCommentByIds(Integer[] ids);

    /**
     * 删除评论管理信息
     * 
     * @param id 评论管理ID
     * @return 结果
     */
    public int deleteFaCmsCommentById(Integer id);

    List<CmsCommentVo> listCmsCommentVo(CmsCommentVo faCmsComment);
}
