package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserTasklogMapper;
import com.ruoyi.system.domain.FaUserTasklog;
import com.ruoyi.system.service.IFaUserTasklogService;

/**
 * 完成任务记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaUserTasklogServiceImpl implements IFaUserTasklogService 
{
    @Autowired
    private FaUserTasklogMapper faUserTasklogMapper;

    /**
     * 查询完成任务记录
     * 
     * @param id 完成任务记录ID
     * @return 完成任务记录
     */
    @Override
    public FaUserTasklog selectFaUserTasklogById(Long id)
    {
        return faUserTasklogMapper.selectFaUserTasklogById(id);
    }

    /**
     * 查询完成任务记录列表
     * 
     * @param faUserTasklog 完成任务记录
     * @return 完成任务记录
     */
    @Override
    public List<FaUserTasklog> selectFaUserTasklogList(FaUserTasklog faUserTasklog)
    {
        return faUserTasklogMapper.selectFaUserTasklogList(faUserTasklog);
    }

    /**
     * 新增完成任务记录
     * 
     * @param faUserTasklog 完成任务记录
     * @return 结果
     */
    @Override
    public int insertFaUserTasklog(FaUserTasklog faUserTasklog)
    {
        return faUserTasklogMapper.insertFaUserTasklog(faUserTasklog);
    }

    /**
     * 修改完成任务记录
     * 
     * @param faUserTasklog 完成任务记录
     * @return 结果
     */
    @Override
    public int updateFaUserTasklog(FaUserTasklog faUserTasklog)
    {
        return faUserTasklogMapper.updateFaUserTasklog(faUserTasklog);
    }

    /**
     * 批量删除完成任务记录
     * 
     * @param ids 需要删除的完成任务记录ID
     * @return 结果
     */
    @Override
    public int deleteFaUserTasklogByIds(Long[] ids)
    {
        return faUserTasklogMapper.deleteFaUserTasklogByIds(ids);
    }

    /**
     * 删除完成任务记录信息
     * 
     * @param id 完成任务记录ID
     * @return 结果
     */
    @Override
    public int deleteFaUserTasklogById(Long id)
    {
        return faUserTasklogMapper.deleteFaUserTasklogById(id);
    }
}
