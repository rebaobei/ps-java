package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ruoyi.system.domain.FaTechnicians;
import com.ruoyi.system.domain.vo.PetCategoryVo;
import com.ruoyi.system.domain.vo.RoomCategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaRoomfenleiMapper;
import com.ruoyi.system.domain.FaRoomfenlei;
import com.ruoyi.system.service.IFaRoomfenleiService;

/**
 * 房间分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@Service
public class FaRoomfenleiServiceImpl implements IFaRoomfenleiService 
{
    @Autowired
    private FaRoomfenleiMapper faRoomfenleiMapper;

    /**
     * 查询房间分类
     * 
     * @param id 房间分类ID
     * @return 房间分类
     */
    @Override
    public FaRoomfenlei selectFaRoomfenleiById(Long id)
    {
        return faRoomfenleiMapper.selectFaRoomfenleiById(id);
    }

    /**
     * 查询房间分类列表
     * 
     * @param faRoomfenlei 房间分类
     * @return 房间分类
     */
    @Override
    public List<FaRoomfenlei> selectFaRoomfenleiList(FaRoomfenlei faRoomfenlei)
    {
        return faRoomfenleiMapper.selectFaRoomfenleiList(faRoomfenlei);
    }

    /**
     * 新增房间分类
     * 
     * @param faRoomfenlei 房间分类
     * @return 结果
     */
    @Override
    public int insertFaRoomfenlei(FaRoomfenlei faRoomfenlei)
    {
        faRoomfenlei.setCreatetime(System.currentTimeMillis()/1000L);
        faRoomfenlei.setUpdatetime(System.currentTimeMillis()/1000L);
        return faRoomfenleiMapper.insertFaRoomfenlei(faRoomfenlei);
    }

    /**
     * 修改房间分类
     * 
     * @param faRoomfenlei 房间分类
     * @return 结果
     */
    @Override
    public int updateFaRoomfenlei(FaRoomfenlei faRoomfenlei)
    {
        faRoomfenlei.setUpdatetime(System.currentTimeMillis()/1000L);
        return faRoomfenleiMapper.updateFaRoomfenlei(faRoomfenlei);
    }

    /**
     * 批量删除房间分类
     * 
     * @param ids 需要删除的房间分类ID
     * @return 结果
     */
    @Override
    public int deleteFaRoomfenleiByIds(Long[] ids)
    {
        return faRoomfenleiMapper.deleteFaRoomfenleiByIds(ids);
    }

    /**
     * 删除房间分类信息
     * 
     * @param id 房间分类ID
     * @return 结果
     */
    @Override
    public int deleteFaRoomfenleiById(Long id)
    {
        return faRoomfenleiMapper.deleteFaRoomfenleiById(id);
    }

    @Override
    public List<RoomCategoryVo> listRoomFenlei(FaRoomfenlei faRoomfenlei) {
        List<FaRoomfenlei> faRoomfenleis = faRoomfenleiMapper.selectFaRoomfenleiList(faRoomfenlei);
        List<RoomCategoryVo> result = new ArrayList<>();
        for (FaRoomfenlei item : faRoomfenleis){
            RoomCategoryVo roomCategoryVo = new RoomCategoryVo();
            roomCategoryVo.setId(item.getId());
            roomCategoryVo.setName(item.getName());
            roomCategoryVo.setUpdatetime(item.getUpdatetime());
            roomCategoryVo.setCreatetime(item.getCreatetime());
            roomCategoryVo.setStatus(item.getStatus());
            roomCategoryVo.setWeigh(item.getWeigh());
            if (item.getImage() != null && !item.getImage().isEmpty()){
                String[] split = item.getImage().split(",");
                roomCategoryVo.setImages(Arrays.asList(split));
            }
            result.add(roomCategoryVo);
        }
        return result;
    }
}
