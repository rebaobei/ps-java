package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.VoteMainVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaVoteMainMapper;
import com.ruoyi.system.domain.FaVoteMain;
import com.ruoyi.system.service.IFaVoteMainService;

/**
 * 投票管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-02
 */
@Service
public class FaVoteMainServiceImpl implements IFaVoteMainService {
    @Autowired
    private FaVoteMainMapper faVoteMainMapper;

    /**
     * 查询投票管理
     *
     * @param id 投票管理ID
     * @return 投票管理
     */
    @Override
    public FaVoteMain selectFaVoteMainById(Long id) {
        return faVoteMainMapper.selectFaVoteMainById(id);
    }

    /**
     * 查询投票管理列表
     *
     * @param faVoteMain 投票管理
     * @return 投票管理
     */
    @Override
    public List<FaVoteMain> selectFaVoteMainList(FaVoteMain faVoteMain) {
        return faVoteMainMapper.selectFaVoteMainList(faVoteMain);
    }

    /**
     * 新增投票管理
     *
     * @param faVoteMain 投票管理
     * @return 结果
     */
    @Override
    public int insertFaVoteMain(FaVoteMain faVoteMain) {
        faVoteMain.setCreatetime(MyDateUtils.nowTimestamp());
        faVoteMain.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteMainMapper.insertFaVoteMain(faVoteMain);
    }

    /**
     * 修改投票管理
     *
     * @param faVoteMain 投票管理
     * @return 结果
     */
    @Override
    public int updateFaVoteMain(FaVoteMain faVoteMain) {
        faVoteMain.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteMainMapper.updateFaVoteMain(faVoteMain);
    }

    /**
     * 批量删除投票管理
     *
     * @param ids 需要删除的投票管理ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteMainByIds(Long[] ids) {

        return faVoteMainMapper.deleteFaVoteMainByIds(ids);
    }

    /**
     * 删除投票管理信息
     *
     * @param id 投票管理ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteMainById(Long id) {
        return faVoteMainMapper.deleteFaVoteMainById(id);
    }

    @Override
    public List<VoteMainVo> listVoteMainVo(FaVoteMain faVoteMain) {
        List<VoteMainVo> voteMainVos = faVoteMainMapper.listVoteMainVo(faVoteMain);
        if (voteMainVos != null) {
            for (VoteMainVo vo : voteMainVos){
                if (vo.getImages() != null && vo.getImages().length() > 0){
                    String[] split = vo.getImages().split(",");
                    vo.setImageList(Arrays.asList(split));
                }
                vo.setBegTimeMillisecond(vo.getBegtime() * 1000);
                vo.setEndTimeMillisecond(vo.getEndtime() * 1000);
            }
        }
        return voteMainVos;
    }

    @Override
    public VoteMainVo getVoteMainVoById(Long id) {
        VoteMainVo voteMainVoById = faVoteMainMapper.getVoteMainVoById(id);
        voteMainVoById.setBegTimeMillisecond(voteMainVoById.getBegtime() * 1000);
        voteMainVoById.setEndTimeMillisecond(voteMainVoById.getEndtime() * 1000);
        return voteMainVoById;
    }
}
