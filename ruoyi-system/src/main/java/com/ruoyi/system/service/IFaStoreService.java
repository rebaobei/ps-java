package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaStore;
import com.ruoyi.system.domain.vo.StoreVo;

/**
 * 门店管理Service接口
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
public interface IFaStoreService 
{
    /**
     * 查询门店管理
     * 
     * @param id 门店管理ID
     * @return 门店管理
     */
    public FaStore selectFaStoreById(Long id);

    /**
     * 查询门店管理列表
     * 
     * @param faStore 门店管理
     * @return 门店管理集合
     */
    public List<FaStore> selectFaStoreList(FaStore faStore);

    /**
     * 新增门店管理
     * 
     * @param faStore 门店管理
     * @return 结果
     */
    public int insertFaStore(FaStore faStore);

    /**
     * 修改门店管理
     * 
     * @param faStore 门店管理
     * @return 结果
     */
    public int updateFaStore(FaStore faStore);

    /**
     * 批量删除门店管理
     * 
     * @param ids 需要删除的门店管理ID
     * @return 结果
     */
    public int deleteFaStoreByIds(Long[] ids);

    /**
     * 删除门店管理信息
     * 
     * @param id 门店管理ID
     * @return 结果
     */
    public int deleteFaStoreById(Long id);

    List<StoreVo> listStoreVo(FaStore faStore);
}
