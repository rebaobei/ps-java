package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaXshopArticle;

/**
 * 规则列表Service接口
 * 
 * @author zzp
 * @date 2021-08-04
 */
public interface IFaXshopArticleService 
{
    /**
     * 查询规则列表
     * 
     * @param id 规则列表ID
     * @return 规则列表
     */
    public FaXshopArticle selectFaXshopArticleById(Long id);

    /**
     * 查询规则列表列表
     * 
     * @param faXshopArticle 规则列表
     * @return 规则列表集合
     */
    public List<FaXshopArticle> selectFaXshopArticleList(FaXshopArticle faXshopArticle);

    /**
     * 新增规则列表
     * 
     * @param faXshopArticle 规则列表
     * @return 结果
     */
    public int insertFaXshopArticle(FaXshopArticle faXshopArticle);

    /**
     * 修改规则列表
     * 
     * @param faXshopArticle 规则列表
     * @return 结果
     */
    public int updateFaXshopArticle(FaXshopArticle faXshopArticle);

    /**
     * 批量删除规则列表
     * 
     * @param ids 需要删除的规则列表ID
     * @return 结果
     */
    public int deleteFaXshopArticleByIds(Long[] ids);

    /**
     * 删除规则列表信息
     * 
     * @param id 规则列表ID
     * @return 结果
     */
    public int deleteFaXshopArticleById(Long id);
}
