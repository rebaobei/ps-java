package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopProductSkuMapper;
import com.ruoyi.system.domain.FaXshopProductSku;
import com.ruoyi.system.service.IFaXshopProductSkuService;

/**
 * 商品属性Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
@Service
public class FaXshopProductSkuServiceImpl implements IFaXshopProductSkuService 
{
    @Autowired
    private FaXshopProductSkuMapper faXshopProductSkuMapper;

    /**
     * 查询商品属性
     * 
     * @param id 商品属性ID
     * @return 商品属性
     */
    @Override
    public FaXshopProductSku selectFaXshopProductSkuById(Integer id)
    {
        return faXshopProductSkuMapper.selectFaXshopProductSkuById(id);
    }

    /**
     * 查询商品属性列表
     * 
     * @param faXshopProductSku 商品属性
     * @return 商品属性
     */
    @Override
    public List<FaXshopProductSku> selectFaXshopProductSkuList(FaXshopProductSku faXshopProductSku)
    {
        return faXshopProductSkuMapper.selectFaXshopProductSkuList(faXshopProductSku);
    }

    /**
     * 新增商品属性
     * 
     * @param faXshopProductSku 商品属性
     * @return 结果
     */
    @Override
    public int insertFaXshopProductSku(FaXshopProductSku faXshopProductSku)
    {
        return faXshopProductSkuMapper.insertFaXshopProductSku(faXshopProductSku);
    }

    /**
     * 修改商品属性
     * 
     * @param faXshopProductSku 商品属性
     * @return 结果
     */
    @Override
    public int updateFaXshopProductSku(FaXshopProductSku faXshopProductSku)
    {
        return faXshopProductSkuMapper.updateFaXshopProductSku(faXshopProductSku);
    }

    /**
     * 批量删除商品属性
     * 
     * @param ids 需要删除的商品属性ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopProductSkuByIds(Integer[] ids)
    {
        return faXshopProductSkuMapper.deleteFaXshopProductSkuByIds(ids);
    }

    /**
     * 删除商品属性信息
     * 
     * @param id 商品属性ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopProductSkuById(Integer id)
    {
        return faXshopProductSkuMapper.deleteFaXshopProductSkuById(id);
    }

    @Override
    public List<FaXshopProductSku> listFaXshopProductSkuById(Integer id) {
        return faXshopProductSkuMapper.listFaXshopProductSkuById(id);
    }

}
