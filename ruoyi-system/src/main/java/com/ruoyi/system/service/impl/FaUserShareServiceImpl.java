package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserShareMapper;
import com.ruoyi.system.domain.FaUserShare;
import com.ruoyi.system.service.IFaUserShareService;

/**
 * 会员分享Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUserShareServiceImpl implements IFaUserShareService 
{
    @Autowired
    private FaUserShareMapper faUserShareMapper;

    /**
     * 查询会员分享
     * 
     * @param id 会员分享ID
     * @return 会员分享
     */
    @Override
    public FaUserShare selectFaUserShareById(Long id)
    {
        return faUserShareMapper.selectFaUserShareById(id);
    }

    /**
     * 查询会员分享列表
     * 
     * @param faUserShare 会员分享
     * @return 会员分享
     */
    @Override
    public List<FaUserShare> selectFaUserShareList(FaUserShare faUserShare)
    {
        return faUserShareMapper.selectFaUserShareList(faUserShare);
    }

    /**
     * 新增会员分享
     * 
     * @param faUserShare 会员分享
     * @return 结果
     */
    @Override
    public int insertFaUserShare(FaUserShare faUserShare)
    {
        return faUserShareMapper.insertFaUserShare(faUserShare);
    }

    /**
     * 修改会员分享
     * 
     * @param faUserShare 会员分享
     * @return 结果
     */
    @Override
    public int updateFaUserShare(FaUserShare faUserShare)
    {
        return faUserShareMapper.updateFaUserShare(faUserShare);
    }

    /**
     * 批量删除会员分享
     * 
     * @param ids 需要删除的会员分享ID
     * @return 结果
     */
    @Override
    public int deleteFaUserShareByIds(Long[] ids)
    {
        return faUserShareMapper.deleteFaUserShareByIds(ids);
    }

    /**
     * 删除会员分享信息
     * 
     * @param id 会员分享ID
     * @return 结果
     */
    @Override
    public int deleteFaUserShareById(Long id)
    {
        return faUserShareMapper.deleteFaUserShareById(id);
    }
}
