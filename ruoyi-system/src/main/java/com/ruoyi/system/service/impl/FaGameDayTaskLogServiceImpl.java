package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.GameDayTaskLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameDayTaskLogMapper;
import com.ruoyi.system.domain.FaGameDayTaskLog;
import com.ruoyi.system.service.IFaGameDayTaskLogService;

/**
 * 每日任务完成记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameDayTaskLogServiceImpl implements IFaGameDayTaskLogService 
{
    @Autowired
    private FaGameDayTaskLogMapper faGameDayTaskLogMapper;

    /**
     * 查询每日任务完成记录
     * 
     * @param id 每日任务完成记录ID
     * @return 每日任务完成记录
     */
    @Override
    public FaGameDayTaskLog selectFaGameDayTaskLogById(Long id)
    {
        return faGameDayTaskLogMapper.selectFaGameDayTaskLogById(id);
    }

    /**
     * 查询每日任务完成记录列表
     * 
     * @param faGameDayTaskLog 每日任务完成记录
     * @return 每日任务完成记录
     */
    @Override
    public List<FaGameDayTaskLog> selectFaGameDayTaskLogList(FaGameDayTaskLog faGameDayTaskLog)
    {
        return faGameDayTaskLogMapper.selectFaGameDayTaskLogList(faGameDayTaskLog);
    }

    /**
     * 新增每日任务完成记录
     * 
     * @param faGameDayTaskLog 每日任务完成记录
     * @return 结果
     */
    @Override
    public int insertFaGameDayTaskLog(FaGameDayTaskLog faGameDayTaskLog)
    {
        return faGameDayTaskLogMapper.insertFaGameDayTaskLog(faGameDayTaskLog);
    }

    /**
     * 修改每日任务完成记录
     * 
     * @param faGameDayTaskLog 每日任务完成记录
     * @return 结果
     */
    @Override
    public int updateFaGameDayTaskLog(FaGameDayTaskLog faGameDayTaskLog)
    {
        return faGameDayTaskLogMapper.updateFaGameDayTaskLog(faGameDayTaskLog);
    }

    /**
     * 批量删除每日任务完成记录
     * 
     * @param ids 需要删除的每日任务完成记录ID
     * @return 结果
     */
    @Override
    public int deleteFaGameDayTaskLogByIds(Long[] ids)
    {
        return faGameDayTaskLogMapper.deleteFaGameDayTaskLogByIds(ids);
    }

    /**
     * 删除每日任务完成记录信息
     * 
     * @param id 每日任务完成记录ID
     * @return 结果
     */
    @Override
    public int deleteFaGameDayTaskLogById(Long id)
    {
        return faGameDayTaskLogMapper.deleteFaGameDayTaskLogById(id);
    }

    @Override
    public List<GameDayTaskLogVo> listGameDayTaskLogVo(GameDayTaskLogVo faGameDayTaskLog) {
        return faGameDayTaskLogMapper.listGameDayTaskLogVo(faGameDayTaskLog);
    }
}
