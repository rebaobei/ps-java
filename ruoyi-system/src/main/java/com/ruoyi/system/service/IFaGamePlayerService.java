package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGamePlayer;
import com.ruoyi.system.domain.vo.PlayerInfoVo;

/**
 * 游戏玩家Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGamePlayerService 
{
    /**
     * 查询游戏玩家
     * 
     * @param id 游戏玩家ID
     * @return 游戏玩家
     */
    public FaGamePlayer selectFaGamePlayerById(Long id);

    /**
     * 查询游戏玩家列表
     * 
     * @param faGamePlayer 游戏玩家
     * @return 游戏玩家集合
     */
    public List<FaGamePlayer> selectFaGamePlayerList(FaGamePlayer faGamePlayer);

    /**
     * 新增游戏玩家
     * 
     * @param faGamePlayer 游戏玩家
     * @return 结果
     */
    public int insertFaGamePlayer(FaGamePlayer faGamePlayer);

    /**
     * 修改游戏玩家
     * 
     * @param faGamePlayer 游戏玩家
     * @return 结果
     */
    public int updateFaGamePlayer(FaGamePlayer faGamePlayer);

    /**
     * 批量删除游戏玩家
     * 
     * @param ids 需要删除的游戏玩家ID
     * @return 结果
     */
    public int deleteFaGamePlayerByIds(Long[] ids);

    /**
     * 删除游戏玩家信息
     * 
     * @param id 游戏玩家ID
     * @return 结果
     */
    public int deleteFaGamePlayerById(Long id);

    List<PlayerInfoVo> lsitPlayerInfoVo(PlayerInfoVo faGamePlayer);
}
