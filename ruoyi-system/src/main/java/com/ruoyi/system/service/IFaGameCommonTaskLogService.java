package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGameCommonTaskLog;
import com.ruoyi.system.domain.vo.GameCommonTaskLogVo;

/**
 * 常规任务记录Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGameCommonTaskLogService 
{
    /**
     * 查询常规任务记录
     * 
     * @param id 常规任务记录ID
     * @return 常规任务记录
     */
    public FaGameCommonTaskLog selectFaGameCommonTaskLogById(Long id);

    /**
     * 查询常规任务记录列表
     * 
     * @param faGameCommonTaskLog 常规任务记录
     * @return 常规任务记录集合
     */
    public List<FaGameCommonTaskLog> selectFaGameCommonTaskLogList(FaGameCommonTaskLog faGameCommonTaskLog);

    /**
     * 新增常规任务记录
     * 
     * @param faGameCommonTaskLog 常规任务记录
     * @return 结果
     */
    public int insertFaGameCommonTaskLog(FaGameCommonTaskLog faGameCommonTaskLog);

    /**
     * 修改常规任务记录
     * 
     * @param faGameCommonTaskLog 常规任务记录
     * @return 结果
     */
    public int updateFaGameCommonTaskLog(FaGameCommonTaskLog faGameCommonTaskLog);

    /**
     * 批量删除常规任务记录
     * 
     * @param ids 需要删除的常规任务记录ID
     * @return 结果
     */
    public int deleteFaGameCommonTaskLogByIds(Long[] ids);

    /**
     * 删除常规任务记录信息
     * 
     * @param id 常规任务记录ID
     * @return 结果
     */
    public int deleteFaGameCommonTaskLogById(Long id);

    List<GameCommonTaskLogVo> listCommonTaskLogVo(GameCommonTaskLogVo faGameCommonTaskLog);
}
