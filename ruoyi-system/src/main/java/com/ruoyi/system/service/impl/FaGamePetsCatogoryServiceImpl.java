package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGamePetsCatogoryMapper;
import com.ruoyi.system.domain.FaGamePetsCatogory;
import com.ruoyi.system.service.IFaGamePetsCatogoryService;

/**
 * 宠物分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGamePetsCatogoryServiceImpl implements IFaGamePetsCatogoryService 
{
    @Autowired
    private FaGamePetsCatogoryMapper faGamePetsCatogoryMapper;

    /**
     * 查询宠物分类
     * 
     * @param mainCategoryId 宠物分类ID
     * @return 宠物分类
     */
    @Override
    public FaGamePetsCatogory selectFaGamePetsCatogoryById(Long mainCategoryId)
    {
        return faGamePetsCatogoryMapper.selectFaGamePetsCatogoryById(mainCategoryId);
    }

    /**
     * 查询宠物分类列表
     * 
     * @param faGamePetsCatogory 宠物分类
     * @return 宠物分类
     */
    @Override
    public List<FaGamePetsCatogory> selectFaGamePetsCatogoryList(FaGamePetsCatogory faGamePetsCatogory)
    {
        return faGamePetsCatogoryMapper.selectFaGamePetsCatogoryList(faGamePetsCatogory);
    }

    /**
     * 新增宠物分类
     * 
     * @param faGamePetsCatogory 宠物分类
     * @return 结果
     */
    @Override
    public int insertFaGamePetsCatogory(FaGamePetsCatogory faGamePetsCatogory)
    {
        return faGamePetsCatogoryMapper.insertFaGamePetsCatogory(faGamePetsCatogory);
    }

    /**
     * 修改宠物分类
     * 
     * @param faGamePetsCatogory 宠物分类
     * @return 结果
     */
    @Override
    public int updateFaGamePetsCatogory(FaGamePetsCatogory faGamePetsCatogory)
    {
        return faGamePetsCatogoryMapper.updateFaGamePetsCatogory(faGamePetsCatogory);
    }

    /**
     * 批量删除宠物分类
     * 
     * @param mainCategoryIds 需要删除的宠物分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsCatogoryByIds(Long[] mainCategoryIds)
    {
        return faGamePetsCatogoryMapper.deleteFaGamePetsCatogoryByIds(mainCategoryIds);
    }

    /**
     * 删除宠物分类信息
     * 
     * @param mainCategoryId 宠物分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsCatogoryById(Long mainCategoryId)
    {
        return faGamePetsCatogoryMapper.deleteFaGamePetsCatogoryById(mainCategoryId);
    }
}
