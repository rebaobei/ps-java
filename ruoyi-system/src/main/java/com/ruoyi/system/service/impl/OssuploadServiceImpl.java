package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.IOssuploadService;
import com.ruoyi.system.tool.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service

public class OssuploadServiceImpl implements IOssuploadService {

    @Autowired
    private OssUtil ossUtil;

    @Override
    public AjaxResult upload(MultipartFile file) {
        if(file == null){
            return AjaxResult.success();
        }
        String url = ossUtil.checkImage(file);
        System.out.println("imageUrl " + url);
        return AjaxResult.ok(url);

    }

    @Override
    public AjaxResult uploadVideo(MultipartFile file) {
        if(file == null){
            return AjaxResult.success();
        }
        String url =  ossUtil.checkVideo(file);
        System.out.println("videoUrl " + url);
        return AjaxResult.success(url);
    }
}
