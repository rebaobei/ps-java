package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameCommonTaskMapper;
import com.ruoyi.system.domain.FaGameCommonTask;
import com.ruoyi.system.service.IFaGameCommonTaskService;

/**
 * 常规任务Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameCommonTaskServiceImpl implements IFaGameCommonTaskService 
{
    @Autowired
    private FaGameCommonTaskMapper faGameCommonTaskMapper;

    /**
     * 查询常规任务
     * 
     * @param id 常规任务ID
     * @return 常规任务
     */
    @Override
    public FaGameCommonTask selectFaGameCommonTaskById(Long id)
    {
        return faGameCommonTaskMapper.selectFaGameCommonTaskById(id);
    }

    /**
     * 查询常规任务列表
     * 
     * @param faGameCommonTask 常规任务
     * @return 常规任务
     */
    @Override
    public List<FaGameCommonTask> selectFaGameCommonTaskList(FaGameCommonTask faGameCommonTask)
    {
        return faGameCommonTaskMapper.selectFaGameCommonTaskList(faGameCommonTask);
    }

    /**
     * 新增常规任务
     * 
     * @param faGameCommonTask 常规任务
     * @return 结果
     */
    @Override
    public int insertFaGameCommonTask(FaGameCommonTask faGameCommonTask)
    {
        return faGameCommonTaskMapper.insertFaGameCommonTask(faGameCommonTask);
    }

    /**
     * 修改常规任务
     * 
     * @param faGameCommonTask 常规任务
     * @return 结果
     */
    @Override
    public int updateFaGameCommonTask(FaGameCommonTask faGameCommonTask)
    {
        return faGameCommonTaskMapper.updateFaGameCommonTask(faGameCommonTask);
    }

    /**
     * 批量删除常规任务
     * 
     * @param ids 需要删除的常规任务ID
     * @return 结果
     */
    @Override
    public int deleteFaGameCommonTaskByIds(Long[] ids)
    {
        return faGameCommonTaskMapper.deleteFaGameCommonTaskByIds(ids);
    }

    /**
     * 删除常规任务信息
     * 
     * @param id 常规任务ID
     * @return 结果
     */
    @Override
    public int deleteFaGameCommonTaskById(Long id)
    {
        return faGameCommonTaskMapper.deleteFaGameCommonTaskById(id);
    }
}
