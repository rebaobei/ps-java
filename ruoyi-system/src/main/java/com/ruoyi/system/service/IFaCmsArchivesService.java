package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaCmsArchives;
import com.ruoyi.system.domain.vo.CmsArchivesVo;

/**
 * 文章管理Service接口
 * 
 * @author zzp
 * @date 2021-08-11
 */
public interface IFaCmsArchivesService 
{
    /**
     * 查询文章管理
     * 
     * @param id 文章管理ID
     * @return 文章管理
     */
    public FaCmsArchives selectFaCmsArchivesById(Integer id);

    /**
     * 查询文章管理列表
     * 
     * @param faCmsArchives 文章管理
     * @return 文章管理集合
     */
    public List<FaCmsArchives> selectFaCmsArchivesList(FaCmsArchives faCmsArchives);

    /**
     * 新增文章管理
     * 
     * @param faCmsArchives 文章管理
     * @return 结果
     */
    public int insertFaCmsArchives(CmsArchivesVo faCmsArchives);

    /**
     * 修改文章管理
     * 
     * @param faCmsArchives 文章管理
     * @return 结果
     */
    public int updateFaCmsArchives(CmsArchivesVo faCmsArchives);

    /**
     * 批量删除文章管理
     * 
     * @param ids 需要删除的文章管理ID
     * @return 结果
     */
    public int deleteFaCmsArchivesByIds(Integer[] ids);

    /**
     * 删除文章管理信息
     * 
     * @param id 文章管理ID
     * @return 结果
     */
    public int deleteFaCmsArchivesById(Integer id);

    /**
     * 查询文章管理
     *
     * @param id 文章管理ID
     * @return 文章管理
     */
    public CmsArchivesVo selectFaCmsArchivesVoById(Integer id);
}
