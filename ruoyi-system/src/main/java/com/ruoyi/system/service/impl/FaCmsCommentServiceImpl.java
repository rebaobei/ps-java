package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.CmsCommentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaCmsCommentMapper;
import com.ruoyi.system.domain.FaCmsComment;
import com.ruoyi.system.service.IFaCmsCommentService;

/**
 * 评论管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-11
 */
@Service
public class FaCmsCommentServiceImpl implements IFaCmsCommentService 
{
    @Autowired
    private FaCmsCommentMapper faCmsCommentMapper;

    /**
     * 查询评论管理
     * 
     * @param id 评论管理ID
     * @return 评论管理
     */
    @Override
    public FaCmsComment selectFaCmsCommentById(Integer id)
    {
        return faCmsCommentMapper.selectFaCmsCommentById(id);
    }

    /**
     * 查询评论管理列表
     * 
     * @param faCmsComment 评论管理
     * @return 评论管理
     */
    @Override
    public List<FaCmsComment> selectFaCmsCommentList(FaCmsComment faCmsComment)
    {
        return faCmsCommentMapper.selectFaCmsCommentList(faCmsComment);
    }

    /**
     * 新增评论管理
     * 
     * @param faCmsComment 评论管理
     * @return 结果
     */
    @Override
    public int insertFaCmsComment(FaCmsComment faCmsComment)
    {
        return faCmsCommentMapper.insertFaCmsComment(faCmsComment);
    }

    /**
     * 修改评论管理
     * 
     * @param faCmsComment 评论管理
     * @return 结果
     */
    @Override
    public int updateFaCmsComment(FaCmsComment faCmsComment)
    {
        return faCmsCommentMapper.updateFaCmsComment(faCmsComment);
    }

    /**
     * 批量删除评论管理
     * 
     * @param ids 需要删除的评论管理ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsCommentByIds(Integer[] ids)
    {
        return faCmsCommentMapper.deleteFaCmsCommentByIds(ids);
    }

    /**
     * 删除评论管理信息
     * 
     * @param id 评论管理ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsCommentById(Integer id)
    {
        return faCmsCommentMapper.deleteFaCmsCommentById(id);
    }

    @Override
    public List<CmsCommentVo> listCmsCommentVo(CmsCommentVo faCmsComment) {
        return faCmsCommentMapper.listCmsCommentVo(faCmsComment);
    }
}
