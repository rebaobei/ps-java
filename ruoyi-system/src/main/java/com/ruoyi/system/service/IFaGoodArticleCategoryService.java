package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGoodArticleCategory;

/**
 * 动保公益分类Service接口
 * 
 * @author ruoyi
 * @date 2021-12-04
 */
public interface IFaGoodArticleCategoryService 
{
    /**
     * 查询动保公益分类
     * 
     * @param id 动保公益分类ID
     * @return 动保公益分类
     */
    public FaGoodArticleCategory selectFaGoodArticleCategoryById(Long id);

    /**
     * 查询动保公益分类列表
     * 
     * @param faGoodArticleCategory 动保公益分类
     * @return 动保公益分类集合
     */
    public List<FaGoodArticleCategory> selectFaGoodArticleCategoryList(FaGoodArticleCategory faGoodArticleCategory);

    /**
     * 新增动保公益分类
     * 
     * @param faGoodArticleCategory 动保公益分类
     * @return 结果
     */
    public int insertFaGoodArticleCategory(FaGoodArticleCategory faGoodArticleCategory);

    /**
     * 修改动保公益分类
     * 
     * @param faGoodArticleCategory 动保公益分类
     * @return 结果
     */
    public int updateFaGoodArticleCategory(FaGoodArticleCategory faGoodArticleCategory);

    /**
     * 批量删除动保公益分类
     * 
     * @param ids 需要删除的动保公益分类ID
     * @return 结果
     */
    public int deleteFaGoodArticleCategoryByIds(Long[] ids);

    /**
     * 删除动保公益分类信息
     * 
     * @param id 动保公益分类ID
     * @return 结果
     */
    public int deleteFaGoodArticleCategoryById(Long id);
}
