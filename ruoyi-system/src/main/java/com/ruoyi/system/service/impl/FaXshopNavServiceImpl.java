package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopNavMapper;
import com.ruoyi.system.domain.FaXshopNav;
import com.ruoyi.system.service.IFaXshopNavService;

/**
 * 首页设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaXshopNavServiceImpl implements IFaXshopNavService 
{
    @Autowired
    private FaXshopNavMapper faXshopNavMapper;

    /**
     * 查询首页设置
     * 
     * @param id 首页设置ID
     * @return 首页设置
     */
    @Override
    public FaXshopNav selectFaXshopNavById(Integer id)
    {
        return faXshopNavMapper.selectFaXshopNavById(id);
    }

    /**
     * 查询首页设置列表
     * 
     * @param faXshopNav 首页设置
     * @return 首页设置
     */
    @Override
    public List<FaXshopNav> selectFaXshopNavList(FaXshopNav faXshopNav)
    {
        return faXshopNavMapper.selectFaXshopNavList(faXshopNav);
    }

    /**
     * 新增首页设置
     * 
     * @param faXshopNav 首页设置
     * @return 结果
     */
    @Override
    public int insertFaXshopNav(FaXshopNav faXshopNav)
    {
        return faXshopNavMapper.insertFaXshopNav(faXshopNav);
    }

    /**
     * 修改首页设置
     * 
     * @param faXshopNav 首页设置
     * @return 结果
     */
    @Override
    public int updateFaXshopNav(FaXshopNav faXshopNav)
    {
        return faXshopNavMapper.updateFaXshopNav(faXshopNav);
    }

    /**
     * 批量删除首页设置
     * 
     * @param ids 需要删除的首页设置ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopNavByIds(Integer[] ids)
    {
        return faXshopNavMapper.deleteFaXshopNavByIds(ids);
    }

    /**
     * 删除首页设置信息
     * 
     * @param id 首页设置ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopNavById(Integer id)
    {
        return faXshopNavMapper.deleteFaXshopNavById(id);
    }
}
