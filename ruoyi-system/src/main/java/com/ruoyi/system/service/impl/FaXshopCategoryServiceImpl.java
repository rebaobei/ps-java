package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopCategoryMapper;
import com.ruoyi.system.domain.FaXshopCategory;
import com.ruoyi.system.service.IFaXshopCategoryService;

/**
 * 商品分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-16
 */
@Service
public class FaXshopCategoryServiceImpl implements IFaXshopCategoryService 
{
    @Autowired
    private FaXshopCategoryMapper faXshopCategoryMapper;

    /**
     * 查询商品分类
     * 
     * @param id 商品分类ID
     * @return 商品分类
     */
    @Override
    public FaXshopCategory selectFaXshopCategoryById(Integer id)
    {
        return faXshopCategoryMapper.selectFaXshopCategoryById(id);
    }

    /**
     * 查询商品分类列表
     * 
     * @param faXshopCategory 商品分类
     * @return 商品分类
     */
    @Override
    public List<FaXshopCategory> selectFaXshopCategoryList(FaXshopCategory faXshopCategory)
    {
        return faXshopCategoryMapper.selectFaXshopCategoryList(faXshopCategory);
    }

    /**
     * 新增商品分类
     * 
     * @param faXshopCategory 商品分类
     * @return 结果
     */
    @Override
    public int insertFaXshopCategory(FaXshopCategory faXshopCategory)
    {
        faXshopCategory.setCreatetime(System.currentTimeMillis()/1000);
        return faXshopCategoryMapper.insertFaXshopCategory(faXshopCategory);
    }

    /**
     * 修改商品分类
     * 
     * @param faXshopCategory 商品分类
     * @return 结果
     */
    @Override
    public int updateFaXshopCategory(FaXshopCategory faXshopCategory)
    {
        faXshopCategory.setUpdatetime(System.currentTimeMillis()/1000);
        return faXshopCategoryMapper.updateFaXshopCategory(faXshopCategory);
    }

    /**
     * 批量删除商品分类
     * 
     * @param ids 需要删除的商品分类ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopCategoryByIds(Integer[] ids)
    {
        return faXshopCategoryMapper.deleteFaXshopCategoryByIds(ids);
    }

    /**
     * 删除商品分类信息
     * 
     * @param id 商品分类ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopCategoryById(Integer id)
    {
        return faXshopCategoryMapper.deleteFaXshopCategoryById(id);
    }
}
