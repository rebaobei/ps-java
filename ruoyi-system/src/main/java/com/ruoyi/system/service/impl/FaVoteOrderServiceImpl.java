package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.VoteOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaVoteOrderMapper;
import com.ruoyi.system.domain.FaVoteOrder;
import com.ruoyi.system.service.IFaVoteOrderService;

/**
 * 打赏订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@Service
public class FaVoteOrderServiceImpl implements IFaVoteOrderService 
{
    @Autowired
    private FaVoteOrderMapper faVoteOrderMapper;

    /**
     * 查询打赏订单
     * 
     * @param id 打赏订单ID
     * @return 打赏订单
     */
    @Override
    public FaVoteOrder selectFaVoteOrderById(Long id)
    {
        return faVoteOrderMapper.selectFaVoteOrderById(id);
    }

    /**
     * 查询打赏订单列表
     * 
     * @param faVoteOrder 打赏订单
     * @return 打赏订单
     */
    @Override
    public List<FaVoteOrder> selectFaVoteOrderList(FaVoteOrder faVoteOrder)
    {
        return faVoteOrderMapper.selectFaVoteOrderList(faVoteOrder);
    }

    /**
     * 新增打赏订单
     * 
     * @param faVoteOrder 打赏订单
     * @return 结果
     */
    @Override
    public int insertFaVoteOrder(FaVoteOrder faVoteOrder)
    {
        faVoteOrder.setCreatetime(MyDateUtils.nowTimestamp());
        faVoteOrder.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteOrderMapper.insertFaVoteOrder(faVoteOrder);
    }

    /**
     * 修改打赏订单
     * 
     * @param faVoteOrder 打赏订单
     * @return 结果
     */
    @Override
    public int updateFaVoteOrder(FaVoteOrder faVoteOrder)
    {
        faVoteOrder.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteOrderMapper.updateFaVoteOrder(faVoteOrder);
    }

    /**
     * 批量删除打赏订单
     * 
     * @param ids 需要删除的打赏订单ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteOrderByIds(Long[] ids)
    {
        return faVoteOrderMapper.deleteFaVoteOrderByIds(ids);
    }

    /**
     * 删除打赏订单信息
     * 
     * @param id 打赏订单ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteOrderById(Long id)
    {
        return faVoteOrderMapper.deleteFaVoteOrderById(id);
    }

    @Override
    public List<VoteOrderVo> listVoteOrderVo(VoteOrderVo faVoteOrder) {
        return faVoteOrderMapper.listVoteOrderVo(faVoteOrder);
    }
}
