package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.FaGameGoods;
import com.ruoyi.system.mapper.FaGameGoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameThingsMapper;
import com.ruoyi.system.domain.FaGameThings;
import com.ruoyi.system.service.IFaGameThingsService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 物品管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameThingsServiceImpl implements IFaGameThingsService {
    @Autowired
    private FaGameThingsMapper faGameThingsMapper;

    @Autowired
    private FaGameGoodsMapper faGameGoodsMapper;

    /**
     * 查询物品管理
     *
     * @param id 物品管理ID
     * @return 物品管理
     */
    @Override
    public FaGameThings selectFaGameThingsById(Long id) {
        return faGameThingsMapper.selectFaGameThingsById(id);
    }

    /**
     * 查询物品管理列表
     *
     * @param faGameThings 物品管理
     * @return 物品管理
     */
    @Override
    public List<FaGameThings> selectFaGameThingsList(FaGameThings faGameThings) {
        return faGameThingsMapper.selectFaGameThingsList(faGameThings);
    }

    /**
     * 新增物品管理
     *
     * @param faGameThings 物品管理
     * @return 结果
     */
    @Override
    public int insertFaGameThings(FaGameThings faGameThings) {
        return faGameThingsMapper.insertFaGameThings(faGameThings);
    }

    /**
     * 修改物品管理 同时更新商品图片 名称
     *
     * @param faGameThings 物品管理
     * @return 结果
     */
    @Override
    @Transactional
    public int updateFaGameThings(FaGameThings faGameThings) {
        FaGameGoods faGameGoods = faGameGoodsMapper.selectGoodsByThingsId(faGameThings.getId());
        if (faGameGoods != null) {
            faGameGoods.setImage(faGameThings.getImage());
            faGameGoods.setName(faGameThings.getName());
            faGameGoodsMapper.updateFaGameGoods(faGameGoods);
        }
        return faGameThingsMapper.updateFaGameThings(faGameThings);
    }

    /**
     * 批量删除物品管理
     *
     * @param ids 需要删除的物品管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGameThingsByIds(Long[] ids) {
        return faGameThingsMapper.deleteFaGameThingsByIds(ids);
    }

    /**
     * 删除物品管理信息
     *
     * @param id 物品管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGameThingsById(Long id) {
        return faGameThingsMapper.deleteFaGameThingsById(id);
    }
}
