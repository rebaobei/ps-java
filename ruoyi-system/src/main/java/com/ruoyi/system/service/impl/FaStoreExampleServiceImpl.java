package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStoreExampleMapper;
import com.ruoyi.system.domain.FaStoreExample;
import com.ruoyi.system.service.IFaStoreExampleService;

/**
 * 店铺服务案Service业务层处理
 *
 * @author ruoyi
 * @date 2021-10-20
 */
@Service
public class FaStoreExampleServiceImpl implements IFaStoreExampleService
{
    @Autowired
    private FaStoreExampleMapper faStoreExampleMapper;

    /**
     * 查询店铺服务案
     *
     * @param id 店铺服务案ID
     * @return 店铺服务案
     */
    @Override
    public FaStoreExample selectFaStoreExampleById(Long id)
    {
        FaStoreExample faStoreExample = faStoreExampleMapper.selectFaStoreExampleById(id);
        String personId = faStoreExample.getPersonId();
            if(personId!=null){
                String[] split = personId.split(",");
                faStoreExample.setPersonIds(split);
            }
        return faStoreExample;
    }

    /**
     * 查询店铺服务案列表
     *
     * @param faStoreExample 店铺服务案
     * @return 店铺服务案
     */
    @Override
    public List<FaStoreExample> selectFaStoreExampleList(FaStoreExample faStoreExample)
    {
        List<FaStoreExample> faStoreExamples = faStoreExampleMapper.selectFaStoreExampleList(faStoreExample);
        return faStoreExamples;
    }

    /**
     * 新增店铺服务案
     *
     * @param faStoreExample 店铺服务案
     * @return 结果
     */
    @Override
    public int insertFaStoreExample(FaStoreExample faStoreExample)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faStoreExample.setStoreId(storeInfoId);
        faStoreExample.setCreateTime(DateUtils.getNowDate());
        return faStoreExampleMapper.insertFaStoreExample(faStoreExample);
    }

    /**
     * 修改店铺服务案
     *
     * @param faStoreExample 店铺服务案
     * @return 结果
     */
    @Override
    public int updateFaStoreExample(FaStoreExample faStoreExample)
    {
        faStoreExample.setUpdateTime(DateUtils.getNowDate());
        return faStoreExampleMapper.updateFaStoreExample(faStoreExample);
    }

    /**
     * 批量删除店铺服务案
     *
     * @param ids 需要删除的店铺服务案ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreExampleByIds(Long[] ids)
    {
        return faStoreExampleMapper.deleteFaStoreExampleByIds(ids);
    }

    /**
     * 删除店铺服务案信息
     *
     * @param id 店铺服务案ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreExampleById(Long id)
    {
        return faStoreExampleMapper.deleteFaStoreExampleById(id);
    }
}
