package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.RechargeOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaRechargeOrderMapper;
import com.ruoyi.system.domain.FaRechargeOrder;
import com.ruoyi.system.service.IFaRechargeOrderService;

/**
 * 充值订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaRechargeOrderServiceImpl implements IFaRechargeOrderService 
{
    @Autowired
    private FaRechargeOrderMapper faRechargeOrderMapper;

    /**
     * 查询充值订单
     * 
     * @param id 充值订单ID
     * @return 充值订单
     */
    @Override
    public FaRechargeOrder selectFaRechargeOrderById(Long id)
    {
        return faRechargeOrderMapper.selectFaRechargeOrderById(id);
    }

    /**
     * 查询充值订单列表
     * 
     * @param faRechargeOrder 充值订单
     * @return 充值订单
     */
    @Override
    public List<FaRechargeOrder> selectFaRechargeOrderList(FaRechargeOrder faRechargeOrder)
    {
        return faRechargeOrderMapper.selectFaRechargeOrderList(faRechargeOrder);
    }

    /**
     * 新增充值订单
     * 
     * @param faRechargeOrder 充值订单
     * @return 结果
     */
    @Override
    public int insertFaRechargeOrder(FaRechargeOrder faRechargeOrder)
    {
        faRechargeOrder.setCreatetime(MyDateUtils.nowTimestamp());
        return faRechargeOrderMapper.insertFaRechargeOrder(faRechargeOrder);
    }

    /**
     * 修改充值订单
     * 
     * @param faRechargeOrder 充值订单
     * @return 结果
     */
    @Override
    public int updateFaRechargeOrder(FaRechargeOrder faRechargeOrder)
    {
        return faRechargeOrderMapper.updateFaRechargeOrder(faRechargeOrder);
    }

    /**
     * 批量删除充值订单
     * 
     * @param ids 需要删除的充值订单ID
     * @return 结果
     */
    @Override
    public int deleteFaRechargeOrderByIds(Long[] ids)
    {
        return faRechargeOrderMapper.deleteFaRechargeOrderByIds(ids);
    }

    /**
     * 删除充值订单信息
     * 
     * @param id 充值订单ID
     * @return 结果
     */
    @Override
    public int deleteFaRechargeOrderById(Long id)
    {
        return faRechargeOrderMapper.deleteFaRechargeOrderById(id);
    }

    @Override
    public List<RechargeOrderVo> listRechargeOrderVo(RechargeOrderVo faRechargeOrder) {
        return faRechargeOrderMapper.listRechargeOrderVo(faRechargeOrder);
    }
}
