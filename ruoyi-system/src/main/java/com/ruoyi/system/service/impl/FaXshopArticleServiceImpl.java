package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopArticleMapper;
import com.ruoyi.system.domain.FaXshopArticle;
import com.ruoyi.system.service.IFaXshopArticleService;

/**
 * 规则列表Service业务层处理
 * 
 * @author zzp
 * @date 2021-08-04
 */
@Service
public class FaXshopArticleServiceImpl implements IFaXshopArticleService 
{
    @Autowired
    private FaXshopArticleMapper faXshopArticleMapper;

    /**
     * 查询规则列表
     * 
     * @param id 规则列表ID
     * @return 规则列表
     */
    @Override
    public FaXshopArticle selectFaXshopArticleById(Long id)
    {
        return faXshopArticleMapper.selectFaXshopArticleById(id);
    }

    /**
     * 查询规则列表列表
     * 
     * @param faXshopArticle 规则列表
     * @return 规则列表
     */
    @Override
    public List<FaXshopArticle> selectFaXshopArticleList(FaXshopArticle faXshopArticle)
    {
        return faXshopArticleMapper.selectFaXshopArticleList(faXshopArticle);
    }

    /**
     * 新增规则列表
     * 
     * @param faXshopArticle 规则列表
     * @return 结果
     */
    @Override
    public int insertFaXshopArticle(FaXshopArticle faXshopArticle)
    {
        faXshopArticle.setCreateTime(MyDateUtils.nowTimestamp());
        faXshopArticle.setUpdateTime(MyDateUtils.nowTimestamp());
        return faXshopArticleMapper.insertFaXshopArticle(faXshopArticle);
    }

    /**
     * 修改规则列表
     * 
     * @param faXshopArticle 规则列表
     * @return 结果
     */
    @Override
    public int updateFaXshopArticle(FaXshopArticle faXshopArticle)
    {
        faXshopArticle.setUpdateTime(MyDateUtils.nowTimestamp());
        return faXshopArticleMapper.updateFaXshopArticle(faXshopArticle);
    }

    /**
     * 批量删除规则列表
     * 
     * @param ids 需要删除的规则列表ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopArticleByIds(Long[] ids)
    {
        return faXshopArticleMapper.deleteFaXshopArticleByIds(ids);
    }

    /**
     * 删除规则列表信息
     * 
     * @param id 规则列表ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopArticleById(Long id)
    {
        return faXshopArticleMapper.deleteFaXshopArticleById(id);
    }
}
