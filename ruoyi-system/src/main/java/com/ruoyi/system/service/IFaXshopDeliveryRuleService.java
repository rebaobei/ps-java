package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaXshopDeliveryRule;

/**
 * 运费规则Service接口
 * 
 * @author ruoyi
 * @date 2021-07-20
 */
public interface IFaXshopDeliveryRuleService 
{
    /**
     * 查询运费规则
     * 
     * @param id 运费规则ID
     * @return 运费规则
     */
    public FaXshopDeliveryRule selectFaXshopDeliveryRuleById(Integer id);

    /**
     * 查询运费规则列表
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 运费规则集合
     */
    public List<FaXshopDeliveryRule> selectFaXshopDeliveryRuleList(FaXshopDeliveryRule faXshopDeliveryRule);

    /**
     * 新增运费规则
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 结果
     */
    public int insertFaXshopDeliveryRule(FaXshopDeliveryRule faXshopDeliveryRule);

    /**
     * 修改运费规则
     * 
     * @param faXshopDeliveryRule 运费规则
     * @return 结果
     */
    public int updateFaXshopDeliveryRule(FaXshopDeliveryRule faXshopDeliveryRule);

    /**
     * 批量删除运费规则
     * 
     * @param ids 需要删除的运费规则ID
     * @return 结果
     */
    public int deleteFaXshopDeliveryRuleByIds(Integer[] ids);

    /**
     * 删除运费规则信息
     * 
     * @param id 运费规则ID
     * @return 结果
     */
    public int deleteFaXshopDeliveryRuleById(Integer id);

    /**
     * 获取运费规则列表通过运费模板id
     */
    List<FaXshopDeliveryRule> listRuleByTemplateId(Integer id);
}
