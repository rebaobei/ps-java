package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaXshopServiceTag;

/**
 * 服务标签Service接口
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
public interface IFaXshopServiceTagService 
{
    /**
     * 查询服务标签
     * 
     * @param id 服务标签ID
     * @return 服务标签
     */
    public FaXshopServiceTag selectFaXshopServiceTagById(Integer id);

    /**
     * 查询服务标签列表
     * 
     * @param faXshopServiceTag 服务标签
     * @return 服务标签集合
     */
    public List<FaXshopServiceTag> selectFaXshopServiceTagList(FaXshopServiceTag faXshopServiceTag);

    /**
     * 新增服务标签
     * 
     * @param faXshopServiceTag 服务标签
     * @return 结果
     */
    public int insertFaXshopServiceTag(FaXshopServiceTag faXshopServiceTag);

    /**
     * 修改服务标签
     * 
     * @param faXshopServiceTag 服务标签
     * @return 结果
     */
    public int updateFaXshopServiceTag(FaXshopServiceTag faXshopServiceTag);

    /**
     * 批量删除服务标签
     * 
     * @param ids 需要删除的服务标签ID
     * @return 结果
     */
    public int deleteFaXshopServiceTagByIds(Integer[] ids);

    /**
     * 删除服务标签信息
     * 
     * @param id 服务标签ID
     * @return 结果
     */
    public int deleteFaXshopServiceTagById(Integer id);
}
