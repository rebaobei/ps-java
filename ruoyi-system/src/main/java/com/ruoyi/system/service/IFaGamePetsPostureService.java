package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGamePetsPosture;

/**
 * 宠物姿势Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGamePetsPostureService 
{
    /**
     * 查询宠物姿势
     * 
     * @param id 宠物姿势ID
     * @return 宠物姿势
     */
    public FaGamePetsPosture selectFaGamePetsPostureById(Long id);

    /**
     * 查询宠物姿势列表
     * 
     * @param faGamePetsPosture 宠物姿势
     * @return 宠物姿势集合
     */
    public List<FaGamePetsPosture> selectFaGamePetsPostureList(FaGamePetsPosture faGamePetsPosture);

    /**
     * 新增宠物姿势
     * 
     * @param faGamePetsPosture 宠物姿势
     * @return 结果
     */
    public int insertFaGamePetsPosture(FaGamePetsPosture faGamePetsPosture);

    /**
     * 修改宠物姿势
     * 
     * @param faGamePetsPosture 宠物姿势
     * @return 结果
     */
    public int updateFaGamePetsPosture(FaGamePetsPosture faGamePetsPosture);

    /**
     * 批量删除宠物姿势
     * 
     * @param ids 需要删除的宠物姿势ID
     * @return 结果
     */
    public int deleteFaGamePetsPostureByIds(Long[] ids);

    /**
     * 删除宠物姿势信息
     * 
     * @param id 宠物姿势ID
     * @return 结果
     */
    public int deleteFaGamePetsPostureById(Long id);
}
