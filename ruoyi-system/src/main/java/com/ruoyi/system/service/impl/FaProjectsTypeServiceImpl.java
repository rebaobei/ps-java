package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.vo.ProjectTypeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaProjectsTypeMapper;
import com.ruoyi.system.domain.FaProjectsType;
import com.ruoyi.system.service.IFaProjectsTypeService;

/**
 * 宠物大小管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@Service
public class FaProjectsTypeServiceImpl implements IFaProjectsTypeService 
{
    @Autowired
    private FaProjectsTypeMapper faProjectsTypeMapper;

    /**
     * 查询宠物大小管理
     * 
     * @param id 宠物大小管理ID
     * @return 宠物大小管理
     */
    @Override
    public FaProjectsType selectFaProjectsTypeById(Long id)
    {
        return faProjectsTypeMapper.selectFaProjectsTypeById(id);
    }

    /**
     * 查询宠物大小管理列表
     * 
     * @param faProjectsType 宠物大小管理
     * @return 宠物大小管理
     */
    @Override
    public List<FaProjectsType> selectFaProjectsTypeList(FaProjectsType faProjectsType)
    {
        return faProjectsTypeMapper.selectFaProjectsTypeList(faProjectsType);
    }

    /**
     * 新增宠物大小管理
     * 
     * @param faProjectsType 宠物大小管理
     * @return 结果
     */
    @Override
    public int insertFaProjectsType(FaProjectsType faProjectsType)
    {
        faProjectsType.setCreatetime(MyDateUtils.nowTimestamp());
        return faProjectsTypeMapper.insertFaProjectsType(faProjectsType);
    }

    /**
     * 修改宠物大小管理
     * 
     * @param faProjectsType 宠物大小管理
     * @return 结果
     */
    @Override
    public int updateFaProjectsType(FaProjectsType faProjectsType)
    {
        return faProjectsTypeMapper.updateFaProjectsType(faProjectsType);
    }

    /**
     * 批量删除宠物大小管理
     * 
     * @param ids 需要删除的宠物大小管理ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectsTypeByIds(Long[] ids)
    {
        return faProjectsTypeMapper.deleteFaProjectsTypeByIds(ids);
    }

    /**
     * 删除宠物大小管理信息
     * 
     * @param id 宠物大小管理ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectsTypeById(Long id)
    {
        return faProjectsTypeMapper.deleteFaProjectsTypeById(id);
    }

    @Override
    public List<FaProjectsType> list(FaProjectsType faProjectsType) {
        return faProjectsTypeMapper.list(faProjectsType);
    }

    @Override
    public List<ProjectTypeVo> listProjectTypeVo(FaProjectsType faProjectsType) {
        return faProjectsTypeMapper.listProjectTypeVo(faProjectsType);
    }
}
