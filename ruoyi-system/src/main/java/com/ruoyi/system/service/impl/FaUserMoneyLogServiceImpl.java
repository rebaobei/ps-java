package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.MoneyLogWithUserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserMoneyLogMapper;
import com.ruoyi.system.domain.FaUserMoneyLog;
import com.ruoyi.system.service.IFaUserMoneyLogService;

/**
 * 余额变动Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaUserMoneyLogServiceImpl implements IFaUserMoneyLogService 
{
    @Autowired
    private FaUserMoneyLogMapper faUserMoneyLogMapper;

    /**
     * 查询余额变动
     * 
     * @param id 余额变动ID
     * @return 余额变动
     */
    @Override
    public FaUserMoneyLog selectFaUserMoneyLogById(Integer id)
    {
        return faUserMoneyLogMapper.selectFaUserMoneyLogById(id);
    }

    /**
     * 查询余额变动列表
     * 
     * @param faUserMoneyLog 余额变动
     * @return 余额变动
     */
    @Override
    public List<FaUserMoneyLog> selectFaUserMoneyLogList(FaUserMoneyLog faUserMoneyLog)
    {
        return faUserMoneyLogMapper.selectFaUserMoneyLogList(faUserMoneyLog);
    }

    /**
     * 新增余额变动
     * 
     * @param faUserMoneyLog 余额变动
     * @return 结果
     */
    @Override
    public int insertFaUserMoneyLog(FaUserMoneyLog faUserMoneyLog)
    {
        return faUserMoneyLogMapper.insertFaUserMoneyLog(faUserMoneyLog);
    }

    /**
     * 修改余额变动
     * 
     * @param faUserMoneyLog 余额变动
     * @return 结果
     */
    @Override
    public int updateFaUserMoneyLog(FaUserMoneyLog faUserMoneyLog)
    {
        return faUserMoneyLogMapper.updateFaUserMoneyLog(faUserMoneyLog);
    }

    /**
     * 批量删除余额变动
     * 
     * @param ids 需要删除的余额变动ID
     * @return 结果
     */
    @Override
    public int deleteFaUserMoneyLogByIds(Integer[] ids)
    {
        return faUserMoneyLogMapper.deleteFaUserMoneyLogByIds(ids);
    }

    /**
     * 删除余额变动信息
     * 
     * @param id 余额变动ID
     * @return 结果
     */
    @Override
    public int deleteFaUserMoneyLogById(Integer id)
    {
        return faUserMoneyLogMapper.deleteFaUserMoneyLogById(id);
    }

    @Override
    public List<MoneyLogWithUserInfoVo> listMoneyLogWithUserInfo(FaUserMoneyLog faUserMoneyLog) {
        return faUserMoneyLogMapper.listMoneyLogWithUserInfo(faUserMoneyLog);
    }
}
