package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.VoteUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaVoteUserMapper;
import com.ruoyi.system.domain.FaVoteUser;
import com.ruoyi.system.service.IFaVoteUserService;

/**
 * 参赛人员Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@Service
public class FaVoteUserServiceImpl implements IFaVoteUserService 
{
    @Autowired
    private FaVoteUserMapper faVoteUserMapper;

    /**
     * 查询参赛人员
     * 
     * @param id 参赛人员ID
     * @return 参赛人员
     */
    @Override
    public FaVoteUser selectFaVoteUserById(Long id)
    {
        return faVoteUserMapper.selectFaVoteUserById(id);
    }

    /**
     * 查询参赛人员列表
     * 
     * @param faVoteUser 参赛人员
     * @return 参赛人员
     */
    @Override
    public List<FaVoteUser> selectFaVoteUserList(FaVoteUser faVoteUser)
    {
        return faVoteUserMapper.selectFaVoteUserList(faVoteUser);
    }

    /**
     * 新增参赛人员
     * 
     * @param faVoteUser 参赛人员
     * @return 结果
     */
    @Override
    public int insertFaVoteUser(FaVoteUser faVoteUser)
    {
        faVoteUser.setCreatetime(MyDateUtils.nowTimestamp());
        faVoteUser.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteUserMapper.insertFaVoteUser(faVoteUser);
    }

    /**
     * 修改参赛人员
     * 
     * @param faVoteUser 参赛人员
     * @return 结果
     */
    @Override
    public int updateFaVoteUser(FaVoteUser faVoteUser)
    {
        faVoteUser.setUpdatetime(MyDateUtils.nowTimestamp());
        return faVoteUserMapper.updateFaVoteUser(faVoteUser);
    }

    /**
     * 批量删除参赛人员
     * 
     * @param ids 需要删除的参赛人员ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteUserByIds(Long[] ids)
    {
        return faVoteUserMapper.deleteFaVoteUserByIds(ids);
    }

    /**
     * 删除参赛人员信息
     * 
     * @param id 参赛人员ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteUserById(Long id)
    {
        return faVoteUserMapper.deleteFaVoteUserById(id);
    }

    @Override
    public List<VoteUserVo> listVoteUserVo(VoteUserVo faVoteUser) {
        List<VoteUserVo> voteUserVos = faVoteUserMapper.listVoteUserVo(faVoteUser);
        if (voteUserVos != null){
            for (VoteUserVo vo : voteUserVos){
                if (vo.getImages() != null && vo.getImages().length()>0){
                    String[] split = vo.getImages().split(",");
                    vo.setImageList(Arrays.asList(split));
                }
            }
        }
        return voteUserVos;
    }
}
