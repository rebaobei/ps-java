package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.system.domain.vo.StoreVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStoreMapper;
import com.ruoyi.system.domain.FaStore;
import com.ruoyi.system.service.IFaStoreService;

/**
 * 门店管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
@Service
public class FaStoreServiceImpl implements IFaStoreService 
{
    @Autowired
    private FaStoreMapper faStoreMapper;

    /**
     * 查询门店管理
     * 
     * @param id 门店管理ID
     * @return 门店管理
     */
    @Override
    public FaStore selectFaStoreById(Long id)
    {
        return faStoreMapper.selectFaStoreById(id);
    }

    /**
     * 查询门店管理列表
     * 
     * @param faStore 门店管理
     * @return 门店管理
     */
    @Override
    public List<FaStore> selectFaStoreList(FaStore faStore)
    {
        return faStoreMapper.selectFaStoreList(faStore);
    }

    /**
     * 新增门店管理
     * 
     * @param faStore 门店管理
     * @return 结果
     */
    @Override
    public int insertFaStore(FaStore faStore)
    {
        faStore.setUpdatetime(System.currentTimeMillis()/1000L);
        faStore.setCreatetime(System.currentTimeMillis()/1000L);
        return faStoreMapper.insertFaStore(faStore);
    }

    /**
     * 修改门店管理
     * 
     * @param faStore 门店管理
     * @return 结果
     */
    @Override
    public int updateFaStore(FaStore faStore)
    {
        faStore.setUpdatetime(System.currentTimeMillis()/1000L);
        return faStoreMapper.updateFaStore(faStore);
    }

    /**
     * 批量删除门店管理
     * 
     * @param ids 需要删除的门店管理ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreByIds(Long[] ids)
    {
        return faStoreMapper.deleteFaStoreByIds(ids);
    }

    /**
     * 删除门店管理信息
     * 
     * @param id 门店管理ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreById(Long id)
    {
        return faStoreMapper.deleteFaStoreById(id);
    }

    @Override
    public List<StoreVo> listStoreVo(FaStore faStore) {
        List<StoreVo> storeVos = faStoreMapper.listStoreVo(faStore);
        List<StoreVo> collect = storeVos.stream().peek((vo) -> {
            if (vo.getBannerimages() != null && vo.getBannerimages().length() > 0) {
                String[] split = vo.getBannerimages().split(",");
                vo.setLunboImage(Arrays.asList(split));
            }
        }).collect(Collectors.toList());
        return collect;
    }
}
