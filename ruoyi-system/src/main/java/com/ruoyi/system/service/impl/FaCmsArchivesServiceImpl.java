package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.FaCmsAddonnews;
import com.ruoyi.system.domain.vo.CmsArchivesVo;
import com.ruoyi.system.mapper.FaCmsAddonnewsMapper;
import org.apache.commons.compress.archivers.ar.ArArchiveEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaCmsArchivesMapper;
import com.ruoyi.system.domain.FaCmsArchives;
import com.ruoyi.system.service.IFaCmsArchivesService;

/**
 * 文章管理Service业务层处理
 *
 * @author zzp
 * @date 2021-08-11
 */
@Service
public class FaCmsArchivesServiceImpl implements IFaCmsArchivesService {
    @Autowired
    private FaCmsArchivesMapper faCmsArchivesMapper;

    @Autowired
    private FaCmsAddonnewsMapper faCmsAddonnewsMapper;

    /**
     * 查询文章管理
     *
     * @param id 文章管理ID
     * @return 文章管理
     */
    @Override
    public FaCmsArchives selectFaCmsArchivesById(Integer id) {
        return faCmsArchivesMapper.selectFaCmsArchivesById(id);
    }

    /**
     * 查询文章管理列表
     *
     * @param faCmsArchives 文章管理
     * @return 文章管理
     */
    @Override
    public List<FaCmsArchives> selectFaCmsArchivesList(FaCmsArchives faCmsArchives) {
        return faCmsArchivesMapper.selectFaCmsArchivesList(faCmsArchives);
    }

    /**
     * 新增文章管理
     *
     * @param
     * @return 结果
     */
    @Override
    public int insertFaCmsArchives(CmsArchivesVo vo) {
        // FaCmsArchives archives = new FaCmsArchives();
        vo.setUpdatetime(System.currentTimeMillis()/1000);
        vo.setCreatetime(System.currentTimeMillis()/1000);
        vo.setModelId(1L);
        faCmsArchivesMapper.insertArticleVo(vo);
        FaCmsAddonnews news = new FaCmsAddonnews();
        news.setId(Long.valueOf(String.valueOf(vo.getId())));
        if (vo.getContent() != null) {
            news.setContent(vo.getContent());
        }
        return faCmsAddonnewsMapper.insertFaCmsAddonnews(news);
    }

    /**
     * 修改文章管理
     *
     * @return 结果
     */
    @Override
    public int updateFaCmsArchives(CmsArchivesVo vo) {
        vo.setUpdatetime(System.currentTimeMillis()/1000);
        faCmsArchivesMapper.updateArticleVo(vo);
        FaCmsAddonnews news = new FaCmsAddonnews();
        news.setId(Long.valueOf(String.valueOf(vo.getId())));
        news.setContent(vo.getContent());
        return faCmsAddonnewsMapper.updateFaCmsAddonnews(news);
    }

    /**
     * 批量删除文章管理
     *
     * @param ids 需要删除的文章管理ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsArchivesByIds(Integer[] ids) {
        return faCmsArchivesMapper.deleteFaCmsArchivesByIds(ids);
    }

    /**
     * 删除文章管理信息
     *
     * @param id 文章管理ID
     * @return 结果
     */
    @Override
    public int deleteFaCmsArchivesById(Integer id) {
        return faCmsArchivesMapper.deleteFaCmsArchivesById(id);
    }

    @Override
    public CmsArchivesVo selectFaCmsArchivesVoById(Integer id) {
        return faCmsArchivesMapper.selectFaCmsArchivesVoById(id);
    }
}
