package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaYuyueOrder;

/**
 * 预约订单Service接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface IFaYuyueOrderService 
{
    /**
     * 查询预约订单
     * 
     * @param id 预约订单ID
     * @return 预约订单
     */
    public FaYuyueOrder selectFaYuyueOrderById(Long id);

    /**
     * 查询预约订单列表
     * 
     * @param faYuyueOrder 预约订单
     * @return 预约订单集合
     */
    public List<FaYuyueOrder> selectFaYuyueOrderList(FaYuyueOrder faYuyueOrder);

    /**
     * 新增预约订单
     * 
     * @param faYuyueOrder 预约订单
     * @return 结果
     */
    public int insertFaYuyueOrder(FaYuyueOrder faYuyueOrder);

    /**
     * 修改预约订单
     * 
     * @param faYuyueOrder 预约订单
     * @return 结果
     */
    public int updateFaYuyueOrder(FaYuyueOrder faYuyueOrder);

    /**
     * 批量删除预约订单
     * 
     * @param ids 需要删除的预约订单ID
     * @return 结果
     */
    public int deleteFaYuyueOrderByIds(Long[] ids);

    /**
     * 删除预约订单信息
     * 
     * @param id 预约订单ID
     * @return 结果
     */
    public int deleteFaYuyueOrderById(Long id);
}
