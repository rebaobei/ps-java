package com.ruoyi.system.service.impl;

import java.util.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.ProductVo;
import com.ruoyi.system.domain.vo.ProjectVo;
import com.ruoyi.system.domain.vo.XshopOrderProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopProductMapper;
import com.ruoyi.system.domain.FaXshopProduct;
import com.ruoyi.system.service.IFaXshopProductService;

/**
 * 商品Service业务层处理
 *
 * @author ruoyi
 * @date 2021-07-16
 */
@Service
public class FaXshopProductServiceImpl implements IFaXshopProductService
{
    @Autowired
    private FaXshopProductMapper faXshopProductMapper;

    /**
     * 查询商品
     *
     * @param id 商品ID
     * @return 商品
     */
    @Override
    public FaXshopProduct selectFaXshopProductById(Integer id)
    {
        return faXshopProductMapper.selectFaXshopProductById(id);
    }

    /**
     * 查询商品列表
     *
     * @param faXshopProduct 商品
     * @return 商品
     */
    @Override
    public List<FaXshopProduct> selectFaXshopProductList(FaXshopProduct faXshopProduct)
    {

        return faXshopProductMapper.selectFaXshopProductList(faXshopProduct);
    }

    /**
     * 新增商品
     *
     * @param faXshopProduct 商品
     * @return 结果
     */
    @Override
    public int insertFaXshopProduct(FaXshopProduct faXshopProduct)
    {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        faXshopProduct.setCreatetime(System.currentTimeMillis()/1000L);
        faXshopProduct.setStoreInfoId(storeInfoId);
        faXshopProduct.setAuditStatus(0);
        return faXshopProductMapper.insertFaXshopProduct(faXshopProduct);
    }

    /**
     * 修改商品
     *
     * @param faXshopProduct 商品
     * @return 结果
     */
    @Override
    public int updateFaXshopProduct(FaXshopProduct faXshopProduct)
    {

        faXshopProduct.setUpdatetime(System.currentTimeMillis()/1000L);
        return faXshopProductMapper.updateFaXshopProduct(faXshopProduct);
    }

    /**
     * 批量删除商品
     *
     * @param ids 需要删除的商品ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopProductByIds(Integer[] ids)
    {
        return faXshopProductMapper.deleteFaXshopProductByIds(ids);
    }

    /**
     * 删除商品信息
     *
     * @param id 商品ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopProductById(Integer id)
    {
        return faXshopProductMapper.deleteFaXshopProductById(id);
    }

    @Override
    public List<ProductVo> listProductVo(FaXshopProduct product) {
        List<ProductVo> productVos = faXshopProductMapper.listProductVo(product);
        for (ProductVo vo : productVos) {
            if (vo.getImage() != null && vo.getImage().length() > 0) {
                String[] split = vo.getImage().split(",");
                vo.setImageList(Arrays.asList(split));
            }
        }
        return productVos;
    }

    @Override
    public List<Map<String, Object>> listProductVoForSelect() {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        FaXshopProduct faXshopProduct = new FaXshopProduct();
        faXshopProduct.setStoreInfoId(storeInfoId);
        List<ProductVo> productVos = faXshopProductMapper.listProductVo(faXshopProduct);
        List<Map<String,Object>> result = new ArrayList<>();
        for (ProductVo vo:productVos){
            Map<String,Object> map = new HashMap<>();
            map.put("id",vo.getId());
            map.put("title",vo.getTitle());
            result.add(map);
        }
        return result;
    }
}
