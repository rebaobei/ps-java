package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.vo.XshopOrderProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopOrderProductMapper;
import com.ruoyi.system.domain.FaXshopOrderProduct;
import com.ruoyi.system.service.IFaXshopOrderProductService;

/**
 * 订单商品Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaXshopOrderProductServiceImpl implements IFaXshopOrderProductService 
{
    @Autowired
    private FaXshopOrderProductMapper faXshopOrderProductMapper;

    /**
     * 查询订单商品
     * 
     * @param id 订单商品ID
     * @return 订单商品
     */
    @Override
    public FaXshopOrderProduct selectFaXshopOrderProductById(Integer id)
    {
        return faXshopOrderProductMapper.selectFaXshopOrderProductById(id);
    }

    /**
     * 查询订单商品列表
     * 
     * @param faXshopOrderProduct 订单商品
     * @return 订单商品
     */
    @Override
    public List<FaXshopOrderProduct> selectFaXshopOrderProductList(FaXshopOrderProduct faXshopOrderProduct)
    {
        return faXshopOrderProductMapper.selectFaXshopOrderProductList(faXshopOrderProduct);
    }

    /**
     * 新增订单商品
     * 
     * @param faXshopOrderProduct 订单商品
     * @return 结果
     */
    @Override
    public int insertFaXshopOrderProduct(FaXshopOrderProduct faXshopOrderProduct)
    {
        faXshopOrderProduct.setCreateTime(MyDateUtils.nowTimestamp());
        faXshopOrderProduct.setUpdateTime(MyDateUtils.nowTimestamp());
        return faXshopOrderProductMapper.insertFaXshopOrderProduct(faXshopOrderProduct);
    }

    /**
     * 修改订单商品
     * 
     * @param faXshopOrderProduct 订单商品
     * @return 结果
     */
    @Override
    public int updateFaXshopOrderProduct(FaXshopOrderProduct faXshopOrderProduct)
    {
        faXshopOrderProduct.setUpdateTime(MyDateUtils.nowTimestamp());
        return faXshopOrderProductMapper.updateFaXshopOrderProduct(faXshopOrderProduct);
    }

    /**
     * 批量删除订单商品
     * 
     * @param ids 需要删除的订单商品ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopOrderProductByIds(Integer[] ids)
    {
        return faXshopOrderProductMapper.deleteFaXshopOrderProductByIds(ids);
    }

    /**
     * 删除订单商品信息
     * 
     * @param id 订单商品ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopOrderProductById(Integer id)
    {
        return faXshopOrderProductMapper.deleteFaXshopOrderProductById(id);
    }

    @Override
    public List<XshopOrderProductVo> listXshopOrderProductVo(XshopOrderProductVo faXshopOrderProduct) {
        return faXshopOrderProductMapper.listXshopOrderProductVo(faXshopOrderProduct);
    }


}
