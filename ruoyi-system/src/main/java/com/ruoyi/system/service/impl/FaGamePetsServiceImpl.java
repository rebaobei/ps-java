package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGamePetsMapper;
import com.ruoyi.system.domain.FaGamePets;
import com.ruoyi.system.service.IFaGamePetsService;

/**
 * 宠物管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGamePetsServiceImpl implements IFaGamePetsService 
{
    @Autowired
    private FaGamePetsMapper faGamePetsMapper;

    /**
     * 查询宠物管理
     * 
     * @param petId 宠物管理ID
     * @return 宠物管理
     */
    @Override
    public FaGamePets selectFaGamePetsById(Long petId)
    {
        return faGamePetsMapper.selectFaGamePetsById(petId);
    }

    /**
     * 查询宠物管理列表
     * 
     * @param faGamePets 宠物管理
     * @return 宠物管理
     */
    @Override
    public List<FaGamePets> selectFaGamePetsList(FaGamePets faGamePets)
    {
        return faGamePetsMapper.selectFaGamePetsList(faGamePets);
    }

    /**
     * 新增宠物管理
     * 
     * @param faGamePets 宠物管理
     * @return 结果
     */
    @Override
    public int insertFaGamePets(FaGamePets faGamePets)
    {
        return faGamePetsMapper.insertFaGamePets(faGamePets);
    }

    /**
     * 修改宠物管理
     * 
     * @param faGamePets 宠物管理
     * @return 结果
     */
    @Override
    public int updateFaGamePets(FaGamePets faGamePets)
    {
        return faGamePetsMapper.updateFaGamePets(faGamePets);
    }

    /**
     * 批量删除宠物管理
     * 
     * @param petIds 需要删除的宠物管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsByIds(Long[] petIds)
    {
        return faGamePetsMapper.deleteFaGamePetsByIds(petIds);
    }

    /**
     * 删除宠物管理信息
     * 
     * @param petId 宠物管理ID
     * @return 结果
     */
    @Override
    public int deleteFaGamePetsById(Long petId)
    {
        return faGamePetsMapper.deleteFaGamePetsById(petId);
    }
}
