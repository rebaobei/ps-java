package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGoodArticleCategoryMapper;
import com.ruoyi.system.domain.FaGoodArticleCategory;
import com.ruoyi.system.service.IFaGoodArticleCategoryService;

/**
 * 动保公益分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-04
 */
@Service
public class FaGoodArticleCategoryServiceImpl implements IFaGoodArticleCategoryService 
{
    @Autowired
    private FaGoodArticleCategoryMapper faGoodArticleCategoryMapper;

    /**
     * 查询动保公益分类
     * 
     * @param id 动保公益分类ID
     * @return 动保公益分类
     */
    @Override
    public FaGoodArticleCategory selectFaGoodArticleCategoryById(Long id)
    {
        return faGoodArticleCategoryMapper.selectFaGoodArticleCategoryById(id);
    }

    /**
     * 查询动保公益分类列表
     * 
     * @param faGoodArticleCategory 动保公益分类
     * @return 动保公益分类
     */
    @Override
    public List<FaGoodArticleCategory> selectFaGoodArticleCategoryList(FaGoodArticleCategory faGoodArticleCategory)
    {
        return faGoodArticleCategoryMapper.selectFaGoodArticleCategoryList(faGoodArticleCategory);
    }

    /**
     * 新增动保公益分类
     * 
     * @param faGoodArticleCategory 动保公益分类
     * @return 结果
     */
    @Override
    public int insertFaGoodArticleCategory(FaGoodArticleCategory faGoodArticleCategory)
    {
        faGoodArticleCategory.setCreateTime(DateUtils.getNowDate());
        return faGoodArticleCategoryMapper.insertFaGoodArticleCategory(faGoodArticleCategory);
    }

    /**
     * 修改动保公益分类
     * 
     * @param faGoodArticleCategory 动保公益分类
     * @return 结果
     */
    @Override
    public int updateFaGoodArticleCategory(FaGoodArticleCategory faGoodArticleCategory)
    {
        faGoodArticleCategory.setUpdateTime(DateUtils.getNowDate());
        return faGoodArticleCategoryMapper.updateFaGoodArticleCategory(faGoodArticleCategory);
    }

    /**
     * 批量删除动保公益分类
     * 
     * @param ids 需要删除的动保公益分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGoodArticleCategoryByIds(Long[] ids)
    {
        return faGoodArticleCategoryMapper.deleteFaGoodArticleCategoryByIds(ids);
    }

    /**
     * 删除动保公益分类信息
     * 
     * @param id 动保公益分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGoodArticleCategoryById(Long id)
    {
        return faGoodArticleCategoryMapper.deleteFaGoodArticleCategoryById(id);
    }
}
