package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.UserVoteDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaVoteUserDetailMapper;
import com.ruoyi.system.domain.FaVoteUserDetail;
import com.ruoyi.system.service.IFaVoteUserDetailService;

/**
 * 投票明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@Service
public class FaVoteUserDetailServiceImpl implements IFaVoteUserDetailService 
{
    @Autowired
    private FaVoteUserDetailMapper faVoteUserDetailMapper;

    /**
     * 查询投票明细
     * 
     * @param id 投票明细ID
     * @return 投票明细
     */
    @Override
    public FaVoteUserDetail selectFaVoteUserDetailById(Long id)
    {
        return faVoteUserDetailMapper.selectFaVoteUserDetailById(id);
    }

    /**
     * 查询投票明细列表
     * 
     * @param faVoteUserDetail 投票明细
     * @return 投票明细
     */
    @Override
    public List<FaVoteUserDetail> selectFaVoteUserDetailList(FaVoteUserDetail faVoteUserDetail)
    {
        return faVoteUserDetailMapper.selectFaVoteUserDetailList(faVoteUserDetail);
    }

    /**
     * 新增投票明细
     * 
     * @param faVoteUserDetail 投票明细
     * @return 结果
     */
    @Override
    public int insertFaVoteUserDetail(FaVoteUserDetail faVoteUserDetail)
    {
        faVoteUserDetail.setCreatetime(MyDateUtils.nowTimestamp());
        return faVoteUserDetailMapper.insertFaVoteUserDetail(faVoteUserDetail);
    }

    /**
     * 修改投票明细
     * 
     * @param faVoteUserDetail 投票明细
     * @return 结果
     */
    @Override
    public int updateFaVoteUserDetail(FaVoteUserDetail faVoteUserDetail)
    {
        return faVoteUserDetailMapper.updateFaVoteUserDetail(faVoteUserDetail);
    }

    /**
     * 批量删除投票明细
     * 
     * @param ids 需要删除的投票明细ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteUserDetailByIds(Long[] ids)
    {
        return faVoteUserDetailMapper.deleteFaVoteUserDetailByIds(ids);
    }

    /**
     * 删除投票明细信息
     * 
     * @param id 投票明细ID
     * @return 结果
     */
    @Override
    public int deleteFaVoteUserDetailById(Long id)
    {
        return faVoteUserDetailMapper.deleteFaVoteUserDetailById(id);
    }

    @Override
    public List<UserVoteDetailVo> listUserVoteDetailVo(UserVoteDetailVo faVoteUserDetail) {
        return faVoteUserDetailMapper.listUserVoteDetailVo(faVoteUserDetail);
    }
}
