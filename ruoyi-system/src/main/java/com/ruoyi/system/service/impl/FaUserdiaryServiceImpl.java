package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.UserDiaryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserdiaryMapper;
import com.ruoyi.system.domain.FaUserdiary;
import com.ruoyi.system.service.IFaUserdiaryService;

/**
 * 宠物日记Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-31
 */
@Service
public class FaUserdiaryServiceImpl implements IFaUserdiaryService 
{
    @Autowired
    private FaUserdiaryMapper faUserdiaryMapper;

    /**
     * 查询宠物日记
     * 
     * @param id 宠物日记ID
     * @return 宠物日记
     */
    @Override
    public FaUserdiary selectFaUserdiaryById(Long id)
    {
        return faUserdiaryMapper.selectFaUserdiaryById(id);
    }

    /**
     * 查询宠物日记列表
     * 
     * @param faUserdiary 宠物日记
     * @return 宠物日记
     */
    @Override
    public List<FaUserdiary> selectFaUserdiaryList(FaUserdiary faUserdiary)
    {
        return faUserdiaryMapper.selectFaUserdiaryList(faUserdiary);
    }

    /**
     * 新增宠物日记
     * 
     * @param faUserdiary 宠物日记
     * @return 结果
     */
    @Override
    public int insertFaUserdiary(FaUserdiary faUserdiary)
    {
        faUserdiary.setUpdatetime(MyDateUtils.nowTimestamp());
        faUserdiary.setCretatetime(MyDateUtils.nowTimestamp());
        return faUserdiaryMapper.insertFaUserdiary(faUserdiary);
    }

    /**
     * 修改宠物日记
     * 
     * @param faUserdiary 宠物日记
     * @return 结果
     */
    @Override
    public int updateFaUserdiary(FaUserdiary faUserdiary)
    {
        faUserdiary.setUpdatetime(MyDateUtils.nowTimestamp());
        return faUserdiaryMapper.updateFaUserdiary(faUserdiary);
    }

    /**
     * 批量删除宠物日记
     * 
     * @param ids 需要删除的宠物日记ID
     * @return 结果
     */
    @Override
    public int deleteFaUserdiaryByIds(Long[] ids)
    {
        return faUserdiaryMapper.deleteFaUserdiaryByIds(ids);
    }

    /**
     * 删除宠物日记信息
     * 
     * @param id 宠物日记ID
     * @return 结果
     */
    @Override
    public int deleteFaUserdiaryById(Long id)
    {
        return faUserdiaryMapper.deleteFaUserdiaryById(id);
    }

    @Override
    public List<UserDiaryVo> listUserDiaryVo(UserDiaryVo userDiaryVo) {
        List<UserDiaryVo> userDiaryVos = faUserdiaryMapper.listUserDiaryVo(userDiaryVo);
        if (userDiaryVos != null){
            for (UserDiaryVo vo : userDiaryVos){
                if (vo.getImages() != null){
                    String[] split = vo.getImages().split(",");
                    vo.setImageList(Arrays.asList(split));
                }
            }
        }
        return userDiaryVos;
    }
}
