package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaProjectSizePrice;
import com.ruoyi.system.domain.vo.ProjectPriceSizeVo;

/**
 * 预约项目价格Service接口
 * 
 * @author ruoyi
 * @date 2021-07-28
 */
public interface IFaProjectSizePriceService 
{
    /**
     * 查询预约项目价格
     * 
     * @param id 预约项目价格ID
     * @return 预约项目价格
     */
    public FaProjectSizePrice selectFaProjectSizePriceById(Long id);

    /**
     * 查询预约项目价格列表
     * 
     * @param faProjectSizePrice 预约项目价格
     * @return 预约项目价格集合
     */
    public List<FaProjectSizePrice> selectFaProjectSizePriceList(FaProjectSizePrice faProjectSizePrice);

    /**
     * 新增预约项目价格
     * 
     * @param faProjectSizePrice 预约项目价格
     * @return 结果
     */
    public int insertFaProjectSizePrice(FaProjectSizePrice faProjectSizePrice);

    /**
     * 修改预约项目价格
     * 
     * @param faProjectSizePrice 预约项目价格
     * @return 结果
     */
    public int updateFaProjectSizePrice(FaProjectSizePrice faProjectSizePrice);

    /**
     * 批量删除预约项目价格
     * 
     * @param ids 需要删除的预约项目价格ID
     * @return 结果
     */
    public int deleteFaProjectSizePriceByIds(Long[] ids);

    /**
     * 删除预约项目价格信息
     * 
     * @param id 预约项目价格ID
     * @return 结果
     */
    public int deleteFaProjectSizePriceById(Long id);


    public List<ProjectPriceSizeVo> listProjectSizeVo(FaProjectSizePrice projectSizePrice);
}
