package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaStoreBrand;

/**
 * 品牌信息Service接口
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
public interface IFaStoreBrandService 
{
    /**
     * 查询品牌信息
     * 
     * @param id 品牌信息ID
     * @return 品牌信息
     */
    public FaStoreBrand selectFaStoreBrandById(Long id);

    /**
     * 查询品牌信息列表
     * 
     * @param faStoreBrand 品牌信息
     * @return 品牌信息集合
     */
    public List<FaStoreBrand> selectFaStoreBrandList(FaStoreBrand faStoreBrand);

    /**
     * 新增品牌信息
     * 
     * @param faStoreBrand 品牌信息
     * @return 结果
     */
    public int insertFaStoreBrand(FaStoreBrand faStoreBrand);

    /**
     * 修改品牌信息
     * 
     * @param faStoreBrand 品牌信息
     * @return 结果
     */
    public int updateFaStoreBrand(FaStoreBrand faStoreBrand);

    /**
     * 批量删除品牌信息
     * 
     * @param ids 需要删除的品牌信息ID
     * @return 结果
     */
    public int deleteFaStoreBrandByIds(Long[] ids);

    /**
     * 删除品牌信息信息
     * 
     * @param id 品牌信息ID
     * @return 结果
     */
    public int deleteFaStoreBrandById(Long id);
}
