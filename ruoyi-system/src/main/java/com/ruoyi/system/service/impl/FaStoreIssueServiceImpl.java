package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaStoreIssueMapper;
import com.ruoyi.system.domain.FaStoreIssue;
import com.ruoyi.system.service.IFaStoreIssueService;

/**
 * 店铺问题Service业务层处理
 *
 * @author ruoyi
 * @date 2021-11-21
 */
@Service
public class FaStoreIssueServiceImpl implements IFaStoreIssueService
{
    @Autowired
    private FaStoreIssueMapper faStoreIssueMapper;

    /**
     * 查询店铺问题
     *
     * @param id 店铺问题ID
     * @return 店铺问题
     */
    @Override
    public FaStoreIssue selectFaStoreIssueById(Long id)
    {
        return faStoreIssueMapper.selectFaStoreIssueById(id);
    }

    /**
     * 查询店铺问题列表
     *
     * @param faStoreIssue 店铺问题
     * @return 店铺问题
     */
    @Override
    public List<FaStoreIssue> selectFaStoreIssueList(FaStoreIssue faStoreIssue)
    {
        return faStoreIssueMapper.selectFaStoreIssueList(faStoreIssue);
    }

    /**
     * 新增店铺问题
     *
     * @param faStoreIssue 店铺问题
     * @return 结果
     */
    @Override
    public int insertFaStoreIssue(FaStoreIssue faStoreIssue)
    {
        faStoreIssue.setCreateTime(DateUtils.getNowDate());
        return faStoreIssueMapper.insertFaStoreIssue(faStoreIssue);
    }

    /**
     * 修改店铺问题
     *
     * @param faStoreIssue 店铺问题
     * @return 结果
     */
    @Override
    public int updateFaStoreIssue(FaStoreIssue faStoreIssue)
    {
        faStoreIssue.setUpdateTime(DateUtils.getNowDate());
        faStoreIssue.setCheckDateTime(DateUtils.getNowDate());
        return faStoreIssueMapper.updateFaStoreIssue(faStoreIssue);
    }

    /**
     * 批量删除店铺问题
     *
     * @param ids 需要删除的店铺问题ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreIssueByIds(Long[] ids)
    {
        return faStoreIssueMapper.deleteFaStoreIssueByIds(ids);
    }

    /**
     * 删除店铺问题信息
     *
     * @param id 店铺问题ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreIssueById(Long id)
    {
        return faStoreIssueMapper.deleteFaStoreIssueById(id);
    }
}
