package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserRankMapper;
import com.ruoyi.system.domain.FaUserRank;
import com.ruoyi.system.service.IFaUserRankService;

/**
 * 等级管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaUserRankServiceImpl implements IFaUserRankService 
{
    @Autowired
    private FaUserRankMapper faUserRankMapper;

    /**
     * 查询等级管理
     * 
     * @param id 等级管理ID
     * @return 等级管理
     */
    @Override
    public FaUserRank selectFaUserRankById(Long id)
    {
        return faUserRankMapper.selectFaUserRankById(id);
    }

    /**
     * 查询等级管理列表
     * 
     * @param faUserRank 等级管理
     * @return 等级管理
     */
    @Override
    public List<FaUserRank> selectFaUserRankList(FaUserRank faUserRank)
    {
        return faUserRankMapper.selectFaUserRankList(faUserRank);
    }

    /**
     * 新增等级管理
     * 
     * @param faUserRank 等级管理
     * @return 结果
     */
    @Override
    public int insertFaUserRank(FaUserRank faUserRank)
    {
        return faUserRankMapper.insertFaUserRank(faUserRank);
    }

    /**
     * 修改等级管理
     * 
     * @param faUserRank 等级管理
     * @return 结果
     */
    @Override
    public int updateFaUserRank(FaUserRank faUserRank)
    {
        return faUserRankMapper.updateFaUserRank(faUserRank);
    }

    /**
     * 批量删除等级管理
     * 
     * @param ids 需要删除的等级管理ID
     * @return 结果
     */
    @Override
    public int deleteFaUserRankByIds(Long[] ids)
    {
        return faUserRankMapper.deleteFaUserRankByIds(ids);
    }

    /**
     * 删除等级管理信息
     * 
     * @param id 等级管理ID
     * @return 结果
     */
    @Override
    public int deleteFaUserRankById(Long id)
    {
        return faUserRankMapper.deleteFaUserRankById(id);
    }
}
