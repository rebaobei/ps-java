package com.ruoyi.system.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.FaStoreInfo;
import com.ruoyi.system.domain.FaXshopProduct;
import com.ruoyi.system.domain.vo.ProductVo;
import com.ruoyi.system.domain.vo.ProjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaProjectsMapper;
import com.ruoyi.system.domain.FaProjects;
import com.ruoyi.system.service.IFaProjectsService;

/**
 * 预约项目Service业务层处理
 *
 * @author ruoyi
 * @date 2021-07-26
 */
@Service
public class FaProjectsServiceImpl implements IFaProjectsService {
    @Autowired
    private FaProjectsMapper faProjectsMapper;

    /**
     * 查询预约项目
     *
     * @param id 预约项目ID
     * @return 预约项目
     */
    @Override
    public FaProjects selectFaProjectsById(Long id) {
        return faProjectsMapper.selectFaProjectsById(id);
    }

    /**
     * 查询预约项目列表
     *
     * @param faProjects 预约项目
     * @return 预约项目
     */
    @Override
    public List<FaProjects> selectFaProjectsList(FaProjects faProjects) {
        return faProjectsMapper.selectFaProjectsList(faProjects);
    }

    /**
     * 新增预约项目
     *
     * @param faProjects 预约项目
     * @return 结果
     */
    @Override
    public int insertFaProjects(FaProjects faProjects) {
        faProjects.setCreatetime(MyDateUtils.nowTimestamp());
        faProjects.setUpdatetime(MyDateUtils.nowTimestamp());
        faProjects.setAuditStatus(0);
        return faProjectsMapper.insertFaProjects(faProjects);
    }

    /**
     * 修改预约项目
     *
     * @param faProjects 预约项目
     * @return 结果
     */
    @Override
    public AjaxResult updateFaProjects(FaProjects faProjects) {
        faProjects.setUpdatetime(MyDateUtils.nowTimestamp());
        if(faProjects.getHomeRecommend()==1){
            Long storeInfoId = faProjects.getStoreInfoId();
            List<FaProjects> faProjects1 = faProjectsMapper.selectCount(storeInfoId);
            List<Long> collect = faProjects1.stream().map(FaProjects::getId).collect(Collectors.toList());
            if (!collect.contains(faProjects.getId()) && collect.size() >= 3) {
                return AjaxResult.error("首页推荐数量不能超过3件");
            }
        }
        faProjectsMapper.updateFaProjects(faProjects);
        return AjaxResult.success();
    }

    /**
     * 批量删除预约项目
     *
     * @param ids 需要删除的预约项目ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectsByIds(Long[] ids) {
        return faProjectsMapper.deleteFaProjectsByIds(ids);
    }

    /**
     * 删除预约项目信息
     *
     * @param id 预约项目ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectsById(Long id) {
        return faProjectsMapper.deleteFaProjectsById(id);
    }

    @Override
    public List<FaProjects> list() {
        return faProjectsMapper.list();
    }

    @Override
    public List<ProjectVo> listProjectVo(FaProjects faProjects) {
        List<ProjectVo> projectVos = faProjectsMapper.listProjectVo(faProjects);
        for (ProjectVo vo : projectVos) {
            if (vo.getLuboimages() != null && vo.getLuboimages().length() > 0) {
                String[] split = vo.getLuboimages().split(",");
                vo.setImageList(Arrays.asList(split));
            }
        }
        return projectVos;
    }

    @Override
    public List<Map<String, Object>> listProjectVoForSelect() {
        Long storeInfoId = SecurityUtils.getLoginUser().getUser().getStoreInfoId();
        FaProjects faProjects = new FaProjects();
        faProjects.setStoreInfoId(storeInfoId);
        List<ProjectVo> projectVos = faProjectsMapper.listProjectVo(faProjects);
        List<Map<String,Object>> result = new ArrayList<>();
        for (ProjectVo vo:projectVos){
            Map<String,Object> map = new HashMap<>();
            map.put("id",vo.getId());
            map.put("title",vo.getName());
            result.add(map);
        }
        return result;
    }
}
