package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaAdszoneZone;

/**
 * 海报&广告位Service接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface IFaAdszoneZoneService 
{
    /**
     * 查询海报&广告位
     * 
     * @param id 海报&广告位ID
     * @return 海报&广告位
     */
    public FaAdszoneZone selectFaAdszoneZoneById(Long id);

    /**
     * 查询海报&广告位列表
     * 
     * @param faAdszoneZone 海报&广告位
     * @return 海报&广告位集合
     */
    public List<FaAdszoneZone> selectFaAdszoneZoneList(FaAdszoneZone faAdszoneZone);

    /**
     * 新增海报&广告位
     * 
     * @param faAdszoneZone 海报&广告位
     * @return 结果
     */
    public int insertFaAdszoneZone(FaAdszoneZone faAdszoneZone);

    /**
     * 修改海报&广告位
     * 
     * @param faAdszoneZone 海报&广告位
     * @return 结果
     */
    public int updateFaAdszoneZone(FaAdszoneZone faAdszoneZone);

    /**
     * 批量删除海报&广告位
     * 
     * @param ids 需要删除的海报&广告位ID
     * @return 结果
     */
    public int deleteFaAdszoneZoneByIds(Long[] ids);

    /**
     * 删除海报&广告位信息
     * 
     * @param id 海报&广告位ID
     * @return 结果
     */
    public int deleteFaAdszoneZoneById(Long id);
}
