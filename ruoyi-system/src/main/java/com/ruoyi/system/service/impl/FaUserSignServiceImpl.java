package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserSignMapper;
import com.ruoyi.system.domain.FaUserSign;
import com.ruoyi.system.service.IFaUserSignService;

/**
 * 签到设置积分Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUserSignServiceImpl implements IFaUserSignService 
{
    @Autowired
    private FaUserSignMapper faUserSignMapper;

    /**
     * 查询签到设置积分
     * 
     * @param id 签到设置积分ID
     * @return 签到设置积分
     */
    @Override
    public FaUserSign selectFaUserSignById(Long id)
    {
        return faUserSignMapper.selectFaUserSignById(id);
    }

    /**
     * 查询签到设置积分列表
     * 
     * @param faUserSign 签到设置积分
     * @return 签到设置积分
     */
    @Override
    public List<FaUserSign> selectFaUserSignList(FaUserSign faUserSign)
    {
        return faUserSignMapper.selectFaUserSignList(faUserSign);
    }

    /**
     * 新增签到设置积分
     * 
     * @param faUserSign 签到设置积分
     * @return 结果
     */
    @Override
    public int insertFaUserSign(FaUserSign faUserSign)
    {
        return faUserSignMapper.insertFaUserSign(faUserSign);
    }

    /**
     * 修改签到设置积分
     * 
     * @param faUserSign 签到设置积分
     * @return 结果
     */
    @Override
    public int updateFaUserSign(FaUserSign faUserSign)
    {
        return faUserSignMapper.updateFaUserSign(faUserSign);
    }

    /**
     * 批量删除签到设置积分
     * 
     * @param ids 需要删除的签到设置积分ID
     * @return 结果
     */
    @Override
    public int deleteFaUserSignByIds(Long[] ids)
    {
        return faUserSignMapper.deleteFaUserSignByIds(ids);
    }

    /**
     * 删除签到设置积分信息
     * 
     * @param id 签到设置积分ID
     * @return 结果
     */
    @Override
    public int deleteFaUserSignById(Long id)
    {
        return faUserSignMapper.deleteFaUserSignById(id);
    }
}
