package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaStoreVideo;

/**
 * 店铺视频Service接口
 * 
 * @author ruoyi
 * @date 2021-12-01
 */
public interface IFaStoreVideoService 
{
    /**
     * 查询店铺视频
     * 
     * @param id 店铺视频ID
     * @return 店铺视频
     */
    public FaStoreVideo selectFaStoreVideoById(Long id);

    /**
     * 查询店铺视频列表
     * 
     * @param faStoreVideo 店铺视频
     * @return 店铺视频集合
     */
    public List<FaStoreVideo> selectFaStoreVideoList(FaStoreVideo faStoreVideo);

    /**
     * 新增店铺视频
     * 
     * @param faStoreVideo 店铺视频
     * @return 结果
     */
    public int insertFaStoreVideo(FaStoreVideo faStoreVideo);

    /**
     * 修改店铺视频
     * 
     * @param faStoreVideo 店铺视频
     * @return 结果
     */
    public int updateFaStoreVideo(FaStoreVideo faStoreVideo);

    /**
     * 批量删除店铺视频
     * 
     * @param ids 需要删除的店铺视频ID
     * @return 结果
     */
    public int deleteFaStoreVideoByIds(Long[] ids);

    /**
     * 删除店铺视频信息
     * 
     * @param id 店铺视频ID
     * @return 结果
     */
    public int deleteFaStoreVideoById(Long id);
}
