package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaAdszoneZoneMapper;
import com.ruoyi.system.domain.FaAdszoneZone;
import com.ruoyi.system.service.IFaAdszoneZoneService;

/**
 * 海报&广告位Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaAdszoneZoneServiceImpl implements IFaAdszoneZoneService 
{
    @Autowired
    private FaAdszoneZoneMapper faAdszoneZoneMapper;

    /**
     * 查询海报&广告位
     * 
     * @param id 海报&广告位ID
     * @return 海报&广告位
     */
    @Override
    public FaAdszoneZone selectFaAdszoneZoneById(Long id)
    {
        return faAdszoneZoneMapper.selectFaAdszoneZoneById(id);
    }

    /**
     * 查询海报&广告位列表
     * 
     * @param faAdszoneZone 海报&广告位
     * @return 海报&广告位
     */
    @Override
    public List<FaAdszoneZone> selectFaAdszoneZoneList(FaAdszoneZone faAdszoneZone)
    {
        return faAdszoneZoneMapper.selectFaAdszoneZoneList(faAdszoneZone);
    }

    /**
     * 新增海报&广告位
     * 
     * @param faAdszoneZone 海报&广告位
     * @return 结果
     */
    @Override
    public int insertFaAdszoneZone(FaAdszoneZone faAdszoneZone)
    {
        faAdszoneZone.setCreatetime(MyDateUtils.nowTimestamp());
        faAdszoneZone.setCreatetime(MyDateUtils.nowTimestamp());
        return faAdszoneZoneMapper.insertFaAdszoneZone(faAdszoneZone);
    }

    /**
     * 修改海报&广告位
     * 
     * @param faAdszoneZone 海报&广告位
     * @return 结果
     */
    @Override
    public int updateFaAdszoneZone(FaAdszoneZone faAdszoneZone)
    {
        faAdszoneZone.setUpdatetime(MyDateUtils.nowTimestamp());
        return faAdszoneZoneMapper.updateFaAdszoneZone(faAdszoneZone);
    }

    /**
     * 批量删除海报&广告位
     * 
     * @param ids 需要删除的海报&广告位ID
     * @return 结果
     */
    @Override
    public int deleteFaAdszoneZoneByIds(Long[] ids)
    {
        return faAdszoneZoneMapper.deleteFaAdszoneZoneByIds(ids);
    }

    /**
     * 删除海报&广告位信息
     * 
     * @param id 海报&广告位ID
     * @return 结果
     */
    @Override
    public int deleteFaAdszoneZoneById(Long id)
    {
        return faAdszoneZoneMapper.deleteFaAdszoneZoneById(id);
    }
}
