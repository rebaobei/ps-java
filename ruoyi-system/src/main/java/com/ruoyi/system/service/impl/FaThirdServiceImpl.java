package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.ThirdLogWithUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaThirdMapper;
import com.ruoyi.system.domain.FaThird;
import com.ruoyi.system.service.IFaThirdService;

/**
 * 第三方登录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaThirdServiceImpl implements IFaThirdService 
{
    @Autowired
    private FaThirdMapper faThirdMapper;

    /**
     * 查询第三方登录
     * 
     * @param id 第三方登录ID
     * @return 第三方登录
     */
    @Override
    public FaThird selectFaThirdById(Integer id)
    {
        return faThirdMapper.selectFaThirdById(id);
    }

    /**
     * 查询第三方登录列表
     * 
     * @param faThird 第三方登录
     * @return 第三方登录
     */
    @Override
    public List<FaThird> selectFaThirdList(FaThird faThird)
    {
        return faThirdMapper.selectFaThirdList(faThird);
    }

    /**
     * 新增第三方登录
     * 
     * @param faThird 第三方登录
     * @return 结果
     */
    @Override
    public int insertFaThird(FaThird faThird)
    {
        return faThirdMapper.insertFaThird(faThird);
    }

    /**
     * 修改第三方登录
     * 
     * @param faThird 第三方登录
     * @return 结果
     */
    @Override
    public int updateFaThird(FaThird faThird)
    {
        return faThirdMapper.updateFaThird(faThird);
    }

    /**
     * 批量删除第三方登录
     * 
     * @param ids 需要删除的第三方登录ID
     * @return 结果
     */
    @Override
    public int deleteFaThirdByIds(Integer[] ids)
    {
        return faThirdMapper.deleteFaThirdByIds(ids);
    }

    /**
     * 删除第三方登录信息
     * 
     * @param id 第三方登录ID
     * @return 结果
     */
    @Override
    public int deleteFaThirdById(Integer id)
    {
        return faThirdMapper.deleteFaThirdById(id);
    }

    @Override
    public List<ThirdLogWithUserInfo> listThirdLog(FaThird third) {
        return faThirdMapper.listThirdLog(third);
    }
}
