package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.system.domain.FaYuyueOrder;
import com.ruoyi.system.domain.vo.XshopOrderVo;
import com.ruoyi.system.mapper.FaYuyueOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopOrderMapper;
import com.ruoyi.system.domain.FaXshopOrder;
import com.ruoyi.system.service.IFaXshopOrderService;

/**
 * 商城订单Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaXshopOrderServiceImpl implements IFaXshopOrderService {
    @Autowired
    private FaXshopOrderMapper faXshopOrderMapper;
    @Autowired
    private FaYuyueOrderMapper faYuyueOrderMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Value("${custom.refundLink}")
    private String REFUND_LINK;

    /**
     * 查询商城订单
     *
     * @param id 商城订单ID
     * @return 商城订单
     */
    @Override
    public FaXshopOrder selectFaXshopOrderById(Integer id) {
        return faXshopOrderMapper.selectFaXshopOrderById(id);
    }

    /**
     * 查询商城订单列表
     *
     * @param faXshopOrder 商城订单
     * @return 商城订单
     */
    @Override
    public List<FaXshopOrder> selectFaXshopOrderList(FaXshopOrder faXshopOrder) {
        return faXshopOrderMapper.selectFaXshopOrderList(faXshopOrder);
    }

    /**
     * 新增商城订单
     *
     * @param faXshopOrder 商城订单
     * @return 结果
     */
    @Override
    public int insertFaXshopOrder(FaXshopOrder faXshopOrder) {
        faXshopOrder.setCreateTime(MyDateUtils.nowTimestamp());
        faXshopOrder.setUpdateTime(MyDateUtils.nowTimestamp());
        return faXshopOrderMapper.insertFaXshopOrder(faXshopOrder);
    }

    /**
     * 修改商城订单
     *
     * @param faXshopOrder 商城订单
     * @return 结果
     */
    @Override
    public int updateFaXshopOrder(FaXshopOrder faXshopOrder) {
        faXshopOrder.setUpdateTime(MyDateUtils.nowTimestamp());
        return faXshopOrderMapper.updateFaXshopOrder(faXshopOrder);
    }

    /**
     * 批量删除商城订单
     *
     * @param ids 需要删除的商城订单ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopOrderByIds(Integer[] ids) {
        return faXshopOrderMapper.deleteFaXshopOrderByIds(ids);
    }

    /**
     * 删除商城订单信息
     *
     * @param id 商城订单ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopOrderById(Integer id) {
        return faXshopOrderMapper.deleteFaXshopOrderById(id);
    }

    @Override
    public List<XshopOrderVo> listXshopOrderVo(XshopOrderVo faXshopOrder) {
        return faXshopOrderMapper.listXshopOrderVo(faXshopOrder);
    }

    @Override
//    @Async
    public Map<String, String> refundMoney(String id) {
        String key = IdUtils.simpleUUID();
        String param = "key=" + key;
        redisTemplate.boundValueOps(key).set(id, 120, TimeUnit.SECONDS);
        String response = HttpUtils.sendGet(REFUND_LINK, param);
        Map map = JSON.parseObject(response, Map.class);
        return map;
    }
}
