package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserTasklog;

/**
 * 完成任务记录Service接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface IFaUserTasklogService 
{
    /**
     * 查询完成任务记录
     * 
     * @param id 完成任务记录ID
     * @return 完成任务记录
     */
    public FaUserTasklog selectFaUserTasklogById(Long id);

    /**
     * 查询完成任务记录列表
     * 
     * @param faUserTasklog 完成任务记录
     * @return 完成任务记录集合
     */
    public List<FaUserTasklog> selectFaUserTasklogList(FaUserTasklog faUserTasklog);

    /**
     * 新增完成任务记录
     * 
     * @param faUserTasklog 完成任务记录
     * @return 结果
     */
    public int insertFaUserTasklog(FaUserTasklog faUserTasklog);

    /**
     * 修改完成任务记录
     * 
     * @param faUserTasklog 完成任务记录
     * @return 结果
     */
    public int updateFaUserTasklog(FaUserTasklog faUserTasklog);

    /**
     * 批量删除完成任务记录
     * 
     * @param ids 需要删除的完成任务记录ID
     * @return 结果
     */
    public int deleteFaUserTasklogByIds(Long[] ids);

    /**
     * 删除完成任务记录信息
     * 
     * @param id 完成任务记录ID
     * @return 结果
     */
    public int deleteFaUserTasklogById(Long id);
}
