package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaXshopReview;
import com.ruoyi.system.domain.vo.ProductReviewVo;

/**
 * 商品评价Service接口
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
public interface IFaXshopReviewService 
{
    /**
     * 查询商品评价
     * 
     * @param id 商品评价ID
     * @return 商品评价
     */
    public FaXshopReview selectFaXshopReviewById(Integer id);

    /**
     * 查询商品评价列表
     * 
     * @param faXshopReview 商品评价
     * @return 商品评价集合
     */
    public List<FaXshopReview> selectFaXshopReviewList(FaXshopReview faXshopReview);

    /**
     * 新增商品评价
     * 
     * @param faXshopReview 商品评价
     * @return 结果
     */
    public int insertFaXshopReview(FaXshopReview faXshopReview);

    /**
     * 修改商品评价
     * 
     * @param faXshopReview 商品评价
     * @return 结果
     */
    public int updateFaXshopReview(FaXshopReview faXshopReview);

    /**
     * 批量删除商品评价
     * 
     * @param ids 需要删除的商品评价ID
     * @return 结果
     */
    public int deleteFaXshopReviewByIds(Integer[] ids);

    /**
     * 删除商品评价信息
     * 
     * @param id 商品评价ID
     * @return 结果
     */
    public int deleteFaXshopReviewById(Integer id);

    List<ProductReviewVo> listProductReviewVo(ProductReviewVo vo);
}
