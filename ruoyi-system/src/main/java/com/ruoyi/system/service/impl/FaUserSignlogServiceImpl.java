package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserSignlogMapper;
import com.ruoyi.system.domain.FaUserSignlog;
import com.ruoyi.system.service.IFaUserSignlogService;

/**
 * 签到明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaUserSignlogServiceImpl implements IFaUserSignlogService 
{
    @Autowired
    private FaUserSignlogMapper faUserSignlogMapper;

    /**
     * 查询签到明细
     * 
     * @param id 签到明细ID
     * @return 签到明细
     */
    @Override
    public FaUserSignlog selectFaUserSignlogById(Long id)
    {
        return faUserSignlogMapper.selectFaUserSignlogById(id);
    }

    /**
     * 查询签到明细列表
     * 
     * @param faUserSignlog 签到明细
     * @return 签到明细
     */
    @Override
    public List<FaUserSignlog> selectFaUserSignlogList(FaUserSignlog faUserSignlog)
    {
        return faUserSignlogMapper.selectFaUserSignlogList(faUserSignlog);
    }

    /**
     * 新增签到明细
     * 
     * @param faUserSignlog 签到明细
     * @return 结果
     */
    @Override
    public int insertFaUserSignlog(FaUserSignlog faUserSignlog)
    {
        return faUserSignlogMapper.insertFaUserSignlog(faUserSignlog);
    }

    /**
     * 修改签到明细
     * 
     * @param faUserSignlog 签到明细
     * @return 结果
     */
    @Override
    public int updateFaUserSignlog(FaUserSignlog faUserSignlog)
    {
        faUserSignlog.setUpdateTime(DateUtils.getNowDate());
        return faUserSignlogMapper.updateFaUserSignlog(faUserSignlog);
    }

    /**
     * 批量删除签到明细
     * 
     * @param ids 需要删除的签到明细ID
     * @return 结果
     */
    @Override
    public int deleteFaUserSignlogByIds(Long[] ids)
    {
        return faUserSignlogMapper.deleteFaUserSignlogByIds(ids);
    }

    /**
     * 删除签到明细信息
     * 
     * @param id 签到明细ID
     * @return 结果
     */
    @Override
    public int deleteFaUserSignlogById(Long id)
    {
        return faUserSignlogMapper.deleteFaUserSignlogById(id);
    }
}
