package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.CarOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaCarOrderMapper;
import com.ruoyi.system.domain.FaCarOrder;
import com.ruoyi.system.service.IFaCarOrderService;

/**
 * 同城接送订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaCarOrderServiceImpl implements IFaCarOrderService 
{
    @Autowired
    private FaCarOrderMapper faCarOrderMapper;

    /**
     * 查询同城接送订单
     * 
     * @param id 同城接送订单ID
     * @return 同城接送订单
     */
    @Override
    public FaCarOrder selectFaCarOrderById(Long id)
    {
        return faCarOrderMapper.selectFaCarOrderById(id);
    }

    /**
     * 查询同城接送订单列表
     * 
     * @param faCarOrder 同城接送订单
     * @return 同城接送订单
     */
    @Override
    public List<FaCarOrder> selectFaCarOrderList(FaCarOrder faCarOrder)
    {
        return faCarOrderMapper.selectFaCarOrderList(faCarOrder);
    }

    /**
     * 新增同城接送订单
     * 
     * @param faCarOrder 同城接送订单
     * @return 结果
     */
    @Override
    public int insertFaCarOrder(FaCarOrder faCarOrder)
    {
        faCarOrder.setUpdatetime(MyDateUtils.nowTimestamp());
        faCarOrder.setCreatetime(MyDateUtils.nowTimestamp());
        return faCarOrderMapper.insertFaCarOrder(faCarOrder);
    }

    /**
     * 修改同城接送订单
     * 
     * @param faCarOrder 同城接送订单
     * @return 结果
     */
    @Override
    public int updateFaCarOrder(FaCarOrder faCarOrder)
    {
        faCarOrder.setUpdatetime(MyDateUtils.nowTimestamp());
        return faCarOrderMapper.updateFaCarOrder(faCarOrder);
    }

    /**
     * 批量删除同城接送订单
     * 
     * @param ids 需要删除的同城接送订单ID
     * @return 结果
     */
    @Override
    public int deleteFaCarOrderByIds(Long[] ids)
    {
        return faCarOrderMapper.deleteFaCarOrderByIds(ids);
    }

    /**
     * 删除同城接送订单信息
     * 
     * @param id 同城接送订单ID
     * @return 结果
     */
    @Override
    public int deleteFaCarOrderById(Long id)
    {
        return faCarOrderMapper.deleteFaCarOrderById(id);
    }

    @Override
    public List<CarOrderVo> listCarOrderVo(CarOrderVo faCarOrder) {
        return faCarOrderMapper.listCarOrderVo(faCarOrder);
    }
}
