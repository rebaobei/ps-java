package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaOrderPaylist;
import com.ruoyi.system.domain.vo.PayListStatisticVo;
import com.ruoyi.system.domain.vo.PayListVo;

/**
 * 支付记录Service接口
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
public interface IFaOrderPaylistService 
{
    /**
     * 查询支付记录
     * 
     * @param id 支付记录ID
     * @return 支付记录
     */
    public FaOrderPaylist selectFaOrderPaylistById(Long id);

    /**
     * 查询支付记录列表
     * 
     * @param faOrderPaylist 支付记录
     * @return 支付记录集合
     */
    public List<FaOrderPaylist> selectFaOrderPaylistList(FaOrderPaylist faOrderPaylist);

    /**
     * 新增支付记录
     * 
     * @param faOrderPaylist 支付记录
     * @return 结果
     */
    public int insertFaOrderPaylist(FaOrderPaylist faOrderPaylist);

    /**
     * 修改支付记录
     * 
     * @param faOrderPaylist 支付记录
     * @return 结果
     */
    public int updateFaOrderPaylist(FaOrderPaylist faOrderPaylist);

    /**
     * 批量删除支付记录
     * 
     * @param ids 需要删除的支付记录ID
     * @return 结果
     */
    public int deleteFaOrderPaylistByIds(Long[] ids);

    /**
     * 删除支付记录信息
     * 
     * @param id 支付记录ID
     * @return 结果
     */
    public int deleteFaOrderPaylistById(Long id);

    List<PayListVo> listPaylistVo(PayListVo faOrderPaylist);

    List<PayListStatisticVo> lsitPayStatisticVo(PayListStatisticVo vo);
}
