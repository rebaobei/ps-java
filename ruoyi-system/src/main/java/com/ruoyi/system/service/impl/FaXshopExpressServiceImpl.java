package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaXshopExpressMapper;
import com.ruoyi.system.domain.FaXshopExpress;
import com.ruoyi.system.service.IFaXshopExpressService;

/**
 * 快递公司Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-19
 */
@Service
public class FaXshopExpressServiceImpl implements IFaXshopExpressService 
{
    @Autowired
    private FaXshopExpressMapper faXshopExpressMapper;

    /**
     * 查询快递公司
     * 
     * @param id 快递公司ID
     * @return 快递公司
     */
    @Override
    public FaXshopExpress selectFaXshopExpressById(Integer id)
    {
        return faXshopExpressMapper.selectFaXshopExpressById(id);
    }

    /**
     * 查询快递公司列表
     * 
     * @param faXshopExpress 快递公司
     * @return 快递公司
     */
    @Override
    public List<FaXshopExpress> selectFaXshopExpressList(FaXshopExpress faXshopExpress)
    {
        return faXshopExpressMapper.selectFaXshopExpressList(faXshopExpress);
    }

    /**
     * 新增快递公司
     * 
     * @param faXshopExpress 快递公司
     * @return 结果
     */
    @Override
    public int insertFaXshopExpress(FaXshopExpress faXshopExpress)
    {
        return faXshopExpressMapper.insertFaXshopExpress(faXshopExpress);
    }

    /**
     * 修改快递公司
     * 
     * @param faXshopExpress 快递公司
     * @return 结果
     */
    @Override
    public int updateFaXshopExpress(FaXshopExpress faXshopExpress)
    {
        return faXshopExpressMapper.updateFaXshopExpress(faXshopExpress);
    }

    /**
     * 批量删除快递公司
     * 
     * @param ids 需要删除的快递公司ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopExpressByIds(Integer[] ids)
    {
        return faXshopExpressMapper.deleteFaXshopExpressByIds(ids);
    }

    /**
     * 删除快递公司信息
     * 
     * @param id 快递公司ID
     * @return 结果
     */
    @Override
    public int deleteFaXshopExpressById(Integer id)
    {
        return faXshopExpressMapper.deleteFaXshopExpressById(id);
    }
}
