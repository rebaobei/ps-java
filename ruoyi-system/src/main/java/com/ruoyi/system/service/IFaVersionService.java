package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaVersion;

/**
 * APP版本管理Service接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface IFaVersionService 
{
    /**
     * 查询APP版本管理
     * 
     * @param id APP版本管理ID
     * @return APP版本管理
     */
    public FaVersion selectFaVersionById(Long id);

    /**
     * 查询APP版本管理列表
     * 
     * @param faVersion APP版本管理
     * @return APP版本管理集合
     */
    public List<FaVersion> selectFaVersionList(FaVersion faVersion);

    /**
     * 新增APP版本管理
     * 
     * @param faVersion APP版本管理
     * @return 结果
     */
    public int insertFaVersion(FaVersion faVersion);

    /**
     * 修改APP版本管理
     * 
     * @param faVersion APP版本管理
     * @return 结果
     */
    public int updateFaVersion(FaVersion faVersion);

    /**
     * 批量删除APP版本管理
     * 
     * @param ids 需要删除的APP版本管理ID
     * @return 结果
     */
    public int deleteFaVersionByIds(Long[] ids);

    /**
     * 删除APP版本管理信息
     * 
     * @param id APP版本管理ID
     * @return 结果
     */
    public int deleteFaVersionById(Long id);
}
