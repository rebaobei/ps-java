package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGameTask;

/**
 * 每日任务Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGameTaskService 
{
    /**
     * 查询每日任务
     * 
     * @param dayTaskId 每日任务ID
     * @return 每日任务
     */
    public FaGameTask selectFaGameTaskById(Long dayTaskId);

    /**
     * 查询每日任务列表
     * 
     * @param faGameTask 每日任务
     * @return 每日任务集合
     */
    public List<FaGameTask> selectFaGameTaskList(FaGameTask faGameTask);

    /**
     * 新增每日任务
     * 
     * @param faGameTask 每日任务
     * @return 结果
     */
    public int insertFaGameTask(FaGameTask faGameTask);

    /**
     * 修改每日任务
     * 
     * @param faGameTask 每日任务
     * @return 结果
     */
    public int updateFaGameTask(FaGameTask faGameTask);

    /**
     * 批量删除每日任务
     * 
     * @param dayTaskIds 需要删除的每日任务ID
     * @return 结果
     */
    public int deleteFaGameTaskByIds(Long[] dayTaskIds);

    /**
     * 删除每日任务信息
     * 
     * @param dayTaskId 每日任务ID
     * @return 结果
     */
    public int deleteFaGameTaskById(Long dayTaskId);
}
