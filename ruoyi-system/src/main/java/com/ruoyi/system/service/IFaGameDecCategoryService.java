package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGameDecCategory;

/**
 * 装扮分类Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGameDecCategoryService 
{
    /**
     * 查询装扮分类
     * 
     * @param id 装扮分类ID
     * @return 装扮分类
     */
    public FaGameDecCategory selectFaGameDecCategoryById(Long id);

    /**
     * 查询装扮分类列表
     * 
     * @param faGameDecCategory 装扮分类
     * @return 装扮分类集合
     */
    public List<FaGameDecCategory> selectFaGameDecCategoryList(FaGameDecCategory faGameDecCategory);

    /**
     * 新增装扮分类
     * 
     * @param faGameDecCategory 装扮分类
     * @return 结果
     */
    public int insertFaGameDecCategory(FaGameDecCategory faGameDecCategory);

    /**
     * 修改装扮分类
     * 
     * @param faGameDecCategory 装扮分类
     * @return 结果
     */
    public int updateFaGameDecCategory(FaGameDecCategory faGameDecCategory);

    /**
     * 批量删除装扮分类
     * 
     * @param ids 需要删除的装扮分类ID
     * @return 结果
     */
    public int deleteFaGameDecCategoryByIds(Long[] ids);

    /**
     * 删除装扮分类信息
     * 
     * @param id 装扮分类ID
     * @return 结果
     */
    public int deleteFaGameDecCategoryById(Long id);
}
