package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.BaomingUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaBaomingUserMapper;
import com.ruoyi.system.domain.FaBaomingUser;
import com.ruoyi.system.service.IFaBaomingUserService;

/**
 * 报名管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@Service
public class FaBaomingUserServiceImpl implements IFaBaomingUserService 
{
    @Autowired
    private FaBaomingUserMapper faBaomingUserMapper;

    /**
     * 查询报名管理
     * 
     * @param id 报名管理ID
     * @return 报名管理
     */
    @Override
    public FaBaomingUser selectFaBaomingUserById(Long id)
    {
        return faBaomingUserMapper.selectFaBaomingUserById(id);
    }

    /**
     * 查询报名管理列表
     * 
     * @param faBaomingUser 报名管理
     * @return 报名管理
     */
    @Override
    public List<FaBaomingUser> selectFaBaomingUserList(FaBaomingUser faBaomingUser)
    {
        return faBaomingUserMapper.selectFaBaomingUserList(faBaomingUser);
    }

    /**
     * 新增报名管理
     * 
     * @param faBaomingUser 报名管理
     * @return 结果
     */
    @Override
    public int insertFaBaomingUser(FaBaomingUser faBaomingUser)
    {
        faBaomingUser.setCreatetime(MyDateUtils.nowTimestamp());
        faBaomingUser.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBaomingUserMapper.insertFaBaomingUser(faBaomingUser);
    }

    /**
     * 修改报名管理
     * 
     * @param faBaomingUser 报名管理
     * @return 结果
     */
    @Override
    public int updateFaBaomingUser(FaBaomingUser faBaomingUser)
    {
        faBaomingUser.setUpdatetime(MyDateUtils.nowTimestamp());
        return faBaomingUserMapper.updateFaBaomingUser(faBaomingUser);
    }

    /**
     * 批量删除报名管理
     * 
     * @param ids 需要删除的报名管理ID
     * @return 结果
     */
    @Override
    public int deleteFaBaomingUserByIds(Long[] ids)
    {
        return faBaomingUserMapper.deleteFaBaomingUserByIds(ids);
    }

    /**
     * 删除报名管理信息
     * 
     * @param id 报名管理ID
     * @return 结果
     */
    @Override
    public int deleteFaBaomingUserById(Long id)
    {
        return faBaomingUserMapper.deleteFaBaomingUserById(id);
    }

    @Override
    public List<BaomingUserVo> listBaomingUserVo(BaomingUserVo faBaomingUser) {
        List<BaomingUserVo> baomingUserVos = faBaomingUserMapper.listBaomingUserVo(faBaomingUser);
        if (baomingUserVos != null){
            for (BaomingUserVo vo : baomingUserVos){
                if (vo.getImages()!= null && vo.getImages().length() > 0){
                    String[] split = vo.getImages().split(",");
                    vo.setImageList(Arrays.asList(split));
                }
            }
        }
        return baomingUserVos;
    }
}
