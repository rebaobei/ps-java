package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaAdszoneAds;

/**
 * 广告项目Service接口
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
public interface IFaAdszoneAdsService 
{
    /**
     * 查询广告项目
     * 
     * @param id 广告项目ID
     * @return 广告项目
     */
    public FaAdszoneAds selectFaAdszoneAdsById(Long id);

    /**
     * 查询广告项目列表
     * 
     * @param faAdszoneAds 广告项目
     * @return 广告项目集合
     */
    public List<FaAdszoneAds> selectFaAdszoneAdsList(FaAdszoneAds faAdszoneAds);

    /**
     * 新增广告项目
     * 
     * @param faAdszoneAds 广告项目
     * @return 结果
     */
    public int insertFaAdszoneAds(FaAdszoneAds faAdszoneAds);

    /**
     * 修改广告项目
     * 
     * @param faAdszoneAds 广告项目
     * @return 结果
     */
    public int updateFaAdszoneAds(FaAdszoneAds faAdszoneAds);

    /**
     * 批量删除广告项目
     * 
     * @param ids 需要删除的广告项目ID
     * @return 结果
     */
    public int deleteFaAdszoneAdsByIds(Long[] ids);

    /**
     * 删除广告项目信息
     * 
     * @param id 广告项目ID
     * @return 结果
     */
    public int deleteFaAdszoneAdsById(Long id);
}
