package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.web.multipart.MultipartFile;

public interface IOssuploadService {
    /**
     * 图片上传
     *
     * @param file
     * @return
     */
    AjaxResult upload(MultipartFile file);

    AjaxResult uploadVideo(MultipartFile file);
}
