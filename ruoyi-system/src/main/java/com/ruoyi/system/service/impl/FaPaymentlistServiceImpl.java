package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaPaymentlistMapper;
import com.ruoyi.system.domain.FaPaymentlist;
import com.ruoyi.system.service.IFaPaymentlistService;

/**
 * 支付类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaPaymentlistServiceImpl implements IFaPaymentlistService 
{
    @Autowired
    private FaPaymentlistMapper faPaymentlistMapper;

    /**
     * 查询支付类型
     * 
     * @param id 支付类型ID
     * @return 支付类型
     */
    @Override
    public FaPaymentlist selectFaPaymentlistById(Long id)
    {
        return faPaymentlistMapper.selectFaPaymentlistById(id);
    }

    /**
     * 查询支付类型列表
     * 
     * @param faPaymentlist 支付类型
     * @return 支付类型
     */
    @Override
    public List<FaPaymentlist> selectFaPaymentlistList(FaPaymentlist faPaymentlist)
    {
        return faPaymentlistMapper.selectFaPaymentlistList(faPaymentlist);
    }

    /**
     * 新增支付类型
     * 
     * @param faPaymentlist 支付类型
     * @return 结果
     */
    @Override
    public int insertFaPaymentlist(FaPaymentlist faPaymentlist)
    {
        faPaymentlist.setCreatetime(MyDateUtils.nowTimestamp());
        faPaymentlist.setUpdatetime(MyDateUtils.nowTimestamp());
        return faPaymentlistMapper.insertFaPaymentlist(faPaymentlist);
    }

    /**
     * 修改支付类型
     * 
     * @param faPaymentlist 支付类型
     * @return 结果
     */
    @Override
    public int updateFaPaymentlist(FaPaymentlist faPaymentlist)
    {
        faPaymentlist.setUpdatetime(MyDateUtils.nowTimestamp());
        return faPaymentlistMapper.updateFaPaymentlist(faPaymentlist);
    }

    /**
     * 批量删除支付类型
     * 
     * @param ids 需要删除的支付类型ID
     * @return 结果
     */
    @Override
    public int deleteFaPaymentlistByIds(Long[] ids)
    {
        return faPaymentlistMapper.deleteFaPaymentlistByIds(ids);
    }

    /**
     * 删除支付类型信息
     * 
     * @param id 支付类型ID
     * @return 结果
     */
    @Override
    public int deleteFaPaymentlistById(Long id)
    {
        return faPaymentlistMapper.deleteFaPaymentlistById(id);
    }
}
