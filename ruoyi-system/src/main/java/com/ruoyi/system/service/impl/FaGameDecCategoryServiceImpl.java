package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGameDecCategoryMapper;
import com.ruoyi.system.domain.FaGameDecCategory;
import com.ruoyi.system.service.IFaGameDecCategoryService;

/**
 * 装扮分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
@Service
public class FaGameDecCategoryServiceImpl implements IFaGameDecCategoryService 
{
    @Autowired
    private FaGameDecCategoryMapper faGameDecCategoryMapper;

    /**
     * 查询装扮分类
     * 
     * @param id 装扮分类ID
     * @return 装扮分类
     */
    @Override
    public FaGameDecCategory selectFaGameDecCategoryById(Long id)
    {
        return faGameDecCategoryMapper.selectFaGameDecCategoryById(id);
    }

    /**
     * 查询装扮分类列表
     * 
     * @param faGameDecCategory 装扮分类
     * @return 装扮分类
     */
    @Override
    public List<FaGameDecCategory> selectFaGameDecCategoryList(FaGameDecCategory faGameDecCategory)
    {
        return faGameDecCategoryMapper.selectFaGameDecCategoryList(faGameDecCategory);
    }

    /**
     * 新增装扮分类
     * 
     * @param faGameDecCategory 装扮分类
     * @return 结果
     */
    @Override
    public int insertFaGameDecCategory(FaGameDecCategory faGameDecCategory)
    {
        return faGameDecCategoryMapper.insertFaGameDecCategory(faGameDecCategory);
    }

    /**
     * 修改装扮分类
     * 
     * @param faGameDecCategory 装扮分类
     * @return 结果
     */
    @Override
    public int updateFaGameDecCategory(FaGameDecCategory faGameDecCategory)
    {
        return faGameDecCategoryMapper.updateFaGameDecCategory(faGameDecCategory);
    }

    /**
     * 批量删除装扮分类
     * 
     * @param ids 需要删除的装扮分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGameDecCategoryByIds(Long[] ids)
    {
        return faGameDecCategoryMapper.deleteFaGameDecCategoryByIds(ids);
    }

    /**
     * 删除装扮分类信息
     * 
     * @param id 装扮分类ID
     * @return 结果
     */
    @Override
    public int deleteFaGameDecCategoryById(Long id)
    {
        return faGameDecCategoryMapper.deleteFaGameDecCategoryById(id);
    }
}
