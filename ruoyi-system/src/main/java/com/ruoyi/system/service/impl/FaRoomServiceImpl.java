package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;

import com.ruoyi.system.domain.vo.RoomVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaRoomMapper;
import com.ruoyi.system.domain.FaRoom;
import com.ruoyi.system.service.IFaRoomService;

/**
 * 房间管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
@Service
public class FaRoomServiceImpl implements IFaRoomService 
{
    @Autowired
    private FaRoomMapper faRoomMapper;

    /**
     * 查询房间管理
     * 
     * @param id 房间管理ID
     * @return 房间管理
     */
    @Override
    public FaRoom selectFaRoomById(Long id)
    {
        return faRoomMapper.selectFaRoomById(id);
    }

    /**
     * 查询房间管理列表
     * 
     * @param faRoom 房间管理
     * @return 房间管理
     */
    @Override
    public List<FaRoom> selectFaRoomList(FaRoom faRoom)
    {
        return faRoomMapper.selectFaRoomList(faRoom);
    }

    /**
     * 新增房间管理
     * 
     * @param faRoom 房间管理
     * @return 结果
     */
    @Override
    public int insertFaRoom(FaRoom faRoom)
    {
        faRoom.setCreatetime(System.currentTimeMillis()/1000L);
        faRoom.setUpdatetime(System.currentTimeMillis()/1000L);
        return faRoomMapper.insertFaRoom(faRoom);
    }

    /**
     * 修改房间管理
     * 
     * @param faRoom 房间管理
     * @return 结果
     */
    @Override
    public int updateFaRoom(FaRoom faRoom)
    {
        faRoom.setUpdatetime(System.currentTimeMillis()/1000L);
        return faRoomMapper.updateFaRoom(faRoom);
    }

    /**
     * 批量删除房间管理
     * 
     * @param ids 需要删除的房间管理ID
     * @return 结果
     */
    @Override
    public int deleteFaRoomByIds(Long[] ids)
    {
        return faRoomMapper.deleteFaRoomByIds(ids);
    }

    /**
     * 删除房间管理信息
     * 
     * @param id 房间管理ID
     * @return 结果
     */
    @Override
    public int deleteFaRoomById(Long id)
    {
        return faRoomMapper.deleteFaRoomById(id);
    }

    /**
     * 房间列表
     * @param faRoom 参数
     * @return 房间列表
     */
    @Override
    public List<RoomVo> listRoomVo(FaRoom faRoom) {
        List<RoomVo> roomVos = faRoomMapper.listRoomVo(faRoom);
        for (RoomVo roomVo : roomVos){
            if(roomVo.getLuboimages() != null && !roomVo.getLuboimages().isEmpty()){
                roomVo.setLunboStr(Arrays.asList(roomVo.getLuboimages().split(",")));
            }
        }
        return roomVos;
    }
}
