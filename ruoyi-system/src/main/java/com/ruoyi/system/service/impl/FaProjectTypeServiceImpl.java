package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.FaProjects;
import com.ruoyi.system.domain.vo.ProjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaProjectTypeMapper;
import com.ruoyi.system.domain.FaProjectType;
import com.ruoyi.system.service.IFaProjectTypeService;

/**
 * 店铺服务分类Service业务层处理
 *
 * @author ruoyi
 * @date 2021-11-13
 */
@Service
public class FaProjectTypeServiceImpl implements IFaProjectTypeService
{
    @Autowired
    private FaProjectTypeMapper faProjectTypeMapper;

    /**
     * 查询店铺服务分类
     *
     * @param id 店铺服务分类ID
     * @return 店铺服务分类
     */
    @Override
    public FaProjectType selectFaProjectTypeById(Long id,String storeId)
    {
        return faProjectTypeMapper.selectFaProjectTypeById(id,storeId);
    }

    /**
     * 查询店铺服务分类列表
     *
     * @param faProjectType 店铺服务分类
     * @return 店铺服务分类
     */
    @Override
    public List<FaProjectType> selectFaProjectTypeList(FaProjectType faProjectType)
    {
        return faProjectTypeMapper.selectFaProjectTypeList(faProjectType);
    }

    /**
     * 新增店铺服务分类
     *
     * @param faProjectType 店铺服务分类
     * @return 结果
     */
    @Override
    public int insertFaProjectType(FaProjectType faProjectType)
    {
        faProjectType.setCreateTime(DateUtils.getNowDate());
        return faProjectTypeMapper.insertFaProjectType(faProjectType);
    }

    /**
     * 修改店铺服务分类
     *
     * @param faProjectType 店铺服务分类
     * @return 结果
     */
    @Override
    public int updateFaProjectType(FaProjectType faProjectType)
    {
        faProjectType.setUpdateTime(DateUtils.getNowDate());
        return faProjectTypeMapper.updateFaProjectType(faProjectType);
    }

    /**
     * 批量删除店铺服务分类
     *
     * @param ids 需要删除的店铺服务分类ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectTypeByIds(Long[] ids)
    {
        return faProjectTypeMapper.deleteFaProjectTypeByIds(ids);
    }

    /**
     * 删除店铺服务分类信息
     *
     * @param id 店铺服务分类ID
     * @return 结果
     */
    @Override
    public int deleteFaProjectTypeById(Long id)
    {
        return faProjectTypeMapper.deleteFaProjectTypeById(id);
    }

    @Override
    public List<SysDictData> listForSelect() {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        String userType = user.getUserType();
        FaProjectType faProjectType = new FaProjectType();
        //店铺用户
        if("10".equals(userType)){
            Long storeInfoId = user.getStoreInfoId();
            faProjectType.setStoreId(storeInfoId);
        }
        List<FaProjectType> faProjectTypes = faProjectTypeMapper.selectFaProjectTypeList(faProjectType);
        List<SysDictData> result = new ArrayList<>();
        for (FaProjectType vo:faProjectTypes){
            Map<String,Object> map = new HashMap<>();
            SysDictData map1 = new SysDictData();
            map.put("dictValue",vo.getId());
            map.put("dictLabel",vo.getName());
            map1.setDictLabel(vo.getName());
            map1.setDictValue(String.valueOf(vo.getId()));
            result.add(map1);
        }
        return result;
    }
}
