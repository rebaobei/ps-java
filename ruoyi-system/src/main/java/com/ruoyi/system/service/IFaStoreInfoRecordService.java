package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.FaStoreInfoRecord;

/**
 * 店铺信息记录Service接口
 *
 * @author ruoyi
 * @date 2021-10-30
 */
public interface IFaStoreInfoRecordService
{
    /**
     * 查询店铺信息记录
     *
     * @param id 店铺信息记录ID
     * @return 店铺信息记录
     */
    public FaStoreInfoRecord selectFaStoreInfoRecordById(Long id);

    /**
     * 查询店铺信息记录列表
     *
     * @param faStoreInfoRecord 店铺信息记录
     * @return 店铺信息记录集合
     */
    public List<FaStoreInfoRecord> selectFaStoreInfoRecordList(FaStoreInfoRecord faStoreInfoRecord);

    /**
     * 新增店铺信息记录
     *
     * @param faStoreInfoRecord 店铺信息记录
     * @return 结果
     */
    public AjaxResult insertFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord);
    public AjaxResult sysInsertFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord);

    /**
     * 修改店铺信息记录
     *
     * @param faStoreInfoRecord 店铺信息记录
     * @return 结果
     */
    public AjaxResult updateFaStoreInfoRecord(FaStoreInfoRecord faStoreInfoRecord);

    /**
     * 批量删除店铺信息记录
     *
     * @param ids 需要删除的店铺信息记录ID
     * @return 结果
     */
    public int deleteFaStoreInfoRecordByIds(Long[] ids);

    /**
     * 删除店铺信息记录信息
     *
     * @param id 店铺信息记录ID
     * @return 结果
     */
    public int deleteFaStoreInfoRecordById(Long id);
}
