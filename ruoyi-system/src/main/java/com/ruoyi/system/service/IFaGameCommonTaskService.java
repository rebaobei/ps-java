package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaGameCommonTask;

/**
 * 常规任务Service接口
 * 
 * @author ruoyi
 * @date 2021-08-09
 */
public interface IFaGameCommonTaskService 
{
    /**
     * 查询常规任务
     * 
     * @param id 常规任务ID
     * @return 常规任务
     */
    public FaGameCommonTask selectFaGameCommonTaskById(Long id);

    /**
     * 查询常规任务列表
     * 
     * @param faGameCommonTask 常规任务
     * @return 常规任务集合
     */
    public List<FaGameCommonTask> selectFaGameCommonTaskList(FaGameCommonTask faGameCommonTask);

    /**
     * 新增常规任务
     * 
     * @param faGameCommonTask 常规任务
     * @return 结果
     */
    public int insertFaGameCommonTask(FaGameCommonTask faGameCommonTask);

    /**
     * 修改常规任务
     * 
     * @param faGameCommonTask 常规任务
     * @return 结果
     */
    public int updateFaGameCommonTask(FaGameCommonTask faGameCommonTask);

    /**
     * 批量删除常规任务
     * 
     * @param ids 需要删除的常规任务ID
     * @return 结果
     */
    public int deleteFaGameCommonTaskByIds(Long[] ids);

    /**
     * 删除常规任务信息
     * 
     * @param id 常规任务ID
     * @return 结果
     */
    public int deleteFaGameCommonTaskById(Long id);
}
