package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaRoomfenlei;
import com.ruoyi.system.domain.vo.RoomCategoryVo;

/**
 * 房间分类Service接口
 * 
 * @author ruoyi
 * @date 2021-07-26
 */
public interface IFaRoomfenleiService 
{
    /**
     * 查询房间分类
     * 
     * @param id 房间分类ID
     * @return 房间分类
     */
    public FaRoomfenlei selectFaRoomfenleiById(Long id);

    /**
     * 查询房间分类列表
     * 
     * @param faRoomfenlei 房间分类
     * @return 房间分类集合
     */
    public List<FaRoomfenlei> selectFaRoomfenleiList(FaRoomfenlei faRoomfenlei);

    /**
     * 新增房间分类
     * 
     * @param faRoomfenlei 房间分类
     * @return 结果
     */
    public int insertFaRoomfenlei(FaRoomfenlei faRoomfenlei);

    /**
     * 修改房间分类
     * 
     * @param faRoomfenlei 房间分类
     * @return 结果
     */
    public int updateFaRoomfenlei(FaRoomfenlei faRoomfenlei);

    /**
     * 批量删除房间分类
     * 
     * @param ids 需要删除的房间分类ID
     * @return 结果
     */
    public int deleteFaRoomfenleiByIds(Long[] ids);

    /**
     * 删除房间分类信息
     * 
     * @param id 房间分类ID
     * @return 结果
     */
    public int deleteFaRoomfenleiById(Long id);

    List<RoomCategoryVo> listRoomFenlei(FaRoomfenlei faRoomfenlei);
}
