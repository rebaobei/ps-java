package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaBallUserqiandaodetail;
import com.ruoyi.system.domain.vo.UserQiandaoLogWithUserInfoVo;

/**
 * 签到明细Service接口
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public interface IFaBallUserqiandaodetailService 
{
    /**
     * 查询签到明细
     * 
     * @param id 签到明细ID
     * @return 签到明细
     */
    public FaBallUserqiandaodetail selectFaBallUserqiandaodetailById(Long id);

    /**
     * 查询签到明细列表
     * 
     * @param faBallUserqiandaodetail 签到明细
     * @return 签到明细集合
     */
    public List<FaBallUserqiandaodetail> selectFaBallUserqiandaodetailList(FaBallUserqiandaodetail faBallUserqiandaodetail);

    /**
     * 新增签到明细
     * 
     * @param faBallUserqiandaodetail 签到明细
     * @return 结果
     */
    public int insertFaBallUserqiandaodetail(FaBallUserqiandaodetail faBallUserqiandaodetail);

    /**
     * 修改签到明细
     * 
     * @param faBallUserqiandaodetail 签到明细
     * @return 结果
     */
    public int updateFaBallUserqiandaodetail(FaBallUserqiandaodetail faBallUserqiandaodetail);

    /**
     * 批量删除签到明细
     * 
     * @param ids 需要删除的签到明细ID
     * @return 结果
     */
    public int deleteFaBallUserqiandaodetailByIds(Long[] ids);

    /**
     * 删除签到明细信息
     * 
     * @param id 签到明细ID
     * @return 结果
     */
    public int deleteFaBallUserqiandaodetailById(Long id);

    List<UserQiandaoLogWithUserInfoVo> listUserQianDaoLog(FaBallUserqiandaodetail faBallUserqiandaodetail);
}
