package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.ScoreLogWithUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserScoreLogMapper;
import com.ruoyi.system.domain.FaUserScoreLog;
import com.ruoyi.system.service.IFaUserScoreLogService;

/**
 * 积分变动Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaUserScoreLogServiceImpl implements IFaUserScoreLogService 
{
    @Autowired
    private FaUserScoreLogMapper faUserScoreLogMapper;

    /**
     * 查询积分变动
     * 
     * @param id 积分变动ID
     * @return 积分变动
     */
    @Override
    public FaUserScoreLog selectFaUserScoreLogById(Integer id)
    {
        return faUserScoreLogMapper.selectFaUserScoreLogById(id);
    }

    /**
     * 查询积分变动列表
     * 
     * @param faUserScoreLog 积分变动
     * @return 积分变动
     */
    @Override
    public List<FaUserScoreLog> selectFaUserScoreLogList(FaUserScoreLog faUserScoreLog)
    {
        return faUserScoreLogMapper.selectFaUserScoreLogList(faUserScoreLog);
    }

    /**
     * 新增积分变动
     * 
     * @param faUserScoreLog 积分变动
     * @return 结果
     */
    @Override
    public int insertFaUserScoreLog(FaUserScoreLog faUserScoreLog)
    {
        return faUserScoreLogMapper.insertFaUserScoreLog(faUserScoreLog);
    }

    /**
     * 修改积分变动
     * 
     * @param faUserScoreLog 积分变动
     * @return 结果
     */
    @Override
    public int updateFaUserScoreLog(FaUserScoreLog faUserScoreLog)
    {
        return faUserScoreLogMapper.updateFaUserScoreLog(faUserScoreLog);
    }

    /**
     * 批量删除积分变动
     * 
     * @param ids 需要删除的积分变动ID
     * @return 结果
     */
    @Override
    public int deleteFaUserScoreLogByIds(Integer[] ids)
    {
        return faUserScoreLogMapper.deleteFaUserScoreLogByIds(ids);
    }

    /**
     * 删除积分变动信息
     * 
     * @param id 积分变动ID
     * @return 结果
     */
    @Override
    public int deleteFaUserScoreLogById(Integer id)
    {
        return faUserScoreLogMapper.deleteFaUserScoreLogById(id);
    }

    @Override
    public List<ScoreLogWithUserInfo> listScoreLogWithUserInfo(FaUserScoreLog scoreLog) {
        return faUserScoreLogMapper.listScoreLogWithUserInfo(scoreLog);
    }
}
