package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUsercoupons;

/**
 * 会员优惠券Service接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface IFaUsercouponsService 
{
    /**
     * 查询会员优惠券
     * 
     * @param id 会员优惠券ID
     * @return 会员优惠券
     */
    public FaUsercoupons selectFaUsercouponsById(Long id);

    /**
     * 查询会员优惠券列表
     * 
     * @param faUsercoupons 会员优惠券
     * @return 会员优惠券集合
     */
    public List<FaUsercoupons> selectFaUsercouponsList(FaUsercoupons faUsercoupons);

    /**
     * 新增会员优惠券
     * 
     * @param faUsercoupons 会员优惠券
     * @return 结果
     */
    public int insertFaUsercoupons(FaUsercoupons faUsercoupons);

    /**
     * 修改会员优惠券
     * 
     * @param faUsercoupons 会员优惠券
     * @return 结果
     */
    public int updateFaUsercoupons(FaUsercoupons faUsercoupons);

    /**
     * 批量删除会员优惠券
     * 
     * @param ids 需要删除的会员优惠券ID
     * @return 结果
     */
    public int deleteFaUsercouponsByIds(Long[] ids);

    /**
     * 删除会员优惠券信息
     * 
     * @param id 会员优惠券ID
     * @return 结果
     */
    public int deleteFaUsercouponsById(Long id);
}
