package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserRule;

/**
 * 会员规则Service接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface IFaUserRuleService 
{
    /**
     * 查询会员规则
     * 
     * @param id 会员规则ID
     * @return 会员规则
     */
    public FaUserRule selectFaUserRuleById(Integer id);

    /**
     * 查询会员规则列表
     * 
     * @param faUserRule 会员规则
     * @return 会员规则集合
     */
    public List<FaUserRule> selectFaUserRuleList(FaUserRule faUserRule);

    /**
     * 新增会员规则
     * 
     * @param faUserRule 会员规则
     * @return 结果
     */
    public int insertFaUserRule(FaUserRule faUserRule);

    /**
     * 修改会员规则
     * 
     * @param faUserRule 会员规则
     * @return 结果
     */
    public int updateFaUserRule(FaUserRule faUserRule);

    /**
     * 批量删除会员规则
     * 
     * @param ids 需要删除的会员规则ID
     * @return 结果
     */
    public int deleteFaUserRuleByIds(Integer[] ids);

    /**
     * 删除会员规则信息
     * 
     * @param id 会员规则ID
     * @return 结果
     */
    public int deleteFaUserRuleById(Integer id);
}
