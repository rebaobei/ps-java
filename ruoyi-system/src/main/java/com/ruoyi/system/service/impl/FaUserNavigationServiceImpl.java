package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserNavigationMapper;
import com.ruoyi.system.domain.FaUserNavigation;
import com.ruoyi.system.service.IFaUserNavigationService;

/**
 * 用户中心导航Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaUserNavigationServiceImpl implements IFaUserNavigationService 
{
    @Autowired
    private FaUserNavigationMapper faUserNavigationMapper;

    /**
     * 查询用户中心导航
     * 
     * @param nId 用户中心导航ID
     * @return 用户中心导航
     */
    @Override
    public FaUserNavigation selectFaUserNavigationById(Integer nId)
    {
        return faUserNavigationMapper.selectFaUserNavigationById(nId);
    }

    /**
     * 查询用户中心导航列表
     * 
     * @param faUserNavigation 用户中心导航
     * @return 用户中心导航
     */
    @Override
    public List<FaUserNavigation> selectFaUserNavigationList(FaUserNavigation faUserNavigation)
    {
        return faUserNavigationMapper.selectFaUserNavigationList(faUserNavigation);
    }

    /**
     * 新增用户中心导航
     * 
     * @param faUserNavigation 用户中心导航
     * @return 结果
     */
    @Override
    public int insertFaUserNavigation(FaUserNavigation faUserNavigation)
    {
        return faUserNavigationMapper.insertFaUserNavigation(faUserNavigation);
    }

    /**
     * 修改用户中心导航
     * 
     * @param faUserNavigation 用户中心导航
     * @return 结果
     */
    @Override
    public int updateFaUserNavigation(FaUserNavigation faUserNavigation)
    {
        return faUserNavigationMapper.updateFaUserNavigation(faUserNavigation);
    }

    /**
     * 批量删除用户中心导航
     * 
     * @param nIds 需要删除的用户中心导航ID
     * @return 结果
     */
    @Override
    public int deleteFaUserNavigationByIds(Integer[] nIds)
    {
        return faUserNavigationMapper.deleteFaUserNavigationByIds(nIds);
    }

    /**
     * 删除用户中心导航信息
     * 
     * @param nId 用户中心导航ID
     * @return 结果
     */
    @Override
    public int deleteFaUserNavigationById(Integer nId)
    {
        return faUserNavigationMapper.deleteFaUserNavigationById(nId);
    }
}
