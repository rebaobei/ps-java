package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaGoodArticleMapper;
import com.ruoyi.system.domain.FaGoodArticle;
import com.ruoyi.system.service.IFaGoodArticleService;

/**
 * 动保公益Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-04
 */
@Service
public class FaGoodArticleServiceImpl implements IFaGoodArticleService 
{
    @Autowired
    private FaGoodArticleMapper faGoodArticleMapper;

    /**
     * 查询动保公益
     * 
     * @param id 动保公益ID
     * @return 动保公益
     */
    @Override
    public FaGoodArticle selectFaGoodArticleById(Long id)
    {
        return faGoodArticleMapper.selectFaGoodArticleById(id);
    }

    /**
     * 查询动保公益列表
     * 
     * @param faGoodArticle 动保公益
     * @return 动保公益
     */
    @Override
    public List<FaGoodArticle> selectFaGoodArticleList(FaGoodArticle faGoodArticle)
    {
        return faGoodArticleMapper.selectFaGoodArticleList(faGoodArticle);
    }

    /**
     * 新增动保公益
     * 
     * @param faGoodArticle 动保公益
     * @return 结果
     */
    @Override
    public int insertFaGoodArticle(FaGoodArticle faGoodArticle)
    {
        faGoodArticle.setCreateTime(DateUtils.getNowDate());
        return faGoodArticleMapper.insertFaGoodArticle(faGoodArticle);
    }

    /**
     * 修改动保公益
     * 
     * @param faGoodArticle 动保公益
     * @return 结果
     */
    @Override
    public int updateFaGoodArticle(FaGoodArticle faGoodArticle)
    {
        faGoodArticle.setUpdateTime(DateUtils.getNowDate());
        return faGoodArticleMapper.updateFaGoodArticle(faGoodArticle);
    }

    /**
     * 批量删除动保公益
     * 
     * @param ids 需要删除的动保公益ID
     * @return 结果
     */
    @Override
    public int deleteFaGoodArticleByIds(Long[] ids)
    {
        return faGoodArticleMapper.deleteFaGoodArticleByIds(ids);
    }

    /**
     * 删除动保公益信息
     * 
     * @param id 动保公益ID
     * @return 结果
     */
    @Override
    public int deleteFaGoodArticleById(Long id)
    {
        return faGoodArticleMapper.deleteFaGoodArticleById(id);
    }
}
