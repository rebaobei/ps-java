package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUsercouponsMapper;
import com.ruoyi.system.domain.FaUsercoupons;
import com.ruoyi.system.service.IFaUsercouponsService;

/**
 * 会员优惠券Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUsercouponsServiceImpl implements IFaUsercouponsService 
{
    @Autowired
    private FaUsercouponsMapper faUsercouponsMapper;

    /**
     * 查询会员优惠券
     * 
     * @param id 会员优惠券ID
     * @return 会员优惠券
     */
    @Override
    public FaUsercoupons selectFaUsercouponsById(Long id)
    {
        return faUsercouponsMapper.selectFaUsercouponsById(id);
    }

    /**
     * 查询会员优惠券列表
     * 
     * @param faUsercoupons 会员优惠券
     * @return 会员优惠券
     */
    @Override
    public List<FaUsercoupons> selectFaUsercouponsList(FaUsercoupons faUsercoupons)
    {
        return faUsercouponsMapper.selectFaUsercouponsList(faUsercoupons);
    }

    /**
     * 新增会员优惠券
     * 
     * @param faUsercoupons 会员优惠券
     * @return 结果
     */
    @Override
    public int insertFaUsercoupons(FaUsercoupons faUsercoupons)
    {
        return faUsercouponsMapper.insertFaUsercoupons(faUsercoupons);
    }

    /**
     * 修改会员优惠券
     * 
     * @param faUsercoupons 会员优惠券
     * @return 结果
     */
    @Override
    public int updateFaUsercoupons(FaUsercoupons faUsercoupons)
    {
        return faUsercouponsMapper.updateFaUsercoupons(faUsercoupons);
    }

    /**
     * 批量删除会员优惠券
     * 
     * @param ids 需要删除的会员优惠券ID
     * @return 结果
     */
    @Override
    public int deleteFaUsercouponsByIds(Long[] ids)
    {
        return faUsercouponsMapper.deleteFaUsercouponsByIds(ids);
    }

    /**
     * 删除会员优惠券信息
     * 
     * @param id 会员优惠券ID
     * @return 结果
     */
    @Override
    public int deleteFaUsercouponsById(Long id)
    {
        return faUsercouponsMapper.deleteFaUsercouponsById(id);
    }
}
