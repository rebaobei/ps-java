package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserSiteMapper;
import com.ruoyi.system.domain.FaUserSite;
import com.ruoyi.system.service.IFaUserSiteService;

/**
 * 会员其他信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class FaUserSiteServiceImpl implements IFaUserSiteService 
{
    @Autowired
    private FaUserSiteMapper faUserSiteMapper;

    /**
     * 查询会员其他信息
     * 
     * @param id 会员其他信息ID
     * @return 会员其他信息
     */
    @Override
    public FaUserSite selectFaUserSiteById(Long id)
    {
        return faUserSiteMapper.selectFaUserSiteById(id);
    }

    /**
     * 查询会员其他信息列表
     * 
     * @param faUserSite 会员其他信息
     * @return 会员其他信息
     */
    @Override
    public List<FaUserSite> selectFaUserSiteList(FaUserSite faUserSite)
    {
        return faUserSiteMapper.selectFaUserSiteList(faUserSite);
    }

    /**
     * 新增会员其他信息
     * 
     * @param faUserSite 会员其他信息
     * @return 结果
     */
    @Override
    public int insertFaUserSite(FaUserSite faUserSite)
    {
        return faUserSiteMapper.insertFaUserSite(faUserSite);
    }

    /**
     * 修改会员其他信息
     * 
     * @param faUserSite 会员其他信息
     * @return 结果
     */
    @Override
    public int updateFaUserSite(FaUserSite faUserSite)
    {
        return faUserSiteMapper.updateFaUserSite(faUserSite);
    }

    /**
     * 批量删除会员其他信息
     * 
     * @param ids 需要删除的会员其他信息ID
     * @return 结果
     */
    @Override
    public int deleteFaUserSiteByIds(Long[] ids)
    {
        return faUserSiteMapper.deleteFaUserSiteByIds(ids);
    }

    /**
     * 删除会员其他信息信息
     * 
     * @param id 会员其他信息ID
     * @return 结果
     */
    @Override
    public int deleteFaUserSiteById(Long id)
    {
        return faUserSiteMapper.deleteFaUserSiteById(id);
    }
}
