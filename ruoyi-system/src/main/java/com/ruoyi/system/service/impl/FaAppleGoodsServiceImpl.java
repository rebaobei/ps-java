package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaAppleGoodsMapper;
import com.ruoyi.system.domain.FaAppleGoods;
import com.ruoyi.system.service.IFaAppleGoodsService;

/**
 * 商品管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-04
 */
@Service
public class FaAppleGoodsServiceImpl implements IFaAppleGoodsService 
{
    @Autowired
    private FaAppleGoodsMapper faAppleGoodsMapper;

    /**
     * 查询商品管理
     * 
     * @param id 商品管理ID
     * @return 商品管理
     */
    @Override
    public FaAppleGoods selectFaAppleGoodsById(Long id)
    {
        return faAppleGoodsMapper.selectFaAppleGoodsById(id);
    }

    /**
     * 查询商品管理列表
     * 
     * @param faAppleGoods 商品管理
     * @return 商品管理
     */
    @Override
    public List<FaAppleGoods> selectFaAppleGoodsList(FaAppleGoods faAppleGoods)
    {
        return faAppleGoodsMapper.selectFaAppleGoodsList(faAppleGoods);
    }

    /**
     * 新增商品管理
     * 
     * @param faAppleGoods 商品管理
     * @return 结果
     */
    @Override
    public int insertFaAppleGoods(FaAppleGoods faAppleGoods)
    {
        return faAppleGoodsMapper.insertFaAppleGoods(faAppleGoods);
    }

    /**
     * 修改商品管理
     * 
     * @param faAppleGoods 商品管理
     * @return 结果
     */
    @Override
    public int updateFaAppleGoods(FaAppleGoods faAppleGoods)
    {
        return faAppleGoodsMapper.updateFaAppleGoods(faAppleGoods);
    }

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的商品管理ID
     * @return 结果
     */
    @Override
    public int deleteFaAppleGoodsByIds(Long[] ids)
    {
        return faAppleGoodsMapper.deleteFaAppleGoodsByIds(ids);
    }

    /**
     * 删除商品管理信息
     * 
     * @param id 商品管理ID
     * @return 结果
     */
    @Override
    public int deleteFaAppleGoodsById(Long id)
    {
        return faAppleGoodsMapper.deleteFaAppleGoodsById(id);
    }
}
