package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.FaStorePerson;

/**
 * 店铺服务团队Service接口
 * 
 * @author ruoyi
 * @date 2021-10-20
 */
public interface IFaStorePersonService 
{
    /**
     * 查询店铺服务团队
     * 
     * @param id 店铺服务团队ID
     * @return 店铺服务团队
     */
    public FaStorePerson selectFaStorePersonById(Long id);

    /**
     * 查询店铺服务团队列表
     * 
     * @param faStorePerson 店铺服务团队
     * @return 店铺服务团队集合
     */
    public List<FaStorePerson> selectFaStorePersonList(FaStorePerson faStorePerson);

    /**
     * 新增店铺服务团队
     * 
     * @param faStorePerson 店铺服务团队
     * @return 结果
     */
    public int insertFaStorePerson(FaStorePerson faStorePerson);

    /**
     * 修改店铺服务团队
     * 
     * @param faStorePerson 店铺服务团队
     * @return 结果
     */
    public int updateFaStorePerson(FaStorePerson faStorePerson);

    /**
     * 批量删除店铺服务团队
     * 
     * @param ids 需要删除的店铺服务团队ID
     * @return 结果
     */
    public int deleteFaStorePersonByIds(Long[] ids);

    /**
     * 删除店铺服务团队信息
     * 
     * @param id 店铺服务团队ID
     * @return 结果
     */
    public int deleteFaStorePersonById(Long id);

    List<Map<String, Object>> listForSelect();
}
