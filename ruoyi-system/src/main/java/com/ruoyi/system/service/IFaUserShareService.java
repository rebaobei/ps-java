package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.FaUserShare;

/**
 * 会员分享Service接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface IFaUserShareService 
{
    /**
     * 查询会员分享
     * 
     * @param id 会员分享ID
     * @return 会员分享
     */
    public FaUserShare selectFaUserShareById(Long id);

    /**
     * 查询会员分享列表
     * 
     * @param faUserShare 会员分享
     * @return 会员分享集合
     */
    public List<FaUserShare> selectFaUserShareList(FaUserShare faUserShare);

    /**
     * 新增会员分享
     * 
     * @param faUserShare 会员分享
     * @return 结果
     */
    public int insertFaUserShare(FaUserShare faUserShare);

    /**
     * 修改会员分享
     * 
     * @param faUserShare 会员分享
     * @return 结果
     */
    public int updateFaUserShare(FaUserShare faUserShare);

    /**
     * 批量删除会员分享
     * 
     * @param ids 需要删除的会员分享ID
     * @return 结果
     */
    public int deleteFaUserShareByIds(Long[] ids);

    /**
     * 删除会员分享信息
     * 
     * @param id 会员分享ID
     * @return 结果
     */
    public int deleteFaUserShareById(Long id);
}
