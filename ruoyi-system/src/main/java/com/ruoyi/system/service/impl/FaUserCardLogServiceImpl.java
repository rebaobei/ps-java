package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.vo.CardLogWithUserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaUserCardLogMapper;
import com.ruoyi.system.domain.FaUserCardLog;
import com.ruoyi.system.service.IFaUserCardLogService;

/**
 * 储值卡变动Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-06
 */
@Service
public class FaUserCardLogServiceImpl implements IFaUserCardLogService 
{
    @Autowired
    private FaUserCardLogMapper faUserCardLogMapper;

    /**
     * 查询储值卡变动
     * 
     * @param id 储值卡变动ID
     * @return 储值卡变动
     */
    @Override
    public FaUserCardLog selectFaUserCardLogById(Integer id)
    {
        return faUserCardLogMapper.selectFaUserCardLogById(id);
    }

    /**
     * 查询储值卡变动列表
     * 
     * @param faUserCardLog 储值卡变动
     * @return 储值卡变动
     */
    @Override
    public List<FaUserCardLog> selectFaUserCardLogList(FaUserCardLog faUserCardLog)
    {
        return faUserCardLogMapper.selectFaUserCardLogList(faUserCardLog);
    }

    /**
     * 新增储值卡变动
     * 
     * @param faUserCardLog 储值卡变动
     * @return 结果
     */
    @Override
    public int insertFaUserCardLog(FaUserCardLog faUserCardLog)
    {
        return faUserCardLogMapper.insertFaUserCardLog(faUserCardLog);
    }

    /**
     * 修改储值卡变动
     * 
     * @param faUserCardLog 储值卡变动
     * @return 结果
     */
    @Override
    public int updateFaUserCardLog(FaUserCardLog faUserCardLog)
    {
        return faUserCardLogMapper.updateFaUserCardLog(faUserCardLog);
    }

    /**
     * 批量删除储值卡变动
     * 
     * @param ids 需要删除的储值卡变动ID
     * @return 结果
     */
    @Override
    public int deleteFaUserCardLogByIds(Integer[] ids)
    {
        return faUserCardLogMapper.deleteFaUserCardLogByIds(ids);
    }

    /**
     * 删除储值卡变动信息
     * 
     * @param id 储值卡变动ID
     * @return 结果
     */
    @Override
    public int deleteFaUserCardLogById(Integer id)
    {
        return faUserCardLogMapper.deleteFaUserCardLogById(id);
    }

    @Override
    public List<CardLogWithUserInfoVo> listCardLogWithUserInfo(FaUserCardLog faUserCardLog) {
        return faUserCardLogMapper.listCardLogWithUserInfo(faUserCardLog);
    }
}
