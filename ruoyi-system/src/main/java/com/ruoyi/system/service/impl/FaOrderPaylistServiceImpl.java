package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.date.MyDateUtils;
import com.ruoyi.system.domain.vo.PayListStatisticVo;
import com.ruoyi.system.domain.vo.PayListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FaOrderPaylistMapper;
import com.ruoyi.system.domain.FaOrderPaylist;
import com.ruoyi.system.service.IFaOrderPaylistService;

/**
 * 支付记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-03
 */
@Service
public class FaOrderPaylistServiceImpl implements IFaOrderPaylistService 
{
    @Autowired
    private FaOrderPaylistMapper faOrderPaylistMapper;

    /**
     * 查询支付记录
     * 
     * @param id 支付记录ID
     * @return 支付记录
     */
    @Override
    public FaOrderPaylist selectFaOrderPaylistById(Long id)
    {
        return faOrderPaylistMapper.selectFaOrderPaylistById(id);
    }

    /**
     * 查询支付记录列表
     * 
     * @param faOrderPaylist 支付记录
     * @return 支付记录
     */
    @Override
    public List<FaOrderPaylist> selectFaOrderPaylistList(FaOrderPaylist faOrderPaylist)
    {
        return faOrderPaylistMapper.selectFaOrderPaylistList(faOrderPaylist);
    }

    /**
     * 新增支付记录
     * 
     * @param faOrderPaylist 支付记录
     * @return 结果
     */
    @Override
    public int insertFaOrderPaylist(FaOrderPaylist faOrderPaylist)
    {
        faOrderPaylist.setCreatetime(MyDateUtils.nowTimestamp());
        return faOrderPaylistMapper.insertFaOrderPaylist(faOrderPaylist);
    }

    /**
     * 修改支付记录
     * 
     * @param faOrderPaylist 支付记录
     * @return 结果
     */
    @Override
    public int updateFaOrderPaylist(FaOrderPaylist faOrderPaylist)
    {
        return faOrderPaylistMapper.updateFaOrderPaylist(faOrderPaylist);
    }

    /**
     * 批量删除支付记录
     * 
     * @param ids 需要删除的支付记录ID
     * @return 结果
     */
    @Override
    public int deleteFaOrderPaylistByIds(Long[] ids)
    {
        return faOrderPaylistMapper.deleteFaOrderPaylistByIds(ids);
    }

    /**
     * 删除支付记录信息
     * 
     * @param id 支付记录ID
     * @return 结果
     */
    @Override
    public int deleteFaOrderPaylistById(Long id)
    {
        return faOrderPaylistMapper.deleteFaOrderPaylistById(id);
    }

    @Override
    public List<PayListVo> listPaylistVo(PayListVo faOrderPaylist) {
        return faOrderPaylistMapper.listPayListVo(faOrderPaylist);
    }

    @Override
    public List<PayListStatisticVo> lsitPayStatisticVo(PayListStatisticVo vo) {
        return faOrderPaylistMapper.statisticMoney(vo);
    }
}
