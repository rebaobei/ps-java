package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.FaStoreBrand;
import com.ruoyi.system.domain.FaStoreInfo;
import com.ruoyi.system.mapper.FaStoreBrandMapper;
import com.ruoyi.system.mapper.FaStoreInfoMapper;
import com.ruoyi.system.service.IFaStoreInfoService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 店铺信息Service业务层处理
 *
 * @author ruoyi
 * @date 2021-10-30
 */
@Service
public class FaStoreInfoServiceImpl implements IFaStoreInfoService {
    @Autowired
    private FaStoreInfoMapper faStoreInfoMapper;
    @Autowired
    private FaStoreBrandMapper faStoreBrandMapper;
    @Autowired
    private ISysUserService iSysUserService;

    /**
     * 查询店铺信息
     *
     * @param id 店铺信息ID
     * @return 店铺信息
     */
    @Override
    public FaStoreInfo selectFaStoreInfoById(Long id) {
        FaStoreInfo faStoreInfo = faStoreInfoMapper.selectFaStoreInfoById(id);

        if (faStoreInfo.getBrandId() != null) {
            FaStoreBrand faStoreBrand = faStoreBrandMapper.selectFaStoreBrandById(faStoreInfo.getBrandId());
            faStoreInfo.setFaStoreBrand(faStoreBrand);
        } else {
            FaStoreBrand faStoreBrand = new FaStoreBrand();
            faStoreInfo.setFaStoreBrand(faStoreBrand);
        }
        return faStoreInfo;
    }

    /**
     * 查询店铺信息列表
     *
     * @param faStoreInfo 店铺信息
     * @return 店铺信息
     */
    @Override
    public List<FaStoreInfo> selectFaStoreInfoList(FaStoreInfo faStoreInfo) {
        return faStoreInfoMapper.selectFaStoreInfoList(faStoreInfo);
    }

    /**
     * 新增店铺信息
     *
     * @param faStoreInfo 店铺信息
     * @return 结果
     */
    @Override
    public int insertFaStoreInfo(FaStoreInfo faStoreInfo) {
        faStoreInfo.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = SecurityUtils.getLoginUser();
        SysUser user = loginUser.getUser();
        faStoreInfo.setUserId(user.getUserId());
        int i = faStoreInfoMapper.insertFaStoreInfo(faStoreInfo);
        user.setStoreInfoId(faStoreInfo.getId());
        iSysUserService.updateUser(user);
        return i;
    }

    /**
     * 修改店铺信息
     *
     * @param faStoreInfo 店铺信息
     * @return 结果
     */
    @Override
    public AjaxResult updateFaStoreInfo(FaStoreInfo faStoreInfo) {
        faStoreInfo.setUpdateTime(DateUtils.getNowDate());
        if (faStoreInfo.getSysHomeShow() == 1) {
            FaStoreInfo param = new FaStoreInfo();
            param.setSysHomeShow(1);
            List<FaStoreInfo> faStoreInfos = faStoreInfoMapper.selectFaStoreInfoList(param);
            List<Long> collect = faStoreInfos.stream().map(FaStoreInfo::getId).collect(Collectors.toList());
            if (!collect.contains(faStoreInfo.getId()) && faStoreInfos.size() >= 3) {
                return AjaxResult.error("首页店铺设置不能超过3个");
            }
        }
        faStoreInfoMapper.updateFaStoreInfo(faStoreInfo);
        return AjaxResult.success();
    }

    /**
     * 批量删除店铺信息
     *
     * @param ids 需要删除的店铺信息ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreInfoByIds(Long[] ids) {
        return faStoreInfoMapper.deleteFaStoreInfoByIds(ids);
    }

    /**
     * 删除店铺信息信息
     *
     * @param id 店铺信息ID
     * @return 结果
     */
    @Override
    public int deleteFaStoreInfoById(Long id) {
        return faStoreInfoMapper.deleteFaStoreInfoById(id);
    }
}
