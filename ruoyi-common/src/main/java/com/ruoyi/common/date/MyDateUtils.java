package com.ruoyi.common.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @Description 时间戳工具
 * @Author fan
 * @Date 2021/7/29
 **/
public class MyDateUtils {

    /**
     * 当前时间戳 10位
     */
    public static Long nowTimestamp() {
        return System.currentTimeMillis() / 1000L;
    }

    /**
     * 获取今日凌晨的10位时间戳
     */
    public static Long getTodayZeroTime() {
        // 当天凌晨(毫秒)
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis() / 1000L;
    }

    /**
     * 获取之前或之后凌晨的10位时间戳
     *
     * @param count -1昨天 1明天
     */
    public static Long getPreDayZeroTime(Integer count) {
        // 当天凌晨(毫秒)
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DAY_OF_MONTH, count);
        return c.getTimeInMillis() / 1000L;
    }

    /**
     * 获取之前或之后某天日期
     *
     * @param count -1昨天 1明天
     */
    public static String getPreDay(Integer count) {
        // 当天凌晨(毫秒)
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DAY_OF_MONTH, count);
        return new SimpleDateFormat("MM-dd").format(c.getTime());
    }

    /**
     * 获取本周凌晨的10位时间戳
     */
    public static Long getWeekZeroTime() {
        // 当周凌晨(毫秒)
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis() / 1000L;
    }

    /**
     * 获取本月凌晨的10位时间戳
     */
    public static Long getMothZeroTime() {
        // 当月凌晨(毫秒)
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH,0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis() / 1000L;
    }


}
